#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponseBadRequest, HttpResponse
from unidecode import unidecode
#from _compact import JsonResponse
from django import forms
import django_excel as excel
from accounts.models import *
from incidents.models import *
from db_management.models import *

from openpyxl import load_workbook, Workbook
import xlrd
import datetime
from openpyxl.compat import range
from openpyxl.utils import get_column_letter

from openpyxl.writer.excel import save_virtual_workbook, save_workbook
from openpyxl.writer.write_only import WriteOnlyCell
from openpyxl.styles import Font

from django.db.models import Q
from oldUpload.models import *

# Create your views here.

def FormatString(s):
    if isinstance(s, unicode):
        try:
            s.encode('ascii')
            return s
        except:
            return unidecode(s)
    else:
        return s

class UploadFileForm(forms.Form):
    file = forms.FileField()

def import_corrections(request, extra_context=None):
    if request.user.is_authenticated():
        user_id = request.user.id
    else:
        user_id = -1
    if request.method == "POST":
        if 'export_excel' in request.POST:
            form = UploadFileForm(request.POST,
                                  request.FILES)
            if form.is_valid():
                model = int(request.POST['model'])
                input_excel = request.FILES['file']
                if model == 1:
                    replace = update_ing_grading(input_excel)
                    if replace:
                        return HttpResponse("OK")
                    else:
                        return HttpResponseBadRequest()    
                elif model == 2:
                    replace = update_actors(input_excel)
                    if replace:
                        return HttpResponse("OK")
                    else:
                        return HttpResponseBadRequest()
                elif model == 3:
                    replace = update_source(input_excel)
                    if replace:
                        return HttpResponse("OK")
                    else:
                        return HttpResponseBadRequest()
                elif model == 4:
                    replace = update_nature(input_excel)
                    if replace:
                        return HttpResponse("OK")
                    else:
                        return HttpResponseBadRequest()        
        elif 'export_natures' in request.POST:
            dest_filename = 'nature_categories.xlsx'
            wb = export_natures(request)
            response = HttpResponse(save_virtual_workbook(wb), content_type='application/vnd.ms-excel')
            response['Content-Disposition'] = 'attachment; filename=%s' % (dest_filename)
            return response
        elif 'export_sources' in request.POST:
            dest_filename = 'source_categories.xlsx'
            wb = export_sources(request)
            response = HttpResponse(save_virtual_workbook(wb), content_type='application/vnd.ms-excel')
            response['Content-Disposition'] = 'attachment; filename=%s' % (dest_filename)
            return response
        elif 'export_inf' in request.POST:
            dest_filename = 'information_grading_categories.xlsx'
            wb = export_inf(request)
            response = HttpResponse(save_virtual_workbook(wb), content_type='application/vnd.ms-excel')
            response['Content-Disposition'] = 'attachment; filename=%s' % (dest_filename)
            return response
        elif 'export_actors' in request.POST:
            dest_filename = 'actors_categories.xlsx'
            wb = export_actors(request)
            response = HttpResponse(save_virtual_workbook(wb), content_type='application/vnd.ms-excel')
            response['Content-Disposition'] = 'attachment; filename=%s' % (dest_filename)
            return response                        
    else:
        form = UploadFileForm()    
    return render(request, 'oldUpload/upload_form_replace.html', {'form': form,})

def update_ing_grading(data):
    result = False
    book = xlrd.open_workbook(file_contents=data.read())
    sheet = book.sheet_by_index(0)
    date_mode = book.datemode
    for ind, r in enumerate(range(1, sheet.nrows)):
        correct = sheet.cell(r, 0).value
        new_inf_obj = InformationGrading.objects.create(name='temp_jaskd_kjansjkdnsjknak_asds')
        for ce in range(1, 200):
            var = str(sheet.cell(r, ce).value)
            if var != 'END_OF_VARIANTS':
                try:
                    del_obj = InformationGrading.objects.filter(name=var)
                    for de_l in del_obj:
                        description_objects = IncindentDescription.objects.filter(inf_grading=de_l)
                        for desc in description_objects:
                            desc.inf_grading = new_inf_obj
                            desc.save()
                        de_l.delete()    
                except InformationGrading.DoesNotExist:
                    pass 
            else:
                break
        new_inf_obj.name = str(correct)
        new_inf_obj.save()        
    result = True                
    return result


def update_source(data):
    result = False
    book = xlrd.open_workbook(file_contents=data.read())
    sheet = book.sheet_by_index(0)
    date_mode = book.datemode
    for ind, r in enumerate(range(1, sheet.nrows)):
        correct = sheet.cell(r, 0).value
        new_inf_obj = Source.objects.create(name='temp_jaskd_kjansjkdnsjknak_asds_abdja')
        for ce in range(1, 200):
            var = str(sheet.cell(r, ce).value)
            if var != 'END_OF_VARIANTS':
                try:
                    del_obj = Source.objects.filter(name=var)
                    for de_l in del_obj:
                        description_objects = IncindentDescription.objects.filter(source=de_l)
                        for desc in description_objects:
                            desc.source = new_inf_obj
                            desc.save()
                        de_l.delete()    
                except Source.DoesNotExist:
                    pass 
            else:
                break
        new_inf_obj.name = str(correct)
        new_inf_obj.save()        
    result = True                
    return result    

def update_actors(data):
    result = False
    book = xlrd.open_workbook(file_contents=data.read())
    sheet = book.sheet_by_index(0)
    date_mode = book.datemode
    for ind, r in enumerate(range(1, sheet.nrows)):
        correct = sheet.cell(r, 0).value
        new_inf_obj = ActorsGroup.objects.create(name='temp_jaskd_kjansjkdnsjknak_asds_asdnkjs')
        for ce in range(1, 500):
            var = str(sheet.cell(r, ce).value)
            if var != 'END_OF_VARIANTS':
                try:
                    del_obj = ActorsGroup.objects.filter(name=var)
                    for de_l in del_obj:
                        filters = Q(actors=de_l)
                        description_objects = EntryIncidentsValue.objects.filter(filters)
                        for desc in description_objects:
                            desc.actors.remove(de_l)
                            desc.actors.add(new_inf_obj)
                            desc.save()
                        de_l.delete()    
                except ActorsGroup.DoesNotExist:
                    pass 
            else:
                break
        new_inf_obj.name = str(correct)
        new_inf_obj.save()        
    result = True                
    return result

def update_nature(data):
    result = False
    book = xlrd.open_workbook(file_contents=data.read())
    sheet = book.sheet_by_index(0)
    date_mode = book.datemode
    state = True
    main = NatureMainGroup.objects.get(id=-1)
    for ind, r in enumerate(range(1, sheet.nrows)):
        if sheet.cell(r, 1).value == 'EMPTY_SLOT':
            correct = sheet.cell(r, 0).value
            new_inf_obj = NatureMainGroup.objects.create(name='temp_jaskd_kjansjkdnsjknak_asds_asdnkj_df_s')
            for ce in range(2, 200):
                var = str(sheet.cell(r, ce).value)
                if var != 'END_OF_VARIANTS':
                    del_obj = NatureMainGroup.objects.filter(name=var)
                    for de_l in del_obj:
                        filters = Q(main=de_l)
                        description_objects = Nature.objects.filter(filters)
                        for desc in description_objects:
                            desc.main = new_inf_obj
                            desc.save()
                        de_l.delete()  
                else:
                    break
            main = new_inf_obj        
            # now deal with it sub groups        
            new_inf_obj.name = str(correct)
            new_inf_obj.save()
        elif sheet.cell(r, 1).value == 'END_OF_SUB':
            pass    
        else:
            sub_name = str(sheet.cell(r, 1).value)
            new_sub_obj = NatureSubGroups.objects.create(name='temp_jaskd_kjansjkdnsjknak_asds_asdnkj_df_s_dd') 
            main.subGroups.add(new_sub_obj)
            for ce in range(3, 200):
                var = str(sheet.cell(r, ce).value)
                if var != 'END_OF_VARIANTS':
                    del_obj = NatureSubGroups.objects.filter(name=var)
                    for de_l in del_obj:
                        # need to act a bit differently
                        filters = Q(sub=de_l)
                        description_objects = Nature.objects.filter(filters)
                        for desc in description_objects:
                            desc.main = main
                            desc.sub = new_sub_obj
                            desc.save()
                        de_l.delete()  
                else:
                    break
            # save correct name
            new_sub_obj.name = str(sub_name)
            new_sub_obj.save()           
    result = True                
    return result    

def export_natures(request):
    wb =  Workbook()
    dest_filename = 'nature_categories.xlsx'
    ws1 = wb.active
    ws1.title = "Nature categories"
    act_row = 2
    nature_object = NatureMainGroup.objects.all()
    ws1.append(['Main name', 'Sub name', 'Variants'])
    for numb, nature in enumerate(nature_object):
        nat_sub = nature.subGroups.all()
        n_m_col = 'A%d' % (act_row)
        n_s_col = 'B%d' % (1 + act_row)
        ws1[n_m_col] = str(nature.name)
        if len(nat_sub) > 0:
            for ind, nat in enumerate(nat_sub):
                n_s_col = 'B%d' % (1 + ind + act_row)
                if nat.id != -1:
                    ws1[n_s_col] = str(nat.name)
                else:
                    ws1[n_s_col] = '--'
        else:
            ws1[n_s_col] = '--'
        act_row = ws1.max_row + 1
    return wb

def export_sources(request):
    wb =  Workbook()
    ws1 = wb.active
    ws1.title = "Source categories"
    act_row = 2
    nature_object = Source.objects.all()
    ws1.append(['name', 'Variants'])
    for numb, nature in enumerate(nature_object):
        mid = [str(nature.name)]
        ws1.append(mid)
    return wb

def export_inf(request):
    wb =  Workbook()
    ws1 = wb.active
    ws1.title = "Information grading categories"
    act_row = 2
    nature_object = InformationGrading.objects.all()
    ws1.append(['Name', 'Variants'])
    for numb, nature in enumerate(nature_object):
        mid = [str(nature.name)]
        ws1.append(mid)
    return wb 

def export_actors(request): 
    wb =  Workbook()
    ws1 = wb.active
    ws1.title = "Actors categories"
    act_row = 2
    nature_object = ActorsGroup.objects.all()
    ws1.append(['Name', 'Variants'])
    for numb, nature in enumerate(nature_object):
        mid = [FormatString(nature.name)]
        ws1.append(mid)
    return wb            

def import_sheet(request, extra_context=None):
    row_obj = LastRow.objects.get(id=1)
    for_cont = int(row_obj.number)
    if request.user.is_authenticated():
        user_id = request.user.id
    else:
        user_id = -1
    if request.method == "POST":
        form = UploadFileForm(request.POST,
                              request.FILES)
        if form.is_valid():
            sheet = int(request.POST['sheet'])
            start_row = int(request.POST['row'])
            new_upload = int(request.POST['new'])
            if int(new_upload) == 0:
                start = int(start_row)
                row_obj.number = start_row
                row_obj.save()
            else:
                start =  int(row_obj.number)   
            input_excel = request.FILES['file']
            book = xlrd.open_workbook(file_contents=input_excel.read())
            sheet = book.sheet_by_index(sheet)
            date_mode = book.datemode
            for ind, r in enumerate(range(start, sheet.nrows)):
                report_date = sheet.cell(r, 0).value
                source = sheet.cell(r, 1).value
                date = sheet.cell(r, 2).value
                itime = sheet.cell(r, 3).value
                nature = sheet.cell(r, 4).value
                inf_grading = sheet.cell(r, 5).value
                fact_tribal = sheet.cell(r, 6).value
                gos = sheet.cell(r, 7).value
                civilian = sheet.cell(r, 8).value
                idp = sheet.cell(r, 9).value
                ingo = sheet.cell(r, 10).value
                un_ag = sheet.cell(r, 11).value
                unamid = sheet.cell(r, 12).value
                no_of_vict = sheet.cell(r, 13).value
                actor = sheet.cell(r, 14).value
                region = sheet.cell(r, 15).value
                locality = sheet.cell(r, 16).value
                village = sheet.cell(r, 17).value
                e = sheet.cell(r, 18).value
                n = sheet.cell(r, 19).value
                descriptions = sheet.cell(r, 20).value
                remarks = sheet.cell(r, 21).value
                # create db entry with all siblings
                CreateEntryFromRow.mainHendler(report_date, source, date, itime, nature, inf_grading, fact_tribal, gos, civilian, idp, ingo, un_ag, unamid, no_of_vict,
                                                actor, region, locality, village, e, n, descriptions, remarks, date_mode, user_id)
                row_obj.number = int(start + ind + 1)
                row_obj.save()
            row_obj.number = int(0)
            row_obj.save()    
            return HttpResponse("OK")
        else:
            return HttpResponseBadRequest()
    else:
        form = UploadFileForm()    
    return render(
        request,
        'oldUpload/upload_form.html',
        {'form': form,
          'num': for_cont})

class UploadLocations(forms.Form): 
    file = forms.FileField()


def import_locations(request):
    if request.user.is_authenticated():
        user_id = request.user.id
    else:
        user_id = -1
    if request.method == "POST":
        form = UploadFileForm(request.POST,
                              request.FILES)
        if form.is_valid():
            input_excel = request.FILES['file']
            book = xlrd.open_workbook(file_contents=input_excel.read())
            sheet = book.sheet_by_index(0)
            date_mode = book.datemode
            for r in range(1, 4746):
                e = sheet.cell(r, 0).value
                n = sheet.cell(r, 1).value
                description = sheet.cell(r, 2).value
                state = sheet.cell(r, 3).value
                locality = sheet.cell(r, 4).value
                village = sheet.cell(r, 5).value
                CreateEntryFromRow.locationHandler(e, n, description, state, locality, village) 
            return HttpResponse("OK")
        else:
            return HttpResponseBadRequest()
    else:
        form = UploadFileForm()
    return render(
        request,
        'oldUpload/upload_form_location.html',
        {'form': form})         

class CreateEntryFromRow(object):

    def locationHandler(self, e, n, description, region, locality, village):
        filter_attempt = Region.objects.filter(name__istartswith=region)
        if len(filter_attempt) > 1:
            filter_attempt_2 = Region.objects.filter(name=region)
            if len(filter_attempt_2) >= 1:
                region_obj = filter_attempt_2[0]
            else:
                region_obj = Region.objects.create(name=region)   
        else:        
            try:
                region_obj = Region.objects.get(name__istartswith=region)
            except Region.DoesNotExist:
                region_obj = Region.objects.create(name=region)   
        
        # processing localitys
        
        filter_attempt_3 = region_obj.localitys.filter(name__istartswith=locality)
        if len(filter_attempt_3) > 1:
            filter_attempt_4 = region_obj.localitys.filter(name=locality)
            if len(filter_attempt_4) >= 1:
                locality_obj = filter_attempt_4[0]
            else:
                locality_obj = Locality.objects.create(name=locality)  
        else:        
            try:
                locality_obj = region_obj.localitys.get(name=locality)
            except Locality.DoesNotExist:
                locality_obj = Locality.objects.create(name=locality)
        region_obj.localitys.add(locality_obj) 
        region_obj.save()   
        
        # processing village
        try:
            val = float(e)
        except ValueError:
            val = 1.0
        try:
            val2 = float(n)
        except ValueError:
            val2 = 1.0    
        filter_attempt_5 = locality_obj.villages.filter(name__istartswith=village)
        if len(filter_attempt_5) > 1:
            filter_attempt_6 = locality_obj.villages.filter(name=village)
            if len(filter_attempt_6) >= 1:
                village_obj = filter_attempt_6[0]
                try:
                    coor_obj = village_obj.coordinates
                    if coor_obj.e == 1.0 and coor_obj.n == 1.0:
                        coor_obj.e = val
                        coor_obj.n == val2
                        coor_obj.save()
                except Coordinats.DoesNotExist: 
                    coor_obj = Coordinats.objects.create(e=val, n=val2)  
                    village_obj.coordinates_id = coor_obj.id 
                    village_obj.save()
            else:
                village_obj = Village.objects.create(name=village)
                coor_obj = Coordinats.objects.create(e=val, n=val2)
                village_obj.coordinates_id = coor_obj.id
                village_obj.save()
                locality_obj.villages.add(village_obj) 
                locality_obj.save()   
        else:        
            try:
                village_obj = locality_obj.villages.get(name=village)
                try:
                    coor_obj = village_obj.coordinates
                except Coordinats.DoesNotExist: 
                    coor_obj = Coordinats.objects.create(e=val, n=val2)  
                    village_obj.coordinates_id = coor_obj.id 
                    village_obj.save()
            except Village.DoesNotExist:
                village_obj = Village.objects.create(name=village)
                coor_obj = Coordinats.objects.create(e=val, n=val2)
                village_obj.coordinates_id = coor_obj.id
                village_obj.save()
                locality_obj.villages.add(village_obj) 
                locality_obj.save()

    def mainHendler(self, report_date, source, date, itime, nature, inf_grading, fact_tribal, gos, civilian, idp, ingo, un_ag, unamid, no_of_vict,
                    actor, region, locality, village, e, n, descriptions, remarks, date_mode, user_id):
        
        """
        INCIDENT DESCRIPTION FIELDS
        
        !!name = models.CharField(max_length=255, blank=True)
        --creation_date = models.DateTimeField(auto_now_add=True)
        !!appearence_date = models.DateField(auto_now=False, auto_now_add=False, default=date(now.year,now.month,now.day)) 
        !!description = models.TextField(blank=True, help_text="Enter description of incindent")
        !!source = models.ForeignKey(Source, blank=True, related_name="incindent_source")
        !!created_by = models.ForeignKey(User, related_name='description_created_by', default=-1)
        --touched = models.ManyToManyField(TouchedDescription, blank=True, null=True, related_name="touch_actions")
        !!inf_grading = models.ForeignKey(InformationGrading, related_name='information_grading', default=1)
        !!inc_date = models.DateField(auto_now=False, auto_now_add=False, default=date(now.year,now.month,now.day)) 
        !!inc_time = models.TimeField(auto_now=False, auto_now_add=False, default=now) 
        !!nature_main = models.ForeignKey(NatureMainGroup, blank=True, related_name="incindent_main_nature_group", default=1)
        !!nature_sub = models.ForeignKey(NatureSubGroups, blank=True, related_name="incindent_sub_nature_group", default=-1) 
        !!victim_class = models.ManyToManyField('db_management.VictimClass', blank=True, related_name="incindent_victim_class")
        !!number_of_victims = models.IntegerField(default=0, blank=True) 
        !!actor_class = models.ForeignKey(ActorsGroup, blank=True, related_name="incindent_actor_class", default=1)
        !!inc_region = models.ForeignKey(Region, blank=True, related_name="incindent_region", default=1)
        !!inc_locality = models.ForeignKey(Locality, blank=True, related_name="incindent_locality", default=-1)
        !!inc_village = models.ForeignKey(Village, blank=True, related_name="incindent_village", default=-1)
        !!inc_coordinates = models.ForeignKey(Coordinats, blank=True, related_name="incindent_coordinates", default=1)

        Entry fields
        !!name = models.CharField(max_length=255, blank=True)
        --creation_date = models.DateTimeField(auto_now_add=True)
        !!created_by = models.ForeignKey(User, related_name='entry_created_by', default=-1)
        !!descriptions_instances = models.ManyToManyField('incidents.IncindentDescription', blank=True, related_name="entry_assigned_descriptions")
        !!victim_class = models.ForeignKey(VictimClass, related_name='victim_class', default=1)
        !!vicMin = models.IntegerField(default=0)
        !!vicMax = models.IntegerField(default=0)
        !!entry_descriptions = models.ManyToManyField(EntryDescriptions, blank=True, related_name="entry_intenal_descriptions") 
        !!remarks = models.TextField(blank=True, help_text="Enter remarks to entry")
        --JMACcomments = models.TextField(blank=True, help_text="Enter comments for entry")

        EntryDescriptions fields
        --creation_date = models.DateTimeField(auto_now_add=True)
        !!created_by = models.ForeignKey(User, related_name='internal_description_created_by', default=-1)
        !!text = models.TextField(blank=True, help_text="Enter description of entry")
        related_instances = models.ManyToManyField('incidents.IncindentDescription', blank=True, related_name="entry_intenal_descriptions_instances")
        """  
        now = datetime.datetime.now()
        if report_date != '':
            r_appearence_date = datetime.datetime(*xlrd.xldate_as_tuple(report_date, date_mode)) # read and convert values date, time
        else:  
            r_appearence_date = datetime.datetime(2010, 01, 01, 11, 0, 0) 
        if date != '':
            try: 
                r_inc_date = datetime.datetime(*xlrd.xldate_as_tuple(date, date_mode)) # read and convert value date
            except:
                r_inc_date = datetime.datetime(2010, 01, 01, 11, 0, 0)    
        else:  
            r_inc_date = datetime.datetime(2010, 01, 01, 11, 0, 0)  
        if itime != '' and itime != ' ':
            try:
                date_values = xlrd.xldate_as_tuple(itime, date_mode)  
                time_value = datetime.time(date_values[3], date_values[4], date_values[5])
            except:
                time_value = datetime.time(11, 0, 0)    
        else:
            time_value = datetime.time(11, 0, 0)
        inc_source = self.handleSource(source, user_id) 
        inc_inf_grading = self.handleInformationGrading(inf_grading)
        inc_actor = self.handleActor(actor)
        inc_nature = self.handleNature(nature)
        try:
            v_n = int(no_of_vict)
        except ValueError:
            v_n = 0    

        # hendle initial creation  
        inc_descr_list = {'name': 'Imported record on %s-%s-%s' % (now.day,now.month,now.year), 'appearence_date': r_appearence_date, 
                            'description':descriptions, 'created_by_id':user_id,  
                            'source_id':inc_source, 'inf_grading_id':inc_inf_grading}
        incindent_obj = IncindentDescription.objects.create(**inc_descr_list)

        # create value instance
        value_obj_data = {'date':r_inc_date, 'time':time_value}
        value_obj = EntryIncidentsValue.objects.create(**value_obj_data)
        value_obj.actors.add(inc_actor)
        # add victims classes to values
        self.handleVictimCreation([['Faction/ Tribal',fact_tribal], ['GoS', gos], ['Civilian / Local', civilian], ['IDP', idp], ['INGO/NGO', ingo], ['UN Agencies', un_ag], ['UNAMID', unamid]], value_obj, v_n) # get or create instance and assign
        # create nature object and assign to value
        nature_obj = Nature.objects.create()
        nature_obj.main_id = inc_nature[0].id
        if inc_nature[1] != False: 
            nature_obj.sub_id = inc_nature[1].id # get or create instance and assign
        nature_obj.save()
        value_obj.nature.add(nature_obj)

        # assigne location to value
        self.handlePlace(region, locality, village, e, n, value_obj)  

        # cretate new db entry
        entry_upd = {'name': 'Imported record on %s-%s-%s' % (now.day,now.month,now.year), 'created_by_id':user_id, 'remarks':remarks}
        entry_obj = Entry.objects.create(**entry_upd)
        entry_obj.descriptions_instances.add(incindent_obj) 
        entry_obj.value_instance.add(value_obj)
        entry_obj.save()       

    def handleSource(self, source_value, user_id):
        if source_value != '':
            val = source_value
        else:
            val = 'Unknown'
        filter_attempt = Source.objects.filter(name__istartswith=val)
        if len(filter_attempt) > 1:
            filter_attempt_2 = Source.objects.filter(name=val)
            if len(filter_attempt_2) >= 1:
                obj = filter_attempt_2[0]
            else:
                obj = Source.objects.create(name=val, created_by_id=user_id) 
        else:        
            try:
                obj = Source.objects.get(name__istartswith=val)
            except Source.DoesNotExist:
                obj = Source.objects.create(name=val, created_by_id=user_id)  
        return int(obj.id)

    def handleInformationGrading(self, inf_value):
        if inf_value != '':
            val = inf_value
        else:
            val = 'A1'
        filter_attempt = InformationGrading.objects.filter(name__istartswith=val)
        if len(filter_attempt) > 1:
            filter_attempt_2 = InformationGrading.objects.filter(name=val)
            if len(filter_attempt_2) >= 1:
                obj = filter_attempt_2[0]
            else:
                obj = InformationGrading.objects.create(name=val) 
        else:        
            try:
                obj = InformationGrading.objects.get(name__istartswith=val)
            except InformationGrading.DoesNotExist:
                obj = InformationGrading.objects.create(name=val)
        return int(obj.id)

    def handleVictimCreation(self, values, val_obj, vics):
        total = 0
        mid = 0
        for inst in values:
            if inst[1] != '' or inst[1] == 'x':
                total += 1
        if total > 0:
            if int(vics) > 0:
                mid = int(total / int(vics))
        else:
            mid = int(vics)        
        for ins in values:
            if ins[1] != '' or ins[1] == 'x':    
                try:
                    obj = VictimClass.objects.get(name__istartswith=ins[0])
                except VictimClass.DoesNotExist:
                    obj = VictimClass.objects.create(name=ins[0])
                vics_obj = EntryVictimsData.objects.create(group=obj, quantity=mid)        
                val_obj.victims.add(vics_obj)
                val_obj.save() 

    def handleActor(self, value):
        if value != '':
            val = value
        else:
            val = 'UNKNOWN ARMED MEN'
        filter_attempt = ActorsGroup.objects.filter(name__istartswith=val)
        if len(filter_attempt) > 1:
            filter_attempt_2 = ActorsGroup.objects.filter(name=val)
            if len(filter_attempt_2) >= 1:
                obj = filter_attempt_2[0]
            else:
                obj = ActorsGroup.objects.create(name=val)   
        else:        
            try:
                obj = ActorsGroup.objects.get(name__istartswith=val)
            except ActorsGroup.DoesNotExist:
                obj = ActorsGroup.objects.create(name=val)
        return obj 

    def handleNature(self, value): # Banditry - Armed Robbery // Banditry # Armed Robbery // Armed Robbery
        main = ''
        begin_sub = False
        obj_s = False
        obj_m = NatureMainGroup.objects.get(id=-1)
        if len(value) > 0:
            for ind, letter in enumerate(value):
                if ind == 0:
                    main += letter
                else:
                    prev = value[ind - 1]
                    try:
                        nextL = value[ind + 1]
                    except IndexError:
                        nextL = False    
                    if letter == ' ' and (prev != ' ' or prev != '–' or prev != '_' or prev != '–') and (nextL and (nextL == '-' or nextL == '–' or nextL == ' ')) or (begin_sub == False and ind == (len(value) - 1)):
                        clear_main = self.clearName(main, 1)
                        if clear_main != '':
                            r_name = clear_main
                        else:
                            r_name = 'Unknown'
                        filter_attempt = NatureMainGroup.objects.filter(name__istartswith=r_name)
                        if len(filter_attempt) > 1:
                            filter_attempt_2 = NatureMainGroup.objects.filter(name=r_name)
                            if len(filter_attempt_2) >= 1:
                                obj = filter_attempt_2[0]
                                obj_m = obj
                            else:
                                obj = NatureMainGroup.objects.create(name=r_name) 
                                obj_m = obj  
                        else:        
                            try:
                                obj = NatureMainGroup.objects.get(name__istartswith=r_name)
                                obj_m = obj
                            except NatureMainGroup.DoesNotExist:
                                obj = NatureMainGroup.objects.create(name=r_name)
                                obj_m = obj  
                        main = ''
                        begin_sub = True
                    else:
                        main += letter
                        if begin_sub and ind == (len(value) - 1):
                            if main != '':
                                new_main = self.clearName(main, 3)
                                filter_attempt_3 = NatureMainGroup.objects.filter(name__istartswith=new_main)
                                if len(filter_attempt_3) > 1:
                                    filter_attempt_4 = obj.subGroups.filter(name=new_main)
                                    if len(filter_attempt_4) >= 1:
                                        obj_s = filter_attempt_4[0]
                                    else:
                                        obj_s = NatureSubGroups.objects.create(name=new_main)
                                        obj_m.subGroups.add(obj_s)
                                        obj_m.save()   
                                else:
                                    obj_ss = obj.subGroups.filter(name__istartswith=new_main)
                                    if len(obj_ss) > 0:
                                        obj_s = obj_ss[0]
                                    else:
                                        obj_s = NatureSubGroups.objects.create(name=new_main)
                                        obj_m.subGroups.add(obj_s)
                                        obj_m.save()                                
        return [obj_m, obj_s] 

    def clearName(self, main, how_much):
        new_main = ''
        for inde, letter in enumerate(main):  
            if inde <= how_much and (letter == ' ' or letter == '-' or letter == '–' or letter == '/'):
                pass
            else:
                new_main += letter 
        return new_main

    def clearLocationName(self, value):
        # what to check?
        # 1. clear empty space at the begining –+
        # 2. clear empty space at the end –+
        # 3. get the first variant where "separator" is presented –+
        # 4. delete double or more spaces beetween words –+
        # 5. lowercase all and then capitalize –+
        # 6. remove separator and –+
        # 7. remove "," separator –+
        # 8. remove "&" separator –+
        # 9. remove everything inside "()" –+
        # 10. check if it is not empty ––
        exceptions = [' ', '/', ',']
        separators = [',', '&', '/']
        result = ''
        lower_value = value.lower()
        unspaced_value = lower_value
        deseparated_value = unspaced_value
        deand_value = deseparated_value
        dequote_value = ''
        quote_enabled = False
        dedoublespaced_value = ''
        execute = True
        rear_index = 0
        # check if value is not empty
        if value != '' or value != ' ':
            # remove space at the begining
            for ind_1, letter in enumerate(lower_value):
                if ind_1 == 0 and letter in exceptions:
                    unspaced_value = lower_value[ind_1 + 1:]
                elif ind_1 == 1 and letter in exceptions and lower_value[0] in exceptions:
                    unspaced_value = lower_value[ind_1 + 1:]
                elif ind_1 == 2 and letter in exceptions and lower_value[0] in exceptions and lower_value[1] in exceptions:
                    unspaced_value = lower_value[ind_1 + 1:]
                else:
                    break
            # remove everything after separators
            for ind_2, letter in enumerate(unspaced_value):
                if letter in separators:
                    deseparated_value = unspaced_value[:ind_2 - 1]
                    break
            # removing and separator
            for ind_3, letter in enumerate(deseparated_value):
                # this can break with index value error !!!!!!!!!!!!!
                if (ind_3 + 2) <= (len(deseparated_value) - 1):
                    if letter == 'a' and deseparated_value[ind_3 - 1] == ' ' and deseparated_value[ind_3 + 1] == 'n' and deseparated_value[ind_3 + 2] == 'd' and deseparated_value[ind_3 + 3] == ' ':
                        deand_value = deseparated_value[:ind_3 - 1]
                else:
                    break        
            # removing everything inside quotes
            for ind_4, letter in enumerate(deand_value):
                if not quote_enabled:
                    if letter == "(":
                        quote_enabled = True
                        continue
                    elif letter == ")":
                        quote_enabled = False
                        continue
                    else:
                        dequote_value += letter        
                else:
                    continue
            # removing double or more spaces
            for ind_5, letter in enumerate(dequote_value):
                if letter == ' ' and  dequote_value[ind_5 - 1] == ' ':
                    continue
                else:
                    dedoublespaced_value += letter
            # removing empty spaces at the end
            preres_length = len(dedoublespaced_value) - 1
            i = 0
            if preres_length > 3:
                check_max = 3
            else:
                check_max = preres_length    
            while i <= check_max:
                if dedoublespaced_value[preres_length - i] == " ":
                    result = dedoublespaced_value[:preres_length - i - 1]
                    i += 1
                else:
                    result = dedoublespaced_value
                    break                                      
        return result    

    def findRegion(self, region):
        # name need to be cleared in perfect way and this will work perfectly
        region_obj = False
        if region != '' or region != ' ':
            new_region = self.clearLocationName(region)
            attempt_1 = Region.objects.filter(name__istartswith=new_region)
            if len(attempt_1) > 1:
                attempt_2 = Region.objects.filter(name=new_region)
                if len(attempt_2) >= 1:
                    region_obj = attempt_2[0]
                else: # how this can be possible? it's start with this sheet but then it's some how loose all values
                    region_obj = Region.objects.create(name=new_region.capitalize())   
            elif len(attempt_1) == 1:        
                region_obj = attempt_1[0]
            else:
                region_obj = Region.objects.create(name=new_region.capitalize())
        return region_obj

    def findLocality(self, locality, region_obj):
        locality_obj = False
        if locality != '' or locality != ' ':
            new_locality = self.clearLocationName(locality)
            attempt_1 = region_obj.localitys.filter(name__istartswith=new_locality)
            if len(attempt_1) > 1:
                attempt_2 = region_obj.localitys.filter(name=new_locality)
                if len(attempt_2) >= 1:
                    locality_obj = attempt_2[0]
                else:
                    locality_obj = Locality.objects.create(name=new_locality.capitalize())
                    region_obj.localitys.add(locality_obj) 
                    region_obj.save()   
            elif len(attempt_1) == 1:        
                locality_obj = attempt_1[0]
            else:    
                locality_obj = Locality.objects.create(name=new_locality.capitalize())
                region_obj.localitys.add(locality_obj) 
                region_obj.save()
        return [locality_obj, region_obj]

    def findVillage(self, locality_obj, village, e, n):
        village_obj = False
        if village != '' or village != ' ':
            new_village = self.clearLocationName(village)
            attempt_1 = locality_obj.villages.filter(name__istartswith=new_village)
            if len(attempt_1) > 1:
                attempt_2 = locality_obj.villages.filter(name=new_village)
                if len(attempt_2) >= 1:
                    village_obj = attempt_2[0]
                    try:
                        coor_obj = village_obj.coordinates
                    except Coordinats.DoesNotExist: 
                        coor_obj = self.adCoordinates(e, n) 
                        village_obj.coordinates_id = coor_obj.id 
                        village_obj.save()
                else:
                    village_obj = Village.objects.create(name=new_village.capitalize())
                    coor_obj = self.adCoordinates(e, n)
                    village_obj.coordinates_id = coor_obj.id
                    village_obj.save()
                    locality_obj.villages.add(village_obj) 
                    locality_obj.save()   
            elif len(attempt_1) == 1:
                village_obj = attempt_1[0]
                try:
                    coor_obj = village_obj.coordinates
                except Coordinats.DoesNotExist: 
                    coor_obj = self.adCoordinates(e, n)  
                    village_obj.coordinates_id = coor_obj.id 
                    village_obj.save()
            else:
                village_obj = Village.objects.create(name=new_village.capitalize())
                coor_obj = self.adCoordinates(e, n)
                village_obj.coordinates_id = coor_obj.id
                village_obj.save()
                locality_obj.villages.add(village_obj) 
                locality_obj.save()
        return [village_obj, coor_obj, locality_obj]

    def adCoordinates(self, e, n):
        one = False
        two = False
        if e != '' or e != ' ':
            try:
                val = float(e)
            except ValueError:
                val = 1.0 
        if n != '' or n != ' ':
            try:
                val2 = float(n)
            except ValueError:
                val2 = 1.0
            two = True
        if one and two:         
            coor_obj = Coordinats.objects.create(e=val, n=val2)
        else:
            coor_obj = Coordinats.objects.create()            
        return coor_obj                                          

    def handlePlace(self, raw_region, raw_locality, raw_village, e, n, inc_obj):

        # region actions
        region = self.findRegion(raw_region)
        if region:
            # create new instance for description
            location_obj = Location.objects.create()
            location_obj.inc_region_id = region.id
            # locality actions
            locality = self.findLocality(raw_locality, region)    
            if locality[0]:
                region = locality[1]
                location_obj.inc_locality_id = locality[0].id
                # village actions
                village = self.findVillage(locality[0], raw_village, e, n)   
                if village[0]:
                    locality[0] = village[2]
                    location_obj.inc_village_id = village[0].id 
                    location_obj.inc_coordinates_id = village[1].id
            location_obj.save()
            inc_obj.locations.add(location_obj)               
            inc_obj.save()                                                                       
                       
CreateEntryFromRow = CreateEntryFromRow()        