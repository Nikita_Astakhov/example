from django.conf.urls import url
from . import views
#from django.conf.urls import *
#from django.contrib.auth import views as auth_views
#from django.views.generic import TemplateView
urlpatterns = [
    url(r'^$', views.import_sheet, name="import_sheet"),
    url(r'^location/$', views.import_locations, name="import_locations"),
    url(r'^correction/$', views.import_corrections, name="import_corrections"),
    ] 