from __future__ import unicode_literals

from django.apps import AppConfig


class OlduploadConfig(AppConfig):
    name = 'oldUpload'
