var path = require("path")
var webpack = require('webpack')
var BundleTracker = require('webpack-bundle-tracker')

module.exports = {
  context: __dirname,

  entry: {  
      "entries_workspace": "./assets/js/index_entries_workspace",
      "edit_entry": "./assets/js/index_editEntry",
      "incidents_page": "./assets/js/index_incidents",
      "decsriptions": "./assets/js/index_descriptions",
      "report": "./assets/js/index_report",
    },

  output: {
      path: path.resolve('./assets/bundles/'),
      filename: "[name]-[hash].js"
  },

  plugins: [
      new webpack.ProvidePlugin({ 
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery' 
        }),
  ], // add all common plugins here

  module: {
    loaders: [] // add all common loaders here
  },

  resolve: {
    modulesDirectories: ['node_modules', 'bower_components'],
    extensions: ['', '.js', '.jsx', '.css',]
  },
}