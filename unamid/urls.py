"""unamid URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.sites.models import Site
from . import views
from django.conf import settings

admin.site.unregister(Site)


urlpatterns = [
    url(r'^upload_excel/', include('oldUpload.urls', namespace="upload_excel")),
    url(r'^admin/', admin.site.urls),
    url(r'^point/', include('main.urls', namespace="main")),
    url(r'^db_manage/', include('db_management.urls', namespace="db_manage")),
    url(r'^analytics/', include('analytics.urls', namespace="analytics")),
    url(r'^docs/', include('docs.urls', namespace="docs")),
    url(r'^incidents_processing/', include('incidents.urls', namespace="incidents")),
    url(r'^', include('userena.urls')),
    url(r'^accounts/', include('accounts.urls', namespace="accounts")),
    url(r'^api/', include('api.urls', namespace="api")),
    #url(r'^uploads/uploads/(?P<path>[a-zA-Z0-9]+)', views.download, name='down'),
    url(r'^uploads/uploads/(?P<path>[a-zA-Z0-9]+.*)$', views.download)
]#(?P<path>.*)$
