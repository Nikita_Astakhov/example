from __future__ import unicode_literals
import os
from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.views.generic import TemplateView

from django.views.static import serve

def download(request, path):
    filepath = os.path.join(settings.MEDIA_ROOT + '/uploads/', path)
    return serve(request, os.path.basename(filepath), os.path.dirname(filepath))