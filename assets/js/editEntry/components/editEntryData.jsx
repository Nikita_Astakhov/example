import _ from "lodash";
var React = require('react')
var ReactDom = require('react-dom')
var PropTypes = React.PropTypes;
var classNames = require('classnames');
var TimerMixin = require('react-timer-mixin');
var dateFormat = require('dateformat');
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css'; 

//var csrftoken = Utils.getCookie('csrftoken');
const FileUpload = require('react-fileupload');
import Dropzone from 'react-dropzone';
import request from 'superagent';
var fileDownload = require('react-file-download');
var Select = require('react-select');
require('react-select/dist/react-select.css');




var EntryUpdateConteiner = React.createClass({
    mixins: [TimerMixin],
    getInitialState: function () {
        return {
            status200: false,
            status200text: '',
            status500: false,
            status500text: '',
        }
    },
    toggle200: function (text) {
        this.setState({
            status200: true,
            status200text: text,
        });
        this.setTimeout(function () {
            this.setState({
                status200: false,
                status200text: '',
            });
        }, 4000);
    },
    toggle500: function (text) {
        this.setState({
            status500: true,
            status500text: text,
        });
        this.setTimeout( function () {
            this.setState({
                status500: false,
                status500text: '',
            });
        }, 4000);
    },
    render: function () {
        var class200 = classNames({
            'responsesuccess__conteiner': true,
            'hiddenstat': !this.state.status200
        });
        var class500 = classNames({
            'responseerror__conteiner': true,
            'hiddentstat': !this.state.status500
        });
        return (
            <div className="manageentrieconteiner">
              <div className="statusconteiner">
                <div className={class200}>
                  <p className="listincidents__form_label db_entries status">{this.state.status200text}</p>
                </div>
                <div className={class500}>
                  <p className="listincidents__form_label db_entries status">{this.state.status500text}</p>
                </div>
              </div>
              <h2 className="blockheading m_db">Entry ID – {AppGlobal.rec}</h2>
              <div className="dbentrieformconteiner">
                <EntryFieldsConteiner toggle200={this.toggle200} toggle500={this.toggle500} />
              </div>
            </div>
        )
    }
})

var EntryFieldsConteiner = React.createClass({
    propTypes: {  
        toggle200: PropTypes.func,
        toggle500: PropTypes.func,
    },
    render: function () {
        return (
            <div className="w-row">
                <div className="w-col w-col-7">
                    <EntryMainINfoConteiner toggle200={this.props.toggle200} toggle500={this.props.toggle500}  />
                </div>
                <div className="w-col w-col-5">
                    <AssignedReportsConteiner toggle200={this.props.toggle200} toggle500={this.props.toggle500} />
                </div>
            </div>
        )
    }
})

var AssignedReportsConteiner = React.createClass({
    mixins: [TimerMixin],
    propTypes: {  
        toggle200: PropTypes.func,
        toggle500: PropTypes.func,
    },
    getInitialState: function () {
        return {
            data: [],
            infoGrading: [],
            assign: false,
            ready: false
        }
    },
    componentWillMount: function () {
        var self = this;
        var source_gradings = 'http://' + AppGlobal.host + "/api/manage_db/db_data/information_grading/" + "?format=json";
        this.serverRequest = $.get(source_gradings, function (results) {
            var gradings_data = results.results;
            this.setState({
              infoGrading: gradings_data,
            });
        }.bind(this));
        var source_descriptions = 'http://' + AppGlobal.host + "/api/manage_db/entry/" + AppGlobal.rec + "/reports/" + "?format=json";
        this.serverRequest = $.get(source_descriptions, function (results) {
            var descriptions_data = results.results;
            this.setState({
              data: descriptions_data,
              ready: true
            });
        }.bind(this));
    },
    reloadData: function () {
        var self = this;
        var source = 'http://' + AppGlobal.host + "/api/manage_db/entry/" + AppGlobal.rec + "/reports/" + "?format=json";
        this.serverRequest = $.get(source, function (results) {
            var data = results.results;
            this.setState({
              data: data,
            });
        }.bind(this));
    },
    assignDescription: function () {
        var self = this;
        this.setState({
            assign: !this.state.assign
        }, self.whatToDo());
    },
    whatToDo: function () {
        this.setTimeout( function () {
            if (!this.state.assign) {
                ReactDom.unmountComponentAtNode(this.refs.searchNewDescription)
            } else {
                ReactDom.render(<AssignDescriptionConteiner key={'searchAndAssign'} unmount={this.assignDescription} reload={this.reloadData} toggle200={this.props.toggle200} toggle500={this.props.toggle500}/>, this.refs.searchNewDescription)
            }
        },200)
    },
    render: function () {
        var self = this;
        var listInstances = this.state.data.map(function (inst, key) {
            return (<AssignedReportINstance 
                        key={inst.id} 
                        number={key+1} 
                        data={inst} 
                        reloadMain={self.reloadData} 
                        toggle200={self.props.toggle200} 
                        toggle500={self.props.toggle500}
                        infoGrading={self.state.infoGrading}
                        natureGroup={self.state.natureMain}
                        victimsGroup={self.state.victimsClass}
                        actors={self.state.actors}
                        location={self.state.location}
                    />)
        });
        if (this.state.assign) {
            var text = 'Cancel';
        } else {
            var text = 'Collate with other incident descriptions';
        }
        return this.state.ready ?(
            <div className="db__reportsblock">
            <br/>
              <h4 className="actionhead db_inst">Assigned Incident Descriptions:</h4>
            <br/>  
              {listInstances}
              <div className="db__addnewdescriptionblock">
                <div className="w-row db__addnewdescriptionrow">
                  <div className="w-col w-col-9 w-clearfix"><a onClick={this.assignDescription} className="w-button listincidents__form_submit db">{text}</a>
                  </div>
                  <div className="w-col w-col-3 fhe"></div>
                </div>
                <div ref="searchNewDescription"></div>
              </div>
            </div>
        ) : null
    }
})

var weeksPageNUmber = 1;
var AssignDescriptionConteiner = React.createClass({
    mixins: [TimerMixin],
    propTypes: {  
        toggle200: PropTypes.func,
        toggle500: PropTypes.func,
        reload: PropTypes.func,
        unmount: PropTypes.func,
    },
    getInitialState: function () {
        return {
            data: [],
            searchName: '',
            searchId: 0,
            filterName: false,
            filterId: true,
            choosedFilter: 3,
            showPaginate: false,
            maxPage: 1
        }
    },
    assignToEntry: function (value) {
        var self = this;
        var source = 'http://'+AppGlobal.host+'/api/manage_db/update_entry/assign_new_description/';
        var post_d = {'user_id': parseInt(AppGlobal.user), 'entry_id':parseInt(AppGlobal.rec), 'de_id':parseInt(value)};
        $.ajax({
            headers: { 
              'Accept': 'application/json',
              'Content-Type': 'application/json' 
            },
            type: "POST",
            url: source,
            data: JSON.stringify(post_d, null, '\t'),
            dataType: 'json',
            statusCode: {
              200: function (data) {self.props.toggle200(data.text); self.props.reload(false, false, false, false, false); self.props.unmount();},
              500: function () {self.props.toggle500('Something went wrong. Please reload the page.');},
            }
        });
    },
    changeFilterPage: function (event) {
        var fil = parseInt(this.state.choosedFilter)
        var m = this.state.maxPage
        var p = event.target.value;
        if (p === '' || p === '0') {
            p = 1;
        }
        if (p > m) {
            p = m;
        }
        weeksPageNUmber = p;
        if (fil === 2) {
            this.updateDataByName();
        } else if (fil === 3) {
            this.updateDataById();
        }
    },
    calculatePageRange: function (number) {
        var result = Math.ceil(number / 30);
        this.setState({
            maxPage: result,
        });
    },
    changeFilter: function (event) {
        weeksPageNUmber = 1;
        this.setState({
            choosedFilter: event.target.value,
        }, this.changeClass(parseInt(event.target.value)));
    },
    changeClass: function (val) {
        weeksPageNUmber = 1;
        if (val === 2) {
            this.setState({
                filterName: true,
                filterId: false
            });
        } else {
            this.setState({
                filterName: false,
                filterId: true
            });
        }
    },
    hendleFilterName: function (event) {
        weeksPageNUmber = 1;
        var self = this;
        var v = event.target.value
        this.setState({
            searchName: v,
        });
        if (v != '') {
            self.updateDataByName()
        }
    },
    updateDataByName: function () {
        this.setTimeout( function () {
            var source = 'http://'+AppGlobal.host + "/api/incidents/list/name/"+ this.state.searchName +"/?format=json;page="+weeksPageNUmber;
            this.serverRequest = $.get(source, function (results) {
                var inc_list = results.results;
                if (results.count > 30) {
                    var s = true
                    this.calculatePageRange(results.count);
                } else {
                    var s = false
                }
                this.setState({
                  data: inc_list,
                  showPaginate: s
                });
            }.bind(this));
        }, 300);
    },
    hendleFilterId: function (event) {
        weeksPageNUmber = 1;
        var self = this;
        this.setState({
            searchId: event.target.value,
        }, self.updateDataById());
    },
    updateDataById: function () {
        this.setTimeout( function () {
            var source = 'http://'+AppGlobal.host + "/api/incidents/list/id/"+ this.state.searchId +"/?format=json;page="+weeksPageNUmber;
            this.serverRequest = $.get(source, function (results) {
                var inc_list = results.results;
                if (results.count > 30) {
                    var s = true
                    this.calculatePageRange(results.count);
                } else {
                    var s = false
                }
                this.setState({
                  data: inc_list,
                  showPaginate: s
                });
            }.bind(this));
        }, 200)
    },
    clearFilters: function () {
        this.setState({
            data: [],
            searchName: '',
            searchId: 0,
            showPaginate: false,
            maxPage: 1
        });
    },
    handleSubmit: function (event) {
        event.preventDefault();
    },
    render: function () {
        var self = this;
        var NamesClass = classNames({
            'filternames': true,
            'hiddenfilter': !this.state.filterName,
        });
        var IdClass = classNames({
            "filterid": true,
            "hiddenbl": !this.state.filterId, 
        });
        var listVariants = this.state.data.map(function (inst) {
            return (<AssignFilteredInstance assign={self.assignToEntry} key={inst.id} data={inst} />)
        });
        var paginator;
        if (this.state.showPaginate) {
            paginator = (
                <div className="paginationconteiner" style={{'marginTop': 20 + 'px'}}>
                <div className="w-container">
                  <div className="w-row">
                    <div className="w-col w-col-3"></div>
                    <div className="w-col w-col-6">
                      <div className="w-row">
                        <div className="w-col w-col-6">
                          <h4 className="listincidents__filterhead midpostion">Page range: 1 - {this.state.maxPage}</h4>
                        </div>
                        <div className="w-col w-col-6">
                          <div className="filterpages">
                            <form onSubmit={this.handleSubmit}>
                              <input onChange={this.changeFilterPage} className="w-input listincidents__form_input" min="1" placeholder="Input page number here" type="number"/>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="w-col w-col-3"></div>
                  </div>
                </div>
              </div>
            )
        }
        return (
            <div className="listincidents__filterconteiner">
              <div className="w-row">
                <div className="w-col w-col-9">
                  <div className="w-row">
                    <div className="w-col w-col-3">
                      <h4 className="listincidents__filterhead">Search by:</h4>
                    </div>
                    <div className="w-col w-col-9">
                      <form onSubmit={this.handleSubmit}>
                        <select onChange={this.changeFilter} value={this.state.choosedFilter} className="w-select filteroptionfield">
                          <option value="2">Title or short description</option>
                          <option value="3">ID</option>
                        </select>
                      </form>
                    </div>
                  </div>
                  <div className={NamesClass}>
                    <form onSubmit={this.handleSubmit}>
                      <div className="w-row">
                        <div className="w-col w-col-10">
                          <label className="listincidents__form_label">Input incident title or short description</label>
                          <input onChange={this.hendleFilterName} value={this.state.searchName} className="w-input listincidents__form_input" maxLength="256" placeholder="Input title here" type="text"/>
                        </div>
                        <div className="w-col w-col-2"><a onClick={this.clearFilters} className="w-button listincidents__form_submit">Clear</a>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div className={IdClass}>
                    <form onSubmit={this.handleSubmit}>
                      <div className="w-row">
                        <div className="w-col w-col-10">
                          <label className="listincidents__form_label">Input incident description ID</label>
                          <input onChange={this.hendleFilterId} value={this.state.searchId} className="w-input listincidents__form_input" maxLength="256" placeholder="Input id here" type="text"/>
                        </div>
                        <div className="w-col w-col-2 w-clearfix"><a onClick={this.clearFilters} className="w-button listincidents__form_submit">Clear</a>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
                <div className="w-col w-col-8"></div>
              </div>
              <div className="filteredentriesblock">
                <div className="w-row headerrow">
                  <div className="w-col w-col-1">
                    <h3 className="teblehead">ID</h3>
                  </div>
                  <div className="w-col w-col-5">
                    <h3 className="teblehead">Title</h3>
                  </div>
                  <div className="w-col w-col-2">
                    <h3 className="teblehead">Description</h3>
                  </div>
                  <div className="w-col w-col-2">
                    <h3 className="teblehead">Assigned db IDs</h3>
                  </div>
                  <div className="w-col w-col-2">
                    <h3 className="teblehead">Action</h3>
                  </div>
                </div>
                <div className="listentriesconteiner">
                  {listVariants}
                </div>
                {paginator}
              </div>
            </div>
        )
    }
});

var AssignFilteredInstance = React.createClass({
    propTypes: {  
        data: PropTypes.object,
        assign: PropTypes.func, 
    },
    renderTouch: function () {
        ReactDom.render(<ToouchedByConteiner key={this.props.data.id} unmount={this.unrenderTouch} instance_id={this.props.data.id}/>, this.refs.touchedConteiner);
    },
    unrenderTouch: function () {
        ReactDom.unmountComponentAtNode(this.refs.touchedConteiner);
    },
    renderAssigned: function () {
        ReactDom.render(<AssignedConteiner right={true} key={this.props.data.id} unmount={this.unrenderAssigned} instance_id={this.props.data.id}/>, this.refs.assignedConteiner);
    },
    unrenderAssigned: function () {
        ReactDom.unmountComponentAtNode(this.refs.assignedConteiner);
    },
    hendleAssign: function () {
        this.props.assign(this.props.data.id);
    },
    render: function () {
        return (
            <div className="listentrieconteiner">
              <div className="w-row">
                <div className="w-col w-col-1">
                  <h3 className="teblehead entrie">{this.props.data.id}</h3>
                </div>
                <div className="w-col w-col-5">
                  <h3 className="teblehead entrie">{this.props.data.name}</h3>
                </div>
                <div className="w-col w-col-2">
                  <h3 className="teblehead entrie link" onClick={this.renderTouch}>info</h3>
                  <div ref="touchedConteiner"></div>
                </div>
                <div className="w-col w-col-2">
                  <div className="assignedidblock">
                    <h3 onClick={this.renderAssigned} className="teblehead entrie link">info</h3>
                    <div ref="assignedConteiner"></div>
                  </div>
                </div>
                <div className="w-col w-col-2">
                  <h3 onClick={this.hendleAssign} className="teblehead entrie link">collate</h3>
                </div>
              </div>
            </div>
        )
    }
})
var AssignedConteiner = React.createClass({
    propTypes: {  
        instance_id: PropTypes.number,
        unmount: PropTypes.func,
        right: PropTypes.bool,
    },
    getInitialState: function () {
        return {
            data: []
        }
    },
    componentWillMount: function () {
        var source = 'http://'+AppGlobal.host + "/api/incidents/assigned/"+this.props.instance_id+"/?format=json";
        this.serverRequest = $.get(source, function (results) {
            var filt_data = results.results;
            this.setState({
              data: filt_data,
            });
        }.bind(this));
    },
    render: function () {
        var self = this;
        if (this.state.data.length > 0) {
            var listInstances = this.state.data.map(function(ins){
                return (<AssignedInstance key={ins.id} data={ins} />)
            });
        } else {
            var listInstances = (<div className="assignedidinfo__entrie"><h3 className="teblehead entrie infot">No data available</h3></div>);
        }
        var assignedClass = classNames({
            "assignedidinfoblock": true,
            'rightconteiner': this.props.right
        });
        return (
            <div className={assignedClass} onMouseLeave={this.props.unmount}>
              <div className="w-row assignedidheaderrow">
                <div className="w-col w-col-1">
                  <h3 className="teblehead entrie infot">ID</h3>
                </div>
                <div className="w-col w-col-5">
                  <h3 className="teblehead entrie infot">Title</h3>
                </div>
                <div className="w-col w-col-2">
                  <h3 className="teblehead entrie infot">Recording Date</h3>
                </div>
                <div className="w-col w-col-2">
                  <h3 className="teblehead entrie infot">Recorded by</h3>
                </div>
                <div className="w-col w-col-2">
                  <h3 className="teblehead entrie infot">Action</h3>
                </div>
              </div>
              {listInstances}
            </div>
        )
    }
});

var AssignedInstance = React.createClass({
    propTypes: {  
        data: PropTypes.object,
    },
    render: function () {
        return (
            <div className="assignedidinfo__entrie">
              <div className="w-row">
                <div className="w-col w-col-1">
                  <h3 className="teblehead entrie infot">{this.props.data.id}</h3>
                </div>
                <div className="w-col w-col-5">
                  <h3 className="teblehead entrie infot">{this.props.data.name}</h3>
                </div>
                <div className="w-col w-col-2">
                  <h3 className="teblehead entrie infot">{this.props.data.creation_date}</h3>
                </div>
                <div className="w-col w-col-2">
                  <h3 className="teblehead entrie infot">{this.props.data.created_by}</h3>
                </div>
                <div className="w-col w-col-2">
                  <h3 className="teblehead entrie infot edit"><a href={"http://"+AppGlobal.host+"/db_manage/edit_db_record/"+this.props.data.id}>edit</a></h3>
                </div>
              </div>
            </div>
        )
    }
})

var ToouchedByConteiner = React.createClass({
    propTypes: {  
        instance_id: PropTypes.number,
        unmount: PropTypes.func,
    },
    getInitialState: function () {
        return {
            data: '',
        }
    },
    componentWillMount: function () {
        var source = 'http://'+AppGlobal.host + "/api/incidents/descriptionTetxt/"+this.props.instance_id+"/?format=json";
        this.serverRequest = $.get(source, function (results) {
            var filt_data = results.results[0];
            this.setState({
              data: filt_data.description,
            });
        }.bind(this));
    },
    render: function () {
        var self = this;
        return (
            <div className="touchedinfoblock description" onMouseLeave={this.props.unmount}>
                <h3 className="teblehead entrie infot">{this.state.data}</h3>
            </div>
        )
    }
});

var AssignedReportINstance = React.createClass({
    mixins: [TimerMixin],
    propTypes: {  
        toggle200: PropTypes.func,
        toggle500: PropTypes.func,
        reloadMain: PropTypes.func, 
        data: PropTypes.object, 
        number: PropTypes.number, 
        infoGrading: PropTypes.array,
        natureGroup: PropTypes.array,
        victimsGroup: PropTypes.array,
        actors: PropTypes.array, 
        location: PropTypes.array, 
    },
    getInitialState: function () {
        return {
            unassign: true,
            content: false,
        }
    },
    toggleContent: function () {
        this.setState({
            content: !this.state.content
        });
    },
    toggleDelete: function () {
        this.setState({
            unassign: !this.state.unassign
        });
    },
    unassignFromRecord: function () {
        var self = this;
        var source = 'http://'+AppGlobal.host+'/api/manage_db/update_entry/unassign_description/';
        var post_d = {'user_id': parseInt(AppGlobal.user), 'entry_id':parseInt(AppGlobal.rec), 'de_id':parseInt(this.props.data.id)};
        $.ajax({
            headers: { 
              'Accept': 'application/json',
              'Content-Type': 'application/json' 
            },
            type: "POST",
            url: source,
            data: JSON.stringify(post_d, null, '\t'),
            dataType: 'json',
            statusCode: {
              200: function (data) {self.props.toggle200(data.text); self.props.reloadMain(); self.toggleDelete();},
              500: function () {self.props.toggle500('Something went wrong. Please reload the page.');self.toggleDelete();},
            }
        });
    },
    updateInformationGrading: function (value) {
        var self = this;
        var source = 'http://'+AppGlobal.host+'/api/manage_db/update_entry/information_grading/';
        var post_d = {'user_id': parseInt(AppGlobal.user), 'entry_id':parseInt(AppGlobal.rec), 'de_id':parseInt(this.props.data.id), 'inf_val': value};
        $.ajax({
            headers: { 
              'Accept': 'application/json',
              'Content-Type': 'application/json' 
            },
            type: "POST",
            url: source,
            data: JSON.stringify(post_d, null, '\t'),
            dataType: 'json',
            statusCode: {
              200: function (data) {self.props.toggle200(data.text); self.props.reloadMain();},
              500: function () {self.props.toggle500('Something went wrong. Please reload the page.');},
            }
        });
    },
    render: function () {
        var self = this;
        var unassignCLass = classNames({
            'deleteconfirm': true,
            'hidden': this.state.unassign
        });
        var contentClass = classNames({
            'db__recordinstance__content': true, 
            'hidden': this.state.content 
        });
        if (this.state.content) {
            var button = 'more info';
        } else {
            var button = 'minimize';
        }
        return (
            <div className="db_reportinstnceconteiner">
              <div className="db__recordinstance__headconteiner">
                <div className="w-row">
                  <div className="w-col w-col-1">
                    <h2 className="blockheading report">{this.props.number}</h2>
                  </div>
                  <div className="w-col w-col-6">
                    <h2 className="blockheading report">{this.props.data.name}</h2>
                  </div>
                  <div className="w-col w-col-3">
                    <h4 className="newincidentform__hint addelement verycloseup unlink" onClick={this.toggleDelete}>Delete connection</h4>
                    <div className={unassignCLass}>
                      <h4>Confirm unlinking</h4>
                      <div className="deletebuttonsblock">
                        <a className="w-button deletebutton" onClick={this.unassignFromRecord}>Confirm</a>
                        <a className="w-button deletebutton" onClick={this.toggleDelete}>Cancel</a>
                      </div>
                    </div>
                  </div>
                  <div onClick={this.toggleContent} className="w-col w-col-2">
                    <h4 className="newincidentform__hint addelement verycloseup unlink">{button}</h4>
                  </div>
                </div>
              </div>
              <div className={contentClass}>
                <div className="w-row">
                  <div className="w-col w-col-2">
                    <h3 className="teblehead" style={{'textAlign': 'left'}}>ID</h3>
                    <p className="listincidents__form_label db_entries">{this.props.data.id}</p>
                  </div>
                  <div className="w-col w-col-3">
                    <h3 className="teblehead" style={{'textAlign': 'left'}}>Report date</h3>
                    <p className="listincidents__form_label db_entries">{this.props.data.appearence_date}</p>
                  </div>
                  <div className="w-col w-col-3">
                    <h3 className="teblehead" style={{'textAlign': 'left'}}>Source</h3>
                    <p className="listincidents__form_label db_entries">{this.props.data.source}</p>
                  </div>
                  <InformationGradingConteiner 
                    key={this.props.data.id} 
                    number={this.props.data.inf_grading}
                    reloadMain={self.props.reloadData} 
                    toggle200={self.props.toggle200} 
                    toggle500={self.props.toggle500}
                    infoGrading={self.props.infoGrading}
                    update={this.updateInformationGrading} 
                  />
                </div>
                <div className="w-row">
                  <div className="w-col w-col-12">
                    <h3 className="teblehead" style={{'textAlign': 'left'}}>Description</h3>
                    <p className="listincidents__form_label db_entries">{this.props.data.description}</p>
                  </div>
                </div>
              </div>
            </div>
        )
    }
});

var InformationGradingConteiner = React.createClass({
    mixins: [TimerMixin],
    propTypes: {  
        toggle200: PropTypes.func,
        toggle500: PropTypes.func,
        reloadMain: PropTypes.func, 
        number: PropTypes.number, 
        infoGrading: PropTypes.array,
        update: PropTypes.func,  
    },
    getInitialState: function () {
        return {
            choosed: 1,
            addNew: false,
            newName: ''
        }
    },
    componentWillMount: function () {
        this.setState({
            choosed: this.props.number
        });
    },
    hendleChange: function (event) {
        var self = this;
        this.setState({
            choosed: event.target.value,
        }, self.sendUpdate());
    },
    sendUpdate: function () {
        var self = this;
        this.setTimeout( function () {
            self.props.update(self.state.choosed);
        },200)
    },
    render: function () {
        var self = this;
        var listOptions = this.props.infoGrading.map(function(inst){
            return (<option key={inst.id} value={inst.id}>{inst.name}</option>)
        });
        var existClass = classNames({
            'gradingList': true,
            'hidden': this.state.addNew
        });
        return (
            <div className="w-col w-col-4">
            <div className={existClass}>
             <div className="w-row">
             <div className="w-col w-col-12">
              <h3 className="teblehead" style={{'textAlign': 'left'}}>Information Grading</h3>
              <select onChange={this.hendleChange} value={this.state.choosed} className="w-select newincidentform__input">
                {listOptions}
              </select>
             </div>
             </div>
             </div> 
            </div>
        )
    },
});

var ChartType = React.createClass({
  propTypes: {
    children: PropTypes.node,
    className: PropTypes.string,
    isDisabled: PropTypes.bool,
    isFocused: PropTypes.bool,
    isSelected: PropTypes.bool,
    onFocus: PropTypes.func,
    onSelect: PropTypes.func,
    option: PropTypes.object,
  },
  handleMouseDown: function (event) {
    event.preventDefault();
    event.stopPropagation();
    this.props.onSelect(this.props.option, event);
  },
  handleMouseEnter: function (event) {
    this.props.onFocus(this.props.option, event);
  },
  handleMouseMove: function (event) {
    if (this.props.isFocused) return;
    this.props.onFocus(this.props.option, event);
  },
  render: function () {
    var gravatarStyle = {
      borderRadius: 3,
      display: 'inline-block',
      marginRight: 10,
      position: 'relative',
      top: -2,
      verticalAlign: 'middle',
      width: '13%'
    };
    return (
      <div style={{"textAlign": "left"}} className={this.props.className}
        onMouseDown={this.handleMouseDown}
        onMouseEnter={this.handleMouseEnter}
        onMouseMove={this.handleMouseMove}
        title={this.props.option.title}>
        {this.props.children}
      </div>
    )
  },
}) 

const ChartValue = React.createClass({
  propTypes: {
    children: PropTypes.node,
    placeholder: PropTypes.string,
    value: PropTypes.object
  },
  render: function () {
    var gravatarStyle = {
      borderRadius: 3,
      display: 'inline-block',
      marginRight: 10,
      position: 'relative',
      top: -2,
      verticalAlign: 'middle',
      width: '13%'
    };
    return (
      <div className="Select-value" style={{"textAlign": "left"}} title={this.props.value.title}>
        <span className="Select-value-label">
          {this.props.children}
        </span>
      </div>
    );
  }
}); 


var fire_name;
var clear_name;
var triggered_name = false;
var re_fire_name;
var re_clear_name;
var re_triggered_name = false;
var re_fire_summary;
var re_clear_summary;
var re_triggered_summary = false;
var co_fire_name;
var co_clear_name;
var co_triggered_name = false;

var EntryMainINfoConteiner = React.createClass({
    mixins: [TimerMixin],
    propTypes: {  
        toggle200: PropTypes.func,
        toggle500: PropTypes.func,
    },
    getInitialState: function () {
        return {
            name: '',
            vicMin: 0,
            vicMax: 0,
            vicChoice: 1,
            remarks: '',
            JMACcomments: '',
            summary: '',
            loading: true,
            filenames: [],
            links: [],
            newLink: '',
            vals: []
        }
    },
    componentWillMount: function () {
        var source = 'http://' + AppGlobal.host + "/api/manage_db/entry/" + AppGlobal.rec + "/mainInfo/" + "?format=json";
        this.serverRequest = $.get(source, function (results) {
            var data = results.results[0];
            this.setState({
              name: data.name,
              JMACcomments: data.JMACcomments,
              remarks: data.remarks,
              summary: data.Summary,
              filenames: data.documents,
              links: data.links,
              vals: data.value_instance 
            });
        }.bind(this));
    },
    componentDidMount: function () {
        this.setState({
            loading: false,
        });
    },
    hendleNameChange: function (event) {
        var self = this;
        this.setState({
            name: event.target.value,
        });
        this.setTimeout( function () {self.startSending();}, 300);
    },
    startSending: function () {
        var self = this;
        if (triggered_name === false) {
            triggered_name = true;
            fire_name = setTimeout(function () {self.updateEntryName();}, 2500);
            clear_name = setTimeout( function (){triggered_name = false;}, 15000);
        } else if (triggered_name === true) {
            clearTimeout(fire_name);
            clearTimeout(clear_name);
            triggered_name = false;
            self.startSending();
        }
    },
    updateEntryName: function () {
        var self = this;
        this.setTimeout( function () {
            var source = 'http://'+AppGlobal.host+'/api/manage_db/update_entry/name/';
            var post_d = {'user_id': parseInt(AppGlobal.user), 'entry_id':parseInt(AppGlobal.rec), 'name':this.state.name};
            $.ajax({
                headers: { 
                  'Accept': 'application/json',
                  'Content-Type': 'application/json' 
                },
                type: "POST",
                url: source,
                data: JSON.stringify(post_d, null, '\t'),
                dataType: 'json',
                statusCode: {
                  200: function (data) {self.props.toggle200(data.text);},
                  500: function () {self.props.toggle500('Something went wrong. Please reload the page.');},
                }
            });
        }, 200)
    },
    hendleRemarksChange: function (event) {
        var self = this;
        this.setState({
            remarks: event.target.value,
        });
        this.setTimeout( function () {self.startSendingRemarks();}, 300);
    },
    startSendingRemarks: function () {
        var self = this;
        if (re_triggered_name === false) {
            re_triggered_name = true;
            re_fire_name = setTimeout(function () {self.updateEntryRemarks();}, 2500);
            re_clear_name = setTimeout( function (){re_triggered_name = false;}, 15000);
        } else if (re_triggered_name === true) {
            clearTimeout(re_fire_name);
            clearTimeout(re_clear_name);
            re_triggered_name = false;
            self.startSendingRemarks();
        }
    },
    updateEntryRemarks: function () {
        var self = this;
        this.setTimeout( function () {
            var source = 'http://'+AppGlobal.host+'/api/manage_db/update_entry/remarks/';
            var post_d = {'user_id': parseInt(AppGlobal.user), 'entry_id':parseInt(AppGlobal.rec), 'remarks':this.state.remarks};
            $.ajax({
                headers: { 
                  'Accept': 'application/json',
                  'Content-Type': 'application/json' 
                },
                type: "POST",
                url: source,
                data: JSON.stringify(post_d, null, '\t'),
                dataType: 'json',
                statusCode: {
                  200: function (data) {self.props.toggle200(data.text);},
                  500: function () {self.props.toggle500('Something went wrong. Please reload the page.');},
                }
            });
        }, 200)
    },
    hendleCommentsChange: function (event) {
        var self = this;
        this.setState({
            JMACcomments: event.target.value,
        });
        this.setTimeout( function () {self.startSendingComments();}, 300);
    },
    startSendingComments: function () {
        var self = this;
        if (co_triggered_name === false) {
            co_triggered_name = true;
            co_fire_name = setTimeout(function () {self.updateEntryComments();}, 2500);
            co_clear_name = setTimeout( function (){co_triggered_name = false;}, 15000);
        } else if (co_triggered_name === true) {
            clearTimeout(co_fire_name);
            clearTimeout(co_clear_name);
            co_triggered_name = false;
            self.startSendingComments();
        }
    },
    updateEntryComments: function () {
        var self = this;
        this.setTimeout( function () {
            var source = 'http://'+AppGlobal.host+'/api/manage_db/update_entry/comments/';
            var post_d = {'user_id': parseInt(AppGlobal.user), 'entry_id':parseInt(AppGlobal.rec), 'comments':this.state.JMACcomments};
            $.ajax({
                headers: { 
                  'Accept': 'application/json',
                  'Content-Type': 'application/json' 
                },
                type: "POST",
                url: source,
                data: JSON.stringify(post_d, null, '\t'),
                dataType: 'json',
                statusCode: {
                  200: function (data) {self.props.toggle200(data.text);},
                  500: function () {self.props.toggle500('Something went wrong. Please reload the page.');},
                }
            });
        }, 200)
    },
    hendleSummaryChange: function (event) {
        var self = this;
        this.setState({
            summary: event.target.value,
        });
        this.setTimeout( function () {self.startSendingSummary();}, 300);
    },
    startSendingSummary: function () {
        var self = this;
        if (re_triggered_summary === false) {
            re_triggered_summary = true;
            re_fire_summary = setTimeout(function () {self.updateEntrySummary();}, 2500);
            re_clear_summary = setTimeout( function (){re_triggered_summary = false;}, 15000);
        } else if (re_triggered_summary === true) {
            clearTimeout(re_fire_summary);
            clearTimeout(re_clear_summary);
            re_triggered_summary = false;
            self.startSendingSummary();
        }
    },
    updateEntrySummary: function () {
        var self = this;
        this.setTimeout( function () {
            var source = 'http://'+AppGlobal.host+'/api/manage_db/update_entry/summary/';
            var post_d = {'user_id': parseInt(AppGlobal.user), 'entry_id':parseInt(AppGlobal.rec), 'summary':self.state.summary};
            $.ajax({
                headers: { 
                  'Accept': 'application/json',
                  'Content-Type': 'application/json' 
                },
                type: "POST",
                url: source,
                data: JSON.stringify(post_d, null, '\t'),
                dataType: 'json',
                statusCode: {
                  200: function (data) {self.props.toggle200(data.text);},
                  500: function () {self.props.toggle500('Something went wrong. Please reload the page.');},
                }
            });
        }, 200)
    },
    saveArray: function (dataset) {
        var self = this;
        var source = 'http://'+AppGlobal.host+'/api/manage_db/update_entry/upload_assigne/';
        var post_d = {'user_id': parseInt(AppGlobal.user), 'entry_id':parseInt(AppGlobal.rec), 'upload_id':dataset.id};
        $.ajax({
            headers: { 
              'Accept': 'application/json',
              'Content-Type': 'application/json' 
            },
            type: "POST",
            url: source,
            data: JSON.stringify(post_d, null, '\t'),
            dataType: 'json',
            statusCode: {
              200: function (data) {self.attachNew(dataset); self.props.toggle200(data.text); },
              500: function () {self.props.toggle500('Something went wrong. Please reload the page.');},
            }
        });
    },
    attachNew: function(data) {
        var self = this;
        var arr = self.state.filenames;
        var newDoc = {'id': data.id, 'description': data.description, 'document': data.document}
        arr.push(newDoc)
        this.setState({
            filenames: arr
        })
    },
    onDrop: function  (files){
        var self = this;
        this.setTimeout( function () {
            var source = 'http://'+AppGlobal.host+'/api/manage_db/update_entry/document_upload/';
            var file = new FormData();
            file.append('document', files[0])
            file.append('description', files[0].name)
            self.setState({fname: files[0].name})
            $.ajax({
                type: "POST",
                url: source,
                data: file,
                processData: false, 
                contentType: false,
                success : function(data) {
                    self.saveArray(data);
                },
                statusCode: {
                  //200: function (data) {self.props.toggle200('cool'); self.saveArray(data)},
                  500: function () {self.props.toggle500('Something went wrong. Please reload the page.');},
                }
            });
        }, 200)
    },
    hendleNewLink: function (event) {
        event.preventDefault();
        this.setState({
            newLink: event.target.value,
        });
    },
    createNewLink: function () {
        var self = this;
        this.setTimeout( function () {
            var source = 'http://'+AppGlobal.host+'/api/manage_db/update_entry/newLink/';
            var post_d = {'user_id': parseInt(AppGlobal.user), 'entry_id':parseInt(AppGlobal.rec), 'link':this.state.newLink};
            $.ajax({
                headers: { 
                  'Accept': 'application/json',
                  'Content-Type': 'application/json' 
                },
                type: "POST",
                url: source,
                data: JSON.stringify(post_d, null, '\t'),
                dataType: 'json',
                statusCode: {
                  200: function (data) {self.props.toggle200(data.text); self.appendNewLink(data);},
                  500: function () {self.props.toggle500('Something went wrong. Please reload the page.');},
                }
            });
        }, 200)
    },
    appendNewLink: function (data) {
        var self = this;
        var arr = self.state.links;
        var newDoc = {'id': data.link_id, 'url': data.new_link,}
        arr.push(newDoc)
        this.setState({
            links: arr,
            newLink: ''
        })
    },
    createValue: function () {
        var self = this;
        var source = 'http://'+AppGlobal.host+'/api/manage_db/update_entry/createValue/';
        var post_d = {'user_id': parseInt(AppGlobal.user), 'entry_id':parseInt(AppGlobal.rec)};
        $.ajax({
            headers: { 
              'Accept': 'application/json',
              'Content-Type': 'application/json' 
            },
            type: "POST",
            url: source,
            data: JSON.stringify(post_d, null, '\t'),
            dataType: 'json',
            statusCode: {
              200: function (data) {self.appendNewValue(data.valId); self.props.toggle200(data.text); },
              500: function () {self.props.toggle500('Something went wrong. Please reload the page.');},
            }
        });
    },
    appendNewValue: function (data) {
        var list = this.state.vals;
        list.push(parseInt(data))
        this.setState({
            vals: list
        })
    },
    removeValueInstance: function (valueId) {
        var self = this;
        var source = 'http://'+AppGlobal.host+'/api/manage_db/update_entry/deleteValue/';
        var post_d = {'user_id': parseInt(AppGlobal.user), 'entry_id':parseInt(AppGlobal.rec), 'remove': valueId};
        $.ajax({
            headers: { 
              'Accept': 'application/json',
              'Content-Type': 'application/json' 
            },
            type: "POST",
            url: source,
            data: JSON.stringify(post_d, null, '\t'),
            dataType: 'json',
            statusCode: {
              200: function (data) {self.removeValue(data.valId); self.props.toggle200(data.text); },
              500: function () {self.props.toggle500('Something went wrong. Please reload the page.');},
            }
        });
    },
    removeValue: function (data) {
        var list = this.state.vals;
        var index = list.indexOf(parseInt(data));
        if (index > -1) {
            list.splice(index, 1);
            this.setState({
                vals: list
            })
        }

    },
    render: function () {
        var self = this;
        var listDocs = this.state.filenames.map((inst, key)=>{
            return (
                <div className="w-col w-col-4" key={'doc_'+inst.id}>
                    <p style={{'marginTop': '0px', 'display': 'inline-block', 'float': 'left'}} className="listincidents__form_label db_entries">{key + 1}.&nbsp;&nbsp;
                        <a style={{'marginTop': '0px', 'display': 'inline-block', 'color': '#489fd9'}} href={inst.document} target="_blank" className="listincidents__form_label db_entries">{inst.description}</a>
                    </p>
                </div>
            )
        });
        var listLinks = this.state.links.map((inst, key)=>{
            return (
                <div className="w-col w-col-12" key={'links_'+inst.id}>
                    <p style={{'marginTop': '0px', 'display': 'inline-block', 'float': 'left'}} className="listincidents__form_label db_entries">{key + 1}.&nbsp;&nbsp;
                        <a style={{'marginTop': '0px', 'display': 'inline-block', 'color': '#489fd9'}} href={inst.url} target="_blank" className="listincidents__form_label db_entries">{inst.url}</a>
                    </p> 
                </div>
            )
        });
        var listValues = this.state.vals.map(function(inst, key) {
            return (<EntryValuesConteiner number={key} valueId={inst} key={'entryValues_'+inst} remove={self.removeValueInstance} toggle200={self.props.toggle200} toggle500={self.props.toggle500}/>)
        })
        return this.state.loading === false
        ? (<div className="db__incidentinfo">
              <h4 className="actionhead db_inst">Entry info:</h4>
              <div className="w-row db__shortnameblock">
                <div className="w-col w-col-12">
                  <h3 className="teblehead" style={{'textAlign': 'left'}}>Title or short description</h3>
                  <input onChange={this.hendleNameChange} value={this.state.name} className="w-input newincidentform__input" maxLength="256" placeholder="Enter title or short description" type="text"/>
                </div>
              </div>
              <div>
                <h3 className="teblehead" style={{'textAlign': 'left'}}>General Info</h3>
                {listValues}
                <div className="w-row">
                    <div className="w-col w-col-2">
                        <h4 onClick={this.createValue} className="newincidentform__hint addelement verycloseup">Add general info</h4>
                    </div>
                </div>
              </div>
              <div className="db__remarksconteiner">
                <div className="w-row">
                  <div className="w-col w-col-12">
                    <h3 className="teblehead" style={{'textAlign': 'left'}}>Remarks</h3>
                    <textarea onChange={this.hendleRemarksChange} value={this.state.remarks} style={{'maxWidth': '937px'}} className="w-input newincidentform__input text db" maxLength="5000" placeholder="Sample"></textarea>
                  </div>
                </div>
                <div className="w-row">  
                  <div className="w-col w-col-12">
                    <h3 className="teblehead" style={{'textAlign': 'left'}}>JMAC db comments</h3>
                    <textarea onChange={this.hendleCommentsChange} value={this.state.JMACcomments} style={{'maxWidth': '937px'}}  className="w-input newincidentform__input text db" maxLength="5000" placeholder="Sample"></textarea>
                  </div>
                </div>
              </div>
              <div className="db__remarksconteiner">
                <div className="w-row">
                  <div className="w-col w-col-12">
                    <h3 className="teblehead" style={{'textAlign': 'left'}}>Summary</h3>
                    <textarea onChange={this.hendleSummaryChange} value={this.state.summary} style={{'maxWidth': '937px'}}  className="w-input newincidentform__input text db" maxLength="5000" placeholder="Sample"></textarea>
                  </div>
                </div>
              </div>
              <div className="db__remarksconteiner">
                <div className="w-row">
                  <div className="w-col w-col-12">
                    <h3 className="teblehead" style={{'textAlign': 'left'}}>Uploaded documents</h3>
                    <Dropzone style={{'borderWidth': '1px'}} className="dropStyle" onDrop={this.onDrop}>
                      <div><p style={{'marginTop': '0px', 'display': 'block' }} className="listincidents__form_label db_entries">Try dropping some files here, or click to select files to upload.</p></div>
                    </Dropzone>
                    <div style={{'marginTop': '10px'}} className="w-row">
                  {listDocs}
                  </div>
                  </div>
                </div>
                <div className="w-row">  
                  <div className="w-col w-col-12">
                    <h3 className="teblehead" style={{'textAlign': 'left'}}>External Links</h3>
                    <div className="w-row">
                        <div className="w-col w-col-10">
                            <input onChange={this.hendleNewLink} value={this.state.newLink} className="w-input newincidentform__input" placeholder="Input external web-address" type="url"/>
                        </div>
                        <div className="w-col w-col-2">
                            <a style={{'marginTop': '0px', 'float': 'right'}} onClick={this.createNewLink} className="w-button listincidents__form_submit db">Save link</a>
                        </div>
                    </div>
                    <div className="w-row">
                      {listLinks}
                    </div>
                  </div>
                </div>
              </div>
            </div>)
        : (<h4 className="actionhead db_inst"> Loading data ...</h4>)
    }
})
//<label className="newincidentform__label">Title or short description:</label>

var EntryValuesConteiner = React.createClass({
    mixins: [TimerMixin],
    propTypes: {  
        valueId: PropTypes.number, 
        remove: PropTypes.func,
        toggle200: PropTypes.func,
        toggle500: PropTypes.func, 
        number: PropTypes.number,
    },
    getInitialState: function () {
        return {
            data: ['id': 0, 'date':"", 'time':"", 'locations':[], 'nature':[], 'actors':[], 'victims':[]],
            // nature
            showNature: false,
            natureMains: [],
            natureSubs: [],
            natureMain: "", 
            natureSub: "",
            // victims
            showVictims: false,
            victimsData: [],
            victimGroup: "",
            victims: 0,
            // locations
            showLocations: false,
            regions: [],
            localitys: [],
            villages: [],
            region: "",
            locality: "",
            village: "",
            e: "",
            n: "",
            // actors
            showActors: false,
            actors: [],
            actor: "",
            //dates
            date: '',
            displayDate: '',
            listDate: '',
            time: '',
            showCalendar: false,
            // general
            ready: false
        }
    },
    componentWillMount: function () {
        var source = 'http://' + AppGlobal.host + "/api/manage_db/entry/" + AppGlobal.rec + "/values/" + this.props.valueId +"?format=json";
        this.serverRequest = $.get(source, function (results) {
            var data = results.results[0];
            this.setState({
              data: data,
              //displayDate: data.date,
              time: data.time,
              ready: true
            });
        }.bind(this));
    },
    // Nature methods
    addNature: function () {
        var self = this;
        if (this.state.natureMain != "") {
            this.toggleNature();
            var su = "";
            if (this.state.natureSub != "") {
                su = this.state.natureSub.value;
            }
            var source = 'http://'+AppGlobal.host+'/api/manage_db/update_entry/nature_add/';
            var post_d = {'user_id': parseInt(AppGlobal.user), 'entry_id':parseInt(AppGlobal.rec), 'de_id':parseInt(this.props.valueId), 'main': this.state.natureMain.value, 'sub': su};
            $.ajax({
                headers: { 
                  'Accept': 'application/json',
                  'Content-Type': 'application/json' 
                },
                type: "POST",
                url: source,
                data: JSON.stringify(post_d, null, '\t'),
                dataType: 'json',
                statusCode: {
                  200: function (data) {self.handleNatureAdding(data), self.props.toggle200(data.text);},
                  500: function () {self.props.toggle500('Something went wrong. Please reload the page.');},
                }
            });
        } else {
            this.toggleNature()
        }
    },
    handleNatureAdding: function (data) {
        var self = this;
        var all = this.state.data;
        var list = all.nature;
        var newVal = {'id': data.valId, 'main': data.mainId, 'sub': data.subId, 'computed': data.computed}
        list.push(newVal)
        this.setState({
            data: all,
            natureMain: "",
            natureSub: "",
        });
    },
    removeNature: function (nature) {
        var self = this;
        var source = 'http://'+AppGlobal.host+'/api/manage_db/update_entry/nature_remove/';
        var post_d = {'user_id': parseInt(AppGlobal.user), 'entry_id':parseInt(AppGlobal.rec), 'de_id':parseInt(this.props.valueId), 'nature_id': parseInt(nature)};
        $.ajax({
            headers: { 
              'Accept': 'application/json',
              'Content-Type': 'application/json' 
            },
            type: "POST",
            url: source,
            data: JSON.stringify(post_d, null, '\t'),
            dataType: 'json',
            statusCode: {
              200: function (data) {self.props.toggle200(data.text);self.handleNatureRemove(parseInt(nature))},
              500: function () {self.props.toggle500('Something went wrong. Please reload the page.');},
            }
        });
    },
    handleNatureRemove: function (data) {
        var all = this.state.data;
        var list = all.nature;
        var length = list.length;
        var i = 0;
        var index = 0;
        for (i=0; i <= length - 1; i++) {
            var item = list[i]
            if (parseInt(item.id) === data) {
                index = i;
                break
            }
        }
        list.splice(index, 1);
        this.setState({
            data: all
        })
    },
    mainNatureChange: function (value) {
        var self = this;    
        this.setState({ 
          natureMain: value, 
          natureSub: ''
        });
        this.fetchSub(value.value);
    },
    subNatureChange: function (value) {
        this.setState({ 
          natureSub: value, 
        });
    },
    fetchMain: function () {
        var source_natureMain = 'http://' + AppGlobal.host + "/api/manage_db/db_data/nature_main/" + "?format=json";
        this.serverRequest = $.get(source_natureMain, function (results) {
            var natureMain_data = results.results;
            this.setState({
                natureMains: natureMain_data,
            });
        }.bind(this));
    },
    fetchSub: function (main) {
        var self = this;
        var source_loc = 'http://' + AppGlobal.host + "/api/manage_db/db_data/nature_sub/" + parseInt(main) + "/?format=json";
        this.serverRequest = $.get(source_loc, function (results) {
            var data = results.results;
            this.setState({
                natureSubs: data
            })
        }.bind(this));
    },
    toggleNature: function () {
        if (this.state.natureMains.length <= 0) {
            this.fetchMain();
        }
        this.setState({
            showNature: !this.state.showNature
        })
    },
    /////////////////////
    // Victims methods //
    /////////////////////
    addVictim: function () {
        var self = this;
        if (this.state.victimGroup != "") {
            var source = 'http://'+AppGlobal.host+'/api/manage_db/update_entry/victims_add/';
            var post_d = {'user_id': parseInt(AppGlobal.user), 'entry_id':parseInt(AppGlobal.rec), 'de_id':parseInt(this.props.valueId), 'group':parseInt(this.state.victimGroup.value), 'quantity': this.state.victims};
            $.ajax({
                headers: { 
                  'Accept': 'application/json',
                  'Content-Type': 'application/json' 
                },
                type: "POST",
                url: source,
                data: JSON.stringify(post_d, null, '\t'),
                dataType: 'json',
                statusCode: {
                  200: function (data) {self.handleVictimAdd(data);},
                  500: function () {self.props.toggle500('Something went wrong. Please reload the page.');},
                }
            });
        } else {
            this.toggleVictims()
        }
    },
    handleVictimAdd: function (data) {
        var self = this;
        var all = this.state.data;
        var list = all.victims;
        var newVal = {'id': data.valId, 'group': data.group, 'quantity': data.quantity, 'computed': data.computed}
        list.push(newVal)
        this.setState({
            data: all,
            victimGroup: "",
            victims: 0,
        }, self.props.toggle200(data.text));
        this.toggleVictims();
        
    },
    removeVictim: function (val) {
        var self = this;
        var source = 'http://'+AppGlobal.host+'/api/manage_db/update_entry/victims_remove/';
        var post_d = {'user_id': parseInt(AppGlobal.user), 'entry_id':parseInt(AppGlobal.rec), 'de_id':parseInt(this.props.valueId), 'val':parseInt(val), };
        $.ajax({
            headers: { 
              'Accept': 'application/json',
              'Content-Type': 'application/json' 
            },
            type: "POST",
            url: source,
            data: JSON.stringify(post_d, null, '\t'),
            dataType: 'json',
            statusCode: {
              200: function (data) {self.handleVictimRemove(parseInt(val), data);},
              500: function () {self.props.toggle500('Something went wrong. Please reload the page.');},
            }
        });
    },
    handleVictimRemove: function (data, responseData) {
        var self = this;
        var all = this.state.data;
        var list = all.victims;
        var length = list.length;
        var i = 0;
        var index = 0;
        for (i=0; i <= length - 1; i++) {
            var item = list[i]
            if (parseInt(item.id) === data) {
                index = i;
                break
            }
        }
        list.splice(index, 1);
        this.setState({
            data: all
        }, self.props.toggle200(responseData.text))
    },
    fetchVictimCategories: function () {
        var source_victims = 'http://' + AppGlobal.host + "/api/manage_db/victims/allChoices/" + "?format=json";
        this.serverRequest = $.get(source_victims, function (results) {
            var victims_data = results.results;
            this.setState({
              victimsData: victims_data,
            });
        }.bind(this));
    },
    victimCategoryChange: function (value) {
        this.setState({
            victimGroup: value
        });
    },
    victimNumberChange: function (event) {
        var num = 0
        try {
            var val = event.target.value;
            if (val) {
               num = parseInt(event.target.value) 
           } else {
            num = 0
           } 
             
        } catch (err) {
            num = 0
        }
        this.setState({
            victims: num
        })
    },
    toggleVictims: function () {
        if (this.state.victimsData.length == 0) {
            this.fetchVictimCategories()
        }
        this.setState({
            showVictims: !this.state.showVictims
        })
    },
    //////////////////////
    // Location methods //
    //////////////////////
    addLocation: function () {
        var self = this;
        if (this.state.region != "") {
            if (this.state.locality == "") {
                var loc = ""
            } else {
                var loc = this.state.locality.value
            }
            if (this.state.village == "") {
                var vil = ""
            } else {
                var vil = this.state.village.value
            }
            var source = 'http://'+AppGlobal.host+'/api/manage_db/update_entry/location_add/';
            var post_d = {'user_id': parseInt(AppGlobal.user), 'entry_id':parseInt(AppGlobal.rec), 
                            'de_id':parseInt(this.props.valueId), 'region':this.state.region.value, 'locality': loc, 
                            'village': vil};
            $.ajax({
                headers: { 
                  'Accept': 'application/json',
                  'Content-Type': 'application/json' 
                },
                type: "POST",
                url: source,
                data: JSON.stringify(post_d, null, '\t'),
                dataType: 'json',
                statusCode: {
                  200: function (data) {self.handleAddLocation(data);},
                  500: function () {self.props.toggle500('Something went wrong. Please reload the page.');},
                }
            });
        } else {
           this.toggelLocation(); 
        }
    },
    handleAddLocation: function (data) {
        var self = this;
        var all = this.state.data;
        var list = all.locations;
        var newVal = {'id': data.valId, 'computed': data.computed}
        list.push(newVal)
        this.setState({
            data: all,
            region: '',
            localitys: [],
            villages: [],
            locality: '',
            village: '',
            e: '',
            n: ''
        }, self.props.toggle200(data.text));
        this.toggelLocation();
    },
    removeLocation: function (value) {
        var self = this;
        var source = 'http://'+AppGlobal.host+'/api/manage_db/update_entry/location_remove/';
        var post_d = {'user_id': parseInt(AppGlobal.user), 'entry_id':parseInt(AppGlobal.rec), 
                        'de_id':parseInt(this.props.valueId), 'instance': value};
        $.ajax({
            headers: { 
              'Accept': 'application/json',
              'Content-Type': 'application/json' 
            },
            type: "POST",
            url: source,
            data: JSON.stringify(post_d, null, '\t'),
            dataType: 'json',
            statusCode: {
              200: function (data) {self.handleRemoveLocation(data, parseInt(value));},
              500: function () {self.props.toggle500('Something went wrong. Please reload the page.');},
            }
        });
    },
    handleRemoveLocation: function (data, value) {
        var self = this;
        var all = this.state.data;
        var list = all.locations;
        var length = list.length;
        var i = 0;
        var index = 0;
        for (i=0; i <= length - 1; i++) {
            var item = list[i]
            if (parseInt(item.id) === value) {
                index = i;
                break
            }
        }
        list.splice(index, 1);
        this.setState({
            data: all
        }, self.props.toggle200(data.text))
    },
    regionChange: function (value) {
        var self = this;  
        this.setState({ 
          region: value,
          localitys: [],
          villages: [],
          locality: '',
          village: '',
          e: '',
          n: '' 
        }, self.fetchLocalitys(parseInt(value.value)));
    },
    localityChange: function (value) {
        var self = this;
        var r = self.state.region.value  
        this.setState({ 
          locality: value,
          villages: [],
          village: '',
          e: '',
          n: '' 
        }, self.fetchVillages(parseInt(r), parseInt(value.value)));
    },
    villageChange: function (value) {
        var self = this;
        var r = self.state.region.value
        var l = self.state.locality.value 
        this.setState({ 
          village: value 
        }, self.fetchCoordinates(r, l, value.value));
    },
    eChange: function (event) {
        var self = this;
        this.setState({
            e: event.target.value
        }, self.updateCoordinates());
    },
    nChange: function (event) {
        var self = this;
        this.setState({
            n: event.target.value
        }, self.updateCoordinates());
    },
    updateCoordinates: function () {
        var self = this;
        var r = self.state.region.value
        var l = self.state.locality.value
        var v = self.state.village.value
        var source = 'http://'+AppGlobal.host+'/api/manage_db/update_entry/coordinates/';
        this.setTimeout( function () {
            var post_d = {'user_id': parseInt(AppGlobal.user), 'e': self.state.e, 'n': self.state.n, 'region': parseInt(r), 'locality': parseInt(l), 'village': parseInt(v)};
            $.ajax({
                headers: { 
                  'Accept': 'application/json',
                  'Content-Type': 'application/json' 
                },
                type: "POST",
                url: source,
                data: JSON.stringify(post_d, null, '\t'),
                dataType: 'json',
                statusCode: {
                  200: function (data) {self.props.toggle200(data.text);},
                  500: function () {self.props.toggle500('Something went wrong. Please reload the page.');},
                }
            });
        }, 200)
    },
    fetchRegions: function () {
        var source_location = 'http://' + AppGlobal.host + "/api/manage_db/db_data/locations/" + "?format=json";
        this.serverRequest = $.get(source_location, function (results) {
            var location_data = results.results;
            this.setState({
              regions: location_data,
            });
        }.bind(this));
    },
    fetchLocalitys: function (region) {
        var self = this;
        var source_loc = 'http://' + AppGlobal.host + "/api/manage_db/db_data/locality/" + region + "/?format=json";
        this.serverRequest = $.get(source_loc, function (results) {
            var location_data = results.results;
            this.setState({
                localitys: location_data
            })
        }.bind(this));
    },
    fetchVillages: function (region, locality) {
        var self = this;
        var source_loc = 'http://' + AppGlobal.host + "/api/manage_db/db_data/village/" + region +"/"+ locality + "/?format=json";
        this.serverRequest = $.get(source_loc, function (results) {
            var location_data = results.results;
            this.setState({
                villages: location_data
            })
        }.bind(this));
    },
    fetchCoordinates: function (region, locality, village) {
        var source_loc = 'http://' + AppGlobal.host + "/api/manage_db/db_data/coordinates/" + region +"/"+ locality + "/" + village + "/?format=json";
        this.serverRequest = $.get(source_loc, function (results) {
            var location_data = results.results[0];
            this.setState({
              e: location_data.coordinates.e,
              n: location_data.coordinates.n
            });
        }.bind(this));
    },
    toggelLocation: function () {
        if (this.state.regions.length == 0) {
            this.fetchRegions()
        }
        this.setState({
            showLocations: !this.state.showLocations
        })
    },
    // Actors methods
    addActor: function () {
        var self = this;
        if (this.state.actor != "") {
            var source = 'http://'+AppGlobal.host+'/api/manage_db/update_entry/actors_add/';
            var post_d = {'user_id': parseInt(AppGlobal.user), 'entry_id':parseInt(AppGlobal.rec), 'de_id':parseInt(this.props.valueId), 'group':this.state.actor.value};
            $.ajax({
                headers: { 
                  'Accept': 'application/json',
                  'Content-Type': 'application/json' 
                },
                type: "POST",
                url: source,
                data: JSON.stringify(post_d, null, '\t'),
                dataType: 'json',
                statusCode: {
                  200: function (data) {self.handleAddActor(data);},
                  500: function () {self.props.toggle500('Something went wrong. Please reload the page.');},
                }
            });
        } else {
            this.toggleActors()
        }
    },
    handleAddActor: function (data) {
        var self = this;
        var self = this;
        var all = this.state.data;
        var list = all.actors;
        var newVal = {'value': data.valId, 'label': data.label}
        list.push(newVal)
        this.setState({
            data: all,
            actor: '',
        }, self.props.toggle200(data.text));
        this.toggleActors();
    },
    removeActor: function (value) {
        var self = this;
        var source = 'http://'+AppGlobal.host+'/api/manage_db/update_entry/actors_remove/';
        var post_d = {'user_id': parseInt(AppGlobal.user), 'entry_id':parseInt(AppGlobal.rec), 'de_id':parseInt(this.props.valueId), 'group':value};
        $.ajax({
            headers: { 
              'Accept': 'application/json',
              'Content-Type': 'application/json' 
            },
            type: "POST",
            url: source,
            data: JSON.stringify(post_d, null, '\t'),
            dataType: 'json',
            statusCode: {
              200: function (data) {self.handleRemoveActor(data, value);},
              500: function () {self.props.toggle500('Something went wrong. Please reload the page.');},
            }
        });
    },
    handleRemoveActor: function (data, value) {
        var self = this;
        var all = this.state.data;
        var list = all.actors;
        var length = list.length;
        var i = 0;
        var index = 0;
        for (i=0; i <= length - 1; i++) {
            var item = list[i]
            if (parseInt(item.value) === value) {
                index = i;
                break
            }
        }
        list.splice(index, 1);
        this.setState({
            data: all
        }, self.props.toggle200(data.text))
    },
    changeActor: function (value) {
        var self = this;
        this.setState({ 
          actor: value 
        });
    },
    fetchActors: function () {
        var source_actors = 'http://' + AppGlobal.host + "/api/manage_db/db_data/actors/" + "?format=json";
        this.serverRequest = $.get(source_actors, function (results) {
            var actors_data = results.results;
            this.setState({
              actors: actors_data,
            });
        }.bind(this));
    },
    toggleActors: function () {
        if (this.state.actors.length == 0) {
            this.fetchActors()
        }
        this.setState({
            showActors: !this.state.showActors
        })
    },
    ///////////
    // Dates //
    ///////////
    updateDateTime: function () {
        var self = this;
        if (this.state.date != "") {
            var source = 'http://'+AppGlobal.host+'/api/manage_db/update_entry/date/';
            var post_d = {'user_id': parseInt(AppGlobal.user), 'entry_id':parseInt(AppGlobal.rec), 'de_id':parseInt(this.props.valueId), 'date': this.state.date, 'time': this.state.time};
            $.ajax({
                headers: { 
                  'Accept': 'application/json',
                  'Content-Type': 'application/json' 
                },
                type: "POST",
                url: source,
                data: JSON.stringify(post_d, null, '\t'),
                dataType: 'json',
                statusCode: {
                  200: function (data) {self.handleDateUpdate(data, self.state.listDate, self.state.time);},
                  500: function () {self.props.toggle500('Something went wrong. Please reload the page.');},
                }
            });
        } else {
            self.props.toggle500('Please choose date and time.')
        }
    },
    handleDateUpdate: function (data, date, time) {
        var self = this;
        var all = this.state.data;
        all.date = date
        if (time != "") {
            all.time = time
        }
        this.setState({
            data: all,
        }, self.props.toggle200(data.text));
        this.toggleCalndar();
    },
    handleDateChange(date) {
        var self = this;
        const format = 'YYYY-MM-DD';
        const format2 = 'DD-MM-YYYY';
        var start = date.format(format).toString();
        var show = date.format(format2).toString();
        this.setState({
          date: start,
          displayDate: date,
          listDate: show
        });
    },
    hendleTimeChange: function (event) {
        var self = this;
        this.setState({
            time: event.target.value,
        })
    },
    toggleCalndar: function () {
        this.setState({
            showCalendar: !this.state.showCalendar
        })
    },
    // And finally
    render: function () {
        var self = this;
        var listNatures;
        var listVictims;
        var listLocations;
        var listActors;
        var calendarInputClass = classNames({
            'hidden': !this.state.showCalendar
        })
        var calendarInstanceClass = classNames({
            'hidden': this.state.showCalendar
        })
        if (this.state.ready) {
            // nature
            listNatures = self.state.data.nature.map(function(inst, key){
                return (
                    <div key={'nature_vals_'+inst.id} className="w-col w-col-3">
                      <div className="w-row">
                        <div className="w-col w-col-1">
                          <p  style={{'fontSize': '14px', 'color': 'black', 'textAlign': 'left', 'marginTop': '10px', 'display': 'inline-block', 'float': 'left'}} className="listincidents__form_label db_entries">{key + 1}.</p>
                        </div>
                        <div className="w-col w-col-9">
                          <p  style={{'fontSize': '14px', 'color': 'black', 'textAlign': 'left', 'marginTop': '10px', 'display': 'inline-block', 'float': 'left'}} className="listincidents__form_label db_entries">{inst.computed}</p>
                        </div>
                        <div className="w-col w-col-2">
                          <p onClick={self.removeNature.bind(null, inst.id)} style={{'marginTop': '10px', 'display': 'inline-block', 'color': 'blue', 'cursor': 'pointer' }} className="listincidents__form_label db_entries">x</p> 
                        </div>
                      </div> 
                    </div>
                )
            })
            //victims
            listVictims = self.state.data.victims.map(function(inst, key){
                return (
                    <div key={'victims_vals_'+inst.id} className="w-col w-col-3">
                      <div className="w-row">
                        <div className="w-col w-col-1">
                          <p  style={{'fontSize': '14px', 'color': 'black', 'textAlign': 'left', 'marginTop': '10px', 'display': 'inline-block', 'float': 'left'}} className="listincidents__form_label db_entries">{key + 1}.</p>
                        </div>
                        <div className="w-col w-col-9">
                          <p  style={{'fontSize': '14px', 'color': 'black', 'textAlign': 'left', 'marginTop': '10px', 'display': 'inline-block', 'float': 'left'}} className="listincidents__form_label db_entries">{inst.computed}</p>
                        </div>
                        <div className="w-col w-col-2">
                          <p onClick={self.removeVictim.bind(null, inst.id)} style={{'marginTop': '10px', 'display': 'inline-block', 'color': 'blue', 'cursor': 'pointer' }} className="listincidents__form_label db_entries">x</p> 
                        </div>
                      </div> 
                    </div>
                )
            })
            //locatoins
            listLocations = self.state.data.locations.map(function(inst, key){
                return (
                    <div key={'victims_vals_'+inst.id} className="w-col w-col-6">
                      <div className="w-row">
                        <div className="w-col w-col-1" style={{'width': '4%'}}>
                          <p  style={{'fontSize': '14px', 'color': 'black', 'textAlign': 'left', 'marginTop': '10px', 'display': 'inline-block', 'float': 'left'}} className="listincidents__form_label db_entries">{key + 1}.</p>
                        </div>
                        <div className="w-col w-col-10" style={{'width': '90%'}}>
                          <p  style={{'fontSize': '14px', 'color': 'black', 'textAlign': 'left', 'marginTop': '10px', 'display': 'inline-block', 'float': 'left'}} className="listincidents__form_label db_entries">{inst.computed}</p>
                        </div>
                        <div className="w-col w-col-1" style={{'width': '5%'}}>
                          <p onClick={self.removeLocation.bind(null, inst.id)} style={{'marginTop': '10px', 'display': 'inline-block', 'color': 'blue', 'cursor': 'pointer' }} className="listincidents__form_label db_entries">x</p> 
                        </div>
                      </div> 
                    </div>
                )
            }) 
            //actors 
            listActors = self.state.data.actors.map(function(inst, key){
                return (
                    <div key={'victims_vals_'+inst.value} className="w-col w-col-3">
                      <div className="w-row">
                        <div className="w-col w-col-1">
                          <p  style={{'fontSize': '14px', 'color': 'black', 'textAlign': 'left', 'marginTop': '10px', 'display': 'inline-block', 'float': 'left'}} className="listincidents__form_label db_entries">{key + 1}.</p>
                        </div>
                        <div className="w-col w-col-9">
                          <p  style={{'fontSize': '14px', 'color': 'black', 'textAlign': 'left', 'marginTop': '10px', 'display': 'inline-block', 'float': 'left'}} className="listincidents__form_label db_entries">{inst.label}</p>
                        </div>
                        <div className="w-col w-col-2">
                          <p onClick={self.removeActor.bind(null, inst.value)} style={{'marginTop': '10px', 'display': 'inline-block', 'color': 'blue', 'cursor': 'pointer' }} className="listincidents__form_label db_entries">x</p> 
                        </div>
                      </div> 
                    </div>
                )
            }) 
        }
        var p_nature_main = <span>Select nature main category</span>;
        var p_nature_sub = <span>Select nature sub category</span>;
        var p_victims = <span>Select victims category</span>;
        var p_region = <span>Select region</span>; 
        var p_locality = <span>Select locality</span>;
        var p_village = <span>Select town/village</span>;
        var p_actor = <span>Select actors</span>;

        var natureButton = 'Cancel'
        var victimsButton = 'Cancel'
        var locationButton = 'Cancel'
        var actorsButton = 'Cancel'

        if (this.state.natureMain != '') {
            natureButton = 'Save'
        }
        if (this.state.victimGroup != '') {
            victimsButton = 'Save'
        }
        if (this.state.region != '') {
            locationButton = 'Save'
        }
        if (this.state.actor != '') {
            actorsButton = 'Save'
        }
        
        return this.state.ready ? (
            <div>
              <div className="w-row" style={{'borderBottom':'grey dotted 1px'}}>
                    <div className="w-col w-col-1" style={{'width': '6%'}}>
                        <h3 style={{'textAlign': 'left'}} className="teblehead">№&nbsp;{this.props.number+1}</h3>
                    </div>
                    <div className="w-col w-col-2">
                        <h4 onClick={this.props.remove.bind(null, this.props.valueId)} style={{'marginTop': '7px'}} className="newincidentform__hint addelement verycloseup">Delete</h4>
                    </div>
                </div>
              <div className="w-row" style={{'borderBottom':'grey dotted 1px'}}>
                <div className="w-col w-col-1" style={{'width': '10%'}}>
                  <h3 style={{'textAlign': 'left'}} className="teblehead">Date</h3>
                </div>
                <div className="w-col w-col-11" style={{'width': '90%'}}>
                  <div className="w-row" style={{'borderLeft':'grey dotted 1px', 'paddingLeft': '10px'}}>
                    
                        <div className={calendarInstanceClass}>
                          <div className="w-col w-col-2">
                            <p  style={{'fontSize': '14px', 'color': 'black', 'textAlign': 'left', 'marginTop': '10px', 'display': 'inline-block', 'float': 'left'}} className="listincidents__form_label db_entries">{this.state.data.date} {this.state.data.time}</p>
                          </div>
                          <div className="w-col w-col-3">
                            <h4 onClick={this.toggleCalndar} style={{'marginTop': '7px'}} className="newincidentform__hint addelement verycloseup">Change</h4>
                          </div>
                        </div>
                        
                        <div className={calendarInputClass}>
                            <div className="w-col w-col-6">
                              <label style={{'fontSize': '13px'}} className="newincidentform__label">Date</label>
                              <DatePicker
                                  dateFormat='DD-MM-YYYY'
                                  selected={self.state.displayDate}
                                  onChange={self.handleDateChange}
                                  peekNextMonth
                                  showMonthDropdown
                                  showYearDropdown
                                  dropdownMode="select"
                                  calendarClassName="calClass"
                                  placeholderText="Choose date"
                                  className="w-input listincidents__form_input"
                              />
                            </div>
                            <div className="w-col w-col-2">
                              <label style={{'fontSize': '13px'}} className="newincidentform__label">Time</label>
                              <input onChange={this.hendleTimeChange}  className="w-input newincidentform__input" value={this.state.time}  placeholder="Choose time" type="time"/>
                            </div>
                            <div className="w-col w-col-2">
                                <label style={{'fontSize': '13px'}} className="newincidentform__label">&nbsp;&nbsp;</label>
                                <a style={{'marginTop': '0px', 'float': 'right', 'height': '36px'}} onClick={this.updateDateTime} className="w-button listincidents__form_submit db">Save</a>
                            </div>
                            <div className="w-col w-col-2">
                                <label style={{'fontSize': '13px'}} className="newincidentform__label">&nbsp;&nbsp;</label>
                                <a style={{'marginTop': '0px', 'float': 'right', 'height': '36px'}} onClick={this.toggleCalndar} className="w-button listincidents__form_submit db">Cancel</a>
                            </div>
                        </div>
                        
                    
                  </div>
                </div>
              </div>
              <div className="w-row" style={{'borderBottom':'grey dotted 1px'}}>  
                <div className="w-col w-col-1" style={{'width': '10%'}}>
                  <h3 style={{'textAlign': 'left'}} className="teblehead">Nature</h3>
                </div>
                <div className="w-col w-col-11" style={{'width': '90%'}}>
                  <div className="w-row" style={{'borderLeft':'grey dotted 1px', 'paddingLeft': '10px'}}>
                    {listNatures}
                    {!this.state.showNature ? (
                        <div className="w-col w-col-3">
                          <h4 onClick={this.toggleNature} style={{'marginTop': '7px'}} className="newincidentform__hint addelement verycloseup">Add</h4>
                        </div>  
                    ) : (
                        <div className="w-col w-col-12" style={{'paddingBottom': '10px'}}>
                        <div className="w-row">
                          <div className="w-col w-col-5" style={{'paddingRight': '10px'}}>
                            <label style={{'fontSize': '13px'}} className="newincidentform__label">Main category</label>
                            <Select
                              onChange={this.mainNatureChange}
                              optionComponent={ChartType}
                              options={this.state.natureMains}
                              placeholder={p_nature_main}
                              value={this.state.natureMain}
                              valueComponent={ChartValue}
                              clearable={false}
                            />
                          </div>
                          <div className="w-col w-col-5">
                            <label style={{'fontSize': '13px'}} className="newincidentform__label">Sub category</label>
                            <Select
                              onChange={this.subNatureChange}
                              optionComponent={ChartType}
                              options={this.state.natureSubs}
                              placeholder={p_nature_sub}
                              value={this.state.natureSub}
                              valueComponent={ChartValue}
                              clearable={false}
                            />
                          </div>
                          <div className="w-col w-col-2">
                            <label style={{'fontSize': '13px'}} className="newincidentform__label">&nbsp;&nbsp;</label>
                            <a style={{'marginTop': '0px', 'float': 'right', 'height': '36px'}} onClick={this.addNature} className="w-button listincidents__form_submit db">{natureButton}</a>
                          </div>
                        </div>
                        </div>
                    )}
                    
                  </div>
                </div>
              </div>
              <div className="w-row" style={{'borderBottom':'grey dotted 1px'}}>  
                <div className="w-col w-col-1" style={{'width': '10%'}}>
                  <h3 style={{'textAlign': 'left'}} className="teblehead">Victims</h3>
                </div>
                <div className="w-col w-col-11" style={{'width': '90%'}}>
                 <div className="w-row" style={{'borderLeft':'grey dotted 1px', 'paddingLeft': '10px'}}>
                  {listVictims}
                  {!this.state.showVictims ? (
                        <div className="w-col w-col-3">
                          <h4 onClick={this.toggleVictims} style={{'marginTop': '7px'}} className="newincidentform__hint addelement verycloseup">Add</h4>
                        </div>
                    ) : (
                        <div className="w-col w-col-12">
                          <div className="w-row">
                            <div className="w-col w-col-5" style={{'paddingRight': '10px'}}>
                                <label style={{'fontSize': '13px'}} className="newincidentform__label">Victims category</label>
                                <Select
                                  onChange={this.victimCategoryChange}
                                  optionComponent={ChartType}
                                  options={this.state.victimsData}
                                  placeholder={p_victims}
                                  value={this.state.victimGroup}
                                  valueComponent={ChartValue}
                                  clearable={false}
                                />
                              </div>
                              <div className="w-col w-col-3">
                                <label style={{'fontSize': '13px'}} className="newincidentform__label">Victims quantity</label>
                                <input style={{'height': '36px'}} onChange={this.victimNumberChange} value={this.state.victims} className="w-input newincidentform__input" min="0" placeholder="Victims quantity" type="number"/>
                              </div>
                              <div className="w-col w-col-2">
                                <label style={{'fontSize': '13px'}} className="newincidentform__label">&nbsp;&nbsp;</label>
                                <a style={{'marginTop': '0px', 'float': 'right', 'height': '36px'}} onClick={this.addVictim} className="w-button listincidents__form_submit db">{victimsButton}</a>
                              </div>
                          </div>
                        </div>
                    )}
                  </div>
                </div>
              </div>
              <div className="w-row" style={{'borderBottom':'grey dotted 1px'}}>  
                <div className="w-col w-col-1" style={{'width': '10%'}}>
                  <h3 style={{'textAlign': 'left'}} className="teblehead">Location</h3>
                </div>
                <div className="w-col w-col-11" style={{'width': '90%'}}>
                  <div className="w-row" style={{'borderLeft':'grey dotted 1px', 'paddingLeft': '10px'}}>
                  {listLocations}
                  {!this.state.showLocations ? (
                        <div className="w-col w-col-6">
                          <h4 onClick={this.toggelLocation} style={{'marginTop': '7px'}} className="newincidentform__hint addelement verycloseup">Add</h4>
                        </div>
                    ) : (
                        <div className="w-col w-col-12">
                            <div className="w-row">
                              <div className="w-col w-col-2" style={{'paddingRight': '10px'}}>
                                  <label style={{'fontSize': '13px'}} className="newincidentform__label">Region</label>
                                  <Select
                                      onChange={this.regionChange}
                                      optionComponent={ChartType}
                                      options={this.state.regions}
                                      placeholder={p_region}
                                      value={this.state.region}
                                      valueComponent={ChartValue}
                                      clearable={false}
                                    /> 
                              </div>
                              <div className="w-col w-col-3" style={{'paddingRight': '10px'}}>
                                  <label style={{'fontSize': '13px'}} className="newincidentform__label">Locality</label>
                                  <Select
                                      onChange={this.localityChange}
                                      optionComponent={ChartType}
                                      options={this.state.localitys}
                                      placeholder={p_locality}
                                      value={this.state.locality}
                                      valueComponent={ChartValue}
                                      clearable={false}
                                    /> 
                              </div>
                              <div className="w-col w-col-3" style={{'paddingRight': '10px'}}>
                                  <label style={{'fontSize': '13px'}} className="newincidentform__label">Village / Town</label>
                                  <Select
                                      onChange={this.villageChange}
                                      optionComponent={ChartType}
                                      options={this.state.villages}
                                      placeholder={p_village}
                                      value={this.state.village}
                                      valueComponent={ChartValue}
                                      clearable={false}
                                    />
                              </div>
                              <div className="w-col w-col-1" style={{'paddingRight': '10px'}}>
                                <label style={{'fontSize': '13px'}} className="newincidentform__label">E</label>
                                <input style={{'height': '36px'}} onChange={this.eChange} value={this.state.e} className="w-input newincidentform__input" placeholder="e" type="text"/>
                              </div>
                              <div className="w-col w-col-1" style={{'paddingRight': '10px'}}>
                                <label style={{'fontSize': '13px'}} className="newincidentform__label">N</label>
                                <input style={{'height': '36px'}} onChange={this.nChange} value={this.state.n} className="w-input newincidentform__input" placeholder="n" type="text"/>
                              </div>
                              <div className="w-col w-col-2">
                                <label style={{'fontSize': '13px'}} className="newincidentform__label">&nbsp;&nbsp;</label>
                                <a style={{'marginTop': '0px', 'float': 'right', 'height': '36px'}} onClick={this.addLocation} className="w-button listincidents__form_submit db">{locationButton}</a>
                              </div>
                            </div>
                        </div>    
                    )}
                  </div>
                </div>
              </div> 
              <div className="w-row headerrow"> 
                <div className="w-col w-col-1" style={{'width': '10%'}}>
                  <h3 style={{'textAlign': 'left'}} className="teblehead">Actors</h3>
                </div>
                <div className="w-col w-col-11" style={{'width': '90%'}}>
                  <div className="w-row" style={{'borderLeft':'grey dotted 1px', 'paddingLeft': '10px'}}>
                  {listActors}
                  {!this.state.showActors ? (
                        <div className="w-col w-col-3">
                          <h4 onClick={this.toggleActors} style={{'marginTop': '7px'}} className="newincidentform__hint addelement verycloseup">Add</h4>
                        </div>
                    ) : (
                        <div className="w-col w-col-12" style={{'paddingBottom': '10px'}}>
                          <div className="w-row">
                            <div className="w-col w-col-5" style={{'paddingRight': '10px'}}>
                                <label style={{'fontSize': '13px'}} className="newincidentform__label">Actors category</label>
                                <Select
                                  onChange={this.changeActor}
                                  optionComponent={ChartType}
                                  options={this.state.actors}
                                  placeholder={p_actor}
                                  value={this.state.actor}
                                  valueComponent={ChartValue}
                                  clearable={false}
                                />
                              </div>
                              <div className="w-col w-col-2">
                                <label style={{'fontSize': '13px'}} className="newincidentform__label">&nbsp;&nbsp;</label>
                                <a style={{'marginTop': '0px', 'float': 'right', 'height': '36px'}} onClick={this.addActor} className="w-button listincidents__form_submit db">{actorsButton}</a>
                              </div>
                          </div>
                        </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
        ) : (null)
    }
})

module.exports = EntryUpdateConteiner;