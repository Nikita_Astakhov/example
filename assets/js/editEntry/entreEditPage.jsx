var React = require('react');
var ReactDom = require('react-dom');
var PropTypes = React.PropTypes;
var classNames = require('classnames');

var Utils = require('../Utils');
var csrftoken = Utils.getCookie('csrftoken');

var EditEntryData = require('./components/editEntryData');


var EditEConteiner = React.createClass({
	render: function () {
		return (
			<div>
                <EditEntryData />
            </div>
		)
	}
})

module.exports = EditEConteiner;
