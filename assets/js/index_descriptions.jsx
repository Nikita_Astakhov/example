var React = require('react');
var ReactDOM = require('react-dom');
var Desc = require('./descriptions/descriptionPage');

var AppInit = require('./AppInit');
AppInit();

ReactDOM.render(<Desc />, document.getElementById('descriptionsWorkspace'));