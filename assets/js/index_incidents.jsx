var React = require('react');
var ReactDOM = require('react-dom');
var Incidents = require('./incs/incPage');

var AppInit = require('./AppInit');
AppInit();

ReactDOM.render(<Incidents />, document.getElementById('manage_incidents'));