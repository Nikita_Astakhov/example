var React = require('react');
var ReactDOM = require('react-dom');
var EntryE = require('./editEntry/entreEditPage');

var AppInit = require('./AppInit');
AppInit();

ReactDOM.render(<EntryE />, document.getElementById('editEntryWorkspace'));