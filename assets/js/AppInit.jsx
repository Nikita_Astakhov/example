var Utils = require('./Utils');

var AppInit = function(){
    var csrftoken = Utils.getCookie('csrftoken');

    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
        if (!Utils.csrfSafeMethod(settings.type) && Utils.sameOrigin(settings.url)) {
            // Send the token to same-origin, relative URLs only.
            // Send the token only if the method warrants CSRF protection
            // Using the CSRFToken value acquired earlier
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
    });
};

module.exports = AppInit;