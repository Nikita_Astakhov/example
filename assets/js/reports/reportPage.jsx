var React = require('react')
var ReactDom = require('react-dom')
var PropTypes = React.PropTypes;
var classNames = require('classnames');
var TimerMixin = require('react-timer-mixin');
var dateFormat = require('dateformat');
var SegementsConteiner = require('./components/segment')
var ChartBlockConteiner = require('./components/chart')
var EntriesConteiner = require('./components/entries')

var Utils = require('../Utils');
var csrftoken = Utils.getCookie('csrftoken');


var needNum = 0;

var ChartConteiner = React.createClass({
    getInitialState: function () {
        return {
            selected: [],
            activeSection: 1,
            chartActive: true,
            start: '',
            end: '',
        }
    },
    changeSegment: function (value) {   
        this.setState({
            selected: value,
        })
    },
    setChart: function () {
        this.setState({
            activeSection: 1,
            chartActive: true
        })
    },
    setEntries: function () {
        this.setState({
            activeSection: 2,
            chartActive: false
        })
    },
    changeDates: function (start, end) {
        this.setState({
            start: start,
            end: end
        })
    },
    writeTemp: function (where) {
      var self = this;
      var source = 'http://'+AppGlobal.host+'/api/segment/createTemp/';
      var post_d = {'segment':this.state.selected};
      $.ajax({
          headers: { 
            'Accept': 'application/json',
            'Content-Type': 'application/json' 
          },
          type: "POST",
          url: source,
          data: JSON.stringify(post_d, null, '\t'),
          dataType: 'json',
          statusCode: {
            200: function (data) { if (where === 1) {self.executeExport();} else {self.exportSegmentExcel();} },
            500: function () {},
          }
      });
    },
    handleExcelExport: function () {
      this.writeTemp(1);
    },
    handleAllExcelExport: function (value) {
      this.writeTemp(2);
      needNum = value;
    },
    executeExport: function () {
      var el = this.refs.executeExport;
      el.click();
    },
    exportSegmentExcel: function () {
      var el = this.refs.executeAllExport;
      el.click();
    },
    render: function () {
        if (this.state.activeSection === 1) {
            var sec = (<ChartBlockConteiner selected={this.state.selected} key='blokOfChart' start={this.state.start} end={this.state.end} exportSegments={this.handleExcelExport} />)
        } else {
            var sec = (<EntriesConteiner key='blockOfEntries' selected={this.state.selected} start={this.state.start} end={this.state.end} />);
        }
        var chartClass = classNames({
            'additional_header__link': true,
            'active_link': this.state.chartActive
        })
        var entryClass = classNames({
            'additional_header__link': true,
            'active_link': !this.state.chartActive
        })
        var nec = this.state.selected.length;
        return (
            <div>
                <div>
                  <form action="" method="POST">
                    <input type="hidden" value={csrftoken} name="csrfmiddlewaretoken"></input>
                    <input type="hidden" value={nec} name="quantity"></input>
                    <input type="hidden" value={this.state.start} name="start"></input>
                    <input type="hidden" value={this.state.end} name="end"></input>
                    <input ref="executeExport" type="submit" style={{'display': 'none'}} name="export_excel" value="Export"/>
                  </form>
                </div>
                <div>
                  <form action="" method="POST">
                    <input type="hidden" value={csrftoken} name="csrfmiddlewaretoken"></input>
                    <input type="hidden" value={needNum} name="number"></input>
                    <input type="hidden" value={this.state.start} name="start"></input>
                    <input type="hidden" value={this.state.end} name="end"></input>
                    <input ref="executeAllExport" type="submit" style={{'display': 'none'}} name="export_excel_all" value="Export"/>
                  </form>
                </div>
                <div className="additional_header">
                  <div className="additional_header__conteiner w-container">
                    <a onClick={this.setChart} className={chartClass}>Create chart</a>
                    <a onClick={this.setEntries} className={entryClass}>Filter entries</a>
                  </div>
                </div>
                <div className="descriptionsblock">
                  <SegementsConteiner select={this.changeSegment} changeDates={this.changeDates} exportAll={this.handleAllExcelExport} />
                  {sec}
                </div>
            </div>
        )
    }
})

module.exports = ChartConteiner;