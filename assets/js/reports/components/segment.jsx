var React = require('react')
var ReactDom = require('react-dom')
var PropTypes = React.PropTypes;
var classNames = require('classnames');
var TimerMixin = require('react-timer-mixin');
var dateFormat = require('dateformat');
import DatePicker from 'react-datepicker';
import moment from 'moment';

import 'react-datepicker/dist/react-datepicker.css'; 

var Select = require('react-select');
require('react-select/dist/react-select.css');

var Utils = require('../../Utils');
var csrftoken = Utils.getCookie('csrftoken');


var segNum = 1;
var SegementsConteiner = React.createClass({
    propTypes: {  
        select: PropTypes.func,
        changeDates: PropTypes.func,
        exportAll: PropTypes.func,
    },
    getInitialState: function () {
        return {
            create: false,
            listSegments: false,
            selectedSegments: [1],
            segmentsData: [],
            start: '',
            end: '',
            excel: false
        }
    },
    addBlock: function () {
      var arr = this.state.selectedSegments;
      segNum += 1
      arr.push(segNum);
      this.setState({
        selectedSegments:arr
      })
    },
    removeBlock: function(number) {
      var arr = this.state.selectedSegments;
      var vals = this.state.segmentsData;
      var index = arr.indexOf(number);
      if (index > -1) {
          arr.splice(index, 1);
      }
      var i=0;
      for (i = 0; i < vals.length; i++) {
          if (vals[i][0] === number) {
              vals.splice(i, 1)
              break;
          }
      }
      //remove values from variants array
      this.setState({
        selectedSegments:arr,
        segmentsData: vals
      })
      this.props.select(vals);
    },
    updateSegmentsData: function (value) {
      var vals = this.state.segmentsData;
      var updated = false;
      var i = 0;
      for (i = 0; i < vals.length; i++) {
          if (vals[i][0] === value[0]) {
              vals[i][2] = value[2]
              updated = true;
              break;
          }
      }
      if (!updated) {
        vals.push(value)
      }
      this.setState({
        segmentsData: vals
      })
      this.props.select(vals);
    },
    //////////////////////
    // Calendar methods //
    //////////////////////
    updateDates: function (start, end, total) {
        this.setState({
            start: start,
            end: end,
            excel: true
        })
        this.props.changeDates(start, end);
    },
    handleSubmit: function (event) {
        event.preventDefault();
    },
    render: function () {
        var self = this;
        if (this.state.showCal) {
            var butText = 'Apply and close';
        } else {
            var butText = 'Select period of time';
        }
        var listSelected = this.state.selectedSegments.map(function(instance, key) {
            return (<ListSegmentsConteiner key={'criterias_num_'+instance} update={self.updateSegmentsData} removeBlock={self.removeBlock} num={key+1} allExport={self.props.exportAll} number={instance} selected={self.state.selectedSegments} select={self.appendSegment} />)
        });
        return (
            <div className="segments__conteiner">
              <div className="segements__choosed__conteiner">
                <div className="w-row">
                  <div className="w-col w-col-12">
                    <div className="list__chosed_segments">
                      <h2 className="blockheading">Selected criterias:</h2>
                      {listSelected}
                      <h3 style={{'textAlign':'left', }} onClick={self.addBlock} className="entrie link teblehead">add search criterias</h3>
                    </div>
                  </div>
                  <div className="w-col w-col-4">
                  </div>  
                </div>
              </div>
              <div ref="segmentsOptionsConteiner"></div>
              <div className="w-row">
                <div className="w-col w-col-7">
                <div className="xaxis__conteiner">
                  <h4 className="listincidents__filterhead">Select period of time:</h4>
                  <div className="filterincidentdate">
                    <CalendarConteiner updateDates={this.updateDates} />
                  </div>
                </div>
              </div>
              </div>
            </div>
        )
    }
})

var CalendarConteiner = React.createClass({
    mixins: [TimerMixin],
    propTypes: {  
        updateDates: PropTypes.func,
    },
    getInitialState: function () {
        return {
            start: '',
            displayStart: '',
            end: '',
            displayEnd: '',
            error: ''
        }
    },
    handleStartChange(date) {
    const format = 'YYYY-MM-DD';
    var start = date.format(format).toString();
      this.setState({
        start: start,
        displayStart: date,
        error: ''
      });
    },
    handleEndChange(date) {
    const format = 'YYYY-MM-DD';
    var start = date.format(format).toString();
      this.setState({
        end: start,
        displayEnd: date,
        error: ''
      });
    },
    sendDatesToApi: function () {
      var self = this;
      if (this.state.start === '') {
        this.setState({
          error: 'Please select start date!'
        })
      } else if (this.state.end === '') {
        this.setState({
          error: 'Please select end date!'
        })
      } else {
        this.setState({error: ''})
        this.props.updateDates(this.state.start, this.state.end);
      }
      
    },
    render: function () {
        var err = this.state.error;
        return (
            <div className="weekchooseblock">
              <div className="w-row">
                <div className="w-col w-col-5">
                  <DatePicker
                      dateFormat='DD-MM-YYYY'
                      selected={this.state.displayStart}
                      onChange={this.handleStartChange}
                      peekNextMonth
                      showMonthDropdown
                      showYearDropdown
                      dropdownMode="select"
                      calendarClassName="calClass"
                      placeholderText="Start date"
                      className="w-input listincidents__form_input"
                  />
                </div>
                <div className="w-col w-col-5">
                  <DatePicker
                      dateFormat='DD-MM-YYYY'
                      selected={this.state.displayEnd}
                      onChange={this.handleEndChange}
                      peekNextMonth
                      showMonthDropdown
                      showYearDropdown
                      dropdownMode="select"
                      calendarClassName="calClass"
                      placeholderText="End date"
                      className="w-input listincidents__form_input"
                  />
                </div>
                <div className="w-col w-col-2 w-clearfix">
                  <a onClick={this.sendDatesToApi} className="w-button listincidents__form_submit filter">Apply</a>
                </div>
                <div className="w-col w-col-12 w-clearfix">
                  <h4 className="listincidents__filterhead">{err}</h4>
                </div>
              </div>
            </div>
        )
    }
})

var ListSegmentsConteiner = React.createClass({
    mixins: [TimerMixin],
    propTypes: {  
        select: PropTypes.func,
        selected: PropTypes.array,
        allExport: PropTypes.func, 
        number: PropTypes.number,
        num: PropTypes.number,
        removeBlock: PropTypes.func,
        update: PropTypes.func 
    },
    getInitialState: function () {
        return {
            data: [],
            name: '',
            presets: [],
            presetData: {},
            showContent: false,
            newFilter: false,
            selectedOption: '',
            newCriteriaName: '',
            allData: [],
            successSave: false,
            showName: false,
            downloaded: false,
            showNameError: false
        }
    },
    componentWillMount: function () {
        var self = this;
        var source = 'http://'+AppGlobal.host + "/api/segment/startEntries/?format=json";
        this.serverRequest = $.get(source, function (results) {
            var segments = results.results;
            this.setState({
              presets: segments,
              allData: [self.props.number, -1, [], '']
            });
        }.bind(this));
    },
    showClearFilter: function () {
      this.setState({
        showContent: true,
        newFilter: true
      })
    },
    toggleContent: function () {
      this.setState({
        showContent: !this.state.showContent
      })
    },
    setNewOption: function (value) {
      var self = this;
      var arr = this.state.allData;
      arr[1] = parseInt(value.value);
      arr[3] = value.label;
      this.setState({
        allData: arr,
        showName: true,
        newCriteriaName: value.label,
        newFilter: true,
      })
      this.downloadPresetData(value.value);
      this.props.update(arr);
    },
    downloadPresetData: function (value) {
      var source = 'http://'+AppGlobal.host + "/api/segment/downloadPreset/"+ value +"/?format=json";
        this.serverRequest = $.get(source, function (results) {
            var segments = results.results[0];
            this.setState({
              presetData: segments,
              showContent: true,
              downloaded: true 
            });
        }.bind(this));
    },
    updateCriteriaName: function (event) {
      var arr = this.state.allData;
      arr[3] = event.target.value;
      this.setState({newCriteriaName: event.target.value, allData: arr,})
      this.props.update(arr);
    },
    saveNewCriteria: function () {
      var self = this;
      if (this.state.newCriteriaName.length > 0 && this.state.allData[2].length > 0) {
        var source = 'http://'+AppGlobal.host+'/api/segment/createNew/';
        var post_d = {'name':this.state.newCriteriaName, 'segment':this.state.allData};
        $.ajax({
            headers: { 
              'Accept': 'application/json',
              'Content-Type': 'application/json' 
            },
            type: "POST",
            url: source,
            data: JSON.stringify(post_d, null, '\t'),
            dataType: 'json',
            statusCode: {
              200: function (data) {self.assigneId(data)},
              500: function () {},
            }
        });
      } else {
        this.showNameError()
      }
    },
    showNameError: function () {
      this.setState({
        showNameError: true
      })
      this.setTimeout(function(){
        this.setState({
          showNameError: false
        })
      }, 10000)
    },
    assigneId: function (data) {
      var arr = this.state.allData;
      arr[1] = parseInt(data.newID);
      this.setState({
        allData: arr,
        showName: true,
      })
      this.props.update(arr);
    },
    sendUpdate: function (value) {
      var arr = this.state.allData;
      arr[2] = value;
      this.setState({
        allData: arr
      })
      this.props.update(arr);
    },
    render: function () {
        var self = this; 
        var contentClass = classNames({
            'hidden': !this.state.showContent,
        });
        var p_state = <span>Select saved criteria</span>;
        return (
            <div className="segment__conteiner">
                <div  style={{ 'padding': '7px 7px 0px 0px'}} className="segment_header_row w-row">
                  <div className="w-col w-col-5">
                    <div className="w-row">
                      <div className="w-col w-col-1">
                        <h4 style={{'lineHeight': '20px', 'textAlign': 'center'}} className="listincidents__filterhead">{this.props.number}</h4>
                      </div>
                      {!this.state.showName ?
                      (<div><div style={{'paddingRight': '15px'}} className="w-col w-col-5">
                         {!this.state.newFilter ? (<Select
                                                    onChange={this.setNewOption}
                                                    optionComponent={ChartType}
                                                    options={this.state.presets}
                                                    placeholder={p_state}
                                                    value={this.state.selectedOption}
                                                    valueComponent={ChartValue}
                                                    clearable={false}
                                                  />) : (<input style={{'width': '100%'}} onChange={this.updateCriteriaName} value={this.state.newCriteriaName} className="listincidents__form_input w-input" maxLength="256" placeholder="Input criteria name here" type="text"/>)}
                       </div>
                       <div className="w-col w-col-3">
                         {!this.state.newFilter ? (<a onClick={this.showClearFilter} style={{'marginTop': '0px'}} className="w-button listincidents__form_submit">Create new</a>) : (<a onClick={this.saveNewCriteria} style={{'marginTop': '0px'}} className="w-button listincidents__form_submit">Save</a>)}
                       {this.state.showNameError ? (<label style={{'fontWeight': 'normal'}} className="newincidentform__label">Please feel the name field and choose at least one parameter with value</label>) : null}</div></div>) 
                      :
                      (<h4 style={{'lineHeight': '20px', 'textAlign': 'left'}} className="listincidents__filterhead">{this.state.newCriteriaName}</h4>)}
                    </div>
                  </div>
                  <div className="w-col w-col-4">
                  </div>
                  <div className="fhe w-col w-col-3" style={{'textAlign': 'right'}}>
                    {this.state.newFilter ? (<div><h3 style={{'textAlign':'right', 'paddingRight': '10px', 'display': 'inline-block'}} onClick={self.toggleContent} className="entrie link teblehead">{this.state.showContent != false ? 'minimize' : 'expand'}</h3>
                    <h3 style={{'textAlign':'right', 'paddingRight': '10px', 'display': 'inline-block'}} onClick={self.props.allExport.bind(null, self.props.num)} className="entrie link teblehead">export data from criteria</h3>
                    <h3 style={{'textAlign':'right', 'paddingRight': '10px', 'display': 'inline-block'}} onClick={self.props.removeBlock.bind(null, self.props.number)} className="entrie link teblehead">remove</h3></div>) : null}
                  </div>
                </div>
                <div className={contentClass}>
                  <SegmentConstructorBody downloaded={self.state.downloaded} preset={self.state.presetData} update={self.sendUpdate} />
                </div>
              </div>
        )
    }
})

//I need centrolised and accumulated values of selected filters to send to a server
var FOPTS = [{value:1, label:'Actors categories'}, {value:2, label:'Victims categories'}, {value:3, label:'Nature categories'}, {value:4, label:'Location'}, {value:5, label:'Source categories'}, {value:6, label:'Information grading categories'}, {value:7, label:'Description keywords'}];

var SegmentConstructorBody = React.createClass({
  propTypes: {  
      update: PropTypes.func,
      downloaded: PropTypes.bool,
      preset: PropTypes.object 
  },
  getInitialState: function () {
    return {
      options: [{value:1, label:'Actors categories'}, {value:2, label:'Victims categories'}, {value:3, label:'Nature categories'}, {value:4, label:'Location'}, {value:5, label:'Source categories'}, {value:6, label:'Information grading categories'}, {value:7, label:'Description keywords'}],
      selectedOption: '',
      filters: [],
      filteredValues: [[],[],[],[],[],[],[]], //0-actors, 1-victims, 2-natures, 3-locations, 4-source, 5-infgrade, 6-keywords
      allready: false
    }
  },
  componentWillReceiveProps: function (nextProps) {
    if (!this.state.allready) {
    if (nextProps.downloaded) {
      var presetData = nextProps.preset;
      var optData = [];
      var filtersData = [];
      var filteredValuesData = [[],[],[],[],[],[],[]];
      // actors
      if (presetData.act_actors.length > 0) {
        filtersData.push(1)
        var itActors = presetData.act_actors.map(function(ins_1, key){
          var t_arr_1 = [key+1, ins_1.value]
          filteredValuesData[0].push(t_arr_1)
        });
      } else {
        optData.push({value:1, label:'Actors categories'})
      }
      // victims
      if (presetData.act_victim.length > 0) {
        filtersData.push(2)
        var itVics = presetData.act_victim.map(function(ins_2, key_2){
          var t_arr_2 = [key_2+1, ins_2.value]
          filteredValuesData[1].push(t_arr_2)
        });
      } else {
        optData.push({value:2, label:'Victims categories'})
      }
      // natures
      if (presetData.act_natures.length > 0) {
        filtersData.push(3)
        var itNat = presetData.act_natures.map(function(ins_3, key_3){
          var t_arr_3 = [key_3+1, ins_3.act_nature_mains[0].value, ins_3.act_nature_subs[0].value]
          filteredValuesData[2].push(t_arr_3)
        });
      } else {
        optData.push({value:3, label:'Nature categories'})
      }
      // locations
      if (presetData.act_location.length > 0) {
        filtersData.push(4)
        var itLoc = presetData.act_location.map(function(ins_4, key_4){
          var t_arr_4 = [key_4+1, ins_4.act_regions[0].value, ins_4.act_localitys[0].value, ins_4.act_villages[0].value]
          filteredValuesData[3].push(t_arr_4)
        });
      } else {
        optData.push({value:4, label:'Location'})
      }
      // source
      if (presetData.act_sources.length > 0) {
        filtersData.push(5)
        var itSou = presetData.act_sources.map(function(ins_5, key_5){
          var t_arr_5 = [key_5+1, ins_5.value]
          filteredValuesData[4].push(t_arr_5)
        });
      } else {
        optData.push({value:5, label:'Source categories'})
      }
      // infgrade
      if (presetData.act_gradings.length > 0) {
        filtersData.push(6)
        var itInf = presetData.act_gradings.map(function(ins_6, key_6){
          var t_arr_6 = [key_6+1, ins_6.value]
          filteredValuesData[5].push(t_arr_6)
        });
      } else {
        optData.push({value:6, label:'Information grading categories'})
      }
      // keyword
      if (presetData.act_keywords.length > 0) {
        filtersData.push(7)
        var itKey = presetData.act_keywords.map(function(ins_7, key_7){
          var t_arr_7 = [key_7+1, ins_7.label]
          filteredValuesData[6].push(t_arr_7)
        });
      } else {
        optData.push({value:7, label:'Description keywords'})
      }
      this.setState({
        options: optData,
        filters: filtersData,
        filteredValues: filteredValuesData,
        allready: true
      })
      nextProps.update(filteredValuesData);
    }
    }
  },
  setNewOption: function (value) {
    var filts = this.state.filters;
    var opt = this.state.options;
    filts.push(value.value)
    var i=0;
    for (i = 0; i < opt.length; i++) {
        if (opt[i].value === value.value) {
            opt.splice(i, 1)
            break;
        }
    }
    this.setState({
      filters: filts,
      options: opt
    }) 
    //remove from options array
    //add to filters array
  },
  removeOption: function (value) {
    var filts = this.state.filters;
    var opt = this.state.options;
    var initOpts = FOPTS;
    var vals = this.state.filteredValues;
    // remove from filters
    var ind = filts.indexOf(value);
    if (ind > -1) {
        filts.splice(ind, 1);
    }
    // add to options
    var i=0;
    for (i = 0; i < initOpts.length; i++) {
        if (initOpts[i].value == parseInt(value)) {
            var need = initOpts[i];
            opt.push(need)
        }
    }
    // clear filtered values
    vals[value-1] = []
    // set all
    this.setState({
      filters: filts,
      options: opt,
      filteredValues: vals
    })
    // upadate upper level
    this.props.update(vals);
  },
  updateValues: function (where, value) {
    var arr = this.state.filteredValues;
    arr[where] = value;
    this.setState({
      filteredValues: arr
    })
    // upadate upper level
    this.props.update(arr);
  },
  render: function () {
    var self = this;
    var p_state = <span>Select option</span>;
    var necessaryData = self.props.preset;
    var listFilters = this.state.filters.map(function(inst,key){
      if (key + 1 == self.state.filters.length) {
        var last = true;
      } else {
        var last = false;
      }
      if (inst === 1) {
        return (<OptionActorsBody last={last} downloaded={self.props.downloaded} preset={necessaryData.act_actors} number={inst} update={self.updateValues} remove={self.removeOption} key={'opt_inst_act_'+inst} />)
      } else if (inst === 2) {
        return (<OptionVictimsBody last={last} downloaded={self.props.downloaded} preset={necessaryData.act_victim} number={inst} update={self.updateValues} remove={self.removeOption} key={'opt_inst_vic_'+inst} />)
      } else if (inst === 3) {
        return (<OptionNaturesBody last={last} downloaded={self.props.downloaded} preset={necessaryData.act_natures} number={inst} update={self.updateValues} remove={self.removeOption} key={'opt_inst_nat_'+inst} />)
      } else if (inst === 4) {
        return (<OptionLocationBody last={last} downloaded={self.props.downloaded} preset={necessaryData.act_location} number={inst} update={self.updateValues} remove={self.removeOption} key={'opt_inst_loc_'+key} />)
      } else if (inst === 5) {
        return (<OptionSourceBody last={last} downloaded={self.props.downloaded} preset={necessaryData.act_sources} number={inst} update={self.updateValues} remove={self.removeOption} key={'opt_inst_sou_'+key} />)
      } else if (inst === 6) {
        return (<OptionInfBody last={last} downloaded={self.props.downloaded} preset={necessaryData.act_gradings} number={inst} update={self.updateValues} remove={self.removeOption} key={'opt_inst_inf_'+key} />)
      } else {
        return (<OptionKeywordBody last={last} downloaded={self.props.downloaded} preset={necessaryData.act_keywords} number={inst} update={self.updateValues} remove={self.removeOption} key={'opt_inst_key_'+key} />)
      }
      
    })
    return (
      <div className="w-row">
        <div className="w-col w-col-2">
          <div style={{'padding': '0px 10px 12px 10px'}}>
          <label className="newincidentform__label">Add filter option</label>
          <Select
            onChange={this.setNewOption}
            optionComponent={ChartType}
            options={this.state.options}
            placeholder={p_state}
            value={this.state.selectedOption}
            valueComponent={ChartValue}
            clearable={false}
          />
          </div>
        </div>  
        <div className="w-col w-col-10" style={{'borderLeft': '1px dashed gray'}}>
          {listFilters}
        </div>
      </div>
    )
  }
})

////////
// Actors constructor
////////

var actNum = 1;
var OptionActorsBody = React.createClass({
  propTypes: {
    update: PropTypes.func,
    number: PropTypes.number,
    remove: PropTypes.func,
    downloaded: PropTypes.bool,
    preset: PropTypes.array,
    last: PropTypes.bool,
  },
  getInitialState: function () {
    return {
      selected: [1],
      variants: [],
      values: []
    }
  },
  componentWillMount: function () {
    var source_actors = 'http://' + AppGlobal.host + "/api/manage_db/db_data/actors/" + "?format=json";
    this.serverRequest = $.get(source_actors, function (results) {
        var actors_data = results.results;
        this.setState({
          variants: actors_data,
        });
    }.bind(this));
    if (this.props.downloaded) {
      var presetData = this.props.preset;
      var filtersData = [];
      var filteredValuesData = [];
      // actors
      if (presetData.length > 0) {
        var itActors = presetData.map(function(ins_1, key){
          filtersData.push(key+1)
          actNum += 1
          var t_arr_1 = [key+1, ins_1.value]
          filteredValuesData.push(t_arr_1)
        });
      } 
      this.setState({
        selected: filtersData,
        values: filteredValuesData
      })
    }
  },
  addBlock: function () {
    var arr = this.state.selected;
    actNum += 1
    arr.push(actNum);
    this.setState({
      selected:arr
    })
  },
  removeBlock: function(number) {
    var arr = this.state.selected;
    var vals = this.state.values;
    var index = arr.indexOf(number);
    if (index > -1) {
        arr.splice(index, 1);
    }
    var i=0;
    for (i = 0; i < vals.length; i++) {
        // This if statement depends on the format of your array
        if (vals[i][0] === number) {
            vals.splice(i, 1)
            break;   // Found it
        }
    }
    this.props.update(0, vals);
    //remove values from variants array
    this.setState({
      selected:arr,
      values: vals
    })
  },
  updateValue: function (value) {
    var vals = this.state.values;
    var i=0;
    var found = false;
    for (i = 0; i < vals.length; i++) {
        // This if statement depends on the format of your array
        if (vals[i][0] == value[0]) {
            found = true;
            break;   // Found it
        }
    }
    if (found) {
      vals[i][1] = value[1];
    } else {
      vals.push(value)
    }
    this.props.update(0, vals);
    this.setState({values:vals})
    // what i'm need here 1. Number of conteiner and his value 2. send all values to some where to be stored 3. I need to know updatind values or adding new
  },
  rem: function () {
    this.props.remove(this.props.number);
  },
  render: function () {
    var self = this;
    var listSelected = this.state.selected.map(function(inst, key){
      if (self.state.values[key]) {
        var dd = self.state.values[key]
      } else {
        var dd = [];
      }
      return (<OptionActorsInstance downloaded={self.props.downloaded} initialData={dd} key={'option_instance_'+ key} updateValue={self.updateValue} number={inst} remove={self.removeBlock} data={self.state.variants} />)
    })
    if (this.props.last) {
      var divStyle = {'padding': '0px 10px 0px 10px'}
    } else {
      var divStyle = {'borderBottom': '1px dashed gray', 'padding': '0px 10px 0px 10px'}
    }
    return (
      <div className="w-row" style={divStyle}>
        <label className="newincidentform__label">Select actors categories&nbsp;<p onClick={self.rem} style={{'marginTop': '0px', 'marginBottom': '0px', 'lineHeight': '20px', 'display': 'inline-block', 'color': 'blue', 'cursor': 'pointer' }} className="listincidents__form_label db_entries">x</p></label>
        {listSelected}
        <div className="w-col w-col-1">
          <h3 onClick={this.addBlock} style={{'textAlign': 'left'}} className="entrie link teblehead">add</h3>
        </div>
      </div>
    )
  }
})

var OptionActorsInstance = React.createClass({
  propTypes: {
    data: PropTypes.array,
    number: PropTypes.number,
    remove: PropTypes.func,
    updateValue: PropTypes.func,
    initialData: PropTypes.array,
    downloaded: PropTypes.bool,
  },
  getInitialState: function () {
    return {
      selectedOption: ''
    }
  },
  componentWillMount: function () {
    if (this.props.downloaded) {
      if (this.props.initialData.length > 0) {
        this.setState({
          selectedOption: this.props.initialData[1]
        })
      }
    }
  },
  setNewOption: function (value) {
    var self = this;    
    this.setState({ 
      selectedOption: value, 
    });
    var send = [self.props.number, value.value];
    this.props.updateValue(send);
  },
  render: function () {
    var self = this;
    var p_state = <span>Select actor category</span>;
    return (
      <div className="w-col w-col-3" style={{'marginBottom': '12px'}}>
        <div className="w-row">
          <div className="w-col w-col-11">
            <Select
              onChange={this.setNewOption}
              optionComponent={ChartType}
              options={this.props.data}
              placeholder={p_state}
              value={this.state.selectedOption}
              valueComponent={ChartValue}
              clearable={false}
            />
          </div>
          <div className="w-col w-col-1">
            <p onClick={self.props.remove.bind(null, self.props.number)} style={{'marginTop': '6px', 'marginLeft': '4px', 'display': 'block', 'color': 'blue', 'cursor': 'pointer' }} className="listincidents__form_label db_entries">x</p>
          </div>
        </div>
      </div>
    )
  }
})

///////
// Victims Constructor
///////

var vicNum = 1;
var OptionVictimsBody = React.createClass({
  propTypes: {
    update: PropTypes.func,
    number: PropTypes.number,
    remove: PropTypes.func,
    downloaded: PropTypes.bool,
    preset: PropTypes.array,
    last: PropTypes.bool,
  },
  getInitialState: function () {
    return {
      selected: [1],
      variants: [],
      values: []
    }
  },
  componentWillMount: function () {
    var source_victims = 'http://' + AppGlobal.host + "/api/manage_db/victims/allChoices/" + "?format=json";
    this.serverRequest = $.get(source_victims, function (results) {
      var victims_data = results.results;
      this.setState({
          variants: victims_data,
      });
    }.bind(this));
    if (this.props.downloaded) {
      var presetData = this.props.preset;
      var filtersData = [];
      var filteredValuesData = [];
      // actors
      if (presetData.length > 0) {
        var itActors = presetData.map(function(ins_1, key){
          filtersData.push(key+1)
          vicNum += 1
          var t_arr_1 = [key+1, ins_1.value]
          filteredValuesData.push(t_arr_1)
        });
      } 
      this.setState({
        selected: filtersData,
        values: filteredValuesData
      })
    }
  },
  addBlock: function () {
    var arr = this.state.selected;
    vicNum += 1
    arr.push(vicNum);
    this.setState({
      selected:arr
    })
  },
  removeBlock: function(number) {
    var arr = this.state.selected;
    var vals = this.state.values;
    var index = arr.indexOf(number);
    if (index > -1) {
        arr.splice(index, 1);
    }
    var i=0;
    for (i = 0; i < vals.length; i++) {
        // This if statement depends on the format of your array
        if (vals[i][0] === number) {
            vals.splice(i, 1)
            break;   // Found it
        }
    }
    this.props.update(1, vals);
    //remove values from variants array
    this.setState({
      selected:arr,
      values: vals
    })
  },
  updateValue: function (value) {
    var vals = this.state.values;
    var i=0;
    var found = false;
    for (i = 0; i < vals.length; i++) {
        // This if statement depends on the format of your array
        if (vals[i][0] == value[0]) {
            found = true;
            break;   // Found it
        }
    }
    if (found) {
      vals[i][1] = value[1];
    } else {
      vals.push(value)
    }
    this.props.update(1, vals);
    this.setState({values:vals})
    // what i'm need here 1. Number of conteiner and his value 2. send all values to some where to be stored 3. I need to know updatind values or adding new
  },
  rem: function () {
    this.props.remove(this.props.number);
  },
  render: function () {
    var self = this;
    var listSelected = this.state.selected.map(function(inst, key){
      if (self.state.values[key]) {
        var dd = self.state.values[key]
      } else {
        var dd = [];
      }
      return (<OptionVictimsInstance downloaded={self.props.downloaded} initialData={dd} key={'option_vic_instance_'+ key} updateValue={self.updateValue} number={inst} remove={self.removeBlock} data={self.state.variants} />)
    })
    if (this.props.last) {
      var divStyle = {'padding': '0px 10px 0px 10px'}
    } else {
      var divStyle = {'borderBottom': '1px dashed gray', 'padding': '0px 10px 0px 10px'}
    }
    return (
      <div className="w-row" style={divStyle}>
        <label className="newincidentform__label">Select victims categories&nbsp;<p onClick={self.rem} style={{'marginTop': '0px', 'marginBottom': '0px', 'lineHeight': '20px', 'display': 'inline-block', 'color': 'blue', 'cursor': 'pointer' }} className="listincidents__form_label db_entries">x</p></label>
        {listSelected}
        <div className="w-col w-col-1">
          <h3 onClick={this.addBlock} style={{'textAlign': 'left'}} className="entrie link teblehead">add</h3>
        </div>
      </div>
    )
  }
})

var OptionVictimsInstance = React.createClass({
  propTypes: {
    data: PropTypes.array,
    number: PropTypes.number,
    remove: PropTypes.func,
    updateValue: PropTypes.func,
    initialData: PropTypes.array,
    downloaded: PropTypes.bool,
  },
  getInitialState: function () {
    return {
      selectedOption: ''
    }
  },
  componentWillMount: function () {
    if (this.props.downloaded) {
      if (this.props.initialData.length > 0) { 
      this.setState({
        selectedOption: this.props.initialData[1]
      })
      }
    }
  },
  setNewOption: function (value) {
    var self = this;    
    this.setState({ 
      selectedOption: value, 
    });
    var send = [self.props.number, value.value];
    this.props.updateValue(send);
  },
  render: function () {
    var self = this;
    var p_state = <span>Select victim category</span>;
    return (
      <div className="w-col w-col-3" style={{'marginBottom': '12px'}}>
        <div className="w-row">
          <div className="w-col w-col-11">
            <Select
              onChange={this.setNewOption}
              optionComponent={ChartType}
              options={this.props.data}
              placeholder={p_state}
              value={this.state.selectedOption}
              valueComponent={ChartValue}
              clearable={false}
            />
          </div>
          <div className="w-col w-col-1">
            <p onClick={self.props.remove.bind(null, self.props.number)} style={{'marginTop': '6px', 'marginLeft': '4px', 'display': 'block', 'color': 'blue', 'cursor': 'pointer' }} className="listincidents__form_label db_entries">x</p>
          </div>
        </div>
      </div>
    )
  }
})

///////
// Natures Constructor
///////

var natNum = 1;
var OptionNaturesBody = React.createClass({
  propTypes: {
    update: PropTypes.func,
    number: PropTypes.number,
    remove: PropTypes.func,
    downloaded: PropTypes.bool,
    preset: PropTypes.array,
    last: PropTypes.bool,
  },
  getInitialState: function () {
    return {
      selected: [1],
      variants: [],
      values: []
    }
  },
  componentWillMount: function () {
    var source_natureMain = 'http://' + AppGlobal.host + "/api/manage_db/db_data/nature_main/" + "?format=json";
    this.serverRequest = $.get(source_natureMain, function (results) {
        var natureMain_data = results.results;
        this.setState({
            variants: natureMain_data,
        });
    }.bind(this));
    if (this.props.downloaded) {
      var presetData = this.props.preset;
      var filtersData = [];
      var filteredValuesData = [];
      // actors
      if (presetData.length > 0) {
        var itNat = presetData.map(function(ins_3, key_3){
          filtersData.push(key_3+1)
          natNum += 1 
          var t_arr_3 = [key_3+1, ins_3.act_nature_mains[0].value, ins_3.act_nature_subs[0].value]
          filteredValuesData.push(t_arr_3)
        });
      } 
      this.setState({
        selected: filtersData,
        values: filteredValuesData
      })
    }
  },
  addBlock: function () {
    var arr = this.state.selected;
    natNum += 1
    arr.push(natNum);
    this.setState({
      selected:arr
    })
  },
  removeBlock: function(number) {
    var arr = this.state.selected;
    var vals = this.state.values;
    var index = arr.indexOf(number);
    if (index > -1) {
        arr.splice(index, 1);
    }
    var i=0;
    for (i = 0; i < vals.length; i++) {
        // This if statement depends on the format of your array
        if (vals[i][0] === number) {
            vals.splice(i, 1)
            break;   // Found it
        }
    }
    this.props.update(2, vals);
    //remove values from variants array
    this.setState({
      selected:arr,
      values: vals
    })
  },
  updateValue: function (value) {
    var vals = this.state.values;
    var i=0;
    var found = false;
    for (i = 0; i < vals.length; i++) {
        // This if statement depends on the format of your array
        if (vals[i][0] == value[0]) {
            found = true;
            break;   // Found it
        }
    }
    if (found) {
      vals[i][1] = value[1];
      vals[i][2] = value[2];
    } else {
      vals.push(value)
    }
    this.props.update(2, vals);
    this.setState({values:vals})
    // what i'm need here 1. Number of conteiner and his value 2. send all values to some where to be stored 3. I need to know updatind values or adding new
  },
  rem: function () {
    this.props.remove(this.props.number);
  },
  render: function () {
    var self = this;
    var listSelected = this.state.selected.map(function(inst, key){
      if (self.state.values[key]) {
        var dd = self.state.values[key]
      } else {
        var dd = [];
      }
      return (<OptionNatureInstance downloaded={self.props.downloaded} initialData={dd} key={'option_vic_instance_'+ key} updateValue={self.updateValue} number={inst} remove={self.removeBlock} data={self.state.variants} />)
    })
    if (this.props.last) {
      var divStyle = {'padding': '0px 10px 0px 10px'}
    } else {
      var divStyle = {'borderBottom': '1px dashed gray', 'padding': '0px 10px 0px 10px'}
    }
    return (
      <div className="w-row" style={divStyle}>
        <label className="newincidentform__label">Select incident nature categories&nbsp;<p onClick={self.rem} style={{'marginTop': '0px', 'marginBottom': '0px', 'lineHeight': '20px', 'display': 'inline-block', 'color': 'blue', 'cursor': 'pointer' }} className="listincidents__form_label db_entries">x</p></label>
        {listSelected}
        <div className="w-col w-col-1">
          <h3 onClick={this.addBlock} style={{'textAlign': 'left'}} className="entrie link teblehead">add</h3>
        </div>
      </div>
    )
  }
})

var OptionNatureInstance = React.createClass({
  propTypes: {
    data: PropTypes.array,
    number: PropTypes.number,
    remove: PropTypes.func,
    updateValue: PropTypes.func,
    initialData: PropTypes.array,
    downloaded: PropTypes.bool,
  },
  getInitialState: function () {
    return {
      selectedOption: '',
      subdata: [],
      sub: ''
    }
  },
  componentWillMount: function () {
    if (this.props.downloaded) {
      if (this.props.initialData.length > 0) {
      if (this.props.initialData[2] != -1) {
        var s = this.props.initialData[2]
        this.getSub(this.props.initialData[1]);
      } else {
        var s = ''
      }
      this.setState({
        selectedOption: this.props.initialData[1],
        sub: s
      })
      }
    }
  },
  getSub: function (main) {
      var self = this;
      var source_loc = 'http://' + AppGlobal.host + "/api/manage_db/db_data/nature_sub/" + parseInt(main) + "/?format=json";
      this.serverRequest = $.get(source_loc, function (results) {
          var data = results.results;
          this.setState({
              subdata: data
          });
      }.bind(this));
  },
  setNewOption: function (value) {
    var self = this;    
    this.setState({ 
      selectedOption: value, 
      sub: ''
    });
    this.getSub(value.value);
    var send = [self.props.number, value.value, -1];
    this.props.updateValue(send);
  },
  setSub: function (value) {
    var self = this; 
    var first = this.state.selectedOption;   
    this.setState({ 
      sub: value, 
    });
    var send = [self.props.number, first.value, value.value];
    this.props.updateValue(send);
  },
  render: function () {
    var self = this;
    var p_state = <span>Select nature main category</span>;
    var p_sub = <span>Select nature sub category</span>;
    return (
      <div className="w-col w-col-5" style={{'marginBottom': '12px'}}>
        <div className="w-row">
          <div className="w-col w-col-6" style={{'paddingRight': '10px'}}>
            <Select
              onChange={this.setNewOption}
              optionComponent={ChartType}
              options={this.props.data}
              placeholder={p_state}
              value={this.state.selectedOption}
              valueComponent={ChartValue}
              clearable={false}
            />
          </div>
          <div className="w-col w-col-5">
            <Select
              onChange={this.setSub}
              optionComponent={ChartType}
              options={this.state.subdata}
              placeholder={p_sub}
              value={this.state.sub}
              valueComponent={ChartValue}
              clearable={false}
            />
          </div>
          <div className="w-col w-col-1">
            <p onClick={self.props.remove.bind(null, self.props.number)} style={{'marginTop': '6px', 'marginLeft': '4px', 'display': 'block', 'color': 'blue', 'cursor': 'pointer' }} className="listincidents__form_label db_entries">x</p>
          </div>
        </div>
      </div>
    )
  }
})

//////
// Location Constructor
//////

var locNum = 1;
var OptionLocationBody = React.createClass({
  propTypes: {
    update: PropTypes.func,
    number: PropTypes.number,
    remove: PropTypes.func,
    downloaded: PropTypes.bool,
    preset: PropTypes.array,
    last: PropTypes.bool,
  },
  getInitialState: function () {
    return {
      selected: [1],
      variants: [],
      values: []
    }
  },
  componentWillMount: function () {
    var source_location = 'http://' + AppGlobal.host + "/api/manage_db/db_data/locations/" + "?format=json";
    this.serverRequest = $.get(source_location, function (results) {
        var location_data = results.results;
        this.setState({
            variants: location_data,
        });
    }.bind(this));
    if (this.props.downloaded) {
      var presetData = this.props.preset;
      var filtersData = [];
      var filteredValuesData = [];
      // location
      if (presetData.length > 0) {
        var itNat = presetData.map(function(ins_3, key_3){
          filtersData.push(key_3+1)
          locNum += 1
          var t_arr_3 = [key_3+1, ins_3.act_regions[0].value, ins_3.act_localitys[0].value, ins_3.act_villages[0].value]
          filteredValuesData.push(t_arr_3)
        });
      } 
      this.setState({
        selected: filtersData,
        values: filteredValuesData
      })
    }
  },
  addBlock: function () {
    var arr = this.state.selected;
    locNum += 1
    arr.push(locNum);
    this.setState({
      selected:arr
    })
  },
  removeBlock: function(number) {
    var arr = this.state.selected;
    var vals = this.state.values;
    var index = arr.indexOf(number);
    if (index > -1) {
        arr.splice(index, 1);
    }
    var i=0;
    for (i = 0; i < vals.length; i++) {
        // This if statement depends on the format of your array
        if (vals[i][0] === number) {
            vals.splice(i, 1)
            break;   // Found it
        }
    }
    this.props.update(3, vals);
    //remove values from variants array
    this.setState({
      selected:arr,
      values: vals
    })
  },
  updateValue: function (value) {
    var vals = this.state.values;
    var i=0;
    var found = false;
    for (i = 0; i < vals.length; i++) {
        // This if statement depends on the format of your array
        if (vals[i][0] == value[0]) {
            found = true;
            break;   // Found it
        }
    }
    if (found) {
      vals[i][1] = value[1];
      vals[i][2] = value[2];
      vals[i][3] = value[3];
    } else {
      vals.push(value)
    }
    this.props.update(3, vals);
    this.setState({values:vals})
    // what i'm need here 1. Number of conteiner and his value 2. send all values to some where to be stored 3. I need to know updatind values or adding new
  },
  rem: function () {
    this.props.remove(this.props.number);
  },
  render: function () {
    var self = this;
    var listSelected = this.state.selected.map(function(inst, key){
      if (self.state.values[key]) {
        var dd = self.state.values[key]
      } else {
        var dd = [];
      }
      return (<OptionLocationInstance downloaded={self.props.downloaded} initialData={dd} key={'option_loc_instance_'+ key} updateValue={self.updateValue} number={inst} remove={self.removeBlock} data={self.state.variants} />)
    })
    if (this.props.last) {
      var divStyle = {'padding': '0px 10px 0px 10px'}
    } else {
      var divStyle = {'borderBottom': '1px dashed gray', 'padding': '0px 10px 0px 10px'}
    }
    return (
      <div className="w-row" style={divStyle}>
        <label className="newincidentform__label">Select locations&nbsp;<p onClick={self.rem} style={{'marginTop': '0px', 'marginBottom': '0px', 'lineHeight': '20px', 'display': 'inline-block', 'color': 'blue', 'cursor': 'pointer' }} className="listincidents__form_label db_entries">x</p></label>
        {listSelected}
        <div className="w-col w-col-1">
          <h3 onClick={this.addBlock} style={{'textAlign': 'left'}} className="entrie link teblehead">add</h3>
        </div>
      </div>
    )
  }
})

/////
//!!!!!check empty states when sending requests - selecting not in order
/////

var OptionLocationInstance = React.createClass({
  propTypes: {
    data: PropTypes.array,
    number: PropTypes.number,
    remove: PropTypes.func,
    updateValue: PropTypes.func,
    initialData: PropTypes.array,
    downloaded: PropTypes.bool,
  },
  getInitialState: function () {
    return {
      selectedOption: '',
      sub: '',
      vil:'',
      subdata: [],
      villages: [],
      
    }
  },
  componentWillMount: function () {
    if (this.props.downloaded) {
      if (this.props.initialData.length > 0) {
          this.getSub(this.props.initialData[1]);
          if (this.props.initialData[2] != -1) {
            this.getVillages(this.props.initialData[1], this.props.initialData[2])
            var s = this.props.initialData[2]
          } else {
            var s = ''
          }
          if (this.props.initialData[3] != -1) {
            var v = this.props.initialData[3]
          } else {
            var v = ''
          }
          this.setState({
            selectedOption: this.props.initialData[1],
            sub: s,
            vil: v
          })}
      }  
  },
  getSub: function (main) {
      var self = this;
      var source_loc = 'http://' + AppGlobal.host + "/api/manage_db/db_data/locality/" + main + "/?format=json";
      this.serverRequest = $.get(source_loc, function (results) {
          var location_data = results.results;
          this.setState({
              subdata: location_data
          });
      }.bind(this));
  },
  getVillages: function (region, locality) {
      var self = this;
      var source_loc = 'http://' + AppGlobal.host + "/api/manage_db/db_data/village/" + region +"/"+ locality + "/?format=json";
      this.serverRequest = $.get(source_loc, function (results) {
          var location_data = results.results;
          self.setState({villages: location_data});
      }.bind(this));
  },
  setNewOption: function (value) {
    var self = this;    
    this.setState({ 
      selectedOption: value,
      sub: '', 
      vil: '',
      villages: []
    });
    this.getSub(value.value);
    var send = [self.props.number, value.value, -1, -1];
    this.props.updateValue(send);
  },
  setSub: function (value) {
    var self = this; 
    var first = this.state.selectedOption;   
    this.setState({ 
      sub: value,
      vil: '' 
    });
    this.getVillages(first.value, value.value)
    var send = [self.props.number, first.value, value.value, -1];
    this.props.updateValue(send);
  },
  setVillage: function (value) {
    var self = this;
    var r = self.state.selectedOption.value
    var l = self.state.sub.value    
    this.setState({ 
      vil: value 
    });
    var send = [self.props.number, r, l, value.value];
    this.props.updateValue(send);
  },
  render: function () {
    var self = this;
    var p_state = <span>Select region</span>;
    var p_sub = <span>Select locality</span>;
    var p_vil = <span>Select village</span>;
    return (
      <div className="w-col w-col-6" style={{'marginBottom': '12px'}}>
        <div className="w-row">
          <div className="w-col w-col-3" style={{'paddingRight': '10px'}}>
            <Select
              onChange={this.setNewOption}
              optionComponent={ChartType}
              options={this.props.data}
              placeholder={p_state}
              value={this.state.selectedOption}
              valueComponent={ChartValue}
              clearable={false}
            />
          </div>
          <div className="w-col w-col-4" style={{'paddingRight': '10px'}}>
            <Select
              onChange={this.setSub}
              optionComponent={ChartType}
              options={this.state.subdata}
              placeholder={p_sub}
              value={this.state.sub}
              valueComponent={ChartValue}
              clearable={false}
            />
          </div>
          <div className="w-col w-col-4">
            <Select
              onChange={this.setVillage}
              optionComponent={ChartType}
              options={this.state.villages}
              placeholder={p_vil}
              value={this.state.vil}
              valueComponent={ChartValue}
              clearable={false}
            />
          </div>
          <div className="w-col w-col-1">
            <p onClick={self.props.remove.bind(null, self.props.number)} style={{'marginTop': '6px', 'marginLeft': '4px', 'display': 'block', 'color': 'blue', 'cursor': 'pointer' }} className="listincidents__form_label db_entries">x</p>
          </div>
        </div>
      </div>
    )
  }
})

//////
// Sources Constructor
//////

var sorNum = 1;
var OptionSourceBody = React.createClass({
  propTypes: {
    update: PropTypes.func,
    number: PropTypes.number,
    remove: PropTypes.func,
    downloaded: PropTypes.bool,
    preset: PropTypes.array,
    last: PropTypes.bool,
  },
  getInitialState: function () {
    return {
      selected: [1],
      variants: [],
      values: []
    }
  },
  componentWillMount: function () {
    var source_victims = 'http://'+AppGlobal.host + "/api/incidents/sources/list/?format=json";
    this.serverRequest = $.get(source_victims, function (results) {
      var victims_data = results.results;
      this.setState({
          variants: victims_data,
      });
    }.bind(this));
    if (this.props.downloaded) {
      var presetData = this.props.preset;
      var filtersData = [];
      var filteredValuesData = [];
      // actors
      if (presetData.length > 0) {
        var itActors = presetData.map(function(ins_1, key){
          filtersData.push(key+1)
          sorNum += 1
          var t_arr_1 = [key+1, ins_1.value]
          filteredValuesData.push(t_arr_1)
        });
      } 
      this.setState({
        selected: filtersData,
        values: filteredValuesData
      })
    }
  },
  addBlock: function () {
    var arr = this.state.selected;
    sorNum += 1
    arr.push(sorNum);
    this.setState({
      selected:arr
    })
  },
  removeBlock: function(number) {
    var arr = this.state.selected;
    var vals = this.state.values;
    var index = arr.indexOf(number);
    if (index > -1) {
        arr.splice(index, 1);
    }
    var i=0;
    for (i = 0; i < vals.length; i++) {
        // This if statement depends on the format of your array
        if (vals[i][0] === number) {
            vals.splice(i, 1)
            break;   // Found it
        }
    }
    this.props.update(4, vals);
    //remove values from variants array
    this.setState({
      selected:arr,
      values: vals
    })
  },
  updateValue: function (value) {
    var vals = this.state.values;
    var i=0;
    var found = false;
    for (i = 0; i < vals.length; i++) {
        // This if statement depends on the format of your array
        if (vals[i][0] == value[0]) {
            found = true;
            break;   // Found it
        }
    }
    if (found) {
      vals[i][1] = value[1];
    } else {
      vals.push(value)
    }
    this.props.update(4, vals);
    this.setState({values:vals})
    // what i'm need here 1. Number of conteiner and his value 2. send all values to some where to be stored 3. I need to know updatind values or adding new
  },
  rem: function () {
    this.props.remove(this.props.number);
  },
  render: function () {
    var self = this;
    var listSelected = this.state.selected.map(function(inst, key){
      if (self.state.values[key]) {
        var dd = self.state.values[key]
      } else {
        var dd = [];
      }
      return (<OptionSourceInstance downloaded={self.props.downloaded} initialData={dd} key={'option_sou_instance_'+ key} updateValue={self.updateValue} number={inst} remove={self.removeBlock} data={self.state.variants} />)
    })
    if (this.props.last) {
      var divStyle = {'padding': '0px 10px 0px 10px'}
    } else {
      var divStyle = {'borderBottom': '1px dashed gray', 'padding': '0px 10px 0px 10px'}
    }
    return (
      <div className="w-row" style={divStyle}>
        <label className="newincidentform__label">Select source categories&nbsp;<p onClick={self.rem} style={{'marginTop': '0px', 'marginBottom': '0px', 'lineHeight': '20px', 'display': 'inline-block', 'color': 'blue', 'cursor': 'pointer' }} className="listincidents__form_label db_entries">x</p></label>
        {listSelected}
        <div className="w-col w-col-1">
          <h3 onClick={this.addBlock} style={{'textAlign': 'left'}} className="entrie link teblehead">add</h3>
        </div>
      </div>
    )
  }
})

var OptionSourceInstance = React.createClass({
  propTypes: {
    data: PropTypes.array,
    number: PropTypes.number,
    remove: PropTypes.func,
    updateValue: PropTypes.func,
    initialData: PropTypes.array,
    downloaded: PropTypes.bool,
  },
  getInitialState: function () {
    return {
      selectedOption: ''
    }
  },
  componentWillMount: function () {
    if (this.props.downloaded) {
      if (this.props.initialData.length > 0) {
      this.setState({
        selectedOption: this.props.initialData[1]
      })
      }
    }
  },
  setNewOption: function (value) {
    var self = this;    
    this.setState({ 
      selectedOption: value, 
    });
    var send = [self.props.number, value.value];
    this.props.updateValue(send);
  },
  render: function () {
    var self = this;
    var p_state = <span>Select source category</span>;
    return (
      <div className="w-col w-col-3" style={{'marginBottom': '12px'}}>
        <div className="w-row">
          <div className="w-col w-col-11">
            <Select
              onChange={this.setNewOption}
              optionComponent={ChartType}
              options={this.props.data}
              placeholder={p_state}
              value={this.state.selectedOption}
              valueComponent={ChartValue}
              clearable={false}
            />
          </div>
          <div className="w-col w-col-1">
            <p onClick={self.props.remove.bind(null, self.props.number)} style={{'marginTop': '6px', 'marginLeft': '4px', 'display': 'block', 'color': 'blue', 'cursor': 'pointer' }} className="listincidents__form_label db_entries">x</p>
          </div>
        </div>
      </div>
    )
  }
})

//////
// Inf grading Constructor
//////

var infNum = 1;
var OptionInfBody = React.createClass({
  propTypes: {
    update: PropTypes.func,
    number: PropTypes.number,
    remove: PropTypes.func,
    downloaded: PropTypes.bool,
    preset: PropTypes.array,
    last: PropTypes.bool,
  },
  getInitialState: function () {
    return {
      selected: [1],
      variants: [],
      values: []
    }
  },
  componentWillMount: function () {
    var source_victims = 'http://' + AppGlobal.host + "/api/manage_db/db_data/information_grading/" + "?format=json";
    this.serverRequest = $.get(source_victims, function (results) {
      var victims_data = results.results;
      this.makeGroups(victims_data);
    }.bind(this));
    if (this.props.downloaded) {
      var presetData = this.props.preset;
      var filtersData = [];
      var filteredValuesData = [];
      // actors
      if (presetData.length > 0) {
        var itActors = presetData.map(function(ins_1, key){
          filtersData.push(key+1)
          infNum += 1
          var t_arr_1 = [key+1, ins_1.value]
          filteredValuesData.push(t_arr_1)
        });
      } 
      this.setState({
        selected: filtersData,
        values: filteredValuesData
      })
    }
  },
  makeGroups: function (data) {
    var self = this;
    var arr = [];
    var iter = data.map((inst)=>{
            var list = { value: inst.id, label: inst.name,};
            arr.push(list);
    })
    this.setState({
        variants: arr,
    });
  },
  addBlock: function () {
    var arr = this.state.selected;
    infNum += 1
    arr.push(infNum);
    this.setState({
      selected:arr
    })
  },
  removeBlock: function(number) {
    var arr = this.state.selected;
    var vals = this.state.values;
    var index = arr.indexOf(number);
    if (index > -1) {
        arr.splice(index, 1);
    }
    var i=0;
    for (i = 0; i < vals.length; i++) {
        // This if statement depends on the format of your array
        if (vals[i][0] === number) {
            vals.splice(i, 1)
            break;   // Found it
        }
    }
    this.props.update(5, vals);
    //remove values from variants array
    this.setState({
      selected:arr,
      values: vals
    })
  },
  updateValue: function (value) {
    var vals = this.state.values;
    var i=0;
    var found = false;
    for (i = 0; i < vals.length; i++) {
        // This if statement depends on the format of your array
        if (vals[i][0] == value[0]) {
            found = true;
            break;   // Found it
        }
    }
    if (found) {
      vals[i][1] = value[1];
    } else {
      vals.push(value)
    }
    this.props.update(5, vals);
    this.setState({values:vals})
    // what i'm need here 1. Number of conteiner and his value 2. send all values to some where to be stored 3. I need to know updatind values or adding new
  },
  rem: function () {
    this.props.remove(this.props.number);
  },
  render: function () {
    var self = this;
    var listSelected = this.state.selected.map(function(inst, key){
      if (self.state.values[key]) {
        var dd = self.state.values[key]
      } else {
        var dd = [];
      }
      return (<OptionSourceInstance downloaded={self.props.downloaded} initialData={dd} key={'option_sou_instance_'+ key} updateValue={self.updateValue} number={inst} remove={self.removeBlock} data={self.state.variants} />)
    })
    if (this.props.last) {
      var divStyle = {'padding': '0px 10px 0px 10px'}
    } else {
      var divStyle = {'borderBottom': '1px dashed gray', 'padding': '0px 10px 0px 10px'}
    }
    return (
      <div className="w-row" style={divStyle}>
        <label className="newincidentform__label">Select information grading categories&nbsp;<p onClick={self.rem} style={{'marginTop': '0px', 'marginBottom': '0px', 'lineHeight': '20px', 'display': 'inline-block', 'color': 'blue', 'cursor': 'pointer' }} className="listincidents__form_label db_entries">x</p></label>
        {listSelected}
        <div className="w-col w-col-1">
          <h3 onClick={this.addBlock} style={{'textAlign': 'left'}} className="entrie link teblehead">add</h3>
        </div>
      </div>
    )
  }
})

var OptionSourceInstance = React.createClass({
  propTypes: {
    data: PropTypes.array,
    number: PropTypes.number,
    remove: PropTypes.func,
    updateValue: PropTypes.func,
    initialData: PropTypes.array,
    downloaded: PropTypes.bool,
  },
  getInitialState: function () {
    return {
      selectedOption: ''
    }
  },
  componentWillMount: function () {
    if (this.props.downloaded) {
      if (this.props.initialData.length > 0) {
      this.setState({
        selectedOption: this.props.initialData[1]
      })
      }
    }
  },
  setNewOption: function (value) {
    var self = this;    
    this.setState({ 
      selectedOption: value, 
    });
    var send = [self.props.number, value.value];
    this.props.updateValue(send);
  },
  render: function () {
    var self = this;
    var p_state = <span>Select information grading category</span>;
    return (
      <div className="w-col w-col-3" style={{'marginBottom': '12px'}}>
        <div className="w-row">
          <div className="w-col w-col-11">
            <Select
              onChange={this.setNewOption}
              optionComponent={ChartType}
              options={this.props.data}
              placeholder={p_state}
              value={this.state.selectedOption}
              valueComponent={ChartValue}
              clearable={false}
            />
          </div>
          <div className="w-col w-col-1">
            <p onClick={self.props.remove.bind(null, self.props.number)} style={{'marginTop': '6px', 'marginLeft': '4px', 'display': 'block', 'color': 'blue', 'cursor': 'pointer' }} className="listincidents__form_label db_entries">x</p>
          </div>
        </div>
      </div>
    )
  }
})

//////
// Keywords Constructor
//////

var keyNum = 1;
var OptionKeywordBody = React.createClass({
  propTypes: {
    update: PropTypes.func,
    number: PropTypes.number,
    remove: PropTypes.func,
    downloaded: PropTypes.bool,
    preset: PropTypes.array,
    last: PropTypes.bool,
  },
  getInitialState: function () {
    return {
      selected: [1],
      values: []
    }
  },
  componentWillMount: function () {
    if (this.props.downloaded) {
      var presetData = this.props.preset;
      var filtersData = [];
      var filteredValuesData = [];
      // actors
      if (presetData.length > 0) {
        var itActors = presetData.map(function(ins_1, key){
          filtersData.push(key+1)
          keyNum += 1
          var t_arr_1 = [key+1, ins_1.label]
          filteredValuesData.push(t_arr_1)
        });
      } 
      this.setState({
        selected: filtersData,
        values: filteredValuesData
      })
    }
  },
  addBlock: function () {
    var arr = this.state.selected;
    keyNum += 1
    arr.push(keyNum);
    this.setState({
      selected:arr
    })
  },
  removeBlock: function(number) {
    var arr = this.state.selected;
    var vals = this.state.values;
    var index = arr.indexOf(number);
    if (index > -1) {
        arr.splice(index, 1);
    }
    var i=0;
    for (i = 0; i < vals.length; i++) {
        // This if statement depends on the format of your array
        if (vals[i][0] === number) {
            vals.splice(i, 1)
            break;   // Found it
        }
    }
    this.props.update(6, vals);
    //remove values from variants array
    this.setState({
      selected:arr,
      values: vals
    })
  },
  updateValue: function (value) {
    var vals = this.state.values;
    var i=0;
    var found = false;
    for (i = 0; i < vals.length; i++) {
        // This if statement depends on the format of your array
        if (vals[i][0] == value[0]) {
            found = true;
            break;   // Found it
        }
    }
    if (found) {
      vals[i][1] = value[1];
    } else {
      vals.push(value)
    }
    this.props.update(6, vals);
    this.setState({values:vals})
    // what i'm need here 1. Number of conteiner and his value 2. send all values to some where to be stored 3. I need to know updatind values or adding new
  },
  rem: function () {
    this.props.remove(this.props.number);
  },
  render: function () {
    var self = this;
    var listSelected = this.state.selected.map(function(inst, key){
      if (self.state.values[key]) {
        var dd = self.state.values[key]
      } else {
        var dd = [];
      }
      return (<OptionKeywordInstance downloaded={self.props.downloaded} initialData={dd} key={'option_key_instance_'+ key} updateValue={self.updateValue} number={inst} remove={self.removeBlock} />)
    })
    if (this.props.last) {
      var divStyle = {'padding': '0px 10px 0px 10px'}
    } else {
      var divStyle = {'borderBottom': '1px dashed gray', 'padding': '0px 10px 0px 10px'}
    }
    return (
      <div className="w-row" style={divStyle}>
        <label className="newincidentform__label">Input Keywords&nbsp;<p onClick={self.rem} style={{'marginTop': '0px', 'marginBottom': '0px', 'lineHeight': '20px', 'display': 'inline-block', 'color': 'blue', 'cursor': 'pointer' }} className="listincidents__form_label db_entries">x</p></label>
        {listSelected}
        <div className="w-col w-col-1">
          <h3 onClick={this.addBlock} style={{'textAlign': 'left'}} className="entrie link teblehead">add</h3>
        </div>
      </div>
    )
  }
})

var OptionKeywordInstance = React.createClass({
  propTypes: {
    number: PropTypes.number,
    remove: PropTypes.func,
    updateValue: PropTypes.func,
    initialData: PropTypes.array,
    downloaded: PropTypes.bool
  },
  getInitialState: function () {
    return {
      selectedOption: ''
    }
  },
  componentWillMount: function () {
    if (this.props.downloaded) {
      if (this.props.initialData.length > 0) {
      this.setState({
        selectedOption: this.props.initialData[1]
      })
      }
    }
  },
  setNewOption: function (event) {
    var self = this;    
    this.setState({ 
      selectedOption: event.target.value 
    });
    var send = [self.props.number, event.target.value];
    this.props.updateValue(send);
  },
  render: function () {
    var self = this;
    var p_state = <span>Select information grading category</span>;
    return (
      <div className="w-col w-col-2" style={{'marginBottom': '12px'}}>
        <div className="w-row">
          <div className="w-col w-col-11">
            <input style={{'width': '100%'}} onChange={this.setNewOption} value={this.state.selectedOption} className="listincidents__form_input w-input" maxLength="256" placeholder="Input one keyword here" type="text"/>
          </div>
          <div className="w-col w-col-1">
            <p onClick={self.props.remove.bind(null, self.props.number)} style={{'marginTop': '6px', 'marginLeft': '4px', 'display': 'block', 'color': 'blue', 'cursor': 'pointer' }} className="listincidents__form_label db_entries">x</p>
          </div>
        </div>
      </div>
    )
  }
})


//////
// Utils
//////

var ChartType = React.createClass({
  propTypes: {
    children: PropTypes.node,
    className: PropTypes.string,
    isDisabled: PropTypes.bool,
    isFocused: PropTypes.bool,
    isSelected: PropTypes.bool,
    onFocus: PropTypes.func,
    onSelect: PropTypes.func,
    option: PropTypes.object,
  },
  handleMouseDown: function (event) {
    event.preventDefault();
    event.stopPropagation();
    this.props.onSelect(this.props.option, event);
  },
  handleMouseEnter: function (event) {
    this.props.onFocus(this.props.option, event);
  },
  handleMouseMove: function (event) {
    if (this.props.isFocused) return;
    this.props.onFocus(this.props.option, event);
  },
  render: function () {
    var gravatarStyle = {
      borderRadius: 3,
      display: 'inline-block',
      marginRight: 10,
      position: 'relative',
      top: -2,
      verticalAlign: 'middle',
      width: '13%'
    };
    return (
      <div style={{"textAlign": "left"}} className={this.props.className}
        onMouseDown={this.handleMouseDown}
        onMouseEnter={this.handleMouseEnter}
        onMouseMove={this.handleMouseMove}
        title={this.props.option.title}>
        {this.props.children}
      </div>
    )
  },
}) 

const ChartValue = React.createClass({
  propTypes: {
    children: PropTypes.node,
    placeholder: PropTypes.string,
    value: PropTypes.object
  },
  render: function () {
    var gravatarStyle = {
      borderRadius: 3,
      display: 'inline-block',
      marginRight: 10,
      position: 'relative',
      top: -2,
      verticalAlign: 'middle',
      width: '13%'
    };
    return (
      <div className="Select-value" style={{"textAlign": "left"}} title={this.props.value.title}>
        <span className="Select-value-label">
          {this.props.children}
        </span>
      </div>
    );
  }
});

module.exports = SegementsConteiner;