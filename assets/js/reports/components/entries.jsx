var React = require('react')
var ReactDom = require('react-dom')
var PropTypes = React.PropTypes;
var classNames = require('classnames');
var TimerMixin = require('react-timer-mixin');
var dateFormat = require('dateformat');


var EntriesConteiner = React.createClass({
  propTypes: {  
      selected: PropTypes.array,
      start: PropTypes.string,
      end: PropTypes.string,
  },
  getInitialState: function () {
      return {
          data: [],
          ready: false,
          buttons: [],
          criteria: 0,
          loadStatus: 0
      }
  },
  componentWillMount: function () {
      var vals = []
      var i=1;
      for (i; i < this.props.selected.length + 1; i++) {
        vals.push(i)
      }
      this.setState({
        buttons: vals
      })
  },
  componentWillReceiveProps: function (nextProps) {
      var vals = []
      var i=1;
      for (i; i < nextProps.selected.length + 1; i++) {
        vals.push(i)
      }
      this.setState({
        buttons: vals
      })
  },
  fetchChartData: function (segment) {
      var self = this;
      this.setState({
        loadStatus: 1
      })
      var start = this.props.start;
      var end =  this.props.end;
          if (start !== '' && end !== '') {
              if (parseInt(this.props.selected) > 0) {
                  var source = 'http://'+AppGlobal.host+'/api/chart/entry/fetch_data/';
                  var post_d = {start: start, end: end, 'segment': this.props.selected[segment-1], 'user_id': parseInt(AppGlobal.user)};
                  $.ajax({
                     headers: { 
                       'Accept': 'application/json',
                       'Content-Type': 'application/json' 
                     },
                     type: "POST",
                     url: source,
                     data: JSON.stringify(post_d, null, '\t'),
                     dataType: 'json',
                       statusCode: {
                         200: function (data) {
                          self.setState({
                            data: data.allData,
                            ready: true,
                            loadStatus: 2
                          })
                         },
                         500: function (data) { self.setState({loadStatus:3}) },
                       } 
                  });
              } else {
              }    
          } else {
          } 
  },
  changeSegment: function (value) {
    this.fetchChartData(value);
    this.setState({
      criteria: value
    })
  },
  render: function () {
    var self = this;
    var listButtons;
    if (this.props.start != '' && this.props.end != '') {
      if (this.props.selected.length > 0) {
        var listButtons = this.state.buttons.map(function (inst) {
            return (<div className="w-col w-col-1" style={{width: '2%'}} key={'seg_num_but_' + inst}>
                {self.state.criteria == inst ? (<h3 style={{'textAlign':'left', 'fontSize': '20px', 'fontWeight': '800' }} onClick={self.changeSegment.bind(null, inst)}  className="entrie link teblehead">{inst}</h3>) : (
                   <h3 style={{'textAlign':'left',  }} onClick={self.changeSegment.bind(null, inst)}  className="entrie link teblehead">{inst}</h3>
                   )}
                 
                </div>)
          })
        if (this.state.criteria != 0) {
          if (this.state.loadStatus === 1) {
            var listData = (<h3 className="teblehead">Loading data... This may take more then one minute.</h3>)
          } else if (this.state.loadStatus === 2) {
            if (this.state.data.length > 0) {
              var listData = this.state.data.map(function (inst) {
                  return (<FilteredInstance key={'filt_entr_crit_'+inst.id} data={inst} />)
              }) 
            } else {
              var listData = (<h3 className="teblehead">No entries found</h3>)
            }
          } else if (this.state.loadStatus === 3) {
            var listData = (<h3 className="teblehead">An error ocured during loading. Please make screenshot of selected criteria and contact developer.</h3>)
          }
            
        } else {
          var listData = (<h3 className="teblehead">Please select criteria</h3>)
        }
      } else {
        var listData = (<h3 className="teblehead">Please choose existing criteria or create new</h3>)
      }
    } else {
      var listData = (<h3 className="teblehead">Please choose start and end date</h3>)
    }
    return (
        <div className="dbentriesblock">
        <h2 className="blockheading">Load entries filtered by criteria</h2>
        <div style={{'display': 'block', 'marginBottom': '60px'}}>
              <h4 className="listincidents__filterhead">Select criteria number:</h4>
              <div className="w-row">
              {listButtons}
              </div>
            </div>
          <div className="dbentriesconteiner">
            
            <div className="headerrow w-row">
              <div className="w-col w-col-1">
                <h3 style={{'float': 'left'}} className="teblehead">ID</h3>
              </div>
              <div className="w-col w-col-3">
                <h3 style={{'float': 'left'}}  className="teblehead">Title</h3>
              </div>
              <div className="w-col w-col-7">
                <div className="w-row">
                <div className="w-col w-col-12">
                <h3 style={{'float': 'left'}}  className="teblehead">Values</h3>
                </div>
                <div className="w-col w-col-12">
                <div className="w-row">
                  <div className="w-col w-col-1">
                    <label className="newincidentform__label">Number</label>
                  </div>
                  <div className="w-col w-col-2">
                    <label className="newincidentform__label">Date</label>
                  </div>
                  <div className="w-col w-col-2">
                    <label className="newincidentform__label">Victims</label>
                  </div>
                  <div className="w-col w-col-2">
                    <label className="newincidentform__label">Actors</label>
                  </div>
                  <div className="w-col w-col-2">
                    <label className="newincidentform__label">Nature</label>
                  </div>
                  <div className="w-col w-col-3">
                    <label className="newincidentform__label">Location</label>
                  </div>
                </div>
                </div>
                </div>
              </div>
              <div className="w-col w-col-1">
                <h3 style={{'float': 'left'}}  className="teblehead">Action</h3>
              </div>
            </div>
            <div className="listdbentriesconteiner">
              {listData}
            </div>
          </div>
        </div>
    )
  }  
})

var FilteredInstance = React.createClass({
    propTypes: {  
        data: PropTypes.object,
    },
    getInitialState: function () {
        return {
            more: false
        }
    },
    hendleMore: function () {
        this.setState({
            more: !this.state.more
        })
    },
    redirectToNewRecord: function (where) {
        window.location.href = 'http://'+AppGlobal.host+'/db_manage/edit_db_record/'+ this.props.data.id +'/';
    },
    render: function () {
        var self = this;
        var additionalClass = classNames({
            'db__entry__preview_conteiner': true,
            'hided': ! this.state.more
        })
        var listDescriptions = this.props.data.entry_descriptions.map(function (inst, key) {
            return (
                <div className="db_descriptioninstance" key={'entr_desc_inst_text_jk_'+ key + '_' + self.props.data.id}>
                  <div className="w-row">
                    <div className="w-col w-col-12">
                      <label className="newincidentform__label">Description {key+1}</label>
                      <p className="db_entries listincidents__form_label">{inst.text}</p>
                    </div>
                  </div>
                </div>
            )
        })
        if (this.state.more) {
            var button = 'minimize'
        } else {
            var button = 'more info'
        }
        var listVals = this.props.data.values_instances.map(function(inst, key){
          return (<FilteredInstanceValue key={'entr_val_inst_ggh_'+key+'_'+self.props.data.id} data={inst} number={key+1} valID={self.props.data.id} />)
        })
        return (
            <div className="dbentryconteiner">
              <div className="w-row">
                <div className="w-col w-col-1">
                  <h3 style={{'float': 'left'}} className="entrie teblehead">{this.props.data.id}</h3>
                </div>
                <div className="w-col w-col-3">
                  <h3 style={{'float': 'left'}} className="entrie teblehead">{this.props.data.name}</h3>
                </div>
                <div className="w-col w-col-7">
                  <div className="w-row">
                    <div className="w-col w-col-12">
                      {listVals}
                    </div>
                  </div>    
                </div>
                <div className="w-col w-col-1">
                  <div className="w-row">
                    <div className="w-col w-col-12">
                      <h3 style={{'float': 'left'}} onClick={this.hendleMore} className="entrie link teblehead">{button}</h3>
                    </div>
                    <div className="w-col w-col-12">
                      <h3 style={{'float': 'left'}} onClick={this.redirectToNewRecord} className="entrie link teblehead">edit</h3>
                    </div>
                  </div>
                </div>
              </div>
              <div className={additionalClass}>
                <div className="db_descriptionsconteiner">
                  {listDescriptions}
                </div>
                <div className="db__remarksconteiner">
                  <div className="w-row">
                    <div className="w-col w-col-6" style={{'paddingRight': '5px'}}>
                      <label className="newincidentform__label">Remarks</label>
                      <p className="db_entries listincidents__form_label">{this.props.data.remarks === '' ? ('--') : this.props.data.remarks}</p>
                    </div>
                    <div className="w-col w-col-6" style={{'paddingRight': '5px'}}>
                      <label className="newincidentform__label">JMAC db comments</label>
                      <p className="db_entries listincidents__form_label">{this.props.data.JMACcomments === '' ? ('--') : this.props.data.JMACcomments}</p>
                    </div>
                  </div>
                  <div className="w-row">
                    <div className="w-col w-col-12">
                      <label className="newincidentform__label">Summary</label>
                      <p className="db_entries listincidents__form_label">{this.props.data.summary === '' ? ('--') : this.props.data.summary}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        )
    }
})

var FilteredInstanceValue = React.createClass({
  propTypes: {  
      data: PropTypes.object,
      number: PropTypes.number,
      valID: PropTypes.number,
  },
  render: function () {
    var self = this;
    var listVictims = this.props.data.victims.map(function(inst, key) {
      return (<div key={'entr_val_inst_vics_'+key+'_'+self.props.valID} style={{'display': 'block'}}><p className="db_entries listincidents__form_label">{inst}</p></div>)
    })
    var listActors = this.props.data.actors.map(function(inst, key){
      return (<div key={'entr_val_inst_actr_'+key+'_'+self.props.valID} style={{'display': 'block'}}><p className="db_entries listincidents__form_label">{inst}</p></div>)
    })
    var listNature = this.props.data.nature.map(function(inst, key) {
      return (<div key={'entr_val_inst_natur_'+key+'_'+self.props.valID} style={{'display': 'block'}}><p className="db_entries listincidents__form_label">{inst}</p></div>)
    })
    var listLocation = this.props.data.locations.map(function(inst, key) {
      return (<div key={'entr_val_inst_locs_'+key+'_'+self.props.valID} style={{'display': 'block'}}><p className="db_entries listincidents__form_label">{inst}</p></div>)
    })
    return (
      <div className="w-row">
        <div className="w-col w-col-1">
          <p className="db_entries listincidents__form_label">{this.props.number}</p>
        </div>
        <div className="w-col w-col-2">
          <p className="db_entries listincidents__form_label">{this.props.data.date}</p>
        </div>
        <div className="w-col w-col-2" style={{'paddingRight': '5px'}}>
          {listVictims}
        </div>
        <div className="w-col w-col-2" style={{'paddingRight': '5px'}}>
          {listActors}
        </div>
        <div className="w-col w-col-2" style={{'paddingRight': '5px'}}>
          {listNature}
        </div>
        <div className="w-col w-col-3">
          {listLocation}
        </div>
      </div>
    )
  }
})

module.exports = EntriesConteiner;