var React = require('react')
var ReactDom = require('react-dom')
var PropTypes = React.PropTypes;
var classNames = require('classnames');
var TimerMixin = require('react-timer-mixin');

//var dateFormat = require('dateformat');

var LineChart = require("react-chartjs-2").Line;
var BarChart = require("react-chartjs-2").Bar;
var HorizontalBar = require("react-chartjs-2").HorizontalBar;
var PieChart = require("react-chartjs-2").Pie;
var RadarChart = require("react-chartjs-2").Radar;
var PolarChart = require("react-chartjs-2").Polar;
var BubbleChart = require("react-chartjs-2").Bubble;

var DateRange = require('react-date-range').DateRange;
//var defaultRanges = require('react-date-range').defaultRanges;
var randomHex = require('random-hex');

var CanvasToImage = require('canvas-to-image-node');
var Select = require('react-select');
require('react-select/dist/react-select.css');
import _ from 'underscore';

var test = [1,2,3,4]
var hehValues = [['#0098ff', 'rgba(0, 152, 255, 0.81)', 'rgba(0, 152, 255, 0.41)'], ['#e0DF1a', 'rgba(224, 223, 26, 0.81)','rgba(224, 223, 26, 0.41)'], ['#e01A66', 'rgba(224, 26, 102, 0.81)','rgba(224, 26, 102, 0.41)'], ['#790777', 'rgba(121, 7, 119, 0.81)','rgba(121, 7, 119, 0.41)']]
var ChartBlockConteiner = React.createClass({
    mixins: [TimerMixin],
    propTypes: {  
        selected: PropTypes.array,
        start: PropTypes.string,
        end: PropTypes.string,
        exportSegments: PropTypes.func,
    },
    getInitialState: function () {
    	return {
        selectedX: 1,
        received: false,
        chart: { value: '1', label: 'Line', image: 'line.png'},
        segments: [],
        refetch: false,
    	}
    },
    componentWillMount: function () {
      this.setState({
        segments: this.props.selected
      })
    },
    componentWillReceiveProps: function (nextProps) {
      if (nextProps.selected != this.props.selected) {
        this.setState({
          segments: nextProps.selected 
        })
      }
    },
    fireChooseHint: function (what) {
        // what 1 = no dates selected
        // what 2 = no segment selected
    },
    ///////////////////
    // X axis change //
    ///////////////////
    handleXchange: function (event) {
      var self = this;
        this.setState({
            selectedX: parseInt(event.target.value)
        });
    },
    exportPng: function () {
      var self = this;
      CanvasToImage.saveAsPNG(ReactDom.findDOMNode(self.refs.myChart), 1800, 1000);
    },
    setValue: function (value) {
      this.setState({ 
        chart: value 
      });
    },
    changeReceived: function (value) {
      this.setState({
        received: value
      })
    },
    updateChart: function () {
      this.setState({
        refetch: true
      })
      this.setTimeout(function(){
        this.setState({
          refetch: false
        })
      }, 300)
    },
    render: function () {
    	var self = this;
        if (this.state.showCal) {
            var butText = 'Apply range and close';
        } else {
            var butText = 'Select dates range';
        }
        var placeholder = <span>&#9786; Select chart type</span>;
        var chartConteiner;
        if (this.state.chart.value === '1') {
          chartConteiner = (<LineChartHolder ref='myChart' selectedX={this.state.selectedX} startDate={this.props.start} endDate={this.props.end} segments={this.state.segments} changeReceived={this.changeReceived} fireChooseHint={this.fireChooseHint} refetch={this.state.refetch} />);
        } else if (this.state.chart.value === '2') {
          chartConteiner = (<BarChartHolder ref='myChart' selectedX={this.state.selectedX} startDate={this.props.start} endDate={this.props.end} segments={this.state.segments} changeReceived={this.changeReceived} fireChooseHint={this.fireChooseHint} refetch={this.state.refetch} />)
        } else if (this.state.chart.value === '3') {
          chartConteiner = (<HorizontalBarChartHolder ref='myChart' selectedX={this.state.selectedX} startDate={this.props.start} endDate={this.props.end} segments={this.state.segments} changeReceived={this.changeReceived} fireChooseHint={this.fireChooseHint} refetch={this.state.refetch} />)
        } else if (this.state.chart.value === '4') {
          chartConteiner = (<PieChartHolder ref='myChart' selectedX={this.state.selectedX} startDate={this.props.start} endDate={this.props.end} segments={this.state.segments} changeReceived={this.changeReceived} fireChooseHint={this.fireChooseHint} refetch={this.state.refetch} />)
        } else if (this.state.chart.value === '5') {
          chartConteiner = (<RadarChartHolder ref='myChart' selectedX={this.state.selectedX} startDate={this.props.start} endDate={this.props.end} segments={this.state.segments} changeReceived={this.changeReceived} fireChooseHint={this.fireChooseHint} refetch={this.state.refetch} />)
        } else if (this.state.chart.value === '6') {
          chartConteiner = (<PolaerChartConteiner ref='myChart' selectedX={this.state.selectedX} startDate={this.props.start} endDate={this.props.end} segments={this.state.segments} changeReceived={this.changeReceived} fireChooseHint={this.fireChooseHint} refetch={this.state.refetch} />)
        } else if (this.state.chart.value === '7') {
          chartConteiner = (<BubbleChartHolder ref='myChart' selectedX={this.state.selectedX} startDate={this.props.start} endDate={this.props.end} segments={this.state.segments} changeReceived={this.changeReceived} fireChooseHint={this.fireChooseHint} refetch={this.state.refetch} />)
        }
        return (
            <div className="dbentriesblock" style={{'marginTop': '40px'}}>
              <h2 className="blockheading">Create data chart</h2>
              <div className="chart__conteiner">
                <div className="w-row">
                  <div className="w-col w-col-2">
                  <div>
                    <h4 className="listincidents__filterhead">Select chart type:</h4>
                    <Select
                      onChange={this.setValue}
                      optionComponent={ChartType}
                      options={CHARTS}
                      placeholder={placeholder}
                      value={this.state.chart}
                      valueComponent={ChartValue}
                      clearable={false}
                    />
                  </div>
                    <div>
                      <h4 className="listincidents__filterhead">Select option for Y axis:</h4>
                      <div className="w-form">
                        <form onSubmit={this.handleSubmit}>
                          <select onChange={this.handleXchange} value={this.state.selectedX} className="w-select" >
                            <option value="1">Total entries</option>
                            <option value="2">Total victims</option>
                          </select>
                        </form>
                      </div>
                    </div>
                    <div>
                    <a onClick={this.updateChart} style={{'width': '100%', 'display': 'inline-block'}} className="w-button listincidents__form_submit db">Update chart</a>
                    {this.state.received ? (<div>
                      <a onClick={this.props.exportSegments} style={{'width': '100%', 'display': 'inline-block', 'marginTop': '15px'}} className="w-button wekkchoosebutton">Export Excel</a>
                      <a onClick={this.exportPng} style={{'width': '100%', 'display': 'inline-block', 'marginTop': '10px'}} className="w-button wekkchoosebutton">Export PNG</a>
                    </div>) : null}
                    </div>
                  </div>
                  <div className="w-col w-col-10">
                    <div className="chart__holder">
                      {chartConteiner}
                    </div>
                  </div>
                </div>
              </div>
            </div>
        )
    }
})

var BubbleChartHolder = React.createClass({
  mixins: [TimerMixin],
  propTypes: {
    selectedX: PropTypes.number,
    startDate: PropTypes.string,
    endDate: PropTypes.string,
    segments: PropTypes.array,
    changeReceived: PropTypes.func,
    refetch: PropTypes.bool,
    fireChooseHint: PropTypes.func
  },
  getInitialState: function () {
    return {
      data: {
            datasets: [{
                    label: "Segment " + 0,
                    data: [1,2],
                    fill: true,
                    borderWidth: 3,
                    borderColor: hehValues[0][1],
                    pointBorderColor: hehValues[0][0],
                    pointBackgroundColor: hehValues[0][0],
                    pointRadius: 3,
                    pointHoverRadius: 5,
                    pointStyle: 'circle',
                    spanGaps: true,
                }],
            labels: [1,2]
        },
        options: {
            hover: {
                    mode: 'label'
                },
            tooltips: {
              mode: 'label'
            },
            legend: {
                    display: true,
                    position: 'top',
                },      
            defaultFontSize: 16,
            defaultFontColor: 'orange',
            responsiveAnimationDuration: 600,
            responsive: true
        },
        ready: false
    }
  },
  componentWillReceiveProps: function (nextProps) {
    if (nextProps.refetch) {
      this.fetchChartData(nextProps.startDate, nextProps.endDate, nextProps.selectedX, nextProps.segments);
    }
  },
  fetchChartData: function (start, end, x, segments) {
        var self = this;
        this.setTimeout(function () {
            if (start !== '' && end !== '') {
                if (this.props.segments.length > 0) {
                    var source = 'http://'+AppGlobal.host+'/api/chart/fetch_data/';
                    var post_d = {start: start, end: end, x: x, 'segments': segments, 'user_id': parseInt(AppGlobal.user)};
                    $.ajax({
                       headers: { 
                         'Accept': 'application/json',
                         'Content-Type': 'application/json' 
                       },
                       type: "POST",
                       url: source,
                       data: JSON.stringify(post_d, null, '\t'),
                       dataType: 'json',
                         statusCode: {
                           200: function (data) {self.asigneFetchedData(data.allData)},
                           500: function (data) {},
                         } 
                    });
                } else {
                    this.props.fireChooseHint(2);
                }    
            } else {
                this.props.fireChooseHint(1);
            } 
        }, 200)
    },
    asigneFetchedData: function (data) {
        var ds = [];
        var mapDatasets = data[0].map( function (inst, key) {
          var obj = {};
          var borderArr = [];
          var colorArr = [];
          var dataArray = [];
          var hoverColorArr = [];
          var iter = inst[0].map(function (ins, ke) {
            dataArray.push({x: parseInt(data[1][ke].substring(0,3)), y: ins, r: ins})
          })
          var hex = randomHex.generate();
          obj = {
                    label: inst[1],
                    data: dataArray,
                    fill: true,
                    borderWidth: 1,
                    borderColor: hex,
                    backgroundColor: hexToRgbA(hex, 0.70),
                    hoverBackgroundColor: hexToRgbA(hex, 0.95)
                }
          return ds.push(obj);
        })
        var newData = {
                datasets: ds,
                labels: data[1]
            };   
        this.setState({
          data: newData,
          ready: true
        })     
        this.props.changeReceived(true);
    },
  render: function () {
    return this.state.ready ? (
      <BubbleChart data={this.state.data} options={this.state.options} />
    ) : null
  }
})

var PolaerChartConteiner = React.createClass({
  mixins: [TimerMixin],
  propTypes: {
    selectedX: PropTypes.number,
    startDate: PropTypes.string,
    endDate: PropTypes.string,
    segments: PropTypes.array,
    changeReceived: PropTypes.func,
    refetch: PropTypes.bool,
    fireChooseHint: PropTypes.func
  },
  getInitialState: function () {
    return {
      data: {
            datasets: [{
                    label: "Segment " + 0,
                    data: [1,2],
                    fill: true,
                    borderWidth: 3,
                    borderColor: hehValues[0][1],
                    pointBorderColor: hehValues[0][0],
                    pointBackgroundColor: hehValues[0][0],
                    pointRadius: 3,
                    pointHoverRadius: 5,
                    pointStyle: 'circle',
                    spanGaps: true,
                }],
            labels: [1,2]
        },
        options: {
            hover: {
                    mode: 'label'
                },
            tooltips: {
              mode: 'label'
            },
            legend: {
                    display: true,
                    position: 'top',
                },      
            defaultFontSize: 16,
            defaultFontColor: 'orange',
            responsiveAnimationDuration: 600,
            responsive: true
        },
        ready: false
    }
  },
  componentWillReceiveProps: function (nextProps) {
    if (nextProps.refetch) {
      this.fetchChartData(nextProps.startDate, nextProps.endDate, nextProps.selectedX, nextProps.segments);
    }
  },
  fetchChartData: function (start, end, x, segments) {
        var self = this;
        this.setTimeout( function () {
            if (start !== '' && end !== '') {
                if (this.props.segments.length > 0) {
                    var source = 'http://'+AppGlobal.host+'/api/chart/fetch_data/';
                    var project_ids = [parseInt(AppGlobal.d_1), parseInt(AppGlobal.d_2), parseInt(AppGlobal.d_3), parseInt(AppGlobal.d_4)];
                    var post_d = {start: start, end: end, x: x, 'segments': segments, 'user_id': parseInt(AppGlobal.user)};
                    $.ajax({
                       headers: { 
                         'Accept': 'application/json',
                         'Content-Type': 'application/json' 
                       },
                       type: "POST",
                       url: source,
                       data: JSON.stringify(post_d, null, '\t'),
                       dataType: 'json',
                         statusCode: {
                           200: function (data) {self.asigneFetchedData(data.allData)},
                           500: function (data) {},
                         } 
                    });
                } else {
                    this.props.fireChooseHint(2);
                }    
            } else {
                this.props.fireChooseHint(1);
            } 
        }, 200)
    },
    asigneFetchedData: function (data) {
        var ds = [];
        var mapDatasets = data[0].map(function (inst, key) {
          var obj = {};
          var colorArr = [];
          var hoverColorArr = [];
          var iter = inst[0].map( function (ins) {
            var hex = randomHex.generate();
            colorArr.push(hexToRgbA(hex, 0.70));
            hoverColorArr.push(hex);
          })
          obj = {
                    label: inst[1],
                    data: inst[0],
                    borderWidth: 1,
                    hoverBorderWidth: 2,
                    backgroundColor: colorArr,
                    hoverBackgroundColor: hoverColorArr
                }
          return ds.push(obj);
        })
        var newData = {
                datasets: ds,
                labels: data[1]
            };   
        this.setState({
          data: newData,
          ready: true
        })     
        this.props.changeReceived(true);
    },
  render: function () {
    return this.state.ready ? (
      <PolarChart data={this.state.data} options={this.state.options} />
    ) : null
  }
})

var RadarChartHolder = React.createClass({
  mixins: [TimerMixin],
  propTypes: {
    selectedX: PropTypes.number,
    startDate: PropTypes.string,
    endDate: PropTypes.string,
    segments: PropTypes.array,
    changeReceived: PropTypes.func,
    refetch: PropTypes.bool,
    fireChooseHint: PropTypes.func
  },
  getInitialState: function () {
    return {
      data: {
            datasets: [{
                    label: "Segment " + 0,
                    data: [1,2],
                    fill: true,
                    borderWidth: 3,
                    borderColor: hehValues[0][1],
                    pointBorderColor: hehValues[0][0],
                    pointBackgroundColor: hehValues[0][0],
                    pointRadius: 3,
                    pointHoverRadius: 5,
                    pointStyle: 'circle',
                    spanGaps: true,
                }],
            labels: [1,2]
        },
        options: {
            hover: {
                    mode: 'label'
                },
            tooltips: {
              mode: 'label'
            },
            legend: {
                    display: true,
                    position: 'top',
                },      
            defaultFontSize: 16,
            defaultFontColor: 'orange',
            responsiveAnimationDuration: 600,
            responsive: true
        },
        ready: false
    }
  },
  componentWillReceiveProps: function (nextProps) { 
    if (nextProps.refetch) {
      this.fetchChartData(nextProps.startDate, nextProps.endDate, nextProps.selectedX, nextProps.segments);
    }
  },
  fetchChartData: function (start, end, x, segments) {
        var self = this;
        this.setTimeout( function () {
            if (start !== '' && end !== '') {
                if (this.props.segments.length > 0) {
                    var source = 'http://'+AppGlobal.host+'/api/chart/fetch_data/';
                    var project_ids = [parseInt(AppGlobal.d_1), parseInt(AppGlobal.d_2), parseInt(AppGlobal.d_3), parseInt(AppGlobal.d_4)];
                    var post_d = {start: start, end: end, x: x, 'segments': segments, 'user_id': parseInt(AppGlobal.user)};
                    $.ajax({
                       headers: { 
                         'Accept': 'application/json',
                         'Content-Type': 'application/json' 
                       },
                       type: "POST",
                       url: source,
                       data: JSON.stringify(post_d, null, '\t'),
                       dataType: 'json',
                         statusCode: {
                           200: function (data) {self.asigneFetchedData(data.allData)},
                           500: function (data) {},
                         } 
                    });
                } else {
                    this.props.fireChooseHint(2);
                }    
            } else {
                this.props.fireChooseHint(1);
            } 
        }, 200)
    },
    asigneFetchedData: function (data) {
        var ds = [];
        var mapDatasets = data[0].map( function (inst, key) {
          var obj = {};
          var hex = randomHex.generate();
          obj = {
                    label: inst[1],
                    data: inst[0],
                    fill: true,
                    borderWidth: 1,
                    hoverBorderWidth: 2,
                    lineTension: 30,
                    pointBorderWidth: 2,
                    borderColor: hexToRgbA(hex, 0.90),
                    pointBackgroundColor: hex,
                    backgroundColor: hexToRgbA(hex, 0.40),
                    hoverBackgroundColor: hexToRgbA(hex, 0.80)
                }
          return ds.push(obj);
        })
        var newData = {
                datasets: ds,
                labels: data[1]
            };   
        this.setState({
          data: newData,
          ready: true
        })     
        this.props.changeReceived(true);
    },
  render: function () {
    return this.state.ready ? (
      <RadarChart data={this.state.data} options={this.state.options} />
    ) : null
  }
})

/////////////////////////
// need to update this //
/////////////////////////
var PieChartHolder = React.createClass({
  mixins: [TimerMixin],
  propTypes: {
    selectedX: PropTypes.number,
    startDate: PropTypes.string,
    endDate: PropTypes.string,
    segments: PropTypes.array,
    changeReceived: PropTypes.func,
    refetch: PropTypes.bool,
    fireChooseHint: PropTypes.func
  },
  getInitialState: function () {
    return {
      data: {
            datasets: [{
                    label: "Segment " + 0,
                    data: [1,2],
                    fill: true,
                    borderWidth: 3,
                    borderColor: hehValues[0][1],
                    pointBorderColor: hehValues[0][0],
                    pointBackgroundColor: hehValues[0][0],
                    pointRadius: 3,
                    pointHoverRadius: 5,
                    pointStyle: 'circle',
                    spanGaps: true,
                }],
            labels: [1,2]
        },
        options: {
            hover: {
                    mode: 'label'
                },
            tooltips: {
              mode: 'label'
            },
            legend: {
                    display: true,
                    position: 'top',
                },      
            defaultFontSize: 16,
            defaultFontColor: 'orange',
            responsiveAnimationDuration: 600,
            responsive: true
        },
        ready: false
    }
  },
  componentWillReceiveProps: function (nextProps) { 
    if (nextProps.refetch) {
      this.fetchChartData(nextProps.startDate, nextProps.endDate, nextProps.selectedX, nextProps.segments);
    }
  },
  fetchChartData: function (start, end, x, segments) {
        var self = this;
        this.setTimeout( function () {
            if (start !== '' && end !== '') {
                if (this.props.segments.length > 0) {
                    var source = 'http://'+AppGlobal.host+'/api/chart/fetch_data/';
                    var project_ids = [parseInt(AppGlobal.d_1), parseInt(AppGlobal.d_2), parseInt(AppGlobal.d_3), parseInt(AppGlobal.d_4)];
                    var post_d = {start: start, end: end, x: x, 'segments': segments, 'user_id': parseInt(AppGlobal.user)};
                    $.ajax({
                       headers: { 
                         'Accept': 'application/json',
                         'Content-Type': 'application/json' 
                       },
                       type: "POST",
                       url: source,
                       data: JSON.stringify(post_d, null, '\t'),
                       dataType: 'json',
                         statusCode: {
                           200: function (data) {self.asigneFetchedData(data.allData)},
                           500: function (data) {},
                         } 
                    });
                } else {
                    this.props.fireChooseHint(2);
                }    
            } else {
                this.props.fireChooseHint(1);
            } 
        }, 200)
    },
    asigneFetchedData: function (data) {
        var ds = [];
        var lab = [];
        var mapDatasets = data[0].map( function (inst, key) {
          var obj = {};
          var colorArr = [];
          var hoverColorArr = [];
          var iter = inst[0].map( function () {
            var hex = randomHex.generate();
            colorArr.push(hexToRgbA(hex, 0.60))
            hoverColorArr.push(hexToRgbA(hex, 0.99))
          })
          obj = {
                  label: inst[1],
                  data: inst[0], 
                  borderWidth: 1,
                  borderColor: '#fbfbfb',
                  backgroundColor: colorArr,
                  hoverBackgroundColor: hoverColorArr
                }
          return ds.push(obj);
        })
        var newData = {
                datasets: ds,
                labels: data[1]
            };   
        this.setState({
          data: newData,
          ready: true
        })     
        this.props.changeReceived(true);
    },
  render: function () {
    return this.state.ready ? (
      <PieChart data={this.state.data} options={this.state.options} />
    ) : null
  }
})

var HorizontalBarChartHolder = React.createClass({
  mixins: [TimerMixin],
  propTypes: {
    selectedX: PropTypes.number,
    startDate: PropTypes.string,
    endDate: PropTypes.string,
    segments: PropTypes.array,
    changeReceived: PropTypes.func,
    refetch: PropTypes.bool,
    fireChooseHint: PropTypes.func
  },
  getInitialState: function () {
    return {
      data: {
            datasets: [{
                    label: "Segment " + 0,
                    data: [1,2],
                    fill: true,
                    borderWidth: 3,
                    borderColor: hehValues[0][1],
                    pointBorderColor: hehValues[0][0],
                    pointBackgroundColor: hehValues[0][0],
                    pointRadius: 3,
                    pointHoverRadius: 5,
                    pointStyle: 'circle',
                    spanGaps: true,
                }],
            labels: [1,2]
        },
        options: {
            hover: {
                    mode: 'label'
                },
            tooltips: {
              mode: 'label'
            },
            legend: {
                    display: true,
                    position: 'top',
                },      
            defaultFontSize: 16,
            defaultFontColor: 'orange',
            responsiveAnimationDuration: 600,
            responsive: true
        },
        ready: false
    }
  },
  componentWillReceiveProps: function (nextProps) { 
    if (nextProps.refetch) {
      this.fetchChartData(nextProps.startDate, nextProps.endDate, nextProps.selectedX, nextProps.segments);
    }
  },
  fetchChartData: function (start, end, x, segments) {
        var self = this;
        this.setTimeout( function () {
            if (start !== '' && end !== '') {
                if (this.props.segments.length > 0) {
                    var source = 'http://'+AppGlobal.host+'/api/chart/fetch_data/';
                    var project_ids = [parseInt(AppGlobal.d_1), parseInt(AppGlobal.d_2), parseInt(AppGlobal.d_3), parseInt(AppGlobal.d_4)];
                    var post_d = {start: start, end: end, x: x, 'segments': segments, 'user_id': parseInt(AppGlobal.user)};
                    $.ajax({
                       headers: { 
                         'Accept': 'application/json',
                         'Content-Type': 'application/json' 
                       },
                       type: "POST",
                       url: source,
                       data: JSON.stringify(post_d, null, '\t'),
                       dataType: 'json',
                         statusCode: {
                           200: function (data) {self.asigneFetchedData(data.allData)},
                           500: function (data) {},
                         } 
                    });
                } else {
                    this.props.fireChooseHint(2);
                }    
            } else {
                this.props.fireChooseHint(1);
            } 
        }, 200)
    },
    asigneFetchedData: function (data) {
        var ds = [];
        var mapDatasets = data[0].map( function (inst, key) {
          var obj = {};
          var hex = randomHex.generate();
          obj = {
                    label: inst[1],
                    data: inst[0],
                    fill: true,
                    borderWidth: 1,
                    hoverBorderWidth: 2,
                    borderColor: hex,
                    backgroundColor: hexToRgbA(hex, 0.41),
                    borderSkipped: 'bottom',
                    hoverBackgroundColor: hexToRgbA(hex, 0.81)
                }
          return ds.push(obj);
        })
        var newData = {
                datasets: ds,
                labels: data[1]
            };   
        this.setState({
          data: newData,
          ready: true
        })     
        this.props.changeReceived(true);
    },
  render: function () {
    return this.state.ready ? (
      <HorizontalBar data={this.state.data} options={this.state.options} />
    ) : null
  }
})

var BarChartHolder = React.createClass({
  mixins: [TimerMixin],
  propTypes: {
    selectedX: PropTypes.number,
    startDate: PropTypes.string,
    endDate: PropTypes.string,
    segments: PropTypes.array,
    changeReceived: PropTypes.func,
    refetch: PropTypes.bool,
    fireChooseHint: PropTypes.func
  },
  getInitialState: function () {
    return {
      data: {
            datasets: [{
                    name: "Segment " + 0,
                    label: "Segment " + 0,
                    data: [1,2],
                    fill: true,
                    borderWidth: 3,
                    borderColor: hehValues[0][1],
                    pointBorderColor: hehValues[0][0],
                    pointBackgroundColor: hehValues[0][0],
                    pointRadius: 3,
                    pointHoverRadius: 5,
                    pointStyle: 'circle',
                    spanGaps: true,
                }],
            labels: [1,2]
        },
        options: {
            hover: {
                    mode: 'label'
                },
            tooltips: {
              mode: 'label'
            }, 
            legend: {
                    display: true,
                    position: 'top',
                },      
            defaultFontSize: 16,
            defaultFontColor: 'orange',
            responsiveAnimationDuration: 600,
            responsive: true
        },
        ready: false
    }
  },
  componentWillReceiveProps: function (nextProps) { 
    if (nextProps.refetch) {
      this.fetchChartData(nextProps.startDate, nextProps.endDate, nextProps.selectedX, nextProps.segments);
    }
  },
  fetchChartData: function (start, end, x, segments) {
        var self = this;
        this.setTimeout( function () {
            if (start !== '' && end !== '') {
                if (this.props.segments.length > 0) {
                    var source = 'http://'+AppGlobal.host+'/api/chart/fetch_data/';
                    var project_ids = [parseInt(AppGlobal.d_1), parseInt(AppGlobal.d_2), parseInt(AppGlobal.d_3), parseInt(AppGlobal.d_4)];
                    var post_d = {start: start, end: end, x: x, 'segments': segments, 'user_id': parseInt(AppGlobal.user)};
                    $.ajax({
                       headers: { 
                         'Accept': 'application/json',
                         'Content-Type': 'application/json' 
                       },
                       type: "POST",
                       url: source,
                       data: JSON.stringify(post_d, null, '\t'),
                       dataType: 'json',
                         statusCode: {
                           200: function (data) {self.asigneFetchedData(data.allData)},
                           500: function (data) {},
                         } 
                    });
                } else {
                    this.props.fireChooseHint(2);
                }    
            } else {
                this.props.fireChooseHint(1);
            } 
        }, 200)
    },
    asigneFetchedData: function (data) {
        var ds = [];
        var mapDatasets = data[0].map( function (inst, key) {
          var obj = {};
          var hex = randomHex.generate();
          obj = {
                    label: inst[1],
                    data: inst[0],
                    fill: true,
                    borderWidth: 1,
                    hoverBorderWidth: 2,
                    borderColor: hex,
                    backgroundColor: hexToRgbA(hex, 0.41),
                    borderSkipped: 'bottom',
                    hoverBackgroundColor: hexToRgbA(hex, 0.81)
                }
          return ds.push(obj);
        })
        var newData = {
                datasets: ds,
                labels: data[1]
            };   
        this.setState({
          data: newData,
          ready: true
        })     
        this.props.changeReceived(true);
    },
  render: function () {
    return this.state.ready ? (
      <BarChart data={this.state.data} options={this.state.options} />
    ) : null
  }
})

var LineChartHolder = React.createClass({
  mixins: [TimerMixin],
  propTypes: {
    selectedX: PropTypes.number,
    startDate: PropTypes.string,
    endDate: PropTypes.string,
    segments: PropTypes.array,
    changeReceived: PropTypes.func,
    refetch: PropTypes.bool,
    fireChooseHint: PropTypes.func
  },
  getInitialState: function () {
    return {
      data: {
            datasets: [{
                    label: "Segment " + 0,
                    data: [1,2],
                    fill: true,
                    borderWidth: 3,
                    borderColor: hehValues[0][1],
                    pointBorderColor: hehValues[0][0],
                    pointBackgroundColor: hehValues[0][0],
                    pointRadius: 3,
                    pointHoverRadius: 5,
                }],
            labels: [1,2]
        },
        options: {
            hover: {
                    mode: 'label'
                },
            tooltips: {
              mode: 'x-axis'
            },    
            legend: {
                    display: true,
                    position: 'top',
                },      
            defaultFontSize: 16,
            defaultFontColor: 'orange',
            responsiveAnimationDuration: 600,
            responsive: true
        },
        ready: false
    }
  },
  componentWillReceiveProps: function (nextProps) {
    if (nextProps.refetch) {
      this.fetchChartData(nextProps.startDate, nextProps.endDate, nextProps.selectedX, nextProps.segments);
    }
  },
  fetchChartData: function (start, end, x, segments) {
        var self = this;
        this.setTimeout( function () {
            if (start !== '' && end !== '') {
                if (this.props.segments.length > 0) {
                    var source = 'http://'+AppGlobal.host+'/api/chart/fetch_data/';
                    var project_ids = [parseInt(AppGlobal.d_1), parseInt(AppGlobal.d_2), parseInt(AppGlobal.d_3), parseInt(AppGlobal.d_4)];
                    var post_d = {start: start, end: end, x: x, 'segments': segments, 'user_id': parseInt(AppGlobal.user)};
                    $.ajax({
                       headers: { 
                         'Accept': 'application/json',
                         'Content-Type': 'application/json' 
                       },
                       type: "POST",
                       url: source,
                       data: JSON.stringify(post_d, null, '\t'),
                       dataType: 'json',
                         statusCode: {
                           200: function (data) {self.asigneFetchedData(data.allData)},
                           500: function (data) {},
                         } 
                    });
                } else {
                    this.props.fireChooseHint(2);
                }    
            } else {
                this.props.fireChooseHint(1);
            } 
        }, 200)
    },
    asigneFetchedData: function (data) {
        var ds = [];
        var mapDatasets = data[0].map( function (inst) {
          var obj = {};
          var hex = randomHex.generate();
          obj = {
                    label: inst[1],
                    data: inst[0],
                    fill: true,
                    borderWidth: 3,
                    borderColor: hexToRgbA(hex, 0.80),
                    pointBorderColor: hex,
                    pointBackgroundColor: hexToRgbA(hex, 0.40),
                    pointRadius: 4,
                    pointHoverRadius: 6,
                    pointBorderWidth: 2,
                    pointHoverBorderWidth: 4,
                    pointStyle: 'circle'
                }
          return ds.push(obj);
        })
        var newData = {
                datasets: ds,
                labels: data[1]
            };   
        this.setState({
          data: newData,
          ready: true
        })     
        this.props.changeReceived(true);
    },
  exportR: function () {
    CanvasToImage.saveAsPNG(document.getElementById("myChart"), 900, 500);
  },  
  render: function () {
    return this.state.ready ? (
      <LineChart data={this.state.data} options={this.state.options} />
    ) : null
  }
})


function hexToRgbA(hex, opacity){
    var c;
    if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
        c= hex.substring(1).split('');
        if(c.length== 3){
            c= [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c= '0x'+c.join('');
        return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+ ','+opacity+')';
    }
    throw new Error('Bad Hex');
}



const CHARTS = [{ value: '1', label: 'Line', image: 'line.png'}, { value: '2', label: 'Bar', image: 'line.png'}, 
  { value: '3', label: 'HorizontalBar', image: 'line.png'}, { value: '4', label: 'Pie', image: 'line.png'}, 
  { value: '5', label: 'Radar', image: 'line.png'}, { value: '6', label: 'Polar', image: 'line.png'}, 
  { value: '7', label: 'Bubble', image: 'line.png'}];

var ChartType = React.createClass({
  propTypes: {
    children: PropTypes.node,
    className: PropTypes.string,
    isDisabled: PropTypes.bool,
    isFocused: PropTypes.bool,
    isSelected: PropTypes.bool,
    onFocus: PropTypes.func,
    onSelect: PropTypes.func,
    option: PropTypes.object,
  },
  handleMouseDown: function (event) {
    event.preventDefault();
    event.stopPropagation();
    this.props.onSelect(this.props.option, event);
  },
  handleMouseEnter: function (event) {
    this.props.onFocus(this.props.option, event);
  },
  handleMouseMove: function (event) {
    if (this.props.isFocused) return;
    this.props.onFocus(this.props.option, event);
  },
  render: function () {
    var gravatarStyle = {
      borderRadius: 3,
      display: 'inline-block',
      marginRight: 10,
      position: 'relative',
      top: -2,
      verticalAlign: 'middle',
      width: '13%'
    };
    return (
      <div style={{"textAlign": "left"}} className={this.props.className}
        onMouseDown={this.handleMouseDown}
        onMouseEnter={this.handleMouseEnter}
        onMouseMove={this.handleMouseMove}
        title={this.props.option.title}>
        {this.props.children}
      </div>
    )
  },
})

const ChartValue = React.createClass({
  propTypes: {
    children: PropTypes.node,
    placeholder: PropTypes.string,
    value: PropTypes.object
  },
  render: function () {
    var gravatarStyle = {
      borderRadius: 3,
      display: 'inline-block',
      marginRight: 10,
      position: 'relative',
      top: -2,
      verticalAlign: 'middle',
      width: '13%'
    };
    return (
      <div className="Select-value" style={{"textAlign": "left"}} title={this.props.value.title}>
        <span className="Select-value-label">
          {this.props.children}
        </span>
      </div>
    );
  }
});

var CalendarConteiner = React.createClass({
    mixins: [TimerMixin],
    propTypes: {  
        updateDates: PropTypes.func,
    },
    getInitialState: function () {
        return {
            linked: {}
        }
    },
    handleDateChange: function (which, payload) {
        this.setState({
          [which]:payload,
        });
        this.setTimeout( function () {this.sendDatesToApi();}, 1000)
    },
    handleInitDateChange: function (which, payload) {
        this.setState({
          [which]:payload,
        });
    },
    sendDatesToApi: function () {
      var self = this;
      const format = 'YYYY-MM-DD';
      const format_2 = 'DD-MM-YYYY';
      var start = this.state.linked['startDate'].format(format).toString();
      var end = this.state.linked['endDate'].format(format).toString();
      var total = this.state.linked['startDate'].format(format_2).toString() + ' – ' + this.state.linked['endDate'].format(format_2).toString();;
      this.props.updateDates(start, end, total);
    },
    render: function () {
        return (
            <DateRange
                firstDayOfWeek={1}
                onInit={ this.handleInitDateChange.bind(this, 'linked') }
                onChange={ this.handleDateChange.bind(this, 'linked') }
                calendars={2}
                linkedCalendars={ true }
                theme={{ 
                  DateRange      : {
                    border       : '1px solid #272727',
                    boxShadow    : '0px 2px 7px rgba(0,0,0,0.25)',
                    width        : '100%',
                    borderRadius : '5px'
                  },
                  Calendar       : {
                    background   : 'transparent',
                    color        : '#95a5a6',
                    width        : '200px'
                  },
                  MonthButton    : {
                    background   : '#fcb326'
                  },
                  MonthArrowPrev : {
                    borderRightColor : '#ffffff',
                  },
                  MonthArrowNext : {
                    borderLeftColor : '#ffffff',
                  },
                  Day            : {
                    transition   : 'transform .1s ease, box-shadow .1s ease, background .1s ease'
                  },
                  DaySelected    : {
                    background   : '#fcb326'
                  },
                  DayActive    : {
                    background   : '#fcb326',
                    boxShadow    : 'none'
                  },
                  DayInRange     : {
                    background   : '#489fd9',
                    color        : '#fff'
                  },
                  DayHover       : {
                    background   : '#ffffff',
                    color        : '#fcb326',
                    transform    : 'scale(1.1) translateY(-10%)',
                    boxShadow    : '0 2px 4px rgba(0, 0, 0, 0.4)'
                  }
                }}
            />
        )
    }
})

module.exports = ChartBlockConteiner;