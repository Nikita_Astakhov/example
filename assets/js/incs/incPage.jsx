var React = require('react');
var ReactDom = require('react-dom');
var PropTypes = React.PropTypes;
var classNames = require('classnames');

var Utils = require('../Utils');
var csrftoken = Utils.getCookie('csrftoken');

var IncData = require('./components/IncData');


var IncidentConteiner = React.createClass({
	render: function () {
		return (
			<div>
                <IncData />
            </div>
		)
	}
})

module.exports = IncidentConteiner;
