var React = require('react')
var ReactDom = require('react-dom')
var PropTypes = React.PropTypes;
var classNames = require('classnames');
var TimerMixin = require('react-timer-mixin');
var Select = require('react-select');
require('react-select/dist/react-select.css');
import DatePicker from 'react-datepicker';
import moment from 'moment';

import 'react-datepicker/dist/react-datepicker.css'; 

var IncidentsBody = React.createClass({
	mixins: [TimerMixin],
	getInitialState: function () {
		return {
			status500: false,
			status200: false,
			rerenderList: false
		}
	},
	showHint: function (what) {
		var self = this;
		if (what === 1) {
			this.setState({
				status200: !this.state.status200
			}, self.hideHint(1));
		} else {
			this.setState({
				status500: !this.state.status500
			}, self.hideHint(2));
		}
	},
	hideHint: function (what) {
		this.setTimeout(function () {
			if (what === 1) {
				this.setState({
					status200: !this.state.status200
				});
			} else {
				this.setState({
					status500: !this.state.status500
				});
			}
		}, 3000)
	},
	rerenderLists: function () {
		this.setState({rerenderList: !this.state.rerenderList});
	},
	render: function () {
		var status200Class = classNames({
			'status200': true,
			'hidden': !this.state.status200
		});
		var status500Class = classNames({
			'status500': true,
			'hidden': !this.state.status500
		});
		return (
			<div>
			<div className="statusBlock">
				<div className={status200Class}>
				Record successfully created
				</div>
				<div className={status500Class}>
				Something went wrong. Please check inputted data.
				</div>
			</div>
			<div className="w-container content">
			    <NewRecordConteiner rer={this.rerenderLists} hint={this.showHint} />
			    <ListIncindentsConteiner rer={this.state.rerenderList} hint={this.showHint} />
			</div>
			</div>
		)
	}
});

var nameLength = 100;

var NewRecordConteiner = React.createClass({
	propTypes: {  
		hint: PropTypes.func.isRequired,
		rer: PropTypes.func.isRequired, 
	},
	getInitialState: function () {
		return {
			formBlock: true,
			buttonText: 'Add new record',
			name: '',
			sources: [],
			sourceSelected: '',
			date: '',
			displayDate: '',
			description: '',
		}
	},
	componentWillMount: function () {
		var source = 'http://'+AppGlobal.host + "/api/incidents/sources/list/?format=json";
		this.serverRequest = $.get(source, function (results) {
			var sources_list = results.results;
			this.setState({
			  sources: sources_list,
			});
		}.bind(this));
	},
	toggleForm: function () {
		this.setState({
			formBlock: !this.state.formBlock,
		});
		if (this.state.buttonText === 'Add new record') {
			this.setState({
				buttonText: 'cancel'
			});
		} else {
			this.setState({
				buttonText: 'Add new record',
			});
		}
	},
	hendleInputName: function (event) {
		var self = this;
		this.setState({
			name: event.target.value.substring(0, 99)
		}, self.updateLength());
	},
	updateLength: function () {
		nameLength = 99 - (this.state.name.length);
	},
	handleStartChange(date) {
		const format = 'YYYY-MM-DD';
		var start = date.format(format).toString();
    	this.setState({
    	  date: start,
    	  displayDate: date
    	});
    },
	hendleInputSource: function (value) {
		this.setState({
			sourceSelected: value
		});
		var self = this;
	},
	hendleInputDescription: function (event) {
		this.setState({
			description: event.target.value
		});
	},
	createNewRecord: function () {
		var self = this;
		var source = 'http://'+AppGlobal.host+'/api/incidents/create/';
		var post_d = {name: this.state.name, 'user_id': parseInt(AppGlobal.user), date:this.state.date, description: this.state.description, source:this.state.sourceSelected.value};
		$.ajax({
			headers: { 
			  'Accept': 'application/json',
			  'Content-Type': 'application/json' 
			},
			type: "POST",
			url: source,
			data: JSON.stringify(post_d, null, '\t'),
			dataType: 'json',
			statusCode: {
			  200: function () {self.toggleForm(); self.props.hint(1); self.props.rer(); self.clearData();},
			  500: function () {self.props.hint(2);},
			}
		});
	},
	clearData: function () {
		this.setState({
			name: '',
			date: '',
			description: '',
			sourceSelected: ''
		});
		nameLength = 100;
	},
	handleSubmit: function (event) {
        event.preventDefault();
    },
	render: function () {
		var formClass = classNames({
			'addnewrecord__fromwrapper': true,
			'hidden': this.state.formBlock
		});
		var buttonClass = classNames({
			'w-button button newrecoed': true,
			'close': !this.state.formBlock
		});
		var p_state = <span>Select source</span>;
		return (
			<div className="addnewrecoed">
			 <div>
			  <a onClick={this.toggleForm} className={buttonClass}>{this.state.buttonText}</a>
			 </div> 
              <div className={formClass} >
                <form onSubmit={this.handleSubmit}>
                  <div className="w-row">
                    <div className="w-col w-col-7">
                      <label className="newincidentform__label" htmlFor="name">Title or short description:</label>
                      <input onInput={this.hendleInputName} className="w-input newincidentform__input" maxLength="256" placeholder="Enter short description" type="text" value={this.state.name}/>
                    </div>
                    <div className="w-col w-col-5">
                      <h4 className="newincidentform__hint">{nameLength} characters with gaps left</h4>
                    </div>
                  </div>
                  <div className="w-row">
                    <div className="w-col w-col-7">
                      <label className="newincidentform__label">Date of report</label>
                      <DatePicker
                          dateFormat='DD-MM-YYYY'
                          selected={this.state.displayDate}
                          onChange={this.handleStartChange}
                          peekNextMonth
                          showMonthDropdown
                          showYearDropdown
                          dropdownMode="select"
                          calendarClassName="calClass"
                          placeholderText="Choose date"
                          className="w-input listincidents__form_input"
                      />
                    </div>
                    <div className="w-col w-col-5"></div>
                  </div>
                  <div className="w-row">
                    <div className="w-col w-col-7">
                      <div className="existingsource">
                        <label className="newincidentform__label">Source</label>
                        <Select
                          onChange={this.hendleInputSource}
                          optionComponent={ChartType}
                          options={this.state.sources}
                          placeholder={p_state}
                          value={this.state.sourceSelected}
                          valueComponent={ChartValue}
                          clearable={false}
                        />
                      </div>
                  </div>
                  </div>
                  <label className="newincidentform__label">Description</label>
                  <textarea onChange={this.hendleInputDescription} className="w-input newincidentform__input text" value={this.state.description} placeholder="Enter description here"/>
                  <input onClick={this.createNewRecord} className="w-button button newrecoed submimt" type="button" value="Submit"/>
                </form>
              </div>
            </div>
		)
	}
})
var weeksPageNUmber = 1;
var ListIncindentsConteiner = React.createClass({
	mixins: [TimerMixin],
	propTypes: {  
		hint: PropTypes.func.isRequired,
		rer: PropTypes.bool.isRequired,
	},
	getInitialState: function () {
		return {
			filterChoosed: 2,
			filterName: true,
			filterId: false,
			data: [],
			searchName: '',
			searchId: '',
			showPaginate: false,
            maxPage: 1,
		}
	},
	componentWillMount: function () {
		var source = 'http://'+AppGlobal.host + "/api/incidents/list/start/?format=json;page="+weeksPageNUmber;
		this.serverRequest = $.get(source, function (results) {
			var inc_list = results.results;
			if (results.count > 30) {
				var s = true
				this.calculatePageRange(results.count);
			} else {
				var s = false
			}
			this.setState({
			  data: inc_list,
			  showPaginate: s
			});
		}.bind(this));
	},
	componentWillReceiveProps: function (nextProps) {
		var source = 'http://'+AppGlobal.host + "/api/incidents/list/start/?format=json;page="+weeksPageNUmber;
		this.serverRequest = $.get(source, function (results) {
			var inc_list = results.results;
			if (results.count > 30) {
				var s = true
				this.calculatePageRange(results.count);
			} else {
				var s = false
			}
			this.setState({
			  data: inc_list,
			  showPaginate: s
			});
		}.bind(this));
	},
	changeFilterPage: function (event) {
		var fil = this.state.filterChoosed
		var m = this.state.maxPage
		var p = event.target.value;
		if (p === '' || p === '0') {
			p = 1;
		}
		if (p > m) {
			p = m;
		}
		weeksPageNUmber = p;
		if (this.state.searchId === '') {
            fil = 3;
        }
        if (fil === 1) {
            this.updateDataByName();
        } else if (fil === 2) {
            this.updateDataById();
        } else {
            this.updateStartList();
        }
    },
    updateStartList: function () {
        var source = 'http://'+AppGlobal.host + "/api/incidents/list/start/?format=json;page="+weeksPageNUmber;
		this.serverRequest = $.get(source, function (results) {
			var inc_list = results.results;
			if (results.count > 30) {
				var s = true
				this.calculatePageRange(results.count);
			} else {
				var s = false
			}
			this.setState({
			  data: inc_list,
			  showPaginate: s
			});
		}.bind(this));
    },
    calculatePageRange: function (number) {
        var result = Math.ceil(number / 30);
        this.setState({
            maxPage: result,
        });
    },
	hendleFilterChange: function (event) {
		weeksPageNUmber = 1;
		var self = this;
		this.setState({
			filterChoosed: parseInt(event.target.value),
		}, self.changeFilterBlock(parseInt(event.target.value)));
	},
	changeFilterBlock: function (what) {
		if (what === 1) {
			this.setState({
				filterName: false,
				filterId: true
			});
		} else {
			this.setState({
				filterName: true,
				filterId: false
			});
		}
	},
	hendleFilterName: function (event) {
		weeksPageNUmber = 1;
		var self = this;
		var v = event.target.value
		this.setState({
			searchName: v,
		});
		if (v != '') {
            self.updateDataByName()
        } else {
            self.updateStartList()
        }
	},
	updateDataByName: function () {
		this.setTimeout( function () {
			var source = 'http://'+AppGlobal.host + "/api/incidents/list/name/"+ this.state.searchName +"/?format=json;page="+weeksPageNUmber;
			this.serverRequest = $.get(source, function (results) {
				var inc_list = results.results;
				if (results.count > 30) {
			    	var s = true
			    	this.calculatePageRange(results.count);
			    } else {
			    	var s = false
			    }
			    this.setState({
			      data: inc_list,
			      showPaginate: s
			    });
			}.bind(this));
		}, 300);
	},
	hendleFilterId: function (event) {
		weeksPageNUmber = 1;
		var self = this;
		this.setState({
			searchId: event.target.value,
		}, self.updateDataById());
	},
	updateDataById: function () {
		this.setTimeout( function () {
			var source = 'http://'+AppGlobal.host + "/api/incidents/list/id/"+ this.state.searchId +"/?format=json;page="+weeksPageNUmber;
			this.serverRequest = $.get(source, function (results) {
				var inc_list = results.results;
				if (results.count > 30) {
			    	var s = true
			    	this.calculatePageRange(results.count);
			    } else {
			    	var s = false
			    }
			    this.setState({
			      data: inc_list,
			      showPaginate: s
			    });
			}.bind(this));
		}, 200)
	},
	getStartList: function () {
		weeksPageNUmber = 1;
		this.updateStartList();
		this.setState({
			searchName: '',
			searchId: '',
		});
	},
	handleSubmit: function (event) {
        event.preventDefault();
    },
	render: function () {
		var self = this;
		var fName = classNames({
			'filternames': true,
			'hiddenfilter': this.state.filterName
		});
		var fId = classNames({
			'filternames': true,
			'hiddenfilter': this.state.filterId
		});
		var listIncidents = this.state.data.map( function (inc) {
			return (<IncidentInstance hint={self.props.hint} key={inc.id} data={inc} />)
		});
		var paginator;
        if (this.state.showPaginate) {
            paginator = (
                <div className="paginationconteiner" style={{'marginTop': 20 + 'px'}}>
                <div className="w-container">
                  <div className="w-row">
                    <div className="w-col w-col-3"></div>
                    <div className="w-col w-col-6">
                      <div className="w-row">
                        <div className="w-col w-col-6">
                          <h4 className="listincidents__filterhead midpostion">Page range: 1 - {this.state.maxPage}</h4>
                        </div>
                        <div className="w-col w-col-6">
                          <div className="filterpages">
                            <form onSubmit={this.handleSubmit}>
                              <input onChange={this.changeFilterPage} className="w-input listincidents__form_input" min="1" placeholder="Input page number here" type="number"/>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="w-col w-col-3"></div>
                  </div>
                </div>
              </div>
            )
        }
		return (
			<div className="listincidents__conteiner">
              <h1 className="listhead">List of incident descriptions</h1>
              <div className="listincidents__filterconteiner">
                <div className="w-row">
                  <div className="w-col w-col-6">
                    <div className="w-row">
                      <div className="w-col w-col-2">
                        <h4 className="listincidents__filterhead">Filter by</h4>
                      </div>
                      <div className="w-col w-col-10">
                        <div>
                          <form onSubmit={this.handleSubmit}>
                            <select onChange={this.hendleFilterChange} value={this.state.filterChoosed} className="w-select filteroptionfield">
                              <option value="1">Title or short description</option>
                              <option value="2">ID</option>
                            </select>
                          </form>
                        </div>
                      </div>
                    </div>
                    <div className={fName}>
                      <form onSubmit={this.handleSubmit}>
                        <div className="w-row">
                          <div className="w-col w-col-11">
                            <div className="w-row newsource">
                              <div className="w-col w-col-9">
                                <label className="listincidents__form_label">Input incident title or short description</label>
                                <input onChange={this.hendleFilterName} className="w-input listincidents__form_input" value={this.state.searchName} placeholder="Input title here"/>
                              </div>
                              <div className="w-col w-col-3">
                                <input onClick={this.getStartList} className="w-button listincidents__form_submit" type="button" value="Clear"/>
                              </div>
                            </div>  
                          </div>
                          <div className="w-col w-col-1"></div>
                        </div>
                      </form>
                    </div>
                    <div className={fId}>
                      <form onSubmit={this.handleSubmit}>
                        <div className="w-row">
                          <div className="w-col w-col-11">
                            <div className="w-row newsource">
                              <div className="w-col w-col-9">
                                <label className="listincidents__form_label">Input incident ID</label>
                                <input onChange={this.hendleFilterId} className="w-input listincidents__form_input" value={this.state.searchId} placeholder="Input ID here" />
                              </div>
                              <div className="w-col w-col-3">
                                <input onClick={this.getStartList} className="w-button listincidents__form_submit" type="button" value="Clear"/>
                              </div>  
                            </div>
                          </div>
                          <div className="w-col w-col-1"></div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <div className="w-col w-col-6"></div>
                </div>
              </div>
              <div className="listincidents_instancesconteiner">
                <div className="w-row listincidents__rowheading">
                  <div className="w-col w-col-2">
                    <h4 className="listincidents__filterhead midpostion">ID</h4>
                  </div>
                  <div className="w-col w-col-6">
                    <h4 className="listincidents__filterhead midpostion">Title or short description</h4>
                  </div>
                  <div className="w-col w-col-2">
                    <h4 className="listincidents__filterhead midpostion">Recorded on</h4>
                  </div>
                  <div className="w-col w-col-2">
                    <h4 className="listincidents__filterhead midpostion">Action</h4>
                  </div>
                </div>
                {listIncidents}
              </div>
              {paginator}
            </div>
		)
	}
});

var IncidentInstance = React.createClass({
	propTypes: {  
		hint: PropTypes.func.isRequired,
		data: PropTypes.object.isRequired,
	},
	getInitialState: function () {
		return {
			edit: false,
			buttonText: 'edit',
			name: ''
		}
	},
	componentWillMount: function () {
		this.setState({
			name: this.props.data.name 
		});
	},
	hendleEdit: function () {
		this.setState({edit: !this.state.edit});
		if (this.state.buttonText === 'edit') {
			this.setState({buttonText: 'cancel'});
			this.renderEditBlock();
		} else {
			this.setState({buttonText: 'edit'});
			this.unmountEditBlock();
		}
	},
	renderEditBlock: function () {
		ReactDom.render(<EditInstance changeButton={this.changeButtonText} changeName={this.updateName} hint={this.props.hint} number={this.props.data.id} key={'instance_'+this.props.data.id} />, this.refs.editBlock);
	},
	unmountEditBlock: function () {
		ReactDom.unmountComponentAtNode(this.refs.editBlock);
	},
	updateName: function (nameString) {
		this.setState({
			name: nameString
		});
	},
	changeButtonText: function () {
		this.setState({buttonText: 'close'});
	},
	render: function () {
		var blockClass = classNames({
			'listincidents__instanceconteiner': true,
			'activeedit': this.state.edit
		});
		return (
			<div className={blockClass}>
              <div className="w-row">
                <div className="w-col w-col-2">
                  <h4 className="listincidents__instance_head">{this.props.data.id}</h4>
                </div>
                <div className="w-col w-col-6">
                  <h4 className="listincidents__instance_head">{this.state.name}</h4>
                </div>
                <div className="w-col w-col-2">
                  <h4 className="listincidents__instance_head">{this.props.data.creation_date}</h4>
                </div>
                <div className="w-col w-col-2">
                  <h4 onClick={this.hendleEdit} className="listincidents__instance_head editbutton" style={{'cursor':'pointer'}}>{this.state.buttonText}</h4>
                </div>
              </div>
              <div ref="editBlock"></div>
            </div>
		)
	}
})

var nameEditLength = 100;

var EditInstance = React.createClass({
	propTypes: {  
		hint: PropTypes.func.isRequired,
		number: PropTypes.number.isRequired,
		changeName: PropTypes.func.isRequired,
		changeButton: PropTypes.func.isRequired,
	},
	getInitialState: function () {
		return {
			name: '',
			newSource: false,
			sources: [],
			sourceSelected: '',
			description: '',
		}
	},
	componentDidMount: function () {
		var source = 'http://'+AppGlobal.host + "/api/incidents/sources/list/?format=json";
		this.serverRequest = $.get(source, function (results) {
			var sources_list = results.results;
			this.setState({
			  sources: sources_list,
			});
		}.bind(this));
		var source_2 = 'http://'+AppGlobal.host + "/api/incidents/data/"+ this.props.number +"/?format=json";
		this.serverRequest = $.get(source_2, function (resultes) {
			var inc_data = resultes.results[0];
			this.setState({
			  name: inc_data.name,
			  sourceSelected: inc_data.source,
			  description: inc_data.description
			}, this.calculateLength(inc_data.name));
		}.bind(this));
	},
	calculateLength: function (nameStr) {
		nameEditLength = 99 - nameStr.length;
	},
	hendleInputName: function (event) {
		var self = this;
		this.setState({
			name: event.target.value.substring(0, 99)
		}, self.updateLength());
	},
	updateLength: function () {
		nameEditLength = 99 - (this.state.name.length);
	},
	toogleNewSource: function () {
		this.setState({
			newSource: !this.state.newSource
		});
		if (this.state.sourceButton === 'Add new source') {
			this.setState({
				sourceButton: 'cancel'
			});
		} else {
			this.setState({
				sourceButton: 'Add new source',
			});
		}
	},
	hendleInputSource: function (value) {
		this.setState({
			sourceSelected: value
		});
	},
	hendleNewSource: function (event) {
		this.setState({
			newSorceName: event.target.value
		});
	},
	hendleInputDescription: function (event) {
		this.setState({
			description: event.target.value
		});
	},
	createNewRecord: function () {
		var self = this;
		var source = 'http://'+AppGlobal.host+'/api/incidents/update/';
		var checked_source;
		if (typeof this.state.sourceSelected === 'number') {
			checked_source = this.state.sourceSelected;
		} else {
			checked_source = this.state.sourceSelected.value;
		}
		var post_d = {name: this.state.name, 'user_id': parseInt(AppGlobal.user), 'inc_id':this.props.number, description: this.state.description, source:checked_source};
		$.ajax({
			headers: { 
			  'Accept': 'application/json',
			  'Content-Type': 'application/json' 
			},
			type: "POST",
			url: source,
			data: JSON.stringify(post_d, null, '\t'),
			dataType: 'json',
			statusCode: {
			  200: function () {self.props.changeName(self.state.name); self.props.changeButton(); self.props.hint(1);},
			  500: function () {self.props.hint(2); self.props.changeButton();},
			}
		});
	},
	handleSubmit: function (event) {
        event.preventDefault();
    },
	render: function () {
		var buttonClass = classNames({
			'w-button button newrecoed': true,
			'close': !this.state.formBlock
		});
		var sourceClass = classNames({
			'existingsource': true,
			'hiddeninstance': this.state.newSource
		});
		var p_state = <span>Select source</span>;
		return (
			<div className="addnewrecord__fromwrapper editform">
              <form onSubmit={this.handleSubmit}>
                  <div className="w-row">
                    <div className="w-col w-col-7">
                      <label className="newincidentform__label">Title or short description:</label>
                      <input onInput={this.hendleInputName} className="w-input newincidentform__input" placeholder="Enter short description" type="text" value={this.state.name}/>
                    </div>
                    <div className="w-col w-col-5">
                      <h4 className="newincidentform__hint">{nameEditLength} characters with gaps left</h4>
                    </div>
                  </div>
                  <div className="w-row">
                    <div className="w-col w-col-7">
                      <div className="existingsource">
                        <label className="newincidentform__label">Source</label>
                        <Select
                          onChange={this.hendleInputSource}
                          optionComponent={ChartType}
                          options={this.state.sources}
                          placeholder={p_state}
                          value={this.state.sourceSelected}
                          valueComponent={ChartValue}
                          clearable={false}
                        />
                      </div>
                    </div>
                    <div className="w-col w-col-5">
                    </div>
                  </div>
                  <label className="newincidentform__label">Description</label>
                  <textarea onChange={this.hendleInputDescription} className="w-input newincidentform__input text" value={this.state.description} placeholder="Enter description here"/>
                  <input onClick={this.createNewRecord} className="w-button button newrecoed submimt" type="button" value="Update" style={{'marginBottom': 15 + 'px'}}/>
                </form>
            </div>
		)
	}
})

var ChartType = React.createClass({
  propTypes: {
    children: PropTypes.node,
    className: PropTypes.string,
    isDisabled: PropTypes.bool,
    isFocused: PropTypes.bool,
    isSelected: PropTypes.bool,
    onFocus: PropTypes.func,
    onSelect: PropTypes.func,
    option: PropTypes.object,
  },
  handleMouseDown: function (event) {
    event.preventDefault();
    event.stopPropagation();
    this.props.onSelect(this.props.option, event);
  },
  handleMouseEnter: function (event) {
    this.props.onFocus(this.props.option, event);
  },
  handleMouseMove: function (event) {
    if (this.props.isFocused) return;
    this.props.onFocus(this.props.option, event);
  },
  render: function () {
    var gravatarStyle = {
      borderRadius: 3,
      display: 'inline-block',
      marginRight: 10,
      position: 'relative',
      top: -2,
      verticalAlign: 'middle',
      width: '13%'
    };
    return (
      <div style={{"textAlign": "left"}} className={this.props.className}
        onMouseDown={this.handleMouseDown}
        onMouseEnter={this.handleMouseEnter}
        onMouseMove={this.handleMouseMove}
        title={this.props.option.title}>
        {this.props.children}
      </div>
    )
  },
}) 

const ChartValue = React.createClass({
  propTypes: {
    children: PropTypes.node,
    placeholder: PropTypes.string,
    value: PropTypes.object
  },
  render: function () {
    var gravatarStyle = {
      borderRadius: 3,
      display: 'inline-block',
      marginRight: 10,
      position: 'relative',
      top: -2,
      verticalAlign: 'middle',
      width: '13%'
    };
    return (
      <div className="Select-value" style={{"textAlign": "left"}} title={this.props.value.title}>
        <span className="Select-value-label">
          {this.props.children}
        </span>
      </div>
    );
  }
}); 

module.exports = IncidentsBody;