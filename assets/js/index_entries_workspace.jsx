var React = require('react');
var ReactDOM = require('react-dom');
var EntriesW = require('./entriesWorkspace/entriesPage');

var AppInit = require('./AppInit');
AppInit();

ReactDOM.render(<EntriesW />, document.getElementById('descriptionsWorkspace'));