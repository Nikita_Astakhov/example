var React = require('react');
var ReactDom = require('react-dom');
var PropTypes = React.PropTypes;
var classNames = require('classnames');

var Utils = require('../Utils');
var csrftoken = Utils.getCookie('csrftoken');

var DescData = require('./components/descriptionData');


var DescriptionConteiner = React.createClass({
	render: function () {
		return (
			<div>
                <DescData />
            </div>
		)
	}
})

module.exports = DescriptionConteiner;
