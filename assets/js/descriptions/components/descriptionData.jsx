var React = require('react')
var ReactDom = require('react-dom')
var PropTypes = React.PropTypes;
var classNames = require('classnames');
var TimerMixin = require('react-timer-mixin');
var dateFormat = require('dateformat');
import DatePicker from 'react-datepicker';
import moment from 'moment';

import 'react-datepicker/dist/react-datepicker.css'; 


var WorkspaceBody = React.createClass({
	mixins: [TimerMixin],
	getInitialState: function () {
		return {
			status500: false,
			status200: false,
			rerenderList: false
		}
	},
	showHint: function (what) {
		var self = this;
		if (what === 1) {
			this.setState({
				status200: !this.state.status200
			}, self.hideHint(1));
		} else {
			this.setState({
				status500: !this.state.status500
			}, self.hideHint(2));
		}
	},
	hideHint: function (what) {
		this.setTimeout(function () {
			if (what === 1) {
				this.setState({
					status200: !this.state.status200
				});
			} else {
				this.setState({
					status500: !this.state.status500
				});
			}
		}, 3000)
	},
	rerenderLists: function () {
		this.setState({rerenderList: !this.state.rerenderList});
	},
	render: function () {
		var status200Class = classNames({
			'responsesuccess__conteiner': true,
			'hiddenstat': !this.state.status200
		});
		var status500Class = classNames({
			'responseerror__conteiner': true,
			'hiddentstat': !this.state.status500
		});
		return (
			<div>
			<div className="statusconteiner">
              <div className={status200Class}>
                <p className="listincidents__form_label db_entries status">Record successfully updated</p>
              </div>
              <div className={status500Class}>
                <p className="listincidents__form_label db_entries status">Record not updated!
                  <br/>Please double check data!</p>
              </div>
            </div>
            <div className="descriptionsblock">
              <h2 className="blockheading">Incidents descriptions records</h2>
              <ListEnriesConteiner hint={this.showHint} />
            </div>
			</div>
		)
	}
});

function DatesCalculation (number) {
	// this is how to find end day of filter
	var today = new Date();
	var day = today.getDay();
	var substr_day = day + (7 * number);
	var plus_day = substr_day - 7;
	if (plus_day <= 0) {
		plus_day = 0
	}
	var end_date = date_by_subtracting_days(new Date(), plus_day);
    var start_day = date_by_subtracting_days(new Date(), substr_day);
    var st_date = dateFormat(start_day, "yyyy-mm-dd");
    var en_date = dateFormat(end_date, "yyyy-mm-dd");
    var hum_st_date = dateFormat(start_day, "dd mmm yyyy");
    var hum_en_date = dateFormat(end_date, "dd mmm yyyy");
    var result = [st_date, en_date, hum_st_date, hum_en_date];
    return result;
};

function date_by_subtracting_days(date, days) {
	return new Date(
        date.getFullYear(), 
        date.getMonth(), 
        date.getDate() - days
    );
};

var currentWeek = 1;
var weeksPageNUmber = 1;

var ListEnriesConteiner = React.createClass({
	mixins: [TimerMixin],
	propTypes: {  
		hint: PropTypes.func.isRequired, 
	},
	getInitialState: function () {
		return {
			choosedFilter: 1,
			filterWeeks: true,
			filterName: false,
			filterId: false,
			data: [],
			currentDate: [],
			showPaginate: false,
			maxPage: 1,
			searchName: '',
			searchId: 0,
			checkedData: [],
			preCheckedData: [],
			allChecked: false,
			start: '',
            end: '',
            displayStart: '',
            displayEnd: ''
		}
	},
	componentWillMount: function () {
		this.setState({
			currentDate: DatesCalculation(currentWeek),
		});
	},
	componentDidMount: function () {
		this.initialDataGet();
	},
	initialDataGet: function () {
		var source = 'http://'+AppGlobal.host + "/api/manage_db/incidents/weeks/"+this.state.currentDate[0]+"/"+this.state.currentDate[1]+"/?format=json;page="+weeksPageNUmber;
		this.serverRequest = $.get(source, function (results) {
			var filt_descriptions = results.results;
			if (results.count > 30) {
				var s = true
				this.calculatePageRange(results.count);
			} else {
				var s = false
			}
			this.setState({
			  data: filt_descriptions,
			  showPaginate: s
			});
		}.bind(this));
	},
	changeFilter: function (event) {
		this.setState({
			choosedFilter: parseInt(event.target.value),
			allChecked: false,
			preCheckedData: [],
		}, this.changeClass(parseInt(event.target.value)));
	},
	changeClass: function (val) {
		weeksPageNUmber = 1;
		if (val === 1) {
			this.setState({
				filterWeeks: true,
				filterName: false,
				filterId: false
			});
			this.clearWeek();
		} else if (val === 2) {
			this.setState({
				filterWeeks: false,
				filterName: true,
				filterId: false
			});
		} else {
			this.setState({
				filterWeeks: false,
				filterName: false,
				filterId: true
			});
		}
	},
	//// Calendar methods
	handleStartChange(date) {
		const format = 'YYYY-MM-DD';
		var start = date.format(format).toString();
    	this.setState({
    	  start: start,
    	  displayStart: date
    	});
    	weeksPageNUmber = 1;
        this.updateWeeksFilterData();
    },
    handleEndChange(date) {
		const format = 'YYYY-MM-DD';
		var start = date.format(format).toString();
    	this.setState({
    	  end: start,
    	  displayEnd: date
    	});
    	weeksPageNUmber = 1;
        this.updateWeeksFilterData();
    },
	clearWeek: function () {
		var self = this;
		currentWeek = 1;
		weeksPageNUmber = 1;
		this.setState({
			currentDate: DatesCalculation(1),
			start: '',
            end: '',
            displayStart: '',
            displayEnd: '',
            preCheckedData: [],
			allChecked: false,
			showPaginate: false,
			data: [],
			searchName: '',
			searchId: 0,
			maxPage: 1,
		}, self.initialDataGet());
	},
	updateWeeksFilterData: function () {
		this.setTimeout( function () {
			var s = this.state.start;
			var e = this.state.end;
			if (e === '') {
				e = s;
			}
			if (s != '' && e != '') {
				var source = 'http://'+AppGlobal.host + "/api/manage_db/incidents/weeks/"+s+"/"+e+"/?format=json;page="+weeksPageNUmber;
				this.serverRequest = $.get(source, function (results) {
					var filt_descriptions = results.results;
					if (results.count > 30) {
						var s = true
						this.calculatePageRange(results.count);
					} else {
						var s = false
					}
					this.setState({
					  data: filt_descriptions,
					  showPaginate: s
					});
				}.bind(this));
			} else {}
		}, 300)
	},
	changeFilterPage: function (event) {
		var fil = this.state.choosedFilter
		var m = this.state.maxPage
		var p = event.target.value;
		if (p === '' || p === '0') {
			p = 1;
		}
		if (p > m) {
			p = m;
		}
		weeksPageNUmber = p;
		this.setState({
			preCheckedData: [],
			allChecked: false,
		});
		if (fil === 1) {
			this.updateWeeksFilterData();
		} else if (fil === 2) {
			this.updateDataByName();
		} else {
			this.updateDataById();
		}
		
	},
	calculatePageRange: function (number) {
		var result = Math.ceil(number / 30);
		this.setState({
			maxPage: result,
		});
	},
	hendleFilterName: function (event) {
		var self = this;
		var v = event.target.value
		this.setState({
			searchName: v
		});
		if (v != '') {
            self.updateDataByName()
        } else {
            weeksPageNUmber = 1
            self.initialDataGet()
        }
	},
	updateDataByName: function () {
		this.setTimeout( function () {
			if (this.state.searchName.length > 0) {
				var source = 'http://'+AppGlobal.host + "/api/incidents/list/name/"+ this.state.searchName +"/?format=json;page="+weeksPageNUmber;
				this.serverRequest = $.get(source, function (results) {
					var filt_descriptions = results.results;
					if (results.count > 30) {
						var s = true
						this.calculatePageRange(results.count);
					} else {
						var s = false
					}
					this.setState({
					  data: filt_descriptions,
					  showPaginate: s
					});
				}.bind(this));
			}	
		}, 300);
	},
	hendleFilterId: function (event) {
		var self = this;
		this.setState({
			searchId: event.target.value
		}, self.updateDataById());
	},
	updateDataById: function () {
		this.setTimeout( function () {
			if (this.state.searchId > 0) {
				var source = 'http://'+AppGlobal.host + "/api/incidents/list/id/"+ this.state.searchId +"/?format=json;page="+weeksPageNUmber;
				this.serverRequest = $.get(source, function (results) {
					var filt_descriptions = results.results;
					if (results.count > 30) {
						var s = true
						this.calculatePageRange(results.count);
					} else {
						var s = false
					}
					this.setState({
					  data: filt_descriptions,
					  showPaginate: s
					});
				}.bind(this));
			}	
		}, 200)
	},
	checkAll: function () {
		var self = this;
		this.setState({
			allChecked: !this.state.allChecked,
		}, self.moveAllToSelect());
	},
	moveAllToSelect: function () {
		var self = this;
		if (this.state.allChecked) {
			var filtered_list = this.state.data;
			var pre_checked_list = this.state.preCheckedData;
			var checked_list = this.state.checkedData;
			var iterate = filtered_list.map(function(inst){
				var exist_1 = pre_checked_list.some(function(pre_c) {
					return inst.id === pre_c.id;
				});
				var exist_2 = checked_list.some(function (c_inst){
					return inst.id === c_inst.id; 
				});
				if (exist_1) {
					var index = pre_checked_list.indexOf(inst);
					return pre_checked_list.splice(index, 1)
				} else {
					if (exist_2) {
						var index = pre_checked_list.indexOf(inst);
						return pre_checked_list.splice(index, 1)
					}
				}
			});
			this.setState({
				preCheckedData: pre_checked_list,
			});
		} else {
			this.setState({
				preCheckedData: this.state.preCheckedData.concat(this.state.data),
			});
		}
	},
	deleteOrAddToChecked: function (what, object) {
		var self = this;
		var list = this.state.preCheckedData; 
		var exist;
		exist = list.some(function (ch_inst) { 
		  return ch_inst === object;
		});
		if (what === 1) {
			if (exist) {
				var index = list.indexOf(object);
				var spliced_list = list.splice(index, 1);
				self.setState({
					preCheckedData: list,
				});
			}
		} else {
			if (!exist) {
				self.setState({
				 preCheckedData: list.concat(object),
				});
			}
		}
	},
	moveToChecked: function () {
		var self = this;
		var checkedList = this.state.checkedData;
		var preCheckedList = this.state.preCheckedData;
		if (checkedList.length > 0) {
			var it = preCheckedList.map(function(pre_c){
				var exist = checkedList.some(function (c_inst){
					return pre_c.id === c_inst.id;
				});
				if (exist) {
					var index = preCheckedList.indexOf(pre_c);
					return preCheckedList.splice(index, 1);
				}
			});
		}
		this.setState({
			checkedData: checkedList.concat(preCheckedList),
			preCheckedData: [],
			allChecked: false,
		});	
	},
	clearAllChecked: function () {
		this.setState({
			checkedData: [],
			preCheckedData: []
		});
	},
	deleteFromChecked: function (object) {
		var self = this;
		var checkedList = this.state.checkedData;
		var preCheckedList = this.state.preCheckedData;
		var exist = checkedList.some(function (c_inst){
			return object.id === c_inst.id;
		});
		if (exist) {
			var index = checkedList.indexOf(object);
			checkedList.splice(index, 1);
		}
		var exist_2 = preCheckedList.some(function (c_inst_2){
			return object.id === c_inst_2.id;
		});
		if (exist_2) {
			var index_2 = preCheckedList.indexOf(object);
			preCheckedList.splice(index_2, 1);
		}
		this.setState({
				checkedData: checkedList,
				preCheckedData: preCheckedList,
			});
	},
	handleSubmit: function (event) {
        event.preventDefault();
    },
	render: function () {
		var self = this;
		var NamesClass = classNames({
			'filternames': true,
			'hiddenfilter': !this.state.filterName,
		});
		var WeeksClass = classNames({
			'filterweek': true,
			'hiddenfilter': !this.state.filterWeeks,
		});
		var IdClass = classNames({
			"filterid": true,
			"hiddenbl": !this.state.filterId, 
		});
		var calStartClass = classNames({
			'hidden': !this.state.showStart
		});
		var calEndClass = classNames({
			'hidden': !this.state.showEnd
		});
		return (
			<div>
			    <div className="listincidents__filterconteiner">
                  <div className="w-row">
                    <div className="w-col w-col-7">
                      <div className="w-row">
                        <div className="w-col w-col-2">
                          <h4 className="listincidents__filterhead">Filter by:</h4>
                        </div>
                        <div className="w-col w-col-10">
                          <div>
                            <form onSubmit={this.handleSubmit}>
                              <select onChange={this.changeFilter} value={this.state.choosedFilter} className="w-select filteroptionfield">
                                <option value="1">Date range</option>
                                <option value="2">Title or short description</option>
                                <option value="3">ID</option>
                              </select>
                            </form>
                          </div>
                        </div>
                      </div>
                      <div className={NamesClass}>
                        <form onSubmit={this.handleSubmit}>
                          <div className="w-row">
                            <div className="w-col w-col-10">
                              <label className="listincidents__form_label">Input incident title or short description</label>
                              <input onChange={this.hendleFilterName} value={this.state.searchName} className="w-input listincidents__form_input" maxLength="256" placeholder="Input title here" type="text"/>
                            </div>
                            <div className="w-col w-col-2"><a onClick={this.clearWeek} className="w-button listincidents__form_submit">Clear</a>
                            </div>
                          </div>
                        </form>
                      </div>
                      <div className={WeeksClass}>
                        <form onSubmit={this.handleSubmit}>
                          <label className="listincidents__form_label">Choose date range</label>
                          <div className="weekchooseblock">
                            <div className="w-row">
                              <div className="w-col w-col-5">
                                <DatePicker
                                    dateFormat='DD-MM-YYYY'
                                    selected={this.state.displayStart}
                                    onChange={this.handleStartChange}
                                    peekNextMonth
                                    showMonthDropdown
                                    showYearDropdown
                                    dropdownMode="select"
                                    calendarClassName="calClass"
                                    placeholderText="Start date"
                                    className="w-input listincidents__form_input"
                                />
                              </div>
                              <div className="w-col w-col-5">
                                <DatePicker
                                    dateFormat='DD-MM-YYYY'
                                    selected={this.state.displayEnd}
                                    onChange={this.handleEndChange}
                                    peekNextMonth
                                    showMonthDropdown
                                    showYearDropdown
                                    dropdownMode="select"
                                    calendarClassName="calClass"
                                    placeholderText="End date"
                                    className="w-input listincidents__form_input"
                                />
                              </div>
                              <div className="w-col w-col-2 w-clearfix"><a onClick={this.clearWeek} className="w-button listincidents__form_submit filter">Clear</a>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                      <div className={IdClass}>
                        <form onSubmit={this.handleSubmit}>
                          <div className="w-row">
                            <div className="w-col w-col-10">
                              <label className="listincidents__form_label">Input incident ID</label>
                              <input onChange={this.hendleFilterId} value={this.state.searchId} className="w-input listincidents__form_input" maxLength="256" placeholder="Input ID here" type="text"/>
                            </div>
                            <div className="w-col w-col-2 w-clearfix"><a onClick={this.clearWeek} className="w-button listincidents__form_submit">Clear</a>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                    <div className="w-col w-col-5"></div>
                  </div>
                </div>
                <div className="entriesblock">
                    <div className="w-row">
                        <div className="w-col w-col-7 dotedcolumn">
                            <ListFilteredContentConteiner 
                                key="allFilteredContent"
                                pageRange={this.state.maxPage} 
                                paginate={this.state.showPaginate} 
                                changePage={this.changeFilterPage} 
                                data={this.state.data} 
                                checkedData={this.state.checkedData}
                                changeAllChecked={this.checkAll}
                                allCheckedState={this.state.allChecked}
                                del={this.deleteOrAddToChecked}
                                moveToChecked={this.moveToChecked}
                            />
                        </div>  
                        <div className="w-col w-col-5 choosedentriesblock">
                            <ListChoosedEntriesConteiner
                                key="checkedFilteredContent"
                                data={this.state.checkedData}
                                clearAll={this.clearAllChecked}
                                uncheck={this.deleteFromChecked}
                            />
                        </div>  
                    </div>
                </div>        
			</div>
		)
	}
})

var ListFilteredContentConteiner = React.createClass({
	propTypes: {  
		data: PropTypes.array,
		checkedData: PropTypes.array,  
		changePage: PropTypes.func, 
		paginate: PropTypes.bool,
		pageRange: PropTypes.number,
		changeAllChecked: PropTypes.func,
		allCheckedState: PropTypes.bool, 
		del: PropTypes.func,
		moveToChecked: PropTypes.func, 
	},
	handleSubmit: function (event) {
        event.preventDefault();
    },
	render: function () {
		var self = this;
		var listEntries = this.props.data.map(function (desc){
			var already_checked = false;
			if (self.props.checkedData) {
				already_checked = self.props.checkedData.some(function (ch_inst) { 
				  return ch_inst.id === desc.id;
				});
			}	
			if (!already_checked) {
				return (<FilteredEntryINstance del={self.props.del} preChecked={self.props.allCheckedState} key={desc.id} data={desc} />);
			}
		});
		var paginator;
		if (this.props.paginate) {
			paginator = (
				<div className="paginationconteiner">
                <div className="w-container">
                  <div className="w-row">
                    <div className="w-col w-col-3"></div>
                    <div className="w-col w-col-6">
                      <div className="w-row">
                        <div className="w-col w-col-6">
                          <h4 className="actionhead">Page range: 1 - {this.props.pageRange}</h4>
                        </div>
                        <div className="w-col w-col-6">
                          <div className="filterpages">
                            <form onSubmit={this.handleSubmit}>
                              <input onChange={this.props.changePage} className="w-input listincidents__form_input" min="1" placeholder="Input page number here" type="number"/>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="w-col w-col-3"></div>
                  </div>
                </div>
              </div>
			)
		}
		var allCheckedClass = classNames({
			'w-button checkbox': true,
			'checked': this.props.allCheckedState
		});
		return (
			<div className="filteredentriesblock">
              <div className="actionblock">
                <div className="w-row somerow">
                  <div className="w-col w-col-1">
                    <h4 className="actionhead">Action:</h4>
                  </div>
                  <div className="w-col w-col-3 w-clearfix"><a onClick={this.props.moveToChecked} className="w-button listincidents__form_submit db">Move to checked</a>
                  </div>
                  <div className="w-col w-col-3 w-clearfix">
                  </div>
                  <div className="w-col w-col-5 fhe"></div>
                </div>
              </div>
              <div className="w-row headerrow">
                <div onClick={this.props.changeAllChecked} className="w-col w-col-1">
                  <a className={allCheckedClass}></a>
                </div>
                <div className="w-col w-col-1">
                  <h3 className="teblehead">ID</h3>
                </div>
                <div className="w-col w-col-6">
                  <h3 className="teblehead">Title</h3>
                </div>
                <div className="w-col w-col-2">
                  <h3 className="teblehead">Recording Date</h3>
                </div>
                <div className="w-col w-col-2">
                  <h3 className="teblehead">Actions</h3>
                </div>
              </div>
              <div className="listentriesconteiner">

                {listEntries}

              </div>
                {paginator}
              
            </div>
		)
	}
});

var FilteredEntryINstance = React.createClass({
	mixins: [TimerMixin],
	propTypes: {  
		data: PropTypes.object,
		preChecked: PropTypes.bool, 
		delit: PropTypes.func,
	},
	getInitialState: function () {
		return {
			checked: false,
			clicked: false,
			show: false
		}
	},
	componentWillMount: function () {
		this.setState({
			checked: this.props.preChecked,
		});
	},
	componentWillReceiveProps: function (nextProps) {
		if (!this.state.clicked) {
			this.setState({
				checked: nextProps.preChecked
			}); 
		}	
	},
	renderPreview: function () {
		this.setState({
			show: !this.state.show
		})
	},
	hendleCheck: function () {
		var self = this;
		this.setState({
			clicked: true,
			checked: !this.state.checked,
		}, self.deleteFromArray());
	},
	deleteFromArray: function () {
		var self = this;
		this.setTimeout( function () {
			if (!self.state.checked) {
				self.props.del(1, self.props.data);
			} else {
				self.props.del(2, self.props.data);
			}
		}, 200)
	},
	render: function () {
		var self = this;
		var checkClass = classNames({
			'w-button checkbox': true,
			'checked': this.state.checked,
		});
		var descClass = classNames({
			'w-col w-col-12': true,
			'hidden': !this.state.show,
		})
		if (this.state.show) {
			var bt = 'hide description'
		} else {
			var bt = 'show description'
		}
		return (
			<div className="listentrieconteiner">
              <div className="w-row">
                <div onClick={this.hendleCheck} className="w-col w-col-1">
                  <a className={checkClass}></a>
                </div>
                <div className="w-col w-col-1">
                  <h3 className="teblehead entrie">{this.props.data.id}</h3>
                </div>
                <div className="w-col w-col-6">
                  <h3 className="teblehead entrie">{this.props.data.name}</h3>
                </div>
                <div className="w-col w-col-2">
                  <h3 className="teblehead entrie">{this.props.data.creation_date}</h3>
                </div>
                <div className="w-col w-col-2">
                  <h3 onClick={this.renderPreview} className="teblehead entrie link">{bt}</h3>
                  
                </div>
                <div className={descClass}>
                    <h3 style={{'fontSize':'16px', 'textAlign': 'left', 'margin': '6px 15px 10px 25px'}} className="teblehead entrie">{self.props.data.description}</h3>
                </div>
              </div>
            </div>
		)
	},
});

var AssignedConteiner = React.createClass({
  propTypes: {  
    instance_id: PropTypes.number,
    unmount: PropTypes.func,
    right: PropTypes.bool,
  },
  getInitialState: function () {
    return {
      data: []
    }
  },
  componentWillMount: function () {
    var source = 'http://'+AppGlobal.host + "/api/incidents/assigned/"+this.props.instance_id+"/?format=json";
    this.serverRequest = $.get(source, function (results) {
      var filt_data = results.results;
      this.setState({
        data: filt_data,
      });
    }.bind(this));
  },
  render: function () {
    var self = this;
    if (this.state.data.length > 0) {
      var listInstances = this.state.data.map(function(ins){
        return (<AssignedInstance key={ins.id} data={ins} />)
      });
    } else {
      var listInstances = (<div className="assignedidinfo__entrie"><h3 className="teblehead entrie infot">No data available</h3></div>);
    }
    var assignedClass = classNames({
      "assignedidinfoblock": true,
      'rightconteiner': this.props.right
    });
    return (
      <div className={assignedClass} onMouseLeave={this.props.unmount}>
              <div className="w-row assignedidheaderrow">
                <div className="w-col w-col-1">
                  <h3 className="teblehead entrie infot">ID</h3>
                </div>
                <div className="w-col w-col-5">
                  <h3 className="teblehead entrie infot">Title</h3>
                </div>
                <div className="w-col w-col-2">
                  <h3 className="teblehead entrie infot">Recording Date</h3>
                </div>
                <div className="w-col w-col-2">
                  <h3 className="teblehead entrie infot">Recorded by</h3>
                </div>
                <div className="w-col w-col-2">
                  <h3 className="teblehead entrie infot">Action</h3>
                </div>
              </div>
              {listInstances}
            </div>
    )
  }
});

var AssignedInstance = React.createClass({
	propTypes: {  
		data: PropTypes.object,
	},
	render: function () {
		return (
			<div className="assignedidinfo__entrie">
              <div className="w-row">
                <div className="w-col w-col-1">
                  <h3 className="teblehead entrie infot">{this.props.data.id}</h3>
                </div>
                <div className="w-col w-col-5">
                  <h3 className="teblehead entrie infot">{this.props.data.name}</h3>
                </div>
                <div className="w-col w-col-2">
                  <h3 className="teblehead entrie infot">{this.props.data.creation_date}</h3>
                </div>
                <div className="w-col w-col-2">
                  <h3 className="teblehead entrie infot">{this.props.data.created_by}</h3>
                </div>
                <div className="w-col w-col-2">
                  <h3 className="teblehead entrie infot edit"><a href={"http://"+AppGlobal.host+"/db_manage/edit_db_record/"+this.props.data.id}>edit</a></h3>
                </div>
              </div>
            </div>
		)
	}
})


var ListChoosedEntriesConteiner = React.createClass({
	propTypes: {  
		data: PropTypes.array,
		clearAll: PropTypes.func,
        uncheck:PropTypes.func,
	},
	createWorkspace: function () {
		if (this.props.data.length > 0) {
			var self = this;
			var descriptions_ids = [];
			var list_all = this.props.data.map(function(inst_a){
				descriptions_ids.push(parseInt(inst_a.id));
			});
			var source = 'http://'+AppGlobal.host+'/api/manage_db/create_descriptions_workspace/';
			var post_d = {'user_id': parseInt(AppGlobal.user), descriptions: descriptions_ids};
			$.ajax({
				headers: { 
				  'Accept': 'application/json',
				  'Content-Type': 'application/json' 
				},
				type: "POST",
				url: source,
				data: JSON.stringify(post_d, null, '\t'),
				dataType: 'json',
				statusCode: {
				  200: function (data) {self.redirectToWorkspace(parseInt(data.workspace_id));},
				  500: function () {},
				}
			});
		}	
	},
	createEntry: function () {
		if (this.props.data.length > 0) {
			var self = this;
			var descriptions_ids = [];
			var list_all = this.props.data.map(function(inst_a){
				descriptions_ids.push(parseInt(inst_a.id));
			});
			var source = 'http://'+AppGlobal.host+'/api/manage_db/create_assigned_entry/';
			var post_d = {'user_id': parseInt(AppGlobal.user), descriptions: descriptions_ids};
			$.ajax({
				headers: { 
				  'Accept': 'application/json',
				  'Content-Type': 'application/json' 
				},
				type: "POST",
				url: source,
				data: JSON.stringify(post_d, null, '\t'),
				dataType: 'json',
				statusCode: {
				  200: function (data) {self.redirectToEntry(parseInt(data.entry_id));},
				  500: function () {},
				}
			});
		}	
	},
	redirectToEntry: function (where) {
		window.location.href = 'http://'+AppGlobal.host+'/db_manage/edit_db_record/'+where+'/';
	},
	redirectToWorkspace: function (where) {
		window.location.href = 'http://'+AppGlobal.host+'/db_manage/descriptions_preview/'+where+'/';
	},
	render: function () {
		var self = this;
		var listInstances = this.props.data.map(function(inst){
			return (<CheckedInstanceConteiner uncheck={self.props.uncheck} key={inst.id} data={inst}/>)
		});
		return (
			<div>
			<div className="actionblock">
              <div className="w-row somerow">
                <div className="w-col w-col-2">
                  <h4 className="actionhead">Action:</h4>
                </div>
                <div className="w-col w-col-4 w-clearfix"><a onClick={this.createWorkspace} className="w-button listincidents__form_submit db">Create workspace</a>
                </div>
                <div className="w-col w-col-4 w-clearfix"><a onClick={this.createEntry} className="w-button listincidents__form_submit db">Create db record</a>
                </div>
                <div className="w-col w-col-2"><a onClick={this.props.clearAll} className="w-button listincidents__form_submit db">Clear</a>
                </div>
              </div>
            </div>
            <div className="w-row headerrow">
              <div className="w-col w-col-1">
              </div>
              <div className="w-col w-col-2">
                <h3 className="teblehead">ID</h3>
              </div>
              <div className="w-col w-col-6">
                <h3 className="teblehead">Title</h3>
              </div>
              <div className="w-col w-col-3">
                <h3 className="teblehead">Assigned ids</h3>
              </div>
            </div>

            <div className="listentriesconteiner">
              {listInstances}
            </div>
            </div>
		)
	},
})

var CheckedInstanceConteiner = React.createClass({
	propTypes: {  
		data: PropTypes.object,
		uncheck: PropTypes.func,
	},
	getInitialState: function () {
		return {
			checked: true,
		}
	},
	renderAssigned: function () {
		ReactDom.render(<AssignedConteiner right={true} key={this.props.data.id} unmount={this.unrenderAssigned} instance_id={this.props.data.id}/>, this.refs.assignedConteiner);
	},
	unrenderAssigned: function () {
		ReactDom.unmountComponentAtNode(this.refs.assignedConteiner);
	},
	hendleCheck: function () {
		this.setState({checked: !this.state.checked});
		this.props.uncheck(this.props.data);
	},
	render: function () {
		var CheckedClass = classNames({
			'w-button checkbox': true,
			'checked': this.state.checked,
		});
		return (
			<div className="listentrieconteiner">
              <div className="w-row">
                <div className="w-col w-col-1">
                  <a onClick={this.hendleCheck} className={CheckedClass}></a>
                </div>
                <div className="w-col w-col-2">
                  <h3 className="teblehead entrie">{this.props.data.id}</h3>
                </div>
                <div className="w-col w-col-6">
                  <h3 className="teblehead entrie">{this.props.data.name}</h3>
                </div>
                <div className="w-col w-col-3">
                  <div className="assignedidblock">
                    <h3 onClick={this.renderAssigned} className="teblehead entrie link">info</h3>
                    <div ref="assignedConteiner"></div>
                  </div>
                </div>
              </div>
            </div>
		)
	}
})

module.exports = WorkspaceBody;