var React = require('react');
var ReactDom = require('react-dom');
var PropTypes = React.PropTypes;
var classNames = require('classnames');

var Utils = require('../Utils');
var csrftoken = Utils.getCookie('csrftoken');

var EntData = require('./components/entriesData');


var EntriesConteiner = React.createClass({
	render: function () {
		return (
			<div>
                <EntData />
            </div>
		)
	}
})

module.exports = EntriesConteiner;
