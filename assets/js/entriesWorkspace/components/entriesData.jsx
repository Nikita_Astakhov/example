var React = require('react')
var ReactDom = require('react-dom')
var PropTypes = React.PropTypes;
var classNames = require('classnames');
var TimerMixin = require('react-timer-mixin');
var dateFormat = require('dateformat');
import DatePicker from 'react-datepicker';
import moment from 'moment';

import 'react-datepicker/dist/react-datepicker.css'; 

var weeksPageNUmber = 1;
var EntriesPointConteiner = React.createClass({
    mixins: [TimerMixin],
    getInitialState: function () {
        return {
            data: [],
            choosedFilter: 1,
            filterWeeks: true,
            filterName: false,
            filterId: false,
            filterKey: false,
            showPaginate: false,
            maxPage: 1,
            searchName: '',
            searchKey: '',
            searchId: 0,
            date: '',
            displayStart: ''
        }
    },
    componentWillMount: function () {
        var source = 'http://'+AppGlobal.host + "/api/manage_db/startEntries/?format=json;page="+weeksPageNUmber;
        this.serverRequest = $.get(source, function (results) {
            var filt_descriptions = results.results;
            if (results.count > 30) {
                var s = true
                this.calculatePageRange(results.count);
            } else {
                var s = false
            }
            this.setState({
              data: filt_descriptions,
              showPaginate: s
            });
        }.bind(this));
    },
    changeFilterPage: function (event) {
        var fil = this.state.choosedFilter
        var m = this.state.maxPage
        var p = event.target.value;
        if (p === '' || p === '0') {
            p = 1;
        }
        if (p > m) {
            p = m;
        }
        weeksPageNUmber = p;
        
        if (fil === 1) {
            if (this.state.date === '') {
                this.updateStartList();
            } else {
                this.updateByDate();
            }
        } else if (fil === 2) {
            this.updateDataByName();
        } else if (fil === 3) {
            this.updateDataById();
        } else if (fil === 4) {
            this.updateDataByKey();
        } else {
            
        }
    },
    updateStartList: function () {
        var source = 'http://'+AppGlobal.host + "/api/manage_db/startEntries/?format=json;page="+weeksPageNUmber;
        this.serverRequest = $.get(source, function (results) {
            var filt_descriptions = results.results;
            if (results.count > 30) {
                var s = true
                this.calculatePageRange(results.count);
            } else {
                var s = false
            }
            this.setState({
              data: filt_descriptions,
              showPaginate: s
            });
        }.bind(this));
    },
    calculatePageRange: function (number) {
        var result = Math.ceil(number / 30);
        this.setState({
            maxPage: result,
        });
    },
    changeFilter: function (event) {
        weeksPageNUmber = 1;
        var self = this;
        this.setState({
            choosedFilter: parseInt(event.target.value),
        }, self.changeClass(parseInt(event.target.value)));
    },
    changeClass: function (val) {
        if (val === 1) {
            this.setState({
                filterWeeks: true,
                filterName: false,
                filterId: false,
                filterKey: false
            });
        } else if (val === 2) {
            this.setState({
                filterWeeks: false,
                filterName: true,
                filterId: false,
                filterKey: false
            });
        } else if (val === 3) {
            this.setState({
                filterWeeks: false,
                filterName: false,
                filterId: true,
                filterKey: false
            });
        } else {
            this.setState({
                filterKey: true,
                filterWeeks: false,
                filterName: false,
                filterId: false
            });
        }
    },
    hendleDate: function (date) {
        var self = this;
        weeksPageNUmber = 1;
        const format = 'YYYY-MM-DD';
        var start = date.format(format).toString();
        this.setState({
          date: start,
          displayStart: date,
        }, self.updateByDate());
        
    },
    updateByDate: function () {
        this.setTimeout(function () {
            var source = 'http://'+AppGlobal.host + "/api/manage_db/entries/date/"+ this.state.date +"/?format=json;page="+weeksPageNUmber;
            this.serverRequest = $.get(source, function (results) {
                var filt_descriptions = results.results;
                if (results.count > 30) {
                    var s = true
                    this.calculatePageRange(results.count);
                } else {
                    var s = false
                }
                this.setState({
                  data: filt_descriptions,
                  showPaginate: s
                });
            }.bind(this));
        }, 300);
    },
    hendleFilterName: function (event) {
        var self = this;
        var v = event.target.value
        this.setState({
            searchName: v,
        });
        if (v != '') {
            self.updateDataByName()
        } else {
            weeksPageNUmber = 1
            self.updateStartList()
        }
    },
    updateDataByName: function () {
        this.setTimeout(function () {
            if (this.state.searchName.length > 0) {
                var source = 'http://'+AppGlobal.host + "/api/manage_db/entries/name/"+ encodeURIComponent(this.state.searchName) +"/?format=json;page="+weeksPageNUmber;
                this.serverRequest = $.get(source, function (results) {
                    var filt_descriptions = results.results;
                    if (results.count > 30) {
                        var s = true
                        this.calculatePageRange(results.count);
                    } else {
                        var s = false
                    }
                    this.setState({
                      data: filt_descriptions,
                      showPaginate: s
                    });
                }.bind(this));
            }   
        }, 300);
    },
    hendleFilterKey: function (event) {
        var self = this;
        var v = event.target.value;
        this.setState({
            searchKey: v,
        });
        if (v != '') {
            self.updateDataByKey()
        } else {
            weeksPageNUmber = 1
            self.updateStartList()
        }
    },
    updateDataByKey: function () {
        this.setTimeout(function () {
            if (this.state.searchKey.length > 0) {
                var source = 'http://'+AppGlobal.host + "/api/manage_db/entries/key/"+ this.state.searchKey +"/?format=json;page="+weeksPageNUmber;
                this.serverRequest = $.get(source, function (results) {
                    var filt_descriptions = results.results;
                    if (results.count > 30) {
                        var s = true
                        this.calculatePageRange(results.count);
                    } else {
                        var s = false
                    }
                    this.setState({
                      data: filt_descriptions,
                      showPaginate: s
                    });
                }.bind(this));
            }   
        }, 300);
    },
    hendleFilterId: function (event) {
        var self = this;
        this.setState({
            searchId: event.target.value,
        }, self.updateDataById());
    },
    updateDataById: function () {
        this.setTimeout(function () {
            if (this.state.searchId > 0) {
                var source = 'http://'+AppGlobal.host + "/api/manage_db/entries/id/"+ this.state.searchId +"/?format=json;page="+weeksPageNUmber;
                this.serverRequest = $.get(source, function (results) {
                    var filt_descriptions = results.results;
                    if (results.count > 30) {
                        var s = true
                        this.calculatePageRange(results.count);
                    } else {
                        var s = false
                    }
                    this.setState({
                      data: filt_descriptions,
                      showPaginate: s
                    });
                }.bind(this));
            }   
        }, 200)
    },
    clearFilters: function () {
        weeksPageNUmber = 1;
        var self = this;
        this.setState({
            preCheckedData: [],
            data: [],
            showPaginate: false,
            maxPage: 1,
            searchName: '',
            searchKey: '',
            searchId: 0,
            date: '',
            displayStart: ''
        }, self.updateStartList());
    },
    createNewRecord: function () {
        var self = this;
        var source = 'http://'+AppGlobal.host+'/api/manage_db/create_new_entry/';
        var post_d = {'user_id': parseInt(AppGlobal.user)};
        $.ajax({
            headers: { 
              'Accept': 'application/json',
              'Content-Type': 'application/json' 
            },
            type: "POST",
            url: source,
            data: JSON.stringify(post_d, null, '\t'),
            dataType: 'json',
            statusCode: {
              200: function (data) {self.redirectToNewRecord(data.entry_id);},
              500: function () {},
            }
        });
    },
    redirectToNewRecord: function (where) {
        window.location.href = 'http://'+AppGlobal.host+'/db_manage/edit_db_record/'+where+'/';
    },
    handleSubmit: function (event) {
        event.preventDefault();
    },
    render: function () {
        var NamesClass = classNames({
            'filternames': true,
            'hiddenfilter': !this.state.filterName,
        });
        var WeeksClass = classNames({
            'filterweek': true,
            'hiddenfilter': !this.state.filterWeeks,
        });
        var IdClass = classNames({
            "filterid": true,
            "hiddenbl": !this.state.filterId, 
        });
        var KeywordClass = classNames({
            'filternames': true,
            'hiddenfilter': !this.state.filterKey,
        })
        return (
            <div className="dbentriesblock">
              <h2 className="blockheading">Database Entries</h2>
              <div className="listincidents__filterconteiner">
                <div className="w-row">
                  <div className="w-col w-col-4">
                    <div className="w-row">
                      <div className="w-col w-col-2">
                        <h4 className="listincidents__filterhead">Filter by:</h4>
                      </div>
                      <div className="w-col w-col-10">
                        <div>
                          <form onSubmit={this.handleSubmit}>
                            <select onChange={this.changeFilter} value={this.state.choosedFilter} className="w-select filteroptionfield">
                              <option value="1">Recording date</option>
                              <option value="3">ID</option>
                              <option value="2">Title or short description</option>
                              <option value="4">Keywords</option>
                            </select>
                          </form>
                        </div>
                      </div>
                    </div>
                    <div className={NamesClass}>
                      <form onSubmit={this.handleSubmit}>
                        <div className="w-row">
                          <div className="w-col w-col-10">
                            <label className="listincidents__form_label">Input entry title or short description</label>
                            <input onChange={this.hendleFilterName} value={this.state.searchName} className="w-input listincidents__form_input"maxLength="256" placeholder="Input title here" type="text"/>
                          </div>
                          <div className="w-col w-col-2"><a onClick={this.clearFilters} className="w-button listincidents__form_submit">Clear</a>
                          </div>
                        </div>
                      </form>
                    </div>
                    <div className={KeywordClass}>
                      <form onSubmit={this.handleSubmit}>
                        <div className="w-row">
                          <div className="w-col w-col-10">
                            <label className="listincidents__form_label">Input keyword from incident description</label>
                            <input onChange={this.hendleFilterKey} value={this.state.searchKey} className="w-input listincidents__form_input"maxLength="256" placeholder="Input keyword here" type="text"/>
                          </div>
                          <div className="w-col w-col-2"><a onClick={this.clearFilters} className="w-button listincidents__form_submit">Clear</a>
                          </div>
                        </div>
                      </form>
                    </div>
                    <div className={WeeksClass}>
                      <form onSubmit={this.handleSubmit}>
                        <div className="w-row">
                          <div className="w-col w-col-10">
                            <label className="listincidents__form_label">Choose date</label>
                            <DatePicker
                                dateFormat='DD-MM-YYYY'
                                selected={this.state.displayStart}
                                onChange={this.hendleDate}
                                peekNextMonth
                                showMonthDropdown
                                showYearDropdown
                                dropdownMode="select"
                                calendarClassName="calClass"
                                placeholderText="Start date"
                                className="w-input listincidents__form_input"
                            />
                          </div>
                          <div className="w-col w-col-2"><a onClick={this.clearFilters} className="w-button listincidents__form_submit">Clear</a>
                          </div>
                        </div>
                      </form>
                    </div>
                    <div className={IdClass}>
                      <form onSubmit={this.handleSubmit}>
                        <div className="w-row">
                          <div className="w-col w-col-10">
                            <label className="listincidents__form_label">Input database entry ID</label>
                            <input onChange={this.hendleFilterId} value={this.state.searchId} className="w-input listincidents__form_input" min="1" placeholder="Input ID here" type="number"/>
                          </div>
                          <div className="w-col w-col-2 w-clearfix"><a onClick={this.clearFilters} className="w-button listincidents__form_submit">Clear</a>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <div className="w-col w-col-8 w-clearfix"><a onClick={this.createNewRecord} className="w-button button newrecoed db">Add new entry</a>
                  </div>
                </div>
              </div>
              <EntriesConteiner 
                key="allEntriesConteiner" 
                data={this.state.data}
                pageRange={this.state.maxPage} 
                paginate={this.state.showPaginate} 
                changePage={this.changeFilterPage} 
              />
            </div>
        )
    }
})

var EntriesConteiner = React.createClass({
    propTypes: {  
        data: PropTypes.array,
        paginate: PropTypes.bool,
        pageRange: PropTypes.number, 
        changePage: PropTypes.func,
    },
    deleteInstance: function (object) {
        var all_list = this.props.data;
        var exist = all_list.some(function(inst){
            return inst === object;
        });  
        if (exist) {
            var index = all_list.indexOf(object);
            var removed = all_list.splice(index, 1)
            this.setState({
                data: all_list
            })
        }
    },
    handleSubmit: function (event) {
        event.preventDefault();
    },
    render: function () {
        var self = this;
        var listEntries = this.props.data.map(function(inst) {
            return (<EntriInstance key={inst.id} data={inst} deleteInstance={self.deleteInstance} />)
        });
        var paginator;
        if (this.props.paginate) {
            paginator = (
                <div className="paginationconteiner">
                <div className="w-container">
                  <div className="w-row">
                    <div className="w-col w-col-3"></div>
                    <div className="w-col w-col-6">
                      <div className="w-row">
                        <div className="w-col w-col-6">
                          <h4 className="actionhead">Page range: 1 - {this.props.pageRange}</h4>
                        </div>
                        <div className="w-col w-col-6">
                          <div className="filterpages">
                            <form onSubmit={this.handleSubmit}>
                              <input onChange={this.props.changePage} className="w-input listincidents__form_input" min="1" placeholder="Input page number here" type="number"/>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="w-col w-col-3"></div>
                  </div>
                </div>
              </div>
            )
        }
        return (
            <div className="dbentriesconteiner">
              <div className="w-row headerrow">
                <div className="w-col w-col-1">
                  <h3 className="teblehead">ID</h3>
                </div>
                <div className="w-col w-col-6">
                  <h3 className="teblehead">Title</h3>
                </div>
                <div className="w-col w-col-2">
                  <h3 className="teblehead">Recording Date</h3>
                </div>
                <div className="w-col w-col-1">
                  <h3 className="teblehead">Recorded by</h3>
                </div>
                <div className="w-col w-col-2">
                  <h3 className="teblehead">Action</h3>
                </div>
              </div>
              <div className="listdbentriesconteiner">
                {listEntries}
              </div>
              {paginator}
            </div>
        )
    }
})

var EntriInstance = React.createClass({
    propTypes: {  
        data: PropTypes.object, 
        deleteInstance: PropTypes.func,
    },
    getInitialState: function () {
        return {
            deleteB: true
        }
    },
    toggleDelete: function () {
        this.setState({
            deleteB: !this.state.deleteB,
        });
    },
    deleteRecord: function () {
        var self = this;
        var source = 'http://'+AppGlobal.host+'/api/manage_db/delete_entry/';
        var post_d = {'user_id': parseInt(AppGlobal.user), entry_id: this.props.data.id};
        $.ajax({
            headers: { 
              'Accept': 'application/json',
              'Content-Type': 'application/json' 
            },
            type: "POST",
            url: source,
            data: JSON.stringify(post_d, null, '\t'),
            dataType: 'json',
            statusCode: {
              200: function () {self.props.deleteInstance(self.props.data);},
              500: function () {},
            }
        });
    },
    render: function () {
        var deleteClass = classNames({
            'deleteconfirm': true,
            'hidden': this.state.deleteB
        })
        return (
            <div className="dbentryconteiner">
              <div className="w-row">
                <div className="w-col w-col-1">
                  <h3 className="teblehead entrie">{this.props.data.id}</h3>
                </div>
                <div className="w-col w-col-6">
                  <h3 className="teblehead entrie">{this.props.data.name}</h3>
                </div>
                <div className="w-col w-col-2">
                  <h3 className="teblehead entrie">{this.props.data.creation_date}</h3>
                </div>
                <div className="w-col w-col-1">
                  <h3 className="teblehead entrie">{this.props.data.created_by}</h3>
                </div>
                <div className="w-col w-col-2">
                  <div className="w-row">
                    <div className="w-col w-col-6">
                      <a href={"http://"+AppGlobal.host+"/db_manage/edit_db_record/"+this.props.data.id} style={{'lineHeight': 39 + 'px'}}  className="teblehead entrie link">edit</a>
                    </div>
                    <div className="w-col w-col-6">
                      <h3 className="teblehead entrie link" onClick={this.toggleDelete}>delete</h3>
                      <div className={deleteClass}>
                        <h4>You will not be able to restore record!</h4>
                        <div className="deletebuttonsblock"><a onClick={this.deleteRecord} className="w-button deletebutton">Confirm</a><a className="w-button deletebutton" onClick={this.toggleDelete}>Cancel</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        )
    }
})

module.exports = EntriesPointConteiner;