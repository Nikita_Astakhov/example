var React = require('react');
var ReactDOM = require('react-dom');
var Report = require('./reports/reportPage');

var AppInit = require('./AppInit');
AppInit();

ReactDOM.render(<Report />, document.getElementById('ReportBlock'));