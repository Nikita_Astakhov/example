
var AssignedConteiner = React.createClass({
  propTypes: {  
    instance_id: PropTypes.number,
    unmount: PropTypes.func,
    right: PropTypes.bool,
  },
  getInitialState: function () {
    return {
      data: []
    }
  },
  componentWillMount: function () {
    var source = 'http://'+AppGlobal.host + "/api/incidents/assigned/"+this.props.instance_id+"/?format=json";
    this.serverRequest = $.get(source, function (results) {
      var filt_data = results.results;
      this.setState({
        data: filt_data,
      });
    }.bind(this));
  },
  render: function () {
    var self = this;
    if (this.state.data.length > 0) {
      var listInstances = this.state.data.map(function(ins){
        return (<AssignedInstance key={ins.id} data={ins} />)
      });
    } else {
      var listInstances = (<div className="assignedidinfo__entrie"><h3 className="teblehead entrie infot">No data available</h3></div>);
    }
    var assignedClass = classNames({
      "assignedidinfoblock": true,
      'rightconteiner': this.props.right
    });
    return (
      <div className={assignedClass} onMouseLeave={this.props.unmount}>
              <div className="w-row assignedidheaderrow">
                <div className="w-col w-col-1">
                  <h3 className="teblehead entrie infot">ID</h3>
                </div>
                <div className="w-col w-col-5">
                  <h3 className="teblehead entrie infot">Title</h3>
                </div>
                <div className="w-col w-col-2">
                  <h3 className="teblehead entrie infot">Recording Date</h3>
                </div>
                <div className="w-col w-col-2">
                  <h3 className="teblehead entrie infot">Recorded by</h3>
                </div>
                <div className="w-col w-col-2">
                  <h3 className="teblehead entrie infot">Action</h3>
                </div>
              </div>
              {listInstances}
            </div>
    )
  }
});

var AssignedInstance = React.createClass({
	propTypes: {  
		data: PropTypes.object,
	},
	render: function () {
		return (
			<div className="assignedidinfo__entrie">
              <div className="w-row">
                <div className="w-col w-col-1">
                  <h3 className="teblehead entrie infot">{this.props.data.id}</h3>
                </div>
                <div className="w-col w-col-5">
                  <h3 className="teblehead entrie infot">{this.props.data.name}</h3>
                </div>
                <div className="w-col w-col-2">
                  <h3 className="teblehead entrie infot">{this.props.data.creation_date}</h3>
                </div>
                <div className="w-col w-col-2">
                  <h3 className="teblehead entrie infot">{this.props.data.created_by}</h3>
                </div>
                <div className="w-col w-col-2">
                  <h3 className="teblehead entrie infot edit"><a href={"http://"+AppGlobal.host+"/db_manage/edit_db_record/"+this.props.data.id}>edit</a></h3>
                </div>
              </div>
            </div>
		)
	}
})

var ToouchedByConteiner = React.createClass({
  propTypes: {  
    instance_id: PropTypes.number,
    unmount: PropTypes.func,
  },
  getInitialState: function () {
    return {
      data: []
    }
  },
  componentWillMount: function () {
    var source = 'http://'+AppGlobal.host + "/api/incidents/touched/"+this.props.instance_id+"/?format=json";
    this.serverRequest = $.get(source, function (results) {
      var filt_data = results.results;
      this.setState({
        data: filt_data,
      });
    }.bind(this));
  },
  render: function () {
    var self = this;
    if (this.state.data.length > 0) {
      var listTouches = this.state.data.map(function(ins){
        return (<TouchInstance key={ins.id} data={ins} />)
      });
    } else {
      var listTouches = (<div className="touchedeventconteiner"><h3 className="teblehead entrie infot">No data available</h3></div>);
    }
    return (
      <div className="touchedinfoblock" onMouseLeave={this.props.unmount}>
      <div className="w-row touchedinfoheadrow">
              <div className="w-col w-col-3">
                <h3 className="teblehead info">Date</h3>
              </div>
              <div className="w-col w-col-4">
                <h3 className="teblehead info">Username</h3>
              </div>
              <div className="w-col w-col-5">
                <h3 className="teblehead info">Action</h3>
              </div>
            </div>
            {listTouches}
            </div>
    )
  }
});

var TouchInstance = React.createClass({
  propTypes: {  
    data: PropTypes.object,
  },
  render: function () {
    return (
      <div className="touchedeventconteiner">
              <div className="w-row">
                <div className="w-col w-col-3">
                  <h3 className="teblehead entrie infot">{this.props.data.date}</h3>
                </div>
                <div className="w-col w-col-4">
                  <h3 className="teblehead entrie infot">{this.props.data.touched_by}</h3>
                </div>
                <div className="w-col w-col-5">
                  <h3 className="teblehead entrie infot">{this.props.data.action_type}</h3>
                </div>
              </div>
            </div>
    )
  }
})

var LocalityConteiner = React.createClass({
    mixins: [TimerMixin],
    propTypes: {  
        toggle200: PropTypes.func,
        toggle500: PropTypes.func,
        reloadMain: PropTypes.func, 
        locationid: PropTypes.number,
        selectedReg: PropTypes.number,
        selectedLoc: PropTypes.number,
        updateLocation: PropTypes.func,
    },
    getInitialState: function () {
        return {
            choosed: -1,
            locality: [],
            existing: false,
            region: 1
        }
    },
    componentWillMount: function () {
        var source_loc = 'http://' + AppGlobal.host + "/api/manage_db/db_data/locality/" + this.props.selectedReg + "/?format=json";
        this.serverRequest = $.get(source_loc, function (results) {
            var location_data = results.results;
            this.setState({
              locality: location_data,
              choosed: this.props.selectedLoc,
              region: this.props.selectedReg
            });
        }.bind(this));
    },
    componentWillReceiveProps: function (nextProps) {
        var source_loca = 'http://' + AppGlobal.host + "/api/manage_db/db_data/locality/" + nextProps.selectedReg + "/?format=json";
        this.serverRequest = $.get(source_loca, function (results) {
            var location_data = results.results;
            this.setState({
              locality: location_data,
              choosed: nextProps.selectedLoc,
              region: nextProps.selectedReg
            });
        }.bind(this));
    },
    hendleChange: function (event) {
        var self = this;
        var num = parseInt(event.target.value);
        this.setState({
            choosed: num
        }, self.props.updateLocation(num, 2, false, self.props.locationid));
    },
    renderSearch: function () {
        this.setState({
            addNew: true,
            existing: true
        });
        ReactDom.render(<LocationSearchConteiner change={this.getFromSearch} unmount={this.unrenderSearch} region={this.state.region} locality={this.state.choosed} what={1}  />, this.refs.searchConteinerPlace);
    },
    unrenderSearch: function () {
        this.setState({
            addNew: true,
            existing: false
        });
        ReactDom.unmountComponentAtNode(this.refs.searchConteinerPlace);
    },
    getFromSearch: function (value) {
        var self = this;
        var num = parseInt(value);
        this.setState({
            choosed: num
        }, self.props.updateLocation(num, 2, false, self.props.locationid), self.unrenderSearch());
    },
    render: function () {
        var self = this;
        var listOptions = this.state.locality.map(function (inst) {
            return (<option key={inst.id} value={inst.id}>{inst.name}</option>)
        });
        var empty;
        if (this.state.choosed === -1) {
            empty = (<option value="-1">Please select locality</option>)
        }
        var existClass = classNames({
            'gradingList': true,
            'hidden': this.state.existing
        });
        return (
            <div className="w-col w-col-3">
              <div className={existClass}>
                <label className="newincidentform__label">Locality</label>
                <select onChange={this.hendleChange} value={this.state.choosed} className="w-select newincidentform__input">
                  {empty}
                  {listOptions}
                </select>
                <div className="w-row">
                  <div className="w-col w-col-12">
                    <h4 onClick={this.renderSearch} className="newincidentform__hint addelement verycloseup">Search</h4>
                  </div>
                </div>
              </div>
              <div ref="searchConteinerPlace"></div>
            </div>
        )
    }
})

var VillageConteiner = React.createClass({
    mixins: [TimerMixin],
    propTypes: {  
        toggle200: PropTypes.func,
        toggle500: PropTypes.func,
        reloadMain: PropTypes.func, 
        locationid: PropTypes.number,
        selectedReg: PropTypes.number,
        selectedLoc: PropTypes.number,
        selectedVil: PropTypes.number,
        updateLocation: PropTypes.func,
    },
    getInitialState: function () {
        return {
            choosed: -1,
            villages: [],
            existing: false,
        }
    },
    componentWillMount: function () {
        var source_loc = 'http://' + AppGlobal.host + "/api/manage_db/db_data/village/" + this.props.selectedReg +"/"+ this.props.selectedLoc + "/?format=json";
        this.serverRequest = $.get(source_loc, function (results) {
            var location_data = results.results;
            this.setState({
              villages: location_data,
              choosed: this.props.selectedVil
            });
        }.bind(this));
    },
    componentWillReceiveProps: function (nextProps) {
        var source_loca = 'http://' + AppGlobal.host + "/api/manage_db/db_data/village/" + nextProps.selectedReg +"/"+ nextProps.selectedLoc + "/?format=json";
        this.serverRequest = $.get(source_loca, function (results) {
            var location_data = results.results;
            this.setState({
              villages: location_data,
              choosed: nextProps.selectedVil,
            });
        }.bind(this));
    },
    hendleChange: function (event) {
        var self = this;
        var num = parseInt(event.target.value);
        this.setState({
            choosed: num
        }, self.props.updateLocation(num, 3, false, self.props.locationid));
    },
    renderSearch: function () {
        this.setState({
            addNew: true,
            existing: true
        });
        ReactDom.render(<LocationSearchConteiner change={this.getFromSearch} unmount={this.unrenderSearch} region={this.props.selectedReg} locality={this.props.selectedLoc} what={2}  />, this.refs.searchConteinerPlace);
    },
    unrenderSearch: function () {
        this.setState({
            addNew: true,
            existing: false
        });
        ReactDom.unmountComponentAtNode(this.refs.searchConteinerPlace);
    },
    getFromSearch: function (value) {
        var self = this;
        var num = parseInt(value);
        this.setState({
            choosed: num
        }, self.props.updateLocation(num, 3, false, self.props.locationid), self.unrenderSearch());
    },
    render: function () {
        var self = this;
        var listOptions = this.state.villages.map(function (inst) {
            return (<option key={inst.id} value={inst.id}>{inst.name}</option>)
        });
        var empty;
        if (this.state.choosed === -1) {
            empty = (<option value="-1">Please select locality</option>)
        }
        var existClass = classNames({
            'gradingList': true,
            'hidden': this.state.existing
        });
        return (
            <div className="w-col w-col-3">
              <div className={existClass}>
                <label className="newincidentform__label">Village / Town</label>
                <select onChange={this.hendleChange} value={this.state.choosed} className="w-select newincidentform__input">
                  {empty}
                  {listOptions}
                </select>
                <div className="w-row">
                  <div className="w-col w-col-12">
                    <h4 onClick={this.renderSearch} className="newincidentform__hint addelement verycloseup">Search</h4>
                  </div>
                </div>
              </div>
              <div ref="searchConteinerPlace"></div>
            </div>
        )
    }
})

var LocationSearchConteiner = React.createClass({
    mixins: [TimerMixin],
    propTypes: {  
        change: PropTypes.func,
        unmount: PropTypes.func,
        region: PropTypes.number,
        locality: PropTypes.number,
        what: PropTypes.number,
    },
    getInitialState: function () {
        return {
            serachN: '',
            data: []
        }
    },
    hendleInput: function (event) {
        var self = this;
        this.setState({
            serachN: event.target.value,
        }, self.updateData());
    },
    updateData: function () {
        var self = this;
        this.setTimeout( function () {
            if (this.state.serachN.length > 0) {
                if (self.props.what === 1) {
                    var source_loca = 'http://' + AppGlobal.host + "/api/manage_db/db_data/searc/region/"+ this.props.region +"/" + this.state.serachN + "/?format=json";
                } else {
                    var source_loca = 'http://' + AppGlobal.host + "/api/manage_db/db_data/searc/village/"+ this.props.region + "/" + this.props.locality + "/" + this.state.serachN + "/?format=json";
                }    
                this.serverRequest = $.get(source_loca, function (results) {
                    var location_data = results.results;
                    this.setState({
                      data: location_data,
                    });
                }.bind(this));
            }    
        },200);
    },
    clearData: function () {
        this.setState({
            data: [],
            serachN: ''
        })
    },
    render: function () {
        var self = this;
        var listVariants = this.state.data.map(function (inst) {
            return (
                <div key={inst.id} className="db__searchresultinstance">
                  <div className="w-row">
                    <div className="w-col w-col-8">
                      <p className="listincidents__form_label db_entries littleup">{inst.name}</p>
                    </div>
                    <div className="w-col w-col-4">
                      <h4 onClick={self.props.change.bind(null, inst.id)} className="newincidentform__hint addelement margin10">Choose</h4>
                    </div>
                  </div>
                </div>
            )
        });
        if (self.props.what === 1) {
            var place = 'locality';
        } else {
            var place = 'village / town';
        }
        return (
            <div>
              <div className="w-row">
                <div className="w-col w-col-8">
                  <label className="newincidentform__label">Input {place} name</label>
                  <input onChange={this.hendleInput} value={this.state.serachN} className="w-input newincidentform__input" placeholder={"Start to input " + place + " name here"} type="text"/>
                  <h4 onClick={this.props.unmount} className="newincidentform__hint addelement verycloseup">Cancel</h4>
                </div>
                <div className="w-col w-col-4"><a onClick={this.clearData} className="w-button listincidents__form_submit apply">Clear</a>
                </div>
              </div>
            <div className="db__searchresultconteiner">
              {listVariants}
            </div>
            </div>
        )
    }
})

var CoordinatesConteiner = React.createClass({
    mixins: [TimerMixin],
    propTypes: {  
        toggle200: PropTypes.func,
        toggle500: PropTypes.func,
        reloadMain: PropTypes.func, 
        locationid: PropTypes.number,
        selectedReg: PropTypes.number,
        selectedLoc: PropTypes.number,
        selectedVil: PropTypes.number,
        selectedCoo: PropTypes.object,
        updateLocation: PropTypes.func,
    },
    getInitialState: function () {
        return {
            e: '',
            n: ''
        }
    },
    componentWillMount: function () {
        var source_loc = 'http://' + AppGlobal.host + "/api/manage_db/db_data/coordinates/" + this.props.selectedReg +"/"+ this.props.selectedLoc + "/" + this.props.selectedVil + "/?format=json";
        this.serverRequest = $.get(source_loc, function (results) {
            var location_data = results.results[0];
            this.setState({
              e: location_data.coordinates.e,
              n: location_data.coordinates.n
            });
        }.bind(this));
    },
    componentWillReceiveProps: function (nextProps) {
        var source_loc = 'http://' + AppGlobal.host + "/api/manage_db/db_data/coordinates/" + nextProps.selectedReg +"/"+ nextProps.selectedLoc + "/" + nextProps.selectedVil + "/?format=json";
        this.serverRequest = $.get(source_loc, function (results) {
            var location_data = results.results[0];
            this.setState({
              e: location_data.coordinates.e,
              n: location_data.coordinates.n
            });
        }.bind(this));
    },
    hendleE: function (event) {
        var self = this;
        this.setState({
            e: event.target.value
        }, self.updateCoordinates());
    },
    hendleN: function (event) {
        var self = this;
        this.setState({
            n: event.target.value
        }, self.updateCoordinates());
    },
    updateCoordinates: function () {
        var self = this;
        var source = 'http://'+AppGlobal.host+'/api/manage_db/update_entry/coordinates/';
        this.setTimeout( function () {
            var post_d = {'user_id': parseInt(AppGlobal.user), 'e': self.state.e, 'n': self.state.n, 'region': parseInt(self.props.selectedReg), 'locality': parseInt(self.props.selectedLoc), 'village': parseInt(this.props.selectedVil)};
            $.ajax({
                headers: { 
                  'Accept': 'application/json',
                  'Content-Type': 'application/json' 
                },
                type: "POST",
                url: source,
                data: JSON.stringify(post_d, null, '\t'),
                dataType: 'json',
                statusCode: {
                  200: function (data) {self.props.toggle200(data.text);},
                  500: function () {self.props.toggle500('Something went wrong. Please reload the page.');},
                }
            });
        }, 200)
    },
    render: function () {
        return (
            <div>
              <div className="w-col w-col-2">
                <label className="newincidentform__label">E</label>
                <input onChange={this.hendleE} value={this.state.e} className="w-input newincidentform__input" placeholder="Input number here" type="text"/>
              </div>
              <div className="w-col w-col-2">
                <label className="newincidentform__label">N</label>
                <input onChange={this.hendleN} value={this.state.n} className="w-input newincidentform__input" placeholder="Input number here" type="text"/>
              </div>
            </div>
        )
    }
})

var DescrVictimInstance = React.createClass({
    mixins: [TimerMixin],
    propTypes: {  
        data: PropTypes.object,
        prechecked: PropTypes.bool,
        move: PropTypes.func,  
    },
    getInitialState: function () {
        return {
            checked: false
        }
    },
    componentWillMount: function () {
        this.setState({
            checked: this.props.prechecked
        });
    },
    componentWillReceiveProps: function (nextProps) {
        this.setState({
            checked: nextProps.prechecked
        });
    },
    toggleCheck: function () {
        var self = this;
        this.setState({
            checked: !this.state.checked
        });
        var extend = 2;
        this.setTimeout( function () {
            if (this.state.checked) {
                extend = 1;
            }
            self.props.update(parseInt(self.props.data.id), extend, false);
        },200)

    },
    render: function () {
        var checkClass = classNames({
            'w-button checkbox': true,
            'checked': this.state.checked
        });
        return (
            
            <div onClick={this.toggleCheck} className="db__victiminstance">
              <div className="w-row">
                <div className="w-col w-col-2">
                  <a className={checkClass}></a>
                </div>
                <div className="w-col w-col-10">
                  <p className="listincidents__form_label db_entries littleup">{this.props.data.name}</p>
                </div>
              </div>
            </div>
        )
    },
})
var DescrActorInstance = React.createClass({
    mixins: [TimerMixin],
    propTypes: {  
        data: PropTypes.object,
        prechecked: PropTypes.bool,
        move: PropTypes.func,  
    },
    getInitialState: function () {
        return {
            checked: false
        }
    },
    componentWillMount: function () {
        this.setState({
            checked: this.props.prechecked
        });
    },
    componentWillReceiveProps: function (nextProps) {
        this.setState({
            checked: nextProps.prechecked
        });
    },
    toggleCheck: function () {
        var self = this;
        this.setState({
            checked: !this.state.checked
        });
        var extend = 2;
        this.setTimeout( function () {
            if (this.state.checked) {
                extend = 1;
            }
            self.props.update(parseInt(self.props.data.id), extend, false);
        },200)

    },
    render: function () {
        var checkClass = classNames({
            'w-button checkbox': true,
            'checked': this.state.checked
        });
        return (
            <div onClick={this.toggleCheck} className="db__victiminstance">
              <div className="w-row">
                <div className="w-col w-col-2">
                  <a className={checkClass}></a>
                </div>
                <div className="w-col w-col-10">
                  <p className="listincidents__form_label db_entries littleup">{this.props.data.name}</p>
                </div>
              </div>
            </div>
        )
    },
})
var NatureSubConteiner = React.createClass({
    mixins: [TimerMixin],
    propTypes: {  
        toggle200: PropTypes.func,
        toggle500: PropTypes.func,
        reloadMain: PropTypes.func, 
        numberMain: PropTypes.number,
        numberSub: PropTypes.number, 
        recId: PropTypes.number,
        updateSubGroup: PropTypes.func, 
    },
    getInitialState: function () {
        return {
            choosed: -1,
            data: []
        }
    },
    componentWillMount: function () {
        var source_loc = 'http://' + AppGlobal.host + "/api/manage_db/db_data/nature_sub/" + parseInt(this.props.numberMain) + "/?format=json";
        this.serverRequest = $.get(source_loc, function (results) {
            var location_data = results.results;
            this.setState({
              data: location_data,
              choosed: parseInt(this.props.numberSub)
            });
        }.bind(this));
    },
    componentWillReceiveProps: function (nextProps) {
        var source_loca = 'http://' + AppGlobal.host + "/api/manage_db/db_data/nature_sub/" + parseInt(nextProps.numberMain) + "/?format=json";
        this.serverRequest = $.get(source_loca, function (results) {
            var location_data = results.results;
            this.setState({
              data: location_data,
              choosed: parseInt(nextProps.numberSub),
            });
        }.bind(this));
    },
    hendleChange: function (event) {
        var self = this;
        this.setState({
            choosed: parseInt(event.target.value),
        })
        this.setTimeout( function () {
            self.props.updateSubGroup(self.state.choosed, self.props.recId, false);
        },200);
    },
    render: function () {
        var self = this;
        var listOptions = this.state.data.map(function (inst) {
            return (<option key={inst.id} value={inst.id}>{inst.name}</option>)
        });
        var select;
        if (parseInt(this.state.choosed) === -1) {
            select = (<option value="-1">Select sub group</option>)
        }
        var existClass = classNames({
            'gradingList': true,
            'hidden': this.state.addNew
        });
        return (
            <div>
            <div className={existClass}>
              <div className="w-row">
               <div className="w-col w-col-11">
                 <label className="newincidentform__label">Sub categorie</label>
                 <select onChange={this.hendleChange} value={this.state.choosed} className="w-select newincidentform__input" >
                   {select}
                   {listOptions}
                 </select>
               </div>
              </div>
            </div>    
            </div>
        )
    }
})

var SelectedInstance = React.createClass({
    propTypes: {  
        data: PropTypes.object,
        number: PropTypes.number,
        remove: PropTypes.func,
    },
    hendle: function () {
        this.props.remove(this.props.data);
    },
    render: function () {
        return (
            <div className="w-row">
              <div className="w-col w-col-4">
                <h3 className="entrie segment teblehead">{this.props.data.name}</h3>
              </div>
              <div className="w-col w-col-1">
                <h3 onClick={this.hendle} className="entrie link teblehead">remove</h3>
              </div>
              <div className="fhe w-col w-col-7"></div>
            </div>
        )
    }
})

var ListSegmentsConteiner = React.createClass({
    mixins: [TimerMixin],
    propTypes: {  
        select: PropTypes.func,
        selected: PropTypes.array, 
    },
    getInitialState: function () {
        return {
            data: [],
            name: ''
        }
    },
    componentWillMount: function () {
        var source = 'http://'+AppGlobal.host + "/api/segment/startEntries/?format=json;page=1";
        this.serverRequest = $.get(source, function (results) {
            var segments = results.results;
            this.setState({
              data: segments,
            });
        }.bind(this));
    },
    filterHendler: function (event) {
        var self = this;
        this.setState({
            name: event.target.value,
        }, self.filterData(event.target.value));
    },
    filterData: function (name) {
        if (name.length > 0) {
            var source = 'http://'+AppGlobal.host + "/api/segment/filterEntries/"+ name +"/?format=json;";
            this.serverRequest = $.get(source, function (results) {
                var segments = results.results;
                this.setState({
                  data: segments,
                });
            }.bind(this));
        }   
    },
    clearFilter: function () {
        var source = 'http://'+AppGlobal.host + "/api/segment/startEntries/?format=json;page=1";
        this.serverRequest = $.get(source, function (results) {
            var segments = results.results;
            this.setState({
              data: segments,
              name: '',
            });
        }.bind(this));
    },
    handleSubmit: function (event) {
        event.preventDefault();
    },
    render: function () {
        var self = this;
        var listSegments = this.state.data.map(function (inst) {
            var exist = self.props.selected.some(function (obj) {
                return inst.id === obj.id;
            })
            if (!exist) {
                return (<SegmentInstance select={self.props.select} data={inst} key={inst.id} />);
            }
        }); 
        return (
            <div className="segment__conteiner">
                <div className="segment_header_row w-row">
                  <div className="w-col w-col-10">
                    <h4 className="listincidents__filterhead">Please choose search criteria</h4>
                  </div>
                  <div className="fhe w-col w-col-2"></div>
                </div>
                <SegmentConstructorBody />
                <div className="segment__actions__block">
                  <div className="w-row">
                    <div className="w-col w-col-4">
                      <div className="filterincidentdate segmentfilter w-form">
                        <form onSubmit={this.handleSubmit}>
                          <div className="w-row">
                            <div className="w-col w-col-10">
                              <label className="listincidents__form_label">Filter search criteria</label>
                              <input onChange={this.filterHendler} value={this.state.name} className="listincidents__form_input w-input" maxLength="256" placeholder="Input search criteria name here" type="text"/>
                            </div>
                            <div className="w-col w-col-2">
                              <a onClick={this.clearFilter} className="listincidents__form_submit w-button">Clear</a>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                    <div className="w-col w-col-8"></div>
                  </div>
                </div>
                <div className="segements__list__conteiner">
                  {listSegments}
                </div>
              </div>
        )
    }
})
<SelectedInstance key={instance.id} number={key+1} data={instance} remove={self.removeSegment} />

var segNum = 1;
var SegementsConteiner = React.createClass({
    propTypes: {  
        select: PropTypes.func,
        changeDates: PropTypes.func
    },
    getInitialState: function () {
        return {
            create: false,
            listSegments: false,
            selectedSegments: [],
            segmentsData: [],
            start: '',
            end: '',
            range: '',
            showCal: false,
        }
    },
    renderSegments: function () {
          ReactDom.render(<ListSegmentsConteiner selected={this.state.selectedSegments} select={this.appendSegment} />, this.refs.segmentsOptionsConteiner);
          this.setState({
              listSegments: true
          })
      },addBlock: function () {
      var arr = this.state.selected;
      actNum += 1
      arr.push(actNum);
      this.setState({
        selected:arr
      })
    },
    removeBlock: function(number) {
      var arr = this.state.selected;
      var vals = this.state.values;
      var index = arr.indexOf(number);
      if (index > -1) {
          arr.splice(index, 1);
      }
      var i=0;
      for (i = 0; i < vals.length; i++) {
          // This if statement depends on the format of your array
          if (vals[i][0] === number) {
              vals.splice(i, 1)
              break;   // Found it
          }
      }
      this.props.update(0, vals);
      //remove values from variants array
      this.setState({
        selected:arr,
        values: vals
      })
    },
    appendSegment: function (obj) {
        this.props.select(1, parseInt(obj.id));
        var seg_list = this.state.selectedSegments;
        seg_list.push(obj);
        this.setState({
            selectedSegments: seg_list,
        });
        ReactDom.unmountComponentAtNode(this.refs.segmentsOptionsConteiner);
    },
    removeSegment: function (obj) {
        var all_list = this.state.selectedSegments;
        var exist = all_list.some(function(inst){
            return inst === obj;
        });  
        if (exist) {
            var index = all_list.indexOf(obj);
            var removed = all_list.splice(index, 1)
            this.setState({
                data: all_list,
            })
            this.props.select(2, parseInt(obj.id));
        }
    },
    //////////////////////
    // Calendar methods //
    //////////////////////
    updateDates: function (start, end, total) {
        this.setState({
            start: start,
            end: end,
            range: total
        })
        this.props.changeDates(start, end);
    },
    handleCalendarClick: function () {
        if (this.state.showCal) {
            this.unmountCalendar();
        } else {
            this.renderRangeConteiner();
        }
    },
    renderRangeConteiner: function () {
        ReactDom.render(<CalendarConteiner updateDates={this.updateDates} key="DatesRangeCalendarConteiner" />, this.refs.datesRangeConteiner);
        this.setState({
            showCal: true
        })
    },
    unmountCalendar: function () {
        ReactDom.unmountComponentAtNode(this.refs.datesRangeConteiner);
        this.setState({
            showCal: false
        })
    },
    render: function () {
        var self = this;
        if (this.state.showCal) {
            var butText = 'Apply and close';
        } else {
            var butText = 'Select period of time';
        }
        if (this.state.selectedSegments.length > 0) {
            var sText = (<h4 className="listincidents__filterhead">Selected criteria:</h4>);
        } else {
            var sText = (<h4 className="listincidents__filterhead"></h4>);
        }
        var listSelected = this.state.selectedSegments.map(function(instance, key) {
            return (<ListSegmentsConteiner selected={self.state.selectedSegments} select={self.appendSegment} />)
        });
        return (
            <div className="segments__conteiner">
              <div className="segements__choosed__conteiner">
                <a onClick={this.renderSegments} style={{'width': '310px'}} className="button newrecoed segment w-button">Choose Search Criteria</a>
                <div className="w-row">
                  <div className="w-col w-col-8">
                    <div className="list__chosed_segments">
                      {sText}
                      {listSelected}
                    </div>
                  </div>
                  <div className="w-col w-col-4">
                  </div>  
                </div>
              </div>
              <div ref="segmentsOptionsConteiner"></div>
              <div className="w-row">
                <div className="w-col w-col-2">
                <div className="xaxis__conteiner">
                  <h4 className="listincidents__filterhead">Select period of time:</h4>
                  {this.state.range != '' ? <p className="listincidents__form_label">{this.state.range}</p> : null}
                  <div className="filterincidentdate">
                    <div onClick={this.handleCalendarClick}> 
                      <div style={{'width': '100%', 'marginTop': '0px', 'marginBottom': '15px'}} className="w-button listincidents__form_submit db">{butText}</div>
                    </div>
                    <div ref="datesRangeConteiner"/>
                  </div>
                </div>
              </div>
              </div>
            </div>
        )
    }
})