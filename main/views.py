from __future__ import unicode_literals
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView

from main.models import *

# Create your views here.

class PointView(TemplateView):
	
	template_name = 'main/index.html'
	
	def get_context_data(self):
		context = super(PointView, self).get_context_data()
		return context