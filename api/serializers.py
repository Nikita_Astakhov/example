from incidents.models import *
from db_management.models import *
from analytics.models import *
from rest_framework import serializers

class FileUploadSerializer(serializers.ModelSerializer):
	document = serializers.FileField(max_length=250, use_url=True)
	class Meta:
		model = Document
		fields = ('id', 'uploaded_at', 'document', 'description')

class SourceSerializer(serializers.ModelSerializer):
	value = serializers.IntegerField(source='id')
	label = serializers.CharField(source='name')
	class Meta:
		model = Source
		fields = ('value', 'label')

class IncidentListSerializer(serializers.ModelSerializer):
	creation_date = serializers.ReadOnlyField(source='convert_date')
	appearence_date = serializers.ReadOnlyField(source='convert_report_date')
	class Meta:
		model = IncindentDescription
		fields = ('id', 'name', 'creation_date', 'description', 'appearence_date')

class IncLocationSerializer(serializers.ModelSerializer):
	class Meta:
		model = Location
		fields = '__all__'

class IncidentDataSerializer(serializers.ModelSerializer):
	creation_date = serializers.ReadOnlyField(source='convert_date')
	source = serializers.ReadOnlyField(source='get_source')
	locations = IncLocationSerializer(read_only=True, many=True)
	class Meta:
		model = IncindentDescription
		fields = ('id', 'name', 'creation_date', 'description', 'source', 'locations')

#class MdbFilterWeeksIncidentSerializer(serializers.ModelSerializer):
#	creation_date = serializers.ReadOnlyField(source='convert_date')
#	#source = serializers.ReadOnlyField(source='get_source')
#	class Meta:
#		model = IncindentDescription
#		fields = ('id', 'name', 'creation_date')

class IncidentTouchSerializer(serializers.ModelSerializer):
	date = serializers.ReadOnlyField(source='convert_date')
	touched_by = serializers.ReadOnlyField(source='get_name')
	action_type = serializers.ReadOnlyField(source='get_type')
	class Meta:
		model = TouchedDescription
		fields = ('id', 'touched_by', 'action_type', 'date')	

class EntryAssignedSerializer(serializers.ModelSerializer):
	creation_date = serializers.ReadOnlyField(source='convert_date')
	created_by = serializers.ReadOnlyField(source='get_name')
	class Meta:
		model = Entry
		fields = ('id', 'name', 'creation_date', 'created_by')

class RegionsSerializer(serializers.ModelSerializer):
	class Meta:
		model = Region
		fields = ('id', 'name')

class LocalitysSerializer(serializers.ModelSerializer):
	class Meta:
		model = Locality
		fields = ('id', 'name')

class VillagesSerializer(serializers.ModelSerializer):
	class Meta:
		model = Village
		fields = ('id', 'name')

class CoordinatesSerializer(serializers.ModelSerializer):
	class Meta:
		model = Coordinats
		fields = ('id', 'e', 'n')

class LocationSerializer(serializers.ModelSerializer):
	computed = serializers.ReadOnlyField(source="computeNames")
	class Meta:
		model = Location
		fields = ('id', 'computed') # remove all unnecessary

class NaturesSerializer(serializers.ModelSerializer):
	computed = serializers.ReadOnlyField(source='computeNames')
	class Meta:
		model = Nature
		fields = ('id', 'main', 'sub', 'computed')

class IncActorsSerializer(serializers.ModelSerializer):
	value = serializers.IntegerField(source='id')
	label = serializers.CharField(source='name')
	class Meta:
		model = ActorsGroup
		fields = ('value', 'label')

class EntrysData_ReportsData(serializers.ModelSerializer):
	creation_date = serializers.ReadOnlyField(source='convert_date')
	appearence_date = serializers.ReadOnlyField(source='convert_report_date')
	source = serializers.ReadOnlyField(source='get_source_name')
	class Meta:
		model = IncindentDescription
		fields = ('id', 'name', 'creation_date', 'appearence_date', 'description', 'source', 'inf_grading',)

class EntrysStartList(serializers.ModelSerializer):
	creation_date = serializers.ReadOnlyField(source='convert_date')
	created_by = serializers.ReadOnlyField(source='get_name')
	class Meta:
		model = Entry
		fields = ('id', 'name', 'creation_date', 'created_by')
		depth = 1

class DocumentsSerializer(serializers.ModelSerializer):
	class Meta:
		model = Document
		fields = ('id', 'description', 'document')	

class LinksSerializer(serializers.ModelSerializer):
	class Meta:
		model = Links
		fields = ('id', 'url')

class ValueVicsSerializer(serializers.ModelSerializer):
	computed = serializers.ReadOnlyField(source='computeNames')
	class Meta:
		model = EntryVictimsData
		fields = ('id', 'group', 'quantity', 'computed')	

class EntrysData_ValueInfo(serializers.ModelSerializer):
	time = serializers.ReadOnlyField(source='convert_time')
	date = serializers.ReadOnlyField(source='convert_date')
	locations = LocationSerializer(read_only=True, many=True)
	nature = NaturesSerializer(read_only=True, many=True)
	actors = IncActorsSerializer(read_only=True, many=True)
	victims = ValueVicsSerializer(read_only=True, many=True)
	class Meta:
		model = EntryIncidentsValue
		fields = ('id', 'date', 'time', 'locations', 'nature', 'actors', 'victims')	

class EntrysData_MainInfo(serializers.ModelSerializer):
	documents = DocumentsSerializer(read_only=True, many=True)
	links = LinksSerializer(read_only=True, many=True)
	#value_instance = ValueInstanceSerializer(read_only=True, many=True)
	class Meta:
		model = Entry
		fields = ('id', 'name', 'remarks', 'JMACcomments', 'Summary', 'documents', 'links', 'value_instance')		

class VictimsData_All(serializers.ModelSerializer):
	value = serializers.IntegerField(source='id')
	label = serializers.CharField(source='name')
	class Meta:
		model = VictimClass
		fields = ('value', 'label')

class DBData_infGradings(serializers.ModelSerializer):
	class Meta:
		model = InformationGrading
		fields = ('id', 'name')

class DBData_natureSubs(serializers.ModelSerializer):
	value = serializers.IntegerField(source='id')
	label = serializers.CharField(source='name')
	class Meta:
		model = NatureSubGroups
		fields = ('value', 'label')

class DBData_natureMains(serializers.ModelSerializer):
	value = serializers.IntegerField(source='id')
	label = serializers.CharField(source='name')
	class Meta:
		model = NatureMainGroup
		fields = ('value', 'label')

class DBData_actors(serializers.ModelSerializer):
	value = serializers.IntegerField(source='id')
	label = serializers.CharField(source='name')
	class Meta:
		model = ActorsGroup
		fields = ('value', 'label')

# class DBData_locations(serializers.ModelSerializer):
# 	class Meta:
# 		model = Region
# 		fields = '__all__'

# class DBData_localitys(serializers.ModelSerializer):
# 	class Meta:
# 		model = Locality
# 		fields ='__all__'

# class DBData_villages(serializers.ModelSerializer):
# 	class Meta:
# 		model = Village
# 		fields = '__all__'

# class DBData_vill(serializers.ModelSerializer):
# 	class Meta:
# 		model = Village
# 		fields = ('id', 'name')

class DBData_coordinates(serializers.ModelSerializer):
	class Meta:
		model = Coordinats
		fields = ('e', 'n')

class DBData_vilCoordinates(serializers.ModelSerializer):
	coordinates = DBData_coordinates(read_only=True)
	class Meta:
		model = Village
		fields = ('id', 'coordinates')

class IncDescriptionSerializer(serializers.ModelSerializer):
	class Meta:
		model = IncindentDescription
		fields = ('id','description')

#########################
# Analytics serializers #
#########################

class SegementsListSerializer(serializers.ModelSerializer):
	value = serializers.IntegerField(source='id')
	label = serializers.CharField(source='name')
	class Meta:
		model = CreatedSegments
		fields =('value', 'label')	

class VillagesLabelSerializer(serializers.ModelSerializer):
	value = serializers.IntegerField(source='id')
	label = serializers.CharField(source='name')
	class Meta:
		model = Village
		fields =('value', 'label')

class LocalityLabelSerializer(serializers.ModelSerializer):
	value = serializers.IntegerField(source='id')
	label = serializers.CharField(source='name')
	class Meta:
		model = Locality
		fields =('value', 'label')

class RegionLabelSerializer(serializers.ModelSerializer):
	value = serializers.IntegerField(source='id')
	label = serializers.CharField(source='name')
	class Meta:
		model = Region
		fields =('value', 'label')

class LocationLabelSerializer(serializers.ModelSerializer):
	act_villages = VillagesLabelSerializer(read_only=True, many=True)
	act_localitys = LocalityLabelSerializer(read_only=True, many=True)
	act_regions = RegionLabelSerializer(read_only=True, many=True)
	class Meta:
		model = SavedLocalitys
		fields = ('id', 'act_villages' ,'act_localitys', 'act_regions')

class ActorsGroupLabelSerializer(serializers.ModelSerializer):
	value = serializers.IntegerField(source='id')
	label = serializers.CharField(source='name')
	class Meta:
		model = ActorsGroup
		fields =('value', 'label')		

class NatureMainGroupLabelSerializer(serializers.ModelSerializer):
	value = serializers.IntegerField(source='id')
	label = serializers.CharField(source='name')
	class Meta:
		model = NatureMainGroup
		fields =('value', 'label')

class NatureSubGroupsLabelSerializer(serializers.ModelSerializer):
	value = serializers.IntegerField(source='id')
	label = serializers.CharField(source='name')
	class Meta:
		model = NatureSubGroups
		fields =('value', 'label')

class NatureLabelSerializer(serializers.ModelSerializer):
	act_nature_mains = NatureMainGroupLabelSerializer(read_only=True, many=True)
	act_nature_subs = NatureSubGroupsLabelSerializer(read_only=True, many=True) 
	class Meta:
		model = SavedNatures
		fields = ('id', 'act_nature_mains', 'act_nature_subs')

class InformationGradingLabelSerializer(serializers.ModelSerializer):
	value = serializers.IntegerField(source='id')
	label = serializers.CharField(source='name')
	class Meta:
		model = InformationGrading
		fields =('value', 'label')	

class SourceLabelSerializer(serializers.ModelSerializer):
	value = serializers.IntegerField(source='id')
	label = serializers.CharField(source='name')
	class Meta:
		model = Source
		fields =('value', 'label')

class VictimClassLabelSerializer(serializers.ModelSerializer):
	value = serializers.IntegerField(source='id')
	label = serializers.CharField(source='name')
	class Meta:
		model = VictimClass
		fields =('value', 'label')

class SavedKeywordsLabelSerializer(serializers.ModelSerializer):
	value = serializers.IntegerField(source='id')
	label = serializers.CharField(source='name')
	class Meta:
		model = SavedKeywords
		fields =('value', 'label')																		

class SegementsDataSerializer(serializers.ModelSerializer):
	act_actors = ActorsGroupLabelSerializer(read_only=True, many=True)
	act_gradings = InformationGradingLabelSerializer(read_only=True, many=True)
	act_sources = SourceLabelSerializer(read_only=True, many=True)
	act_victim = VictimClassLabelSerializer(read_only=True, many=True)
	act_keywords = SavedKeywordsLabelSerializer(read_only=True, many=True)
	act_location = LocationLabelSerializer(read_only=True, many=True)
	act_natures = NatureLabelSerializer(read_only=True, many=True)
	
	class Meta:
		model = CreatedSegments
		fields = ('id' , 'name', 'act_actors', 'act_location', 'act_natures', 'act_gradings', 'act_sources', 'act_victim', 'act_keywords')	

class emptyspace(serializers.ModelSerializer):
	pass	