#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.http import HttpResponseRedirect, HttpResponseBadRequest, HttpResponse
from django.shortcuts import render
from rest_framework import viewsets, renderers
from rest_framework import generics
from rest_framework.parsers import JSONParser, MultiPartParser
from rest_framework.pagination import PageNumberPagination
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from django.views.decorators.csrf import csrf_exempt
from datetime import date, time, datetime, timedelta
from dateutil import rrule
from dateutil.relativedelta import relativedelta
import delorean
from django.db.models import Prefetch
from django.db.models import Q
from urllib import unquote

from openpyxl import load_workbook, Workbook
import xlrd
from openpyxl.compat import range
from openpyxl.utils import get_column_letter
from openpyxl.writer.excel import save_virtual_workbook, save_workbook
from openpyxl.writer.write_only import WriteOnlyCell
from openpyxl.styles import Font


from incidents.models import *
from db_management.models import *
from analytics.models import *
from api.serializers import *
# Create your views here.

class CsrfExemptSessionAuthentication(SessionAuthentication):

    def enforce_csrf(self, request):
        return  # To not perform the csrf check previously happening	

class TonsItemsSetPagination(PageNumberPagination):
    page_size = 1000

class NotPaginatedSetPagination(PageNumberPagination):
    page_size = None    

class SourceList(generics.ListAPIView):
	serializer_class = SourceSerializer

	def get_queryset(self):
		result = Source.objects.all()
		return result 

class IncidentCreate(APIView):
	permission_classes = (AllowAny,)
	parser_classes = (JSONParser,)
	
	@csrf_exempt
	def post(self, request, format=None):
		user_id = request.data['user_id']
		name = request.data['name']
		date = request.data['date']
		description = request.data['description']
		source = request.data['source']
		inf_gr_list = InformationGrading.objects.filter(name__icontains='A1')
		if len(inf_gr_list) > 0:
			inf_gr = inf_gr_list[0]
		else:
			inf_gr = InformationGrading.objects.filter(name__icontains='B1')[0]   
		obj = IncindentDescription.objects.create(created_by_id=user_id, name=name, appearence_date=date, description=description, source_id=source, inf_grading_id=inf_gr.id)
		response = Response(status=status.HTTP_200_OK)
		return response	

	def get(self, request):
		return Response({'some data': request.data})

class IncidentStartList(generics.ListAPIView):
	serializer_class = IncidentListSerializer

	def get_queryset(self):
		result = IncindentDescription.objects.filter().order_by("id").reverse()
		return result 

class GetIncindentData(generics.ListAPIView):
	serializer_class = IncidentDataSerializer

	def get_queryset(self):
		inc_id = self.kwargs['inc_id']
		result = IncindentDescription.objects.filter(id=inc_id)
		return result 

class IncidentUpdate(APIView):
	permission_classes = (AllowAny,)
	parser_classes = (JSONParser,)
	
	@csrf_exempt
	def post(self, request, format=None):
		user_id = request.data['user_id']
		inc_id = request.data['inc_id']
		name = request.data['name']
		description = request.data['description']
		source = request.data['source']
		try:
			update_list = {'name':name, 'description':description, 'source_id':source}
			obj = IncindentDescription.objects.filter(id=inc_id).update(**update_list)
			response = Response(status=status.HTTP_200_OK)
		except IncindentDescription.DoesNotExist:
			response = Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		return response	

	def get(self, request):
		return Response({'some data': request.data})

class IncidentSearchNameList(generics.ListAPIView):
	serializer_class = IncidentListSerializer

	def get_queryset(self):
		inc_name = self.kwargs['inc_name']
		result = IncindentDescription.objects.filter(name__icontains=inc_name)
		return result

class IncidentSearchIdList(generics.ListAPIView):
	serializer_class = IncidentListSerializer

	def get_queryset(self):
		inc_id = self.kwargs['inc_id']
		result = IncindentDescription.objects.filter(id__icontains=inc_id)
		return result		

class MdbIncindentsFilterWeeks(generics.ListAPIView):
	serializer_class = IncidentListSerializer

	def get_queryset(self):
		start_date = self.kwargs['start_date']
		end_date = self.kwargs['end_date']
		if str(start_date) == str(end_date) or str(end_date) == '':
			result = IncindentDescription.objects.filter(creation_date__date=str(start_date))
		else:	
			new_end = delorean.parse(end_date).date + timedelta(days=1)
			result = IncindentDescription.objects.filter(creation_date__range=[delorean.parse(str(start_date)).date, new_end])
		return result

class GetIncindentToucheData(generics.ListAPIView):
	serializer_class = IncidentTouchSerializer

	def get_queryset(self):
		inc_id = self.kwargs['inc_id']
		result = IncindentDescription.objects.get(id=inc_id).touched.all()
		return result

class GetIncindentAssignedData(generics.ListAPIView):	
	serializer_class = EntryAssignedSerializer

	def get_queryset(self):
		inc_id = self.kwargs['inc_id']
		result = Entry.objects.filter(descriptions_instances=inc_id)
		return result

class GetIncindentDescriptionData(generics.ListAPIView):
	serializer_class = IncDescriptionSerializer

	def get_queryset(self):
		inc_id = self.kwargs['inc_id']
		result = IncindentDescription.objects.filter(id=inc_id)
		return result		

class CreateDescriptionsWorkspace(APIView):
	permission_classes = (AllowAny,)
	parser_classes = (JSONParser,)
	
	@csrf_exempt
	def post(self, request, format=None):
		user_id = request.data['user_id']
		descriptions = Utility.clean_project_ids(request.data['descriptions'])
		new_entry = DescriptionWorkSpace.objects.create(created_by_id=user_id)
		for description in descriptions:
			try:
				obj = IncindentDescription.objects.get(id=int(description))
				touch = TouchedDescription.objects.create(touched_by_id=user_id, action_type_id=1)
				obj.touched.add(touch)
				new_entry.descriptions.add(obj)
				new_entry.save()
			except IncindentDescription.DoesNotExist:
				pass
		response = Response({'workspace_id':new_entry.id},status=status.HTTP_200_OK)
		return response	

	def get(self, request):
		return Response({'some data': request.data})

class CreateAssignedEntry(APIView):
	permission_classes = (AllowAny,)
	parser_classes = (JSONParser,)
	
	@csrf_exempt
	def post(self, request, format=None):
		user_id = request.data['user_id']
		descriptions = Utility.clean_project_ids(request.data['descriptions'])
		new_entry = Entry.objects.create(created_by_id=user_id)
		for description in descriptions:
			try:
				obj = IncindentDescription.objects.get(id=int(description))
				touch = TouchedDescription.objects.create(touched_by_id=user_id, action_type_id=2)
				obj.touched.add(touch)
				new_entry.descriptions_instances.add(obj)
				new_entry.save()
			except IncindentDescription.DoesNotExist:
				pass		
		response = Response({'entry_id':new_entry.id},status=status.HTTP_200_OK)
		return response	

	def get(self, request):
		return Response({'some data': request.data})

class GetStartEntries(generics.ListAPIView):
	serializer_class = EntrysStartList

	def get_queryset(self):
		result = Entry.objects.filter().order_by("id").reverse()
		return result

class DeleteEntry(APIView):	
	permission_classes = (AllowAny,)
	parser_classes = (JSONParser,)
	
	@csrf_exempt
	def post(self, request, format=None):
		user_id = request.data['user_id']
		entry_id = request.data['entry_id']
		try:
			obj = Entry.objects.get(id=entry_id).delete()
		except Entry.DoesNotExist:
			pass
		response = Response(status=status.HTTP_200_OK)
		return response	

	def get(self, request):
		return Response({'some data': request.data})

class FilterEntryName(generics.ListAPIView):
	serializer_class = EntrysStartList

	def get_queryset(self):
		entry_name = unquote(self.kwargs['entry_name'])
		result = Entry.objects.filter(name__icontains=entry_name)
		return result
######
class FilterEntryKey(generics.ListAPIView):
	serializer_class = EntrysStartList

	def get_queryset(self):
		entry_name = self.kwargs['entry_name']
		result = Entry.objects.filter(descriptions_instances__description__icontains=entry_name)
		return result		

class FilterEntryId(generics.ListAPIView):
	serializer_class = EntrysStartList

	def get_queryset(self):
		entry_id = self.kwargs['entry_id']
		result = Entry.objects.filter(id__icontains=entry_id)
		return result	

class FilterEntryDate(generics.ListAPIView):
	serializer_class = EntrysStartList

	def get_queryset(self):
		entry_date = self.kwargs['entry_date']
		result = Entry.objects.filter(creation_date__date=entry_date)
		return result	

class CreateNewEntry(APIView):
	permission_classes = (AllowAny,)
	parser_classes = (JSONParser,)
	
	@csrf_exempt
	def post(self, request, format=None):
		user_id = request.data['user_id']
		obj = Entry.objects.create(created_by_id=user_id)
		response = Response({'entry_id':obj.id}, status=status.HTTP_200_OK)
		return response	

	def get(self, request):
		return Response({'some data': request.data})

class EntryData_MainInfo(generics.ListAPIView):
	serializer_class = EntrysData_MainInfo

	def get_queryset(self):
		entry_id = self.kwargs['entry_id']
		result = Entry.objects.filter(id=entry_id)
		return result

class EntryData_ValueInfo(generics.ListAPIView):
	serializer_class = EntrysData_ValueInfo

	def get_queryset(self):
		entry_id = self.kwargs['entry_id']
		value_id = self.kwargs['value_id']
		result = Entry.objects.get(id=entry_id).value_instance.filter(id=value_id)
		return result	

class VictimsData_All(generics.ListAPIView):
	serializer_class = VictimsData_All

	def get_queryset(self):
		result = VictimClass.objects.all().order_by('name')
		return result

#class EntryDataUpdate_victims(APIView):
#	permission_classes = (AllowAny,)
#	parser_classes = (JSONParser,)
#	
#	@csrf_exempt
#	def post(self, request, format=None):
#		response = Response({'text': 'Something went wrong. Please reload the page.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
#		user_id = request.data['user_id']
#		entry_id = request.data['entry_id']
#		victim = request.data['victim']
#		try:
#			obj = Entry.objects.get(id=entry_id)
#			obj.victim_class_id = int(victim)
#			obj.save()
#			response = Response({'text': 'Entry victim class successfully updated.'},status=status.HTTP_200_OK)
#		except Entry.DoesNotExist:
#			pass
#		return response	

class EntryDataUpdate_name(APIView):
	permission_classes = (AllowAny,)
	parser_classes = (JSONParser,)
	
	@csrf_exempt
	def post(self, request, format=None):
		response = Response({'text': 'Something went wrong. Please reload the page.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		user_id = request.data['user_id']
		entry_id = request.data['entry_id']
		name = request.data['name']
		try:
			obj = Entry.objects.get(id=entry_id)
			obj.name = name
			obj.save()
			response = Response({'text': 'Entry name successfully updated.'},status=status.HTTP_200_OK)
		except Entry.DoesNotExist:
			pass
		return response	

class EntryDataUpdate_remarks(APIView):	
	@csrf_exempt
	def post(self, request, format=None):
		response = Response({'text': 'Something went wrong. Please reload the page.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		user_id = request.data['user_id']
		entry_id = request.data['entry_id']
		remarks = request.data['remarks']
		try:
			obj = Entry.objects.get(id=entry_id)
			obj.remarks = remarks
			obj.save()
			response = Response({'text': 'Entry remarks successfully updated.'},status=status.HTTP_200_OK)
		except Entry.DoesNotExist:
			pass
		return response

class EntryDataUpdate_Summary(APIView):	
	@csrf_exempt
	def post(self, request, format=None):
		response = Response({'text': 'Something went wrong. Please reload the page.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		user_id = request.data['user_id']
		entry_id = request.data['entry_id']
		summary = request.data['summary']
		try:
			obj = Entry.objects.get(id=entry_id)
			obj.Summary = summary
			obj.save()
			response = Response({'text': 'Entry summary successfully updated.'},status=status.HTTP_200_OK)
		except Entry.DoesNotExist:
			pass
		return response		

class EntryDataUpdate_comments(APIView):	
	@csrf_exempt
	def post(self, request, format=None):
		response = Response({'text': 'Something went wrong. Please reload the page.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		user_id = request.data['user_id']
		entry_id = request.data['entry_id']
		comments = request.data['comments']
		try:
			obj = Entry.objects.get(id=entry_id)
			obj.JMACcomments = comments
			obj.save()
			response = Response({'text': 'Entry comments successfully updated.'},status=status.HTTP_200_OK)
		except Entry.DoesNotExist:
			pass
		return response

class UploadFile(viewsets.ModelViewSet):
    queryset = Document.objects.all()
    serializer_class = FileUploadSerializer
    parser_classes = (MultiPartParser,)
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)

class UploadAssigne(APIView):
	authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
	parser_classes = (JSONParser,)
	
	def post(self, request, format=None):
		response = Response({'text': 'Something went wrong. Please reload the page.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		user_id = request.data['user_id']
		entry = request.data['entry_id']
		upload = request.data['upload_id']
		try:
			doc = Document.objects.get(id=upload)
			try:
				entr = Entry.objects.get(id=entry)
				entr.documents.add(doc)
				entr.save()
				response = Response({'text': 'Document successfully uploaded.'}, status=status.HTTP_200_OK)
			except Entry.DoesNotExist:
				pass
		except Document.DoesNotExist:
			pass
		return response

class CreateNewLink(APIView):
	authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
	parser_classes = (JSONParser,)
	
	def post(self, request, format=None):
		response = Response({'text': 'Something went wrong. Please reload the page.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		user_id = request.data['user_id']
		entry = request.data['entry_id']
		link = request.data['link']
		new_obj = Links.objects.create(url=link)
		try:
			entr = Entry.objects.get(id=entry)
			entr.links.add(new_obj)
			entr.save()
			response = Response({'text': 'External link successfully added.', 'link_id': new_obj.id, 'new_link':new_obj.url}, status=status.HTTP_200_OK)
		except Entry.DoesNotExist:
			pass
		return response	

class CreateNewValue(APIView):
	authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
	parser_classes = (JSONParser,)
	
	def post(self, request, format=None):
		response = Response({'text': 'Something went wrong. Please reload the page.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		user_id = request.data['user_id']
		entry = request.data['entry_id']
		new_obj = EntryIncidentsValue.objects.create()
		try:
			entr = Entry.objects.get(id=entry)
			entr.value_instance.add(new_obj)
			entr.save()
			response = Response({'text': 'Victims data successfully added.', 'valId': new_obj.id}, status=status.HTTP_200_OK)
		except Entry.DoesNotExist:
			pass
		return response

class DeleteValue(APIView):
	authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
	parser_classes = (JSONParser,)
	
	def post(self, request, format=None):
		response = Response({'text': 'Something went wrong. Please reload the page.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		user_id = request.data['user_id']
		entry = request.data['entry_id']
		val = request.data['remove']
		new_obj = EntryIncidentsValue.objects.create()
		try:
			entr = Entry.objects.get(id=entry)
			try:
				del_obj = EntryIncidentsValue.objects.get(id=val)
				entr.value_instance.remove(del_obj)
				del_obj.delete()
				entr.save()
				response = Response({'text': 'Victims data successfully removed.', 'valId': int(val)}, status=status.HTTP_200_OK)
			except EntryIncidentsValue.DoesNotExist:
				pass
		except Entry.DoesNotExist:
			pass
		return response						

class EntryDataUpdate_desc_unassigns(APIView):
	@csrf_exempt
	def post(self, request, format=None):
		response = Response({'text': 'Something went wrong. Please reload the page.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		user_id = request.data['user_id']
		entry_id = request.data['entry_id']
		desc_id = request.data['de_id']
		try:
			obj = Entry.objects.get(id=entry_id)
			try:
				un_obj = IncindentDescription.objects.get(id=desc_id)
				obj.descriptions_instances.remove(un_obj)
				#Utility.updateMinMax(entry_id)
				response = Response({'text': 'Entry related description successfully removed.'},status=status.HTTP_200_OK)
			except IncindentDescription.DoesNotExist:
				pass
		except Entry.DoesNotExist:
			pass
		return response	

class EntryDataUpdate_infGrading(APIView):
	@csrf_exempt
	def post(self, request, format=None):
		response = Response({'text': 'Something went wrong. Please reload the page.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		user_id = request.data['user_id']
		entry_id = request.data['entry_id']
		desc_id = request.data['de_id']
		inf_val = request.data['inf_val']
		try:
			obj = IncindentDescription.objects.get(id=desc_id)
			obj.inf_grading_id = int(inf_val)
			obj.save()
			response = Response({'text': 'Description information grading successfully updated.'},status=status.HTTP_200_OK)
		except IncindentDescription.DoesNotExist:
			pass
		return response		

class EntryDataUpdate_NatureMainGroup(APIView):	
	@csrf_exempt
	def post(self, request, format=None):
		response = Response({'text': 'Something went wrong. Please reload the page.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		user_id = request.data['user_id']
		entry_id = request.data['entry_id']
		desc_id = request.data['de_id']
		nature_id = request.data['nature_id']
		val = request.data['val']
		try:
			obj = IncindentDescription.objects.get(id=desc_id).nature.get(id=nature_id)
			m = NatureMainGroup.objects.get(id=int(val))
			s = NatureSubGroups.objects.get(id=-1)
			obj.main = m
			obj.sub = s
			obj.save()
			response = Response({'text': 'Description nature main categorie successfully updated.'},status=status.HTTP_200_OK)
		except Nature.DoesNotExist:
			pass
		return response	

class EntryDataUpdate_NatureSubGroup(APIView):	
	@csrf_exempt
	def post(self, request, format=None):
		response = Response({'text': 'Something went wrong. Please reload the page.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		user_id = request.data['user_id']
		entry_id = request.data['entry_id']
		desc_id = request.data['de_id']
		nature_id = request.data['nature_id']
		val = request.data['val']
		try:
			obj = IncindentDescription.objects.get(id=desc_id).nature.get(id=nature_id)
			s = NatureSubGroups.objects.get(id=int(val))
			obj.sub = s
			obj.save()
			response = Response({'text': 'Description nature sub categorie successfully updated.'},status=status.HTTP_200_OK)
		except Nature.DoesNotExist:
			pass
		return response

class EntryDataUpdate_NatureAdd(APIView):
	
	@csrf_exempt
	def post(self, request, format=None):
		response = Response({'text': 'Something went wrong. Please reload the page.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		user_id = request.data['user_id']
		entry_id = request.data['entry_id']
		desc_id = request.data['de_id']
		main = request.data['main']
		sub = request.data['sub']
		try:
			obj = Entry.objects.get(id=entry_id).value_instance.get(id=desc_id)
			if sub != "":
				m = NatureMainGroup.objects.get(id=int(main))
				s = NatureSubGroups.objects.get(id=int(sub))
				new = Nature.objects.create(main=m, sub=s)
				string = "%s - %s" % (m.name, s.name)
			else:
				m = NatureMainGroup.objects.get(id=int(main))
				new = Nature.objects.create(main=m)
				string = "%s - %s" % (m.name, "--")
			obj.nature.add(new)
			obj.save()
			response = Response({'text': 'Nature instance successfully added.', 'valId':new.id, 'mainId': main, 'subId': sub, 'computed': str(string)},status=status.HTTP_200_OK)
		except EntryIncidentsValue.DoesNotExist:
			pass
		return response

class EntryDataUpdate_NatureRemove(APIView):
	@csrf_exempt
	def post(self, request, format=None):
		response = Response({'text': 'Something went wrong. Please reload the page.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		user_id = request.data['user_id']
		entry_id = request.data['entry_id']
		desc_id = request.data['de_id']
		nature_id = request.data['nature_id']
		try:
			obj = Entry.objects.get(id=entry_id).value_instance.get(id=desc_id)
			un = Nature.objects.get(id=nature_id)
			obj.nature.remove(un)
			obj.save()
			response = Response({'text': 'Nature instance successfully removed.'},status=status.HTTP_200_OK)
		except EntryIncidentsValue.DoesNotExist:
			pass
		return response		

class EntryDataUpdate_VictimAdd(APIView):
	@csrf_exempt
	def post(self, request, format=None):
		response = Response({'text': 'Something went wrong. Please reload the page.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		user_id = Utility.check_value(request.data['user_id'], 2)
		entry_id = Utility.check_value(request.data['entry_id'], 2)
		desc_id = Utility.check_value(request.data['de_id'], 2)
		group = Utility.check_value(request.data['group'], 2)
		quantity = Utility.check_value(request.data['quantity'], 2)
		try:
			obj = Entry.objects.get(id=entry_id).value_instance.get(id=desc_id)
			ex_obj = VictimClass.objects.get(id=int(group))
			new = EntryVictimsData.objects.create(group=ex_obj, quantity=quantity)
			obj.victims.add(new)
			obj.save()
			string = "%s - %s" % (ex_obj.name, quantity)	
			response = Response({'text': 'Victims group successfully added.', 'valId':new.id, 'group': ex_obj.id, 'quantity': quantity, 'computed': str(string)},status=status.HTTP_200_OK)
		except EntryIncidentsValue.DoesNotExist:
			pass
		return response

class EntryDataUpdate_VictimRemove(APIView):
	@csrf_exempt
	def post(self, request, format=None):
		response = Response({'text': 'Something went wrong. Please reload the page.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		user_id = Utility.check_value(request.data['user_id'], 2)
		entry_id = Utility.check_value(request.data['entry_id'], 2)
		desc_id = Utility.check_value(request.data['de_id'], 2)
		remove = Utility.check_value(request.data['val'], 2)
		try:
			obj = Entry.objects.get(id=entry_id).value_instance.get(id=desc_id)
			remove = EntryVictimsData.objects.get(id=remove)
			obj.victims.remove(remove)
			obj.save()	
			response = Response({'text': 'Victims group successfully removed.'},status=status.HTTP_200_OK)
		except EntryIncidentsValue.DoesNotExist:
			pass
		return response				
#remove this
class EntryDataUpdate_VictimsNum(APIView):
	@csrf_exempt
	def post(self, request, format=None):
		response = Response({'text': 'Something went wrong. Please reload the page.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		user_id = request.data['user_id']
		entry_id = request.data['entry_id']
		desc_id = request.data['de_id']
		val = request.data['val']
		try:
			obj = IncindentDescription.objects.get(id=desc_id)
			obj.number_of_victims = int(val)
			obj.save()	
			#Utility.updateMinMax(entry_id)
			response = Response({'text': 'Description victims number successfully updated.'},status=status.HTTP_200_OK)
		except IncindentDescription.DoesNotExist:
			pass
		return response

class EntryDataUpdate_ActorAdd(APIView):
	@csrf_exempt
	def post(self, request, format=None):
		response = Response({'text': 'Something went wrong. Please reload the page.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		user_id = Utility.check_value(request.data['user_id'], 2)
		entry_id = Utility.check_value(request.data['entry_id'], 2)
		desc_id = Utility.check_value(request.data['de_id'], 2)
		group = Utility.check_value(request.data['group'], 2)
		try:
			obj = Entry.objects.get(id=entry_id).value_instance.get(id=desc_id)
			new = ActorsGroup.objects.get(id=int(group))
			obj.actors.add(new)
			obj.save()	
			response = Response({'text': 'Actor category successfully added.', 'valId': new.id, 'label': new.name},status=status.HTTP_200_OK)
		except EntryIncidentsValue.DoesNotExist:
			pass
		return response	

class EntryDataUpdate_ActorRemove(APIView):
	@csrf_exempt
	def post(self, request, format=None):
		response = Response({'text': 'Something went wrong. Please reload the page.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		user_id = Utility.check_value(request.data['user_id'], 2)
		entry_id = Utility.check_value(request.data['entry_id'], 2)
		desc_id = Utility.check_value(request.data['de_id'], 2)
		group = Utility.check_value(request.data['group'], 2)
		try:
			obj = Entry.objects.get(id=entry_id).value_instance.get(id=desc_id)
			new = ActorsGroup.objects.get(id=int(group))
			obj.actors.remove(new)
			obj.save()	
			response = Response({'text': 'Actor category successfully removed.'},status=status.HTTP_200_OK)
		except EntryIncidentsValue.DoesNotExist:
			pass
		return response									

class EntryDataUpdate_locationAdd(APIView):
	@csrf_exempt
	def post(self, request, format=None):
		response = Response({'text': 'Something went wrong. Please reload the page.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		user_id = Utility.check_value(request.data['user_id'], 2)
		entry_id = Utility.check_value(request.data['entry_id'], 2)
		desc_id = Utility.check_value(request.data['de_id'], 2)
		region = Utility.check_value(request.data['region'], 2)
		locality = request.data['locality']
		village = request.data['village']
		try:
			obj = Entry.objects.get(id=entry_id).value_instance.get(id=desc_id)
			if locality != "":
				if village != "":
					r = Region.objects.get(id = region)
					l = Locality.objects.get(id = Utility.check_value(locality, 2))
					v = Village.objects.get(id = Utility.check_value(village, 2))
					new = Location.objects.create(inc_region=r, inc_locality=l, inc_village=v, inc_coordinates=v.coordinates)
					string = "%s - %s - %s - E: %s, N: %s " % (r.name, l.name, v.name, v.coordinates.e, v.coordinates.n) 
				else:
					r = Region.objects.get(id = region)
					l = Locality.objects.get(id = Utility.check_value(locality, 2))
					new = Location.objects.create(inc_region=r, inc_locality=l)
					string = "%s - %s - %s - E: %s, N: %s " % (r.name, l.name, "--", '-', '-')
			else:
				r = Region.objects.get(id = region)
				new = Location.objects.create(inc_region=r)
				string = "%s - %s - %s - E: %s, N: %s " % (r.name, "--", "--", '-', '-')
			obj.locations.add(new)	
			obj.save()	
			response = Response({'text': 'Location data successfully added.', 'valId': new.id, 'computed': str(string)}, status=status.HTTP_200_OK)
		except EntryIncidentsValue.DoesNotExist:
			pass
		return response

class EntryDataUpdate_locationRemove(APIView):
	@csrf_exempt
	def post(self, request, format=None):
		response = Response({'text': 'Something went wrong. Please reload the page.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		user_id = Utility.check_value(request.data['user_id'], 2)
		entry_id = Utility.check_value(request.data['entry_id'], 2)
		desc_id = Utility.check_value(request.data['de_id'], 2)
		instance = Utility.check_value(request.data['instance'], 2)
		try:
			obj = Entry.objects.get(id=entry_id).value_instance.get(id=desc_id)
			remove_obj = Location.objects.get(id=instance)
			obj.locations.remove(remove_obj)
			obj.save()
			response = Response({'text': 'Location data successfully removed.'}, status=status.HTTP_200_OK)
		except EntryIncidentsValue.DoesNotExist:
			pass
		return response	

class EntryDataUpdate_coordinates(APIView):
	@csrf_exempt
	def post(self, request, format=None):
		user_id = request.data['user_id']
		region = request.data['region']
		locality = request.data['locality']
		village = request.data['village']
		e = Utility.check_value(request.data['e'], 1)
		n = Utility.check_value(request.data['n'], 1)
		try:
			reg = Region.objects.get(id=region).localitys.get(id=locality).villages.get(id=village)
			if int(reg.coordinates_id) == 1:
				obj = Coordinats.objects.create(e=e, n=n)
				reg.coordinates_id = int(obj.id)
				reg.save()
			else:	 
				coor_obj = Coordinats.objects.get(id=reg.coordinates_id)
				coor_obj.e = e
				coor_obj.n = n
				coor_obj.save()
			response = Response({'text': '%s coordinates successfully updated.' % (reg.name),},status=status.HTTP_200_OK)	
		except Village.DoesNotExist:
			pass
		return response	

class EntryDataUpdate_assignNewDescription(APIView):
	@csrf_exempt
	def post(self, request, format=None):
		response = Response({'text': 'Something went wrong. Please reload the page.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		user_id = request.data['user_id']
		description = request.data['de_id']
		entry_id = request.data['entry_id']
		try:
			main_obj = Entry.objects.get(id=entry_id)
			try:
				de_obj = IncindentDescription.objects.get(id=description)
				main_obj.descriptions_instances.add(de_obj)
				main_obj.save()
				#Utility.updateMinMax(entry_id)
				response = Response({'text': 'Description with id #%s successfully assigned to the entry.' % (description),},status=status.HTTP_200_OK)
			except IncindentDescription.DoesNotExist:
				pass
		except Entry.DoesNotExist:
			pass
		return response	

class EntryDataUpdate_date(APIView):
	@csrf_exempt
	def post(self, request, format=None):
		response = Response({'text': 'Something went wrong. Please reload the page.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		user_id = Utility.check_value(request.data['user_id'], 2)
		description = Utility.check_value(request.data['de_id'], 2)
		entry_id = Utility.check_value(request.data['entry_id'], 2)
		date = request.data['date']
		time = request.data['time']
		try:
			obj = Entry.objects.get(id=entry_id).value_instance.get(id=description)
			obj.date = date
			if time != "":
				obj.time = time
			obj.save()
			response = Response({'text': 'Incident date and time successfully updated.'},status=status.HTTP_200_OK)
		except EntryIncidentsValue.DoesNotExist:
			pass
		return response	

class EntryDataUpdate_time(APIView):
	@csrf_exempt
	def post(self, request, format=None):
		response = Response({'text': 'Something went wrong. Please reload the page.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		user_id = request.data['user_id']
		description = request.data['de_id']
		entry_id = request.data['entry_id']
		time = request.data['time']
		try:
			main_obj = IncindentDescription.objects.get(id=description)
			main_obj.inc_time = time
			main_obj.save()
			response = Response({'text': 'Incident time successfully updated.',},status=status.HTTP_200_OK)
		except IncindentDescription.DoesNotExist:
			pass
		return response					

class EntryData_ReportsData(generics.ListAPIView):	
	serializer_class = EntrysData_ReportsData

	def get_queryset(self):
		entry_id = self.kwargs['entry_id']
		result = Entry.objects.get(id=entry_id).descriptions_instances.all()
		return result

class DBData_infGrading(generics.ListAPIView):
	pagination_class = TonsItemsSetPagination
	serializer_class = DBData_infGradings

	def get_queryset(self):
		result = InformationGrading.objects.all()
		return result	

class DBData_natureMain(generics.ListAPIView):
	pagination_class = TonsItemsSetPagination
	serializer_class = DBData_natureMains

	def get_queryset(self):
		result = NatureMainGroup.objects.all().order_by('name')
		return result

class DBData_natureSub(generics.ListAPIView):
	pagination_class = TonsItemsSetPagination	
	serializer_class = DBData_natureSubs

	def get_queryset(self):
		main = self.kwargs['main_id']
		result = NatureMainGroup.objects.get(id=main).subGroups.all().order_by('name')
		return result		

class DBData_actors(generics.ListAPIView):
	pagination_class = TonsItemsSetPagination
	serializer_class = IncActorsSerializer

	def get_queryset(self):
		result = ActorsGroup.objects.all().order_by('name')
		return result	

class DBData_locations(generics.ListAPIView):
	pagination_class = TonsItemsSetPagination
	serializer_class = RegionLabelSerializer

	def get_queryset(self):
		result = Region.objects.all().order_by('name')
		return result

class DBData_locality(generics.ListAPIView):
	pagination_class = TonsItemsSetPagination
	serializer_class = LocalityLabelSerializer

	def get_queryset(self):
		region = self.kwargs['loc_id']
		result = Region.objects.get(id=region).localitys.all().order_by('name')
		return result	

class DBData_villages(generics.ListAPIView):
	pagination_class = TonsItemsSetPagination
	serializer_class = VillagesLabelSerializer

	def get_queryset(self):
		locality = self.kwargs['loc_id']
		region = self.kwargs['reg_id']
		result = Region.objects.get(id=region).localitys.get(id=locality).villages.all().order_by('name')
		return result

class DBData_coorsinates(generics.ListAPIView):
	serializer_class = DBData_vilCoordinates

	def get_queryset(self):
		locality = self.kwargs['loc_id']
		region = self.kwargs['reg_id']
		village = self.kwargs['vil_id']
		result = Region.objects.get(id=region).localitys.get(id=locality).villages.filter(id=village)
		return result			

#####################
# Analytics methods #
#####################

class SegmentsStart(generics.ListAPIView):	
	serializer_class = SegementsListSerializer

	def get_queryset(self):
		result = CreatedSegments.objects.filter(id__gte=1).order_by('name')
		return result

class SegmentsData(generics.ListAPIView):
	serializer_class = SegementsDataSerializer

	def get_queryset(self):
		what = self.kwargs['segment_id']
		result = CreatedSegments.objects.filter(id=what)
		return result		

#0-actors, 1-victims, 2-natures, 3-locations, 4-source, 5-infgrade, 6-keywords
class SegmentsCreate(APIView):
	def post(self, request, format=None):
		response = Response({'text': 'Something went wrong. Please reload the page.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		segment = Utility.prepare_array_for_save(request.data['segment'])
		name = request.data['name']
		try:
			obj = CreatedSegments.objects.get(name=name)
			obj.act_actors.clear()
			obj.act_victim.clear()
			obj.act_location.clear()
			obj.act_natures.clear()
			obj.act_sources.clear()
			obj.act_gradings.clear()
			obj.act_keywords.clear()
		except CreatedSegments.DoesNotExist:	
			obj = CreatedSegments.objects.create(name=name)
		if len(segment[0]) > 0:
			for act in segment[0]:
				try:
					act_obj = ActorsGroup.objects.get(id=int(act))
					obj.act_actors.add(act_obj)
				except ActorsGroup.DoesNotExist:
					pass
		if len(segment[1]) > 0:
			for vic in segment[1]:
				try:
					vic_obj = VictimClass.objects.get(id=int(vic))
					obj.act_victim.add(vic_obj)
				except VictimClass.DoesNotExist:
					pass
		if len(segment[2]) > 0:
			for ni in segment[2]:
				ni_obj = SavedNatures.objects.create()
				try:
					nm_obj = NatureMainGroup.objects.get(id=int(ni[0]))
					ni_obj.act_nature_mains.add(nm_obj)
				except NatureMainGroup.DoesNotExist:
					pass
				try:
					ns_obj = NatureSubGroups.objects.get(id=int(ni[1]))
					ni_obj.act_nature_subs.add(ns_obj)
				except NatureSubGroups.DoesNotExist:
					pass
				obj.act_natures.add(ni_obj)		
		if len(segment[3]) > 0:
			for li in segment[3]:
				li_obj = SavedLocalitys.objects.create()
				try:
					lr_obj = Region.objects.get(id=int(li[0]))
					li_obj.act_regions.add(lr_obj)
				except Region.DoesNotExist:
					pass
				try:
					ll_obj = Locality.objects.get(id=int(li[1]))
					li_obj.act_localitys.add(ll_obj)
				except Locality.DoesNotExist:
					pass
				try:
					lv_obj = Village.objects.get(id=int(li[2]))
					li_obj.act_villages.add(lv_obj)
				except Village.DoesNotExist:
					pass
				obj.act_location.add(li_obj)
		if len(segment[4]) > 0:
			for sou in segment[4]:
				try:
					sou_obj = Source.objects.get(id=int(sou))
					obj.act_sources.add(sou_obj)
				except Source.DoesNotExist:
					pass		
		if len(segment[5]) > 0:
			for inf in segment[5]:
				try:
					inf_obj = InformationGrading.objects.get(id=int(inf))
					obj.act_gradings.add(inf_obj)
				except InformationGrading.DoesNotExist:
					pass
		if len(segment[6]) > 0:
			for wor in segment[6]:
				wor_obj = SavedKeywords.objects.create(name=wor)
				obj.act_keywords.add(wor_obj)
		obj.save()								
		response = Response({'newID': obj.id, 'text': 'New search criteria successfully created.'},status=status.HTTP_200_OK)
		return response	

class SegmentsCreateTemp(APIView):

	def post(self, request, format=None):
		response = Response({'text': 'Something went wrong. Please reload the page.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		segments = request.data['segment']
		if len(segments) > 0:
			l = len(segments)
			for ins in list(range(0, l)):
				segment = Utility.prepare_array_for_save(segments[ins])
				v = int(-(ins+1))
				try:
					obj = CreatedSegments.objects.get(id=v)
					obj.act_actors.clear()
					obj.act_victim.clear()
					obj.act_location.clear()
					obj.act_natures.clear()
					obj.act_sources.clear()
					obj.act_gradings.clear()
					obj.act_keywords.clear()
				except CreatedSegments.DoesNotExist:
					obj = CreatedSegments.objects.create(id = v)	
				if len(segment[0]) > 0:
					for act in segment[0]:
						try:
							act_obj = ActorsGroup.objects.get(id=int(act))
							obj.act_actors.add(act_obj)
						except ActorsGroup.DoesNotExist:
							pass
				if len(segment[1]) > 0:
					for vic in segment[1]:
						try:
							vic_obj = VictimClass.objects.get(id=int(vic))
							obj.act_victim.add(vic_obj)
						except VictimClass.DoesNotExist:
							pass
				if len(segment[2]) > 0:
					for ni in segment[2]:
						ni_obj = SavedNatures.objects.create()
						try:
							nm_obj = NatureMainGroup.objects.get(id=int(ni[0]))
							ni_obj.act_nature_mains.add(nm_obj)
						except NatureMainGroup.DoesNotExist:
							pass
						try:
							ns_obj = NatureSubGroups.objects.get(id=int(ni[1]))
							ni_obj.act_nature_subs.add(ns_obj)
						except NatureSubGroups.DoesNotExist:
							pass
						obj.act_natures.add(ni_obj)		
				if len(segment[3]) > 0:
					for li in segment[3]:
						li_obj = SavedLocalitys.objects.create()
						try:
							lr_obj = Region.objects.get(id=int(li[0]))
							li_obj.act_regions.add(lr_obj)
						except Region.DoesNotExist:
							pass
						try:
							ll_obj = Locality.objects.get(id=int(li[1]))
							li_obj.act_localitys.add(ll_obj)
						except Locality.DoesNotExist:
							pass
						try:
							lv_obj = Village.objects.get(id=int(li[2]))
							li_obj.act_villages.add(lv_obj)
						except Village.DoesNotExist:
							pass
						obj.act_location.add(li_obj)
				if len(segment[4]) > 0:
					for sou in segment[4]:
						try:
							sou_obj = Source.objects.get(id=int(sou))
							obj.act_sources.add(sou_obj)
						except Source.DoesNotExist:
							pass		
				if len(segment[5]) > 0:
					for inf in segment[5]:
						try:
							inf_obj = InformationGrading.objects.get(id=int(inf))
							obj.act_gradings.add(inf_obj)
						except InformationGrading.DoesNotExist:
							pass
				if len(segment[6]) > 0:
					for wor in segment[6]:
						wor_obj = SavedKeywords.objects.create(name=wor)
						obj.act_keywords.add(wor_obj)
				obj.save()								
			response = Response({'text': 'New search criteria successfully created.'},status=status.HTTP_200_OK)
		else:
			response = Response({'text': 'NO data inside.'},status=status.HTTP_200_OK)
		return response		

#descriptions_instances
class EntrysSegmented(generics.ListAPIView):
	authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
	def post(self, request, format=None):
		response = Response({'text': 'Something went wrong. Please reload the page.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		user_id = Utility.check_value(request.data['user_id'], 2)
		start = request.data['start']
		end = request.data['end']
		segment = Utility.prepare_array(request.data['segment'])
		fetched_data = self.prepareData(segment, start, end)
		response = Response({'allData': fetched_data,},status=status.HTTP_200_OK)
		return response

	def prepareData(self, segment, start, end):
		result_array = []
		result_array = self.fetchAllSegmentData(start, end, segment)		
		return result_array

	def fetchAllSegmentData(self, start, end, segment):
		"""
		i need to get all entries that appeared in selected dates range
		and then filter them by segment preferences
		"""
		"""
		firstly i need to split range on monthes and then calculate segments here and add them to final array
		"""
		result_array = []
		start = start + ' 00:00'
		end = end + ' 00:00'
		i_now = datetime.strptime(str(start), '%Y-%m-%d %H:%M')
		i_end = datetime.strptime(str(end), '%Y-%m-%d %H:%M')

		result_array = self.iterationInstance(i_now, i_end, segment)
		return result_array

	def iterationInstance(self, item_start, item_end, all_data):
		result = []
		filters = Q(value_instance__date__range=[item_start, item_end])
		if len(all_data[0]) > 0:
			filters &= Q(value_instance__actors__id__in=all_data[0])
		if len(all_data[1]) > 0:
			filters &= Q(value_instance__victims__group__id__in=all_data[1])
		if len(all_data[2]) > 0:
			filters &= Q(value_instance__nature__main__id__in=all_data[2])
		if len(all_data[3]) > 0:
			filters &= Q(value_instance__nature__sub__id__in=all_data[3])
		if len(all_data[4]) > 0:
			filters &= Q(value_instance__locations__inc_region__id__in=all_data[4])
		if len(all_data[5]) > 0:
			filters &= Q(value_instance__locations__inc_locality__id__in=all_data[5])
		if len(all_data[6]) > 0:
			filters &= Q(value_instance__locations__inc_village__id__in=all_data[6])
		if len(all_data[7]) > 0:
			filters &= Q(descriptions_instances__source__id__in=all_data[7])
		if len(all_data[8]) > 0:
			filters &= Q(descriptions_instances__inf_grading__id__in=all_data[8])
		if len(all_data[9]) > 0:
			filters &= reduce(lambda x, y: x & y, [Q(descriptions_instances__description__icontains=word) for word in all_data[9]])
		result_data = Entry.objects.filter(filters).distinct()[:500]	
		for entry in result_data:
			descrip = []
			for de in entry.descriptions_instances.all():
				pre = {'text':de.description}
				descrip.append(pre)
			vals = []	
			for val in entry.value_instance.all():
				vic = []
				act = []
				loc = []
				nat = []
				for v in val.victims.all():
					vic.append(v.computeNames())
				for a in val.actors.all():
					act.append(a.name)
				for n in val.nature.all():
					nat.append(n.computeNames())
				for l in val.locations.all():
					loc.append(l.computeNames())
				date = '%s %s' % (val.convert_date(), val.convert_time())	
				dic = {'victims':vic, 'date': date, 'actors': act, 'locations': loc, 'nature': nat}
				vals.append(dic)
			mid = {'id':entry.id, 'name':entry.name, 'remarks':entry.remarks, 'JMACcomments':entry.JMACcomments, 'summary': entry.Summary,
					'entry_descriptions': descrip, 'values_instances': vals}
			result.append(mid)	
		return result				
		

class FetchChartData(APIView):
	authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
	
	def post(self, request, format=None):
		response = Response({'text': 'Something went wrong. Please reload the page.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		user_id = Utility.check_value(request.data['user_id'], 2)
		start = request.data['start']
		end = request.data['end']
		x = Utility.check_value(request.data['x'], 2)
		segments = request.data['segments']
		fetched_data = ProcessingSegmentData.prepareData(segments, x, start, end)
		response = Response({'allData': fetched_data,},status=status.HTTP_200_OK)
		return response	

class ProcessingSegmentData(object):
		
	def prepareData(self, segments, x, start, end):
		result_array = []
		result = [[],[]]
		for segment in segments:
			pre_data = self.fetchAllSegmentData(start, end, segment, x)
			result_array.append(pre_data)
		for ind, entry in enumerate(result_array):
			if ind == 0:
				result[1] = entry[1]
			result[0].append([entry[0], entry[2]])		
		return result			
		"""
		data array for each segment data: [10, 20, 30, 40, 50, 60, 70, 80, 90]
		labels array for each month
		"""	
	def fetchAllSegmentData(self, start, end, segment, x):
		"""
		i need to get all entries that appeared in selected dates range
		and then filter them by segment preferences
		"""
		"""
		firstly i need to split range on monthes and then calculate segments here and add them to final array
		"""
		result_array = []
		values_array = []
		monthes_array = []
		start = start + ' 00:00'
		end = end + ' 00:00'
		i_now = datetime.strptime(str(start), '%Y-%m-%d %H:%M')
		i_end = datetime.strptime(str(end), '%Y-%m-%d %H:%M')
		all_data = Utility.prepare_array(segment)
		if i_now == i_end:
			result_array = self.iterateThroughHours(i_now, all_data, x) # i need to get segmented data on necessary date and split it by the hours
		elif i_now + timedelta(days=30) >= i_end and i_end < (i_now + timedelta(days=89)):
			result_array = self.iterateThroughDays(i_now, i_end, all_data, x)
		else:
			result_array = self.iterateThroughMonthes(i_now, i_end, all_data, x)
		result_array.append([segment[3]])
		return result_array

	def iterateThroughHours(self, start, segment, x):
		result_array = []
		values_array = []
		monthes_array = []
		end_obj = start + timedelta(days=1)
		for dt in rrule.rrule(rrule.HOURLY, dtstart=start, interval=1, until=end_obj):
			item_end = dt + timedelta(hours=1)
			data = self.iterationInstance(start, dt, item_end, segment, x, True)
			values_array.append(data)
			monthes_array.append(dt.strftime('%d-%m-%Y %H:%M'))
		result_array.extend([values_array, monthes_array])
		return result_array

	def iterateThroughDays(self, start, end, segment, x):
		result_array = []
		values_array = []
		monthes_array = []
		for dt in rrule.rrule(rrule.DAILY, dtstart=start, interval=1, until=end):
			item_end = dt + timedelta(days=1)
			data = self.iterationInstance(start, dt, item_end, segment, x, False)
			values_array.append(data)
			monthes_array.append(dt.strftime('%d-%m-%Y'))
		result_array.extend([values_array, monthes_array])
		return result_array

	def iterateThroughMonthes(self, start, end, segment, x):
		result_array = []
		values_array = []
		monthes_array = []
		for dt in rrule.rrule(rrule.MONTHLY, dtstart=start, interval=1, until=end):
			item_end = dt + relativedelta(months=+1)
			data = self.iterationInstance(start, dt, item_end, segment, x, False)
			values_array.append(data)
			monthes_array.append(dt.strftime('%m-%Y'))
		result_array.extend([values_array, monthes_array])
		return result_array	

#0-actors, 1-victims, 2-natures-main, 3-natures-sub, 4-locations-region, 5-locations-locality, 6-locations-village, 7-source, 8-infgrade, 9-keywords
	def iterationInstance(self, start_date, item_start, item_end, all_data, x, timeState):
		if timeState:
			filters = Q(value_instance__date=start_date)
			filters &= Q(value_instance__time__range=[item_start, item_end])
		else:
			filters = Q(value_instance__date__range=[item_start, item_end])
		if len(all_data[0]) > 0:
			filters &= Q(value_instance__actors__id__in=all_data[0])
		if len(all_data[1]) > 0:
			filters &= Q(value_instance__victims__group__id__in=all_data[1])
		if len(all_data[2]) > 0:
			filters &= Q(value_instance__nature__main__id__in=all_data[2])
		if len(all_data[3]) > 0:
			filters &= Q(value_instance__nature__sub__id__in=all_data[3])
		if len(all_data[4]) > 0:
			filters &= Q(value_instance__locations__inc_region__id__in=all_data[4])
		if len(all_data[5]) > 0:
			filters &= Q(value_instance__locations__inc_locality__id__in=all_data[5])
		if len(all_data[6]) > 0:
			filters &= Q(value_instance__locations__inc_village__id__in=all_data[6])
		if len(all_data[7]) > 0:
			filters &= Q(descriptions_instances__source__id__in=all_data[7])
		if len(all_data[8]) > 0:
			filters &= Q(descriptions_instances__inf_grading__id__in=all_data[8])
		if len(all_data[9]) > 0:
			filters &= reduce(lambda x, y: x & y, [Q(descriptions_instances__description__icontains=word) for word in all_data[9]])
		result_data = Entry.objects.filter(filters).distinct()
		if x == 1:
			result = len(result_data)
		elif x == 2:
			vics = 0
			for entry in result_data:
				values = entry.value_instance.all()
				for val in values:
					vici = val.victims.all()
					for vi in vici:
						if len(all_data[1]) > 0:
							if int(vi.id) in all_data[1]:
								vics += int(vi.quantity)
						else:
							vics += int(vi.quantity)	
			result = vics			
		return result										 						

ProcessingSegmentData = ProcessingSegmentData()

class Utility(object):
	def clean_project_ids(self, project_ids):
		result = []
		for element in project_ids:
			result.append(int(element))
		return result 

#0-actors, 1-victims, 2-natures-main, 3-natures-sub, 4-locations-region, 5-locations-locality, 6-locations-village, 7-source, 8-infgrade, 9-keywords
	def prepare_array(self, data):
		result = [[], [], [], [], [], [], [], [], [], []]
	 	for actors in data[2][0]:
			result[0].append(actors[1])
		for victims in data[2][1]:
			result[1].append(victims[1])
		for nature in data[2][2]:
			result[2].append(nature[1])
			if nature[2] != -1:
				result[3].append(nature[2])
		for location in data[2][3]:
			result[4].append(location[1])
			if location[2] != -1:
				result[5].append(location[2])
			if location[3] != -1:
				result[6].append(location[3])
		for source in data[2][4]:
			result[7].append(source[1])
		for inf in data[2][5]:
			result[8].append(inf[1])	
		for key in data[2][6]:
			result[9].append(key[1])
		return result

#0-actors, 1-victims, 2-natures, 3-locations, 4-source, 5-infgrade, 6-keywords
	def prepare_array_for_save(self, data):
		result = [[], [], [], [], [], [], []]
	 	for actors in data[2][0]:
			result[0].append(actors[1])
		for victims in data[2][1]:
			result[1].append(victims[1])
		for nature in data[2][2]:
			result[2].append([nature[1], nature[2]])
		for location in data[2][3]:
			result[3].append([location[1], location[2], location[3]])
		for source in data[2][4]:
			result[4].append(source[1])
		for inf in data[2][5]:
			result[5].append(inf[1])	
		for key in data[2][6]:
			result[6].append(key[1])
		return result		
	
	#def updateMinMax(self, entry_id):
	#	values = []
	#	try:
	#		obj = Entry.objects.get(id=entry_id)
	#		d_list = obj.descriptions_instances.all()
	#		for desc in d_list:
	#			values.append(int(desc.number_of_victims))
	#		values.sort()
	#		obj.vicMin = values[0]
	#		obj.vicMax = values[-1]
	#		obj.save()	
	#	except Entry.DoesNotExist:
	#		pass

	def check_value(self, value, what):
		try:
			if what == 1:
				value = float(value)
			elif what == 2:
				value = int(value)
		except ValueError:
			if what == 1:
				value = 1.0
			elif what == 2:
				value = 1
		return value			

Utility = Utility()	#Utility.updateMinMax(entry_id) Utility.check_value(value, what)						