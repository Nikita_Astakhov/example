from __future__ import unicode_literals
import os
from django.db import models
from datetime import date, datetime, timedelta
from django.utils import timezone
from django.contrib.auth.models import User
from incidents.models import * 

# Create your models here.

class Links(models.Model):
	url = models.URLField(max_length=200, blank=True)

	class Meta:
		verbose_name = 'Entry Links'
		verbose_name_plural = "Entry Links"

	def __unicode__(self):
		return unicode(self.url)

class Document(models.Model):
    description = models.CharField(max_length=255, blank=True)
    document = models.FileField(upload_to='uploads/')
    uploaded_at = models.DateTimeField(auto_now_add=True)

    class Meta:
		verbose_name = 'Entry Documents'
		verbose_name_plural = "Entry Documents"

    def __unicode__(self):
		return unicode(self.description)

class EntryVictimsData(models.Model):
	group = models.ForeignKey('incidents.VictimClass', related_name='entry_victim_class', blank=True, null=True)
	quantity = models.IntegerField(default=0, verbose_name='Victim quantity')

	def computeNames(self):
		m = self.group.name
		result = str("%s - %s" % (m, self.quantity))
		return result	

class EntryIncidentsValue(models.Model):
	now = datetime.now()

	victims = models.ManyToManyField(EntryVictimsData, blank=True, related_name="entry_assigned_victims_data")
	date = models.DateField(auto_now=False, auto_now_add=False, blank=True, null=True, verbose_name='Entry value date') 
	time = models.TimeField(auto_now=False, auto_now_add=False, blank=True, null=True, verbose_name='Entry value time')
	actors = models.ManyToManyField('incidents.ActorsGroup', blank=True, related_name='entry_value_actors_groups')
	locations = models.ManyToManyField('incidents.Location', blank=True, related_name='entry_value_locations')
	nature = models.ManyToManyField('incidents.Nature', blank=True, related_name='entry_value_natures')	

	def convert_time(self):
		if self.time is not None:
			date = time.strftime(self.time, '%H:%M')
		else:
			m_date = time(11,0,0)
			date = time.strftime(m_date, '%H:%M')
		return date	

	def convert_date(self): 
		if self.date is not None: 
			date = datetime.strftime(self.date, '%d-%m-%Y')
		else:
			now = datetime.now()
			date = datetime.strftime(now, '%d-%m-%Y')
		return date	

class Entry(models.Model):
	name = models.CharField(max_length=255, blank=True, verbose_name='Title') #+
	creation_date = models.DateTimeField(auto_now_add=True, verbose_name='Recorded on') #+
	created_by = models.ForeignKey(User, related_name='entry_created_by', blank=True, null=True) #+
	descriptions_instances = models.ManyToManyField('incidents.IncindentDescription', blank=True, related_name="entry_assigned_descriptions")
	value_instance = models.ManyToManyField(EntryIncidentsValue, blank=True, related_name="entry_assigned_values")
	remarks = models.TextField(blank=True, help_text="Enter remarks to entry")
	JMACcomments = models.TextField(blank=True, help_text="Enter comments for entry")
	Summary = models.TextField(blank=True, help_text="Enter summary for entry")
	links = models.ManyToManyField(Links, blank=True, related_name="entry_assigned_links")
	documents = models.ManyToManyField(Document, blank=True, related_name="entry_assigned_documents")

	class Meta:
		verbose_name = 'Database Entry'

	def __unicode__(self):
		return unicode(self.name)	

	def get_name(self):
		result = self.created_by.username
		return result

	def convert_date(self):
		creation_date = datetime.strftime(self.creation_date, '%d %B %Y')
		return creation_date