from __future__ import unicode_literals
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView
from db_management.models import *
from incidents.models import *
from wkhtmltopdf.views import PDFTemplateView
import pdfkit

# Create your views here.

class ExtraContextTemplateView(TemplateView):
	""" Add extra context to a simple template view """
	extra_context = None

	def get_context_data(self, *args, **kwargs):
		context = super(ExtraContextTemplateView, self).get_context_data(*args, **kwargs)
		if self.extra_context:
		    context.update(self.extra_context)
		return context

	# this view is used in POST requests, e.g. signup when the form is not valid
	post = TemplateView.get	

def TestView(request, template_name='db_management/db_point_2.html', extra_context=None, **kwargs):
	#inc_desc = IncindentDescription.objects.filter(locations__inc_region__id__in=[1,22])# inc_locality__name__contains=a_loc,)
													#inc_village__name__contains=a_vil, actor_class__name__contains=a_actor,
													#inf_grading__name__contains=a_grade, source__name__contains=a_source,
													#nature_main__name__contains=a_n_main, nature_sub__name__contains=a_n_sub)
	#inc = Entry.objects.filter(descriptions_instances__locations__inc_region__id__in=[1], descriptions_instances__victim_class__id__in=[19])
	
	#your_values = { 'descriptions_instances__victim_class__id__in' : [19], 'descriptions_instances__locations__inc_region__id__in' : [1]}
	#inc = Entry.objects.filter(**your_values)
	#inc.filter(descriptions_instances__victim_class__id__in=[19]).filter(descriptions_instances__locations__inc_region__id__in=[1])
	#inc
	#mid = 0
	#for entry in inc:
	#			mid += int(entry.vicMin)	
	dic = [0,1,2,3]
	f = list(range(1, 3))											
	if not extra_context: extra_context = dict()
	extra_context['test'] = f
	return ExtraContextTemplateView.as_view(template_name=template_name,
											extra_context=extra_context)(request)	

def ManagePoint(request, template_name='db_management/db_point.html', extra_context=None, **kwargs):
	if not extra_context: extra_context = dict()
	return ExtraContextTemplateView.as_view(template_name=template_name,
											extra_context=extra_context)(request)

def ManageDescriptionWorkSpace(request, template_name='db_management/descriptions_workspace.html', extra_context=None, **kwargs):
	if request.user.is_authenticated():
		user_id = request.user.id
	else:
		user_id = -1
	if not extra_context: extra_context = dict()
	extra_context['user'] = user_id
	extra_context['host'] = request.get_host()
	return ExtraContextTemplateView.as_view(template_name=template_name,
											extra_context=extra_context)(request)	

def ManageEntriesWorkSpace(request, template_name='db_management/entries_workspace.html', extra_context=None, **kwargs):
	if request.user.is_authenticated():
		user_id = request.user.id
	else:
		user_id = -1
	if not extra_context: extra_context = dict()
	extra_context['user'] = user_id
	extra_context['host'] = request.get_host()
	return ExtraContextTemplateView.as_view(template_name=template_name,
											extra_context=extra_context)(request)	

def ManageDescriptionsPreview(request, template_name='db_management/descriptions_preview.html', extra_context=None, **kwargs):	
	description_id = kwargs['workspace_id']
	list_of_entries = DescriptionWorkSpace.objects.get(id=description_id).descriptions.all()
	list_of_assigned = [];
	for entr in list_of_entries:
		filtered = Entry.objects.filter(descriptions_instances=entr.id)
		entry_list = [entr.id]
		for filt in filtered: 
			entry_list.append(filt.id)
		list_of_assigned.append(entry_list)
	if not extra_context: extra_context = dict()
	extra_context['descriptions'] = list_of_entries
	extra_context['assigned'] = list_of_assigned
	extra_context['where'] = description_id
	return ExtraContextTemplateView.as_view(template_name=template_name,
											extra_context=extra_context)(request)

def EditDBEntrie(request, template_name='db_management/edit_entrie.html', extra_context=None, **kwargs):
	if request.user.is_authenticated():
		user_id = request.user.id
	else:
		user_id = -1
	if not extra_context: extra_context = dict()
	extra_context['user'] = user_id
	extra_context['host'] = request.get_host()
	extra_context['record'] = kwargs['entry_id']
	#if request.method == 'POST' and request.FILES['myfile']:
    #    myfile = request.FILES['myfile']
    #    fs = FileSystemStorage()
    #    filename = fs.save(myfile.name, myfile)
    #    uploaded_file_url = fs.url(filename)
	return ExtraContextTemplateView.as_view(template_name=template_name,
											extra_context=extra_context)(request)

class MyPDF(PDFTemplateView):
	filename = 'descriptions_workspace.pdf'
	template_name = 'db_management/workspace_pdf_template.html'
	cmd_options = {
		'viewport-size': 3000,
		'zoom': 0.6,
		'dpi': 720,
		}
	def get_context_data(self, **kwargs):
		arg1 = self.kwargs['arg1']
		list_of_entries = DescriptionWorkSpace.objects.get(id=arg1).descriptions.all()
		list_of_assigned = [];
		for entr in list_of_entries:
			filtered = Entry.objects.filter(descriptions_instances=entr.id)
			entry_list = [entr.id]
			for filt in filtered: 
				entry_list.append(filt.id)
		list_of_assigned.append(entry_list)
		context = super(MyPDF, self).get_context_data()
		context['descriptions'] = list_of_entries
		context['assigned'] = list_of_assigned
		return context																																																