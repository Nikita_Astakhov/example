from __future__ import unicode_literals

from django.apps import AppConfig


class DbManagementConfig(AppConfig):
    name = 'db_management'
