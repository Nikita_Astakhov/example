from django.conf.urls import url
from . import views
#from django.conf.urls import *
#from django.contrib.auth import views as auth_views
#from django.views.generic import TemplateView
urlpatterns = [
    url(r'^point/$', views.ManagePoint, name='db_point'),
    url(r'^descriptions_workspace', views.ManageDescriptionWorkSpace, name='descriptions_workspace'),
    url(r'^entries_workspace', views.ManageEntriesWorkSpace, name='entries_workspace'),
    url(r'^descriptions_preview/(?P<workspace_id>[0-9]+)/', views.ManageDescriptionsPreview, name='descriptions_preview'),
    url(r'^pdf/(?P<arg1>[0-9]+)/$', views.MyPDF.as_view(), name='pdf'),
    url(r'^edit_db_record/(?P<entry_id>[0-9]+)/', views.EditDBEntrie, name='edit_db_record'),
    url(r'^test/', views.TestView, name='test_view'),
    ] 