from django.contrib import admin
from db_management.models import *

# Register your models here.

class DocumentAdmin(admin.ModelAdmin):
	search_fields = ['description']

class LinksAdmin(admin.ModelAdmin):
	search_fields = ['url']	

class EntryAdmin(admin.ModelAdmin):
	search_fields = ['name']
	readonly_fields = ('creation_date', 'created_by')
	fieldsets = [
        ('About',               {'fields': ['name', 'creation_date', 'created_by', 'remarks', 'JMACcomments', 'Summary']}),
    ]

admin.site.register(Entry, EntryAdmin)
admin.site.register(Document, DocumentAdmin)
admin.site.register(Links, LinksAdmin)