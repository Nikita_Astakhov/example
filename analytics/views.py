# -*- coding: utf-8 -*-
#from __future__ import unicode_literals
from unidecode import unidecode
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponseBadRequest, HttpResponse
from django.views.generic import TemplateView
from django.utils.text import capfirst

from db_management.models import *
from incidents.models import *
from analytics.models import *

from wkhtmltopdf.views import PDFTemplateView

from openpyxl import load_workbook, Workbook
import xlrd
from openpyxl.compat import range
from openpyxl.utils import get_column_letter
from openpyxl.writer.excel import save_virtual_workbook, save_workbook
from openpyxl.writer.write_only import WriteOnlyCell
from openpyxl.styles import Font

from datetime import date, time, datetime, timedelta
from dateutil import rrule
from dateutil.relativedelta import relativedelta
import delorean
from django.db.models import Prefetch
from django.db.models import Q
#import pdfkit

# Create your views here.

class ExtraContextTemplateView(TemplateView):
	""" Add extra context to a simple template view """
	extra_context = None

	def get_context_data(self, *args, **kwargs):
		context = super(ExtraContextTemplateView, self).get_context_data(*args, **kwargs)
		if self.extra_context:
		    context.update(self.extra_context)
		return context

	# this view is used in POST requests, e.g. signup when the form is not valid
	post = TemplateView.get	

def ChartView(request, template_name='analytics/chart_template.html', extra_context=None, **kwargs):
	if request.user.is_authenticated():
		user_id = request.user.id
	else:
		user_id = -1
	if request.method == 'POST':	
		if 'export_excel' in request.POST:
			#wb =  Workbook()
			dest_filename = 'empty_book.xlsx'
			#ws1 = wb.active
			#ws1.title = "range"
			#for row in range(1, 40):
			#	ws1.append(range(40))
			#ws2 = wb.create_sheet(title="Pizda")
			#ws2['F5'] = 3.14
			#ws3 = wb.create_sheet(title="Data")
			#for row in range(10, 20):
			#	for col in range(27, 54):
			#		_ = ws3.cell(column=col, row=row, value="{0}".format(get_column_letter(col)))
			wb = prepareChartExcel(request)		
			response = HttpResponse(save_virtual_workbook(wb), content_type='application/vnd.ms-excel')
			response['Content-Disposition'] = 'attachment; filename=%s' % (dest_filename)
			#response = HttpResponse(request.POST.getlist('segment'))
			return response
		else:
			dest_filename = 'segmented_entries.xlsx'
			wb = prepareAllDataExcel(request)
			response = HttpResponse(save_virtual_workbook(wb), content_type='application/vnd.ms-excel')
			response['Content-Disposition'] = 'attachment; filename=%s' % (dest_filename)
			return response	
	if not extra_context: extra_context = dict()
	extra_context['user'] = user_id
	extra_context['host'] = request.get_host()
	return ExtraContextTemplateView.as_view(template_name=template_name,
											extra_context=extra_context)(request)									

def SegmentView(request, template_name='analytics/segment_entries.html', extra_context=None, **kwargs):
	if request.user.is_authenticated():
		user_id = request.user.id
	else:
		user_id = -1
	if not extra_context: extra_context = dict()
	extra_context['user'] = user_id
	extra_context['host'] = request.get_host()
	return ExtraContextTemplateView.as_view(template_name=template_name,
											extra_context=extra_context)(request)

def prepareChartExcel(request):
	now = datetime.now()
	le = int(request.POST['quantity'])
	start = request.POST['start']
	end = request.POST['end']
	fetched_data = ProcessingSegment.prepareData(le, start, end)
	wb =  Workbook()
	dest_filename = 'empty_book.xlsx'
	ws1 = wb.active
	ws1.title = "Filtered data"
	text = 'Statistic export performed on %s'  % (datetime.strftime(now, '%d %B %Y'))
	cell_head = WriteOnlyCell(ws1, value=text)
	cell_head.font = Font(size=14, bold=True)
	ws1.append([cell_head])
	ws1.merge_cells(start_row=1,start_column=1,end_row=1,end_column=10)
	act_row = 1
	for numb, ins in enumerate(list(range(1, le + 1))):
		v = -(ins)
		ws1.append(['Victims', 'Actors', 'Nature categories', None, 'Locations', None, None, 'Source categories', 'Information grading', 'Keywords'])
		ws1.merge_cells(start_row=1 + act_row, start_column=3, end_row=1 + act_row, end_column=4)
		ws1.merge_cells(start_row=1 + act_row, start_column=5, end_row=1 + act_row, end_column=7)
		try:
			obj = CreatedSegments.objects.get(id=v)
			
			# read and write all victims cats
			vic_col = 'A%d' % (2 + act_row)
			vic_obj = obj.act_victim.all()
			if len(vic_obj) > 0:
				for ind, vic in enumerate(vic_obj):
					vic_col = 'A%d' % (2+ind + act_row)
					ws1[vic_col] = str(vic.name)
			else:
				ws1[vic_col] = '--'
			# read actors classes		
			act_obj = obj.act_actors.all()
			act_col = 'B%d' % (2 + act_row)
			if len(act_obj) > 0:
				for ind, vic in enumerate(act_obj):
					act_col = 'B%d' % (2+ind + act_row)
					ws1[act_col] = str(vic.name)
			else:
				ws1[act_col] = '--'
			# read natures categories		
			nat_obj = obj.act_natures.all()
			n_m_col = 'C%d' % (2 + act_row)
			n_s_col = 'D%d' % (2 + act_row)
			if len(nat_obj) > 0:
				for ind, nat in enumerate(nat_obj):
					n_m_col = 'C%d' % (2+ind + act_row)
					n_s_col = 'D%d' % (2+ind + act_row)
					m = nat.act_nature_mains.all()
					s = nat.act_nature_subs.all()
					ws1[n_m_col] = str(m[0].name)
					if s[0].id != -1:
						ws1[n_s_col] = str(s[0].name)
					else:
						ws1[n_s_col] = '--'
			else:
				ws1[n_m_col] = '--'
			# read location categories		
			loc_obj = obj.act_location.all()
			l_r_col = 'E%d' % (2 + act_row)
			l_l_col = 'F%d' % (2 + act_row)
			l_v_col = 'G%d' % (2 + act_row)
			if len(loc_obj) > 0:
				for ind, loc in enumerate(loc_obj):
					l_r_col = 'E%d' % (2+ind + act_row)
					l_l_col = 'F%d' % (2+ind + act_row)
					l_v_col = 'G%d' % (2+ind + act_row)
					r = loc.act_regions.all()
					l = loc.act_localitys.all()
					v = loc.act_villages.all()
					ws1[l_r_col] = str(r[0].name)
					if l[0].id != -1:
						ws1[l_l_col] = str(l[0].name)
						if v[0].id != -1:
							ws1[l_v_col] = str(v[0].name)
						else:
							ws1[l_v_col] = '--'
					else:
						ws1[l_l_col] = '--'
						ws1[l_v_col] = '--'
			else:
				ws1[l_r_col] = '--'
			# read and write all source cats
			sou_col = 'H%d' % (2 + act_row)
			sou_obj = obj.act_sources.all()
			if len(sou_obj) > 0:
				for ind, sou in enumerate(sou_obj):
					sou_col = 'H%d' % (2+ind + act_row) 
					ws1[sou_col] = str(sou.name)
			else:
				ws1[sou_col] = '--'
			# read and write all grading cats
			inf_col = 'I%d' % (2 + act_row)
			inf_obj = obj.act_gradings.all()
			if len(inf_obj) > 0:
				for ind, inf in enumerate(inf_obj):
					inf_col = 'I%d' % (2+ind + act_row)
					ws1[inf_col] = str(inf.name)
			else:
				ws1[inf_col] = '--'
			# read and write all keywords
			key_col = 'J%d' % (2 + act_row)
			key_obj = obj.act_keywords.all()
			if len(key_obj) > 0:
				for ind, keyw in enumerate(key_obj):
					key_col = 'J%d' % (2+ind + act_row)
					ws1[key_col] = keyw.name
			else:
				ws1[key_col] = '--'
			ws1.append([None])
			if numb == 0:	
				fetched_data[1].insert(0, None)
				fetched_data[1].append('Total')
			ws1.append(fetched_data[1])
			t1 = []
			t2 = []
			t3 = []
			t4 = []
			for value in fetched_data[0][numb][0]:
				t1.append(value[0])
				t2.append(value[1])
			t1.append(sum(t1))
			t2.append(sum(t2))	
			t1.insert(0, 'Total entries')
			t2.insert(0, 'Total victims')  
			ws1.append(t1)
			ws1.append(t2)
			ws1.append([None])	
			ws1.append([None])
			ws1.merge_cells(start_row=ws1.max_row-1,start_column=1,end_row=ws1.max_row-1,end_column=10)
			ws1.merge_cells(start_row=ws1.max_row,start_column=1,end_row=ws1.max_row,end_column=10)
			act_row = ws1.max_row	
			#for months in :
				
			# [ [ [ '[ [2, 3, 4, 5] , [2, 3, 4, 5] ]', 'name'], [2,3], 'name'] ], [jul, jun] ]

			#ws1[er] = 'hui'
			#for row in range(1, 40):
			#	ws1.append(range(40))
			#ws2 = wb.create_sheet(title="Pizda")
			#ws2['F5'] = 3.14
			#ws3 = wb.create_sheet(title="Data")
			#for row in range(10, 20):
			#	for col in range(27, 54):
			#		_ = ws3.cell(column=col, row=row, value="{0}".format(get_column_letter(col)))
		except CreatedSegments.DoesNotExist:
			pass
	return wb

def FormatString(s):
	if isinstance(s, unicode):
		try:
			s.encode('ascii')
			return s
		except:
			return unidecode(s)
	else:
		return s

def prepareAllDataExcel(request):
	now = datetime.now()
	number = int(request.POST['number'])
	start = request.POST['start']
	end = request.POST['end']
	if start == '' or end == '':
		withoutdates = True
	else:
		withoutdates = False
	fetched_data = ProcessingSegment.fetchEntries(start, end, number, withoutdates)
	wb =  Workbook()
	wb.encoding = "utf-8"
	dest_filename = 'empty_book.xlsx'
	ws1 = wb.active
	ws1.title = "Entries"
	text = 'Entries export performed on %s'  % (datetime.strftime(now, '%d %B %Y'))
	cell_head = WriteOnlyCell(ws1, value=text)
	cell_head.font = Font(size=14, bold=True)
	ws1.append([cell_head])
	ws1.merge_cells(start_row=1,start_column=1,end_row=1,end_column=10)
	ws1.append(['Main info', None, None, None, None, 'Desciptions', None, None, None, None, 'Values', None, None, None, None, None, None, None, None, None, None, None, None])
	ws1.merge_cells(start_row=2, start_column=1, end_row=2, end_column=5)
	ws1.merge_cells(start_row=2, start_column=6, end_row=2, end_column=10)
	ws1.merge_cells(start_row=2, start_column=11, end_row=2, end_column=22)
	ws1.append(['Number', 'Name', 'Remarks', 'JMAC comments', 'Summary', 
				'Number', 'Report date', 'Source', 'Inf Grading', 'Description', 
				'Number', 'Date', 'Time', 'Actors', 'Victims', None, 'Nature', None, 'Location', None, None, None, None])
	ws1.merge_cells(start_row=3, start_column=15, end_row=3, end_column=16)
	ws1.merge_cells(start_row=3, start_column=17, end_row=3, end_column=18)
	ws1.merge_cells(start_row=3, start_column=19, end_row=3, end_column=23)
	act_row = 1
	for numb, entry in enumerate(fetched_data):
		main_info_array = [numb+1, entry.name, entry.remarks, entry.JMACcomments, entry.Summary]
		ws1.append(main_info_array)
		act_row = ws1.max_row
		# add description instances
		entry_descriptions = entry.descriptions_instances.all()
		if len(entry_descriptions) > 0:
			for dNum, desc in enumerate(entry_descriptions, start=0):
				ws1['F%d' % (dNum + act_row)] = dNum + 1 # number
				ws1['G%d' % (dNum + act_row)] = str(desc.convert_report_date()) # report date
				ws1['H%d' % (dNum + act_row)] = FormatString(desc.source.name) # source
				ws1['I%d' % (dNum + act_row)] = FormatString(desc.inf_grading.name) # inf grading
				#dataframe = desc.description.applymap(lambda x: x.encode('unicode_escape').decode('utf-8') if isinstance(x, str) else x)
				tem = FormatString(desc.description)
				ws1['J%d' % (dNum + act_row)] = tem # description text
		entry_values = entry.value_instance.all()
		if len(entry_values) > 0:
			for vNum, val in enumerate(entry_values, start=0):
				ws1['K%d' % (vNum + act_row)] = vNum + 1 # number
				ws1['L%d' % (vNum + act_row)] = str(val.convert_date()) # date
				ws1['M%d' % (vNum + act_row)] = str(val.convert_time()) # time
				all_actors = val.actors.all()
				if len(all_actors) > 0:
					for aNum, act in enumerate(all_actors, start=0):
						ws1['N%d' % (vNum + aNum + act_row)] = FormatString(act.name) # actors
				all_victims = val.victims.all()
				if len(all_victims) > 0:
					for viNum, vic in enumerate(all_victims, start=0):
						ws1['O%d' % (vNum + viNum + act_row)] = FormatString(vic.group.name) # victim group
						ws1['P%d' % (vNum + viNum + act_row)] = vic.quantity # victim quantity
				all_natures = val.nature.all()
				if len(all_natures) > 0:
					for nNum, nat in enumerate(all_natures, start=0):
						if nat.main:
							ws1['Q%d' % (vNum + nNum + act_row)] = FormatString(nat.main.name) # nature main
						else:
							ws1['Q%d' % (vNum + nNum + act_row)] = str('--')	
						if nat.sub: 
							ws1['R%d' % (vNum + nNum + act_row)] = FormatString(nat.sub.name) # nature sub
						else:
							ws1['R%d' % (vNum + nNum + act_row)] = str('--') # nature sub
				all_locations = val.locations.all()
				if len(all_locations) > 0:
					for lNum, loc in enumerate(all_locations, start=0):
						if loc.inc_region:
							ws1['S%d' % (vNum + lNum + act_row)] = FormatString(loc.inc_region.name) # region
						else:
							ws1['S%d' % (vNum + lNum + act_row)] = str('--') # region
						if loc.inc_locality:	
							ws1['T%d' % (vNum + lNum + act_row)] = FormatString(loc.inc_locality.name) # state
						else:
							ws1['T%d' % (vNum + lNum + act_row)] = str('--') # state
						if loc.inc_village:	
							ws1['U%d' % (vNum + lNum + act_row)] = FormatString(loc.inc_village.name) # town
						else:
							ws1['U%d' % (vNum + lNum + act_row)] = str('--') # town
						# try to get coordinates
						if loc.inc_coordinates:
							ws1['V%d' % (vNum + lNum + act_row)] = FormatString(loc.inc_coordinates.e) # e
							ws1['W%d' % (vNum + lNum + act_row)] = FormatString(loc.inc_coordinates.n) # n
						else:
							ws1['V%d' % (vNum + lNum + act_row)] = str('--') # e
							ws1['W%d' % (vNum + lNum + act_row)] = str('--') # n	
				act_row = ws1.max_row
	return wb					
		

#0-actors, 1-victims, 2-natures-main, 3-natures-sub, 4-locations-region, 5-locations-locality, 6-locations-village, 7-source, 8-infgrade, 9-keywords
class ProcessingSegment(object):

	def fetchEntries(self, start, end, number, withoutdates):
		all_data = self.prepare_array(number)
		if not withoutdates:
			start = start + ' 00:00'
			end = end + ' 00:00'
			start_date = datetime.strptime(str(start), '%Y-%m-%d %H:%M')
			end_date = datetime.strptime(str(end), '%Y-%m-%d %H:%M')
			filters = Q(value_instance__date__range=[start_date, end_date])
		else:
			filters = Q()
		if len(all_data[0]) > 0:
			filters &= Q(value_instance__actors__id__in=all_data[0])
		if len(all_data[1]) > 0:
			filters &= Q(value_instance__victims__group__id__in=all_data[1])
		if len(all_data[2]) > 0:
			filters &= Q(value_instance__nature__main__id__in=all_data[2])
		if len(all_data[3]) > 0:
			filters &= Q(value_instance__nature__sub__id__in=all_data[3])
		if len(all_data[4]) > 0:
			filters &= Q(value_instance__locations__inc_region__id__in=all_data[4])
		if len(all_data[5]) > 0:
			filters &= Q(value_instance__locations__inc_locality__id__in=all_data[5])
		if len(all_data[6]) > 0:
			filters &= Q(value_instance__locations__inc_village__id__in=all_data[6])
		if len(all_data[7]) > 0:
			filters &= Q(descriptions_instances__source__id__in=all_data[7])
		if len(all_data[8]) > 0:
			filters &= Q(descriptions_instances__inf_grading__id__in=all_data[8])
		if len(all_data[9]) > 0:
			filters &= reduce(lambda x, y: x & y, [Q(descriptions_instances__description__icontains=word) for word in all_data[9]])
		result_data = Entry.objects.filter(filters).distinct()	
		return result_data
		
	def prepareData(self, le, start, end):
		result_array = []
		result = [[],[]]
		segments = list(range(1, le + 1))
		for segment in segments:
			pre_data = self.fetchAllSegmentData(start, end, segment)
			result_array.append(pre_data) #[ [[2,3],[jul, jun], 'name'], #[[2,3],[jul, jun], 'name']]
		for ind, entry in enumerate(result_array):
			if ind == 0:
				result[1] = entry[1]
			result[0].append([entry[0], entry[2]]) # [ [[[2,3], 'name'], [2,3], 'name']], [jul, jun] ]		
		return result			
		"""
		data array for each segment data: [10, 20, 30, 40, 50, 60, 70, 80, 90]
		labels array for each month
		"""	
	def fetchAllSegmentData(self, start, end, segment):
		"""
		i need to get all entries that appeared in selected dates range
		and then filter them by segment preferences
		"""
		"""
		firstly i need to split range on monthes and then calculate segments here and add them to final array
		"""
		result_array = [] #[[2,3],[jul, jun], 'name']
		start = start + ' 00:00'
		end = end + ' 00:00'
		i_now = datetime.strptime(str(start), '%Y-%m-%d %H:%M')
		i_end = datetime.strptime(str(end), '%Y-%m-%d %H:%M')
		all_data = self.prepare_array(segment)
		if i_now == i_end:
			result_array = self.iterateThroughHours(i_now, all_data) # i need to get segmented data on necessary date and split it by the hours
		elif i_now + timedelta(days=30) >= i_end and i_end < (i_now + timedelta(days=89)):
			result_array = self.iterateThroughDays(i_now, i_end, all_data)
		else:
			result_array = self.iterateThroughMonthes(i_now, i_end, all_data)
		result_array.append(['no name'])
		return result_array

	def iterateThroughHours(self, start, segment):
		result_array = []
		values_array = []
		monthes_array = []
		end_obj = start + timedelta(days=1)
		for dt in rrule.rrule(rrule.HOURLY, dtstart=start, interval=1, until=end_obj):
			item_end = dt + timedelta(hours=1)
			data = self.iterationInstance(start, dt, item_end, segment, True)
			values_array.append(data)
			monthes_array.append(dt.strftime('%d-%m-%Y %H:%M'))
		result_array.extend([values_array, monthes_array])
		return result_array

	def iterateThroughDays(self, start, end, segment):
		result_array = []
		values_array = []
		monthes_array = []
		for dt in rrule.rrule(rrule.DAILY, dtstart=start, interval=1, until=end):
			item_end = dt + timedelta(days=1)
			data = self.iterationInstance(start, dt, item_end, segment, False)
			values_array.append(data)
			monthes_array.append(dt.strftime('%d-%m-%Y'))
		result_array.extend([values_array, monthes_array])
		return result_array
#[[2,3],[jul, jun]]
	def iterateThroughMonthes(self, start, end, segment):
		result_array = []
		values_array = []
		monthes_array = []
		for dt in rrule.rrule(rrule.MONTHLY, dtstart=start, interval=1, until=end):
			item_end = dt + relativedelta(months=+1)
			data = self.iterationInstance(start, dt, item_end, segment, False)
			values_array.append(data)
			monthes_array.append(dt.strftime('%m-%Y'))
		result_array.extend([values_array, monthes_array])
		return result_array	

#0-actors, 1-victims, 2-natures-main, 3-natures-sub, 4-locations-region, 5-locations-locality, 6-locations-village, 7-source, 8-infgrade, 9-keywords
	def iterationInstance(self, start_date, item_start, item_end, all_data, timeState):
		if timeState:
			filters = Q(value_instance__date=start_date)
			filters &= Q(value_instance__time__range=[item_start, item_end])
		else:
			filters = Q(value_instance__date__range=[item_start, item_end])
		if len(all_data[0]) > 0:
			filters &= Q(value_instance__actors__id__in=all_data[0])
		if len(all_data[1]) > 0:
			filters &= Q(value_instance__victims__group__id__in=all_data[1])
		if len(all_data[2]) > 0:
			filters &= Q(value_instance__nature__main__id__in=all_data[2])
		if len(all_data[3]) > 0:
			filters &= Q(value_instance__nature__sub__id__in=all_data[3])
		if len(all_data[4]) > 0:
			filters &= Q(value_instance__locations__inc_region__id__in=all_data[4])
		if len(all_data[5]) > 0:
			filters &= Q(value_instance__locations__inc_locality__id__in=all_data[5])
		if len(all_data[6]) > 0:
			filters &= Q(value_instance__locations__inc_village__id__in=all_data[6])
		if len(all_data[7]) > 0:
			filters &= Q(descriptions_instances__source__id__in=all_data[7])
		if len(all_data[8]) > 0:
			filters &= Q(descriptions_instances__inf_grading__id__in=all_data[8])
		if len(all_data[9]) > 0:
			filters &= reduce(lambda x, y: x & y, [Q(descriptions_instances__description__icontains=word) for word in all_data[9]])
		result_data = Entry.objects.filter(filters).distinct()
		vics = 0
		for entry in result_data:
			values = entry.value_instance.all()
			for val in values:
				vici = val.victims.all()
				for vi in vici:
					if len(all_data[1]) > 0:
						if int(vi.id) in all_data[1]:
							vics += int(vi.quantity)
					else:
						vics += int(vi.quantity)	
		result = [len(result_data), vics]		
		return result

	def prepare_array(self, where):
		result = [[], [], [], [], [], [], [], [], [], []]
		v = -(where)
		try:
			obj = CreatedSegments.objects.get(id=v)
			
			# read and write all victims cats
			vic_obj = obj.act_victim.all()
			if len(vic_obj) > 0:
				for victims in vic_obj:
					result[1].append(victims.id)
			# read actors classes
			act_obj = obj.act_actors.all()
			if len(act_obj) > 0:
				for actors in act_obj:
					result[0].append(actors.id)
			# read natures categories
			nat_obj = obj.act_natures.all()
			if len(nat_obj) > 0:
				for nature in nat_obj:
					m = nature.act_nature_mains.all()
					s = nature.act_nature_subs.all()
					result[2].append(m[0].id)
					if s[0].id != -1:
						result[3].append(s[0].id)		
			# read location categories
			loc_obj = obj.act_location.all()
			if len(loc_obj) > 0:
				for location in loc_obj:
					r = location.act_regions.all()
					l = location.act_localitys.all()
					v = location.act_villages.all()
					result[4].append(r[0].id)
					if l[0].id != -1:
						result[5].append(l[0].id)
					if v[0].id != -1:
						result[6].append(v[0].id)
			# read and write all source cats
			sou_obj = obj.act_sources.all()
			if len(sou_obj) > 0:
				for source in sou_obj:
					result[7].append(source.id)
			# read and write all grading cats
			inf_obj = obj.act_gradings.all()
			if len(inf_obj) > 0:
				for inf in inf_obj:
					result[8].append(inf.id)
			# read and write all keywords
			key_obj = obj.act_keywords.all()
			if len(key_obj) > 0:
				for key in key_obj:
					result[9].append(key.name)							
		except CreatedSegments.DoesNotExist:
			pass	
		return result											 						

ProcessingSegment = ProcessingSegment()