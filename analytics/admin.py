from django.contrib import admin
from analytics.models import *
# Register your models here.

class SavedNaturesAdmin(admin.ModelAdmin):
    filter_horizontal = ('act_nature_mains', 'act_nature_subs')

class SavedLocalitysAdmin(admin.ModelAdmin):
    filter_horizontal = ('act_regions', 'act_localitys', 'act_villages', )    

class CreatedSegmentsAdmin(admin.ModelAdmin):
	search_fields = ['name']
	filter_horizontal = ('act_natures', 'act_location', 'act_victim', 'act_actors', 'act_sources', 'act_gradings')
	fieldsets = [
        ('About',                {'fields': ['name',]}),
        ('Incident Category',    {'fields': ['act_natures',]}),
        ('Victim',               {'fields': ['act_victim']}),
        ('Location',             {'fields': ['act_location',]}),
        ('Actor',                {'fields': ['act_actors']}),
        ('Other',                {'fields': ['act_sources', 'act_gradings']}),
    ]

admin.site.register(CreatedSegments, CreatedSegmentsAdmin)
admin.site.register(SavedNatures, SavedNaturesAdmin)
admin.site.register(SavedLocalitys, SavedLocalitysAdmin)