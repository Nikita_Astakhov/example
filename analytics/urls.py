from django.conf.urls import url
from . import views
#from django.conf.urls import *
#from django.contrib.auth import views as auth_views
#from django.views.generic import TemplateView
urlpatterns = [
    url(r'^chart/', views.ChartView, name='chart'),
    url(r'^segments', views.SegmentView, name='segment'),
    ] 