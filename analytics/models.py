from __future__ import unicode_literals

from django.db import models

from incidents.models import *
from db_management.models import *
# Create your models here.

class SavedKeywords(models.Model):
	name = models.CharField(max_length=255, blank=True, verbose_name='Keyword') 

	def __unicode__(self):
		return unicode(self.name)

class SavedLocalitys(models.Model):
	act_villages = models.ManyToManyField('incidents.Village', blank=True, related_name="villages_in_segment", verbose_name='Village or Town')
	act_localitys = models.ManyToManyField('incidents.Locality', blank=True, related_name="localitys_in_segment", verbose_name='Locality')
	act_regions = models.ManyToManyField('incidents.Region', blank=True, related_name="regions_in_segment", verbose_name='State')

	class Meta:
		verbose_name = 'Criteria location objects (do not use it)'
		verbose_name_plural = 'Criteria location objects (do not use it)'	

class SavedNatures(models.Model):
	act_nature_mains = models.ManyToManyField('incidents.NatureMainGroup', blank=True, related_name="nature_mains_in_segment", verbose_name='Incident Main Category')
	act_nature_subs = models.ManyToManyField('incidents.NatureSubGroups', blank=True, related_name="nature_subs_in_segment", verbose_name='Incident Sub Category')

	class Meta:
		verbose_name = 'Criteria nature objects (do not use it)'
		verbose_name_plural = 'Criteria nature objects (do not use it)'		

class CreatedSegments(models.Model):
	name = models.CharField(max_length=255, blank=True, verbose_name='Title')
	act_actors = models.ManyToManyField('incidents.ActorsGroup', blank=True, related_name="actors_in_segment", verbose_name='Actor')
	act_gradings = models.ManyToManyField('incidents.InformationGrading', blank=True, related_name="gradings_in_segment", verbose_name='Information Grading')
	act_sources = models.ManyToManyField('incidents.Source', blank=True, related_name="sources_in_segment", verbose_name='Source')
	act_victim = models.ManyToManyField('incidents.VictimClass', blank=True, related_name="victim_class_in_segment", verbose_name='Victim Class')
	act_keywords = models.ManyToManyField(SavedKeywords, blank=True, related_name="keywords_in_segment", verbose_name='Keywords')
	act_location = models.ManyToManyField(SavedLocalitys, blank=True, related_name="locations_in_segment", verbose_name='Locations')
	act_natures = models.ManyToManyField(SavedNatures, blank=True, related_name="natures_in_segment", verbose_name='Natures')

	class Meta:
		verbose_name = 'Search Criteria'

	def __unicode__(self):
		return unicode(self.name)