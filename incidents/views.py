from __future__ import unicode_literals
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView
from . import models

# Create your views here.

class ManageIncidentsView(TemplateView):
	
	template_name = 'incidents/manage_incidents.html'
	
	def get_context_data(self):
		if self.request.user.is_authenticated():
			user_id = self.request.user.id
		else:
			user_id = -1
		context = super(ManageIncidentsView, self).get_context_data()
		context['user'] = user_id
		context['host'] = self.request.get_host()
		return context