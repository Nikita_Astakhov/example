from __future__ import unicode_literals
import os
from django.db import models
from datetime import date, time, datetime, timedelta
from django.utils import timezone
from django.contrib.auth.models import User

# Create your models here.

class VictimClass(models.Model): 
	name = models.CharField(max_length=255, blank=True)

	class Meta:
		verbose_name = 'Victim class'
		verbose_name_plural = 'Victim classes'
	
	def __unicode__(self):
		return unicode(self.name)

class Coordinats(models.Model):
	e = models.FloatField(default=1.0)
	n = models.FloatField(default=1.0)

	class Meta:
		verbose_name = 'Coordinates'
		verbose_name_plural = 'Coordinates'

class Village(models.Model):
	name = models.CharField(max_length=255)
	coordinates = models.ForeignKey(Coordinats, related_name='village_coordinates', blank=True, null=True)
	description = models.CharField(max_length=255, default='')

	class Meta:
		verbose_name = 'Village and Town'
		verbose_name_plural = 'Villages and Towns'

	def __unicode__(self):
		return unicode(self.name)

class Locality(models.Model):
	name = models.CharField(max_length=255)
	villages = models.ManyToManyField(Village, blank=True, related_name="locality_villages", default=1) 

	class Meta:
		verbose_name = 'Locality'
		verbose_name_plural = 'Localitys'

	def __unicode__(self):
		return unicode(self.name)

class Region(models.Model):
	name = models.CharField(max_length=255)
	localitys = models.ManyToManyField(Locality, blank=True, related_name="region_localitys", default=1) 

	def __unicode__(self):
		return unicode(self.name) 

class Location(models.Model):
	inc_region = models.ForeignKey(Region, related_name="location_region", blank=True, null=True, verbose_name='State')
	inc_locality = models.ForeignKey(Locality, related_name="location_locality", blank=True, null=True, verbose_name='Locality')
	inc_village = models.ForeignKey(Village, related_name="location_village", blank=True, null=True, verbose_name='Village or Town')
	inc_coordinates = models.ForeignKey(Coordinats, related_name="location_coordinates", blank=True, null=True, verbose_name='Coordinates')	

	def computeNames(self):
		reg = self.inc_region.name
		if self.inc_locality is not None:
			loc = self.inc_locality.name
		else:
			loc = "--"
		if self.inc_village is not None:
			vil = self.inc_village.name
			e = self.inc_coordinates.e
			n = self.inc_coordinates.n
		else:
			vil = "--"
			e = '-'
			n = '-'	
		result = str("%s - %s - %s - E: %s, N: %s " % (reg, loc, vil, e, n))
		return result	

class ActorsGroup(models.Model): 
	name = models.CharField(max_length=255)

	def __unicode__(self):
		return unicode(self.name)

class NatureSubGroups(models.Model): 
	name = models.CharField(max_length=255)

	class Meta:
		verbose_name = 'Incident Sub Categorie'

	def __unicode__(self):
		return unicode(self.name)

class NatureMainGroup(models.Model): 
	name = models.CharField(max_length=255)
	subGroups = models.ManyToManyField(NatureSubGroups, blank=True, related_name="nature_sub_groups", verbose_name='Sub Group') 

	class Meta:
		verbose_name = 'Incident Main Categorie'

	def __unicode__(self):
		return unicode(self.name)

class Nature(models.Model):
	main = models.ForeignKey(NatureMainGroup, related_name="nature_main_g", blank=True, null=True, verbose_name='Main')	
	sub = models.ForeignKey(NatureSubGroups, related_name="nature_sub_g", blank=True, null=True, verbose_name='Sub')

	def computeNames(self):
		m = self.main.name
		if self.sub is not None:
			su = self.sub.name
		else:
			su = "--"
		result = str("%s - %s" % (m, su))
		return result	

class InformationGrading(models.Model): 
	name = models.CharField(max_length=255)

	def __unicode__(self):
		return unicode(self.name)

class TouchTypes(models.Model):
	name = models.CharField(max_length=255, blank=True)
	
	def __unicode__(self):
		return unicode(self.name)

class TouchedDescription(models.Model):
	date = models.DateTimeField(auto_now_add=True)
	touched_by = models.ForeignKey(User, related_name='touched_by', blank=True, null=True)
	action_type = models.ForeignKey(TouchTypes, related_name='touch_type', blank=True, null=True)

	def get_type(self):
		result = self.action_type.name
		return result

	def get_name(self):
		result = self.touched_by.username
		return result

	def convert_date(self):
		date = datetime.strftime(self.date, '%d %B %Y')
		return date

class Source(models.Model): 
	name = models.CharField(max_length=255, blank=True)
	creation_date = models.DateTimeField(auto_now_add=True)
	created_by = models.ForeignKey(User, related_name='source_created_by', blank=True, null=True) 
	
	def __unicode__(self):
		return unicode(self.name)

class IncindentDescription(models.Model): 
	now = datetime.now()

	name = models.CharField(max_length=255, blank=True, verbose_name='Title') # +
	creation_date = models.DateTimeField(auto_now_add=True, verbose_name='Recorded on') # +
	appearence_date = models.DateField(auto_now=False, auto_now_add=False, blank=True, null=True) # + 
	description = models.TextField(blank=True, help_text="Enter description of incindent") # +
	source = models.ForeignKey(Source, blank=True, null=True, related_name="incindent_source") # +
	created_by = models.ForeignKey(User, related_name='description_created_by', blank=True, null=True) # +
	touched = models.ManyToManyField(TouchedDescription, blank=True, related_name="touch_actions") # +
	inf_grading = models.ForeignKey(InformationGrading, related_name='information_grading', default=1, verbose_name='Information grading') # +

	class Meta:
		verbose_name = 'Incident Description'

	def convert_date(self):      
		creation_date = datetime.strftime(self.creation_date, '%d %B %Y')
		return creation_date

	def convert_report_date(self):
		date = datetime.strftime(self.appearence_date, '%d %B %Y')
		return date	

	def get_source(self):
		result = self.source.id
		return result

	def get_source_name(self):
		result = self.source.name
		return result

	def __unicode__(self):
		return unicode(self.name)

class DescriptionWorkSpace(models.Model):
	descriptions = models.ManyToManyField(IncindentDescription, blank=True, related_name="descriptions_workspace")	
	creation_date = models.DateTimeField(auto_now_add=True)	
	created_by = models.ForeignKey(User, related_name='workspace_created_by', blank=True, null=True)

	def convert_date(self):
		creation_date = datetime.strftime(self.creation_date, '%d %B %Y')
		return creation_date

	def get_name(self):
		result = self.created_by.username
		return result	 