# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2017-03-26 14:23
from __future__ import unicode_literals

from django.db import models, migrations

def make_many_authors(apps, schema_editor):
    """
        Adds the Author object in Book.author to the
        many-to-many relationship in Book.authors
    """
    INC = apps.get_model('incidents', 'IncindentDescription')

    for inci in INC.objects.all():
    	if inci.actor_class is not None:
            inci.actors.add(inci.actor_class)

class Migration(migrations.Migration):

    dependencies = [
        ('incidents', '0009_incindentdescription_actors'),
    ]

    operations = [
        migrations.RunPython(make_many_authors),
    ]
