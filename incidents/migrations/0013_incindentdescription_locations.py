# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2017-03-27 14:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('incidents', '0012_location'),
    ]

    operations = [
        migrations.AddField(
            model_name='incindentdescription',
            name='locations',
            field=models.ManyToManyField(blank=True, related_name='incindent_locations', to='incidents.Location'),
        ),
    ]
