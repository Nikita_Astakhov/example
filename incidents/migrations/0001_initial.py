# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2017-01-31 13:21
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='ActorsGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Coordinats',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('e', models.FloatField(default=1.0)),
                ('n', models.FloatField(default=1.0)),
            ],
        ),
        migrations.CreateModel(
            name='DescriptionWorkSpace',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('created_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='workspace_created_by', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='IncindentDescription',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=255)),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('appearence_date', models.DateField(blank=True, null=True)),
                ('description', models.TextField(blank=True, help_text='Enter description of incindent')),
                ('inc_date', models.DateField(blank=True, null=True)),
                ('inc_time', models.TimeField(blank=True, null=True)),
                ('number_of_victims', models.IntegerField(blank=True, default=0)),
                ('actor_class', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='incindent_actor_class', to='incidents.ActorsGroup')),
                ('created_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='description_created_by', to=settings.AUTH_USER_MODEL)),
                ('inc_coordinates', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='incindent_coordinates', to='incidents.Coordinats')),
            ],
        ),
        migrations.CreateModel(
            name='InformationGrading',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Locality',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='NatureMainGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='NatureSubGroups',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('localitys', models.ManyToManyField(blank=True, default=1, related_name='region_localitys', to='incidents.Locality')),
            ],
        ),
        migrations.CreateModel(
            name='Source',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=255)),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('created_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='source_created_by', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='TouchedDescription',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='TouchTypes',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Village',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('coordinates', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='village_coordinates', to='incidents.Coordinats')),
            ],
        ),
        migrations.AddField(
            model_name='toucheddescription',
            name='action_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='touch_type', to='incidents.TouchTypes'),
        ),
        migrations.AddField(
            model_name='toucheddescription',
            name='touched_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='touched_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='naturemaingroup',
            name='subGroups',
            field=models.ManyToManyField(blank=True, related_name='nature_sub_groups', to='incidents.NatureSubGroups'),
        ),
        migrations.AddField(
            model_name='locality',
            name='villages',
            field=models.ManyToManyField(blank=True, default=1, related_name='locality_villages', to='incidents.Village'),
        ),
        migrations.AddField(
            model_name='incindentdescription',
            name='inc_locality',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='incindent_locality', to='incidents.Locality'),
        ),
        migrations.AddField(
            model_name='incindentdescription',
            name='inc_region',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='incindent_region', to='incidents.Region'),
        ),
        migrations.AddField(
            model_name='incindentdescription',
            name='inc_village',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='incindent_village', to='incidents.Village'),
        ),
        migrations.AddField(
            model_name='incindentdescription',
            name='inf_grading',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='information_grading', to='incidents.InformationGrading'),
        ),
        migrations.AddField(
            model_name='incindentdescription',
            name='nature_main',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='incindent_main_nature_group', to='incidents.NatureMainGroup'),
        ),
        migrations.AddField(
            model_name='incindentdescription',
            name='nature_sub',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='incindent_sub_nature_group', to='incidents.NatureSubGroups'),
        ),
        migrations.AddField(
            model_name='incindentdescription',
            name='source',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='incindent_source', to='incidents.Source'),
        ),
        migrations.AddField(
            model_name='incindentdescription',
            name='touched',
            field=models.ManyToManyField(blank=True, related_name='touch_actions', to='incidents.TouchedDescription'),
        ),
        migrations.AddField(
            model_name='descriptionworkspace',
            name='descriptions',
            field=models.ManyToManyField(blank=True, related_name='descriptions_workspace', to='incidents.IncindentDescription'),
        ),
    ]
