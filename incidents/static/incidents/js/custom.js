var $input = $( '.sratr_date_input' ).pickadate({
  container: '#date_container',
    selectYears: true,
    selectMonths: true,
    firstDay: 0,
    //monthsFull: ['January', 'Febru', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
    //weekdaysShort: ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
    //today: 'Cегодня',
    //clear: 'Удалить',
    //close: 'Закрыть',
    formatSubmit: 'yyyy-mm-dd'
 });
var picker = $input.pickadate('picker');