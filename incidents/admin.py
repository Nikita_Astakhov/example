from django.contrib import admin
from incidents.models import *
# Register your models here.

class ActorsGroupAdmin(admin.ModelAdmin):
	search_fields = ['name']
admin.site.register(ActorsGroup, ActorsGroupAdmin)

class VictimClassAdmin(admin.ModelAdmin):
	search_fields = ['name']
admin.site.register(VictimClass, VictimClassAdmin)

class InformationGradingAdmin(admin.ModelAdmin):
	search_fields = ['name']
admin.site.register(InformationGrading, InformationGradingAdmin)

class NatureMainGroupAdmin(admin.ModelAdmin):
	search_fields = ['name']
	filter_horizontal = ('subGroups',)
admin.site.register(NatureMainGroup, NatureMainGroupAdmin)

class NatureSubGroupsAdmin(admin.ModelAdmin):
	search_fields = ['name']
admin.site.register(NatureSubGroups, NatureSubGroupsAdmin)

class SourceAdmin(admin.ModelAdmin):
	search_fields = ['name']
	readonly_fields = ('creation_date', 'created_by')
admin.site.register(Source, SourceAdmin)

class IncindentDescriptionAdmin(admin.ModelAdmin):
	search_fields = ['name']
	list_display = ('name', 'creation_date',)
	readonly_fields = ('creation_date', 'appearence_date', )
	fieldsets = [
        ('About',               {'fields': ['name', 'source', 'inf_grading', 'description']}),
        ('Dates and time',            {'fields': ['creation_date', 'appearence_date']}),
    ]
admin.site.register(IncindentDescription, IncindentDescriptionAdmin)

class RegionAdmin(admin.ModelAdmin):
	search_fields = ['name']
	filter_horizontal = ('localitys',)
admin.site.register(Region, RegionAdmin)

class LocalityAdmin(admin.ModelAdmin):
	search_fields = ['name']
	filter_horizontal = ('villages',)
admin.site.register(Locality, LocalityAdmin)

class VillageAdmin(admin.ModelAdmin):
	search_fields = ['name']
admin.site.register(Village, VillageAdmin)

class CoordinatsAdmin(admin.ModelAdmin):
	search_fields = ['e', 'n']
	list_display = ('e', 'n')
admin.site.register(Coordinats, CoordinatsAdmin)
