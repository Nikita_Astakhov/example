#from __future__ import unicode_literals
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView

# Create your views here.

class ExtraContextTemplateView(TemplateView):
	""" Add extra context to a simple template view """
	extra_context = None

	def get_context_data(self, *args, **kwargs):
		context = super(ExtraContextTemplateView, self).get_context_data(*args, **kwargs)
		if self.extra_context:
		    context.update(self.extra_context)
		return context

	# this view is used in POST requests, e.g. signup when the form is not valid
	post = TemplateView.get	

def DocsView(request, template_name='docs/docs_point.html', extra_context=None, **kwargs):
	if request.user.is_authenticated():
		user_id = request.user.id
	else:
		user_id = -1
	if not extra_context: extra_context = dict()
	extra_context['user'] = user_id
	extra_context['host'] = request.get_host()
	return ExtraContextTemplateView.as_view(template_name=template_name,
											extra_context=extra_context)(request)